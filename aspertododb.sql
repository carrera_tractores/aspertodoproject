-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 05-06-2017 a les 12:32:07
-- Versió del servidor: 5.7.18-0ubuntu0.16.04.1
-- Versió de PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `aspertododb`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `companyEvents`
--

CREATE TABLE `companyEvents` (
  `id` int(11) NOT NULL,
  `eventName` varchar(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `eventDate` datetime NOT NULL,
  `startEventDate` datetime NOT NULL,
  `endEventDate` datetime NOT NULL,
  `AllDayEvent` tinyint(1) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `companyEvents`
--

INSERT INTO `companyEvents` (`id`, `eventName`, `Title`, `eventDate`, `startEventDate`, `endEventDate`, `AllDayEvent`, `image`) VALUES
(1, 'Anglès', 'Fitxa dels colors', '2017-05-17 08:30:00', '2017-05-30 12:33:58', '2017-05-17 12:30:00', 0, 'ingles.jpg'),
(2, 'Matemàtiques', 'Sumes i restes', '2017-05-17 08:30:00', '2017-05-30 12:33:29', '2017-05-17 12:00:00', 0, 'mates.jpg'),
(10, 'Català', 'Fitxa dels sinonims i antonims', '2017-05-31 09:55:38', '2017-05-15 08:13:00', '2017-05-15 11:00:00', 0, 'classe_de_llenguatge.png'),
(11, 'Educació Fisica', 'Futbol', '2017-05-31 10:12:55', '2017-05-10 15:00:00', '2017-05-10 17:00:00', 0, 'educacioFisica.jpg'),
(16, 'Matemàtiques', 'Sumes', '2017-05-31 10:14:35', '2017-05-03 15:00:00', '2017-05-03 16:00:00', 0, 'matemàtiques.png');

-- --------------------------------------------------------

--
-- Estructura de la taula `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Bolcant dades de la taula `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'anna', 'anna', 'anna@gmail.com', 'anna@gmail.com', 1, NULL, '$2y$13$VVjT6UUrn/XApYbTIuvqeOokQpIgVRkpH/MsucgxFwcMAeYFhjb4q', '2017-06-04 21:03:59', NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}'),
(2, 'david', 'david', 'david@gmail.com', 'david@gmail.com', 1, NULL, '$2y$13$WO2rGjRdm09FSi8R4oyRcOzqfqJdtE5ER1gWUN2OCDVn1nf8sGyC6', '2017-05-18 08:20:00', NULL, NULL, 'a:0:{}'),
(3, 'arnau', 'arnau', 'arnau@gmail.com', 'arnau@gmail.com', 1, NULL, '$2y$13$xZ8JDhJWf5/QCeF2lPV/2OBpql9ymMW62KgVPAkN7jskU/YIm9Mw6', '2017-05-30 09:15:59', NULL, NULL, 'a:0:{}'),
(4, 'joan', 'joan', 'joan@gmail.com', 'joan@gmail.com', 1, NULL, '$2y$13$S.Vy7h1KCf9tpYlB91/Y3upM/XYbBcPBi9Hf0gD9xFfyZcqb3exsO', NULL, NULL, NULL, 'a:0:{}');

-- --------------------------------------------------------

--
-- Estructura de la taula `tasca_extra`
--

CREATE TABLE `tasca_extra` (
  `id_tasca_extra` int(11) NOT NULL,
  `nom_tasca_extra` varchar(15) NOT NULL,
  `descripcio` varchar(200) NOT NULL,
  `image` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `tasca_extra`
--

INSERT INTO `tasca_extra` (`id_tasca_extra`, `nom_tasca_extra`, `descripcio`, `image`, `updated_at`) VALUES
(10, 'Jeroglífics', 'Els jeroglífics van ser un sistema d\'escriptura inventat i utilitzat pels antics egipcis per a comunicar-se per escrit.', 'pica.jpg', '2017-05-22 12:02:31'),
(11, 'Sudokus', 'omple els buits amb els números de l\'1 al 10.', 'Sudoku_Puzzle_by_L2G-20050714_standardized_layout.svg.png', '2017-05-30 12:02:49');

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `companyEvents`
--
ALTER TABLE `companyEvents`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Index de la taula `tasca_extra`
--
ALTER TABLE `tasca_extra`
  ADD PRIMARY KEY (`id_tasca_extra`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `companyEvents`
--
ALTER TABLE `companyEvents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT per la taula `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT per la taula `tasca_extra`
--
ALTER TABLE `tasca_extra`
  MODIFY `id_tasca_extra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

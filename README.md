Aspertodo
=========

AsperToDo és una aplicació per ajudar a mestres amb nens que tenen el síndrome d’Asperger. Els nens amb aquest síndrome, normalment tenen un aspecte i intel·ligència normal i habilitats especials en àrees específiques però tenen problemes per relacionar-se i comportaments inadequats. Aquests nens necessiten tenir rutines diàries per tal de facilitar el seu aprenentage i certa tranquil·litat. per això, volem crear una aplicació web que servirà per ajudar el nen a organitzar el dia a dia a l’escola mostrant les tasques que realitzarà diàriament.

Aqui podreu trobar la App AsperTodo:
https://bitbucket.org/a14davsanpap/aspertodo_ionic/

Integrants del grup: David Sancho i Anna Serra

Estat final del projecte 
Part del mestre: Els mestres es poden logejar, crear usuaris, gestionar les tasques que ha de realitzar l'alumne durant el dia amb les seves imatges i les dades corresponents. 
Part de l'alumne: Els alumnes poden visualitzar les tasques que han de realitzar.
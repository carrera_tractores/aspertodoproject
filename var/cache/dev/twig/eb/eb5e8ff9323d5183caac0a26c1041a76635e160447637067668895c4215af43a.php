<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_595c4ef8825245857fa7be77f387408ac9b72473d3405fc2389241b909c45ffb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f29cbd4eb63d1f77f4b8f75fee7c027b79695d0aef0e859085d965b9ad0487d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f29cbd4eb63d1f77f4b8f75fee7c027b79695d0aef0e859085d965b9ad0487d8->enter($__internal_f29cbd4eb63d1f77f4b8f75fee7c027b79695d0aef0e859085d965b9ad0487d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_bddeca3fe045ea7d027b15aa2ceda33e2e56fdda0ffd3b2ba11535882dd8e3f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bddeca3fe045ea7d027b15aa2ceda33e2e56fdda0ffd3b2ba11535882dd8e3f3->enter($__internal_bddeca3fe045ea7d027b15aa2ceda33e2e56fdda0ffd3b2ba11535882dd8e3f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_f29cbd4eb63d1f77f4b8f75fee7c027b79695d0aef0e859085d965b9ad0487d8->leave($__internal_f29cbd4eb63d1f77f4b8f75fee7c027b79695d0aef0e859085d965b9ad0487d8_prof);

        
        $__internal_bddeca3fe045ea7d027b15aa2ceda33e2e56fdda0ffd3b2ba11535882dd8e3f3->leave($__internal_bddeca3fe045ea7d027b15aa2ceda33e2e56fdda0ffd3b2ba11535882dd8e3f3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}

<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_f66d67fbb208e76242889af6139872fdbefea353ec4c62f709bd4ca68fc8becc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e89ed0b1454e69159223ae814b18db33e20a3b7d8a034afff9c5ec8d579fb7ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e89ed0b1454e69159223ae814b18db33e20a3b7d8a034afff9c5ec8d579fb7ae->enter($__internal_e89ed0b1454e69159223ae814b18db33e20a3b7d8a034afff9c5ec8d579fb7ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $__internal_d9610d30c88a56fa8f237893965a6c4b24c7d8fe409f00ef42ba3944d77fe684 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9610d30c88a56fa8f237893965a6c4b24c7d8fe409f00ef42ba3944d77fe684->enter($__internal_d9610d30c88a56fa8f237893965a6c4b24c7d8fe409f00ef42ba3944d77fe684_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e89ed0b1454e69159223ae814b18db33e20a3b7d8a034afff9c5ec8d579fb7ae->leave($__internal_e89ed0b1454e69159223ae814b18db33e20a3b7d8a034afff9c5ec8d579fb7ae_prof);

        
        $__internal_d9610d30c88a56fa8f237893965a6c4b24c7d8fe409f00ef42ba3944d77fe684->leave($__internal_d9610d30c88a56fa8f237893965a6c4b24c7d8fe409f00ef42ba3944d77fe684_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f3f7d33f96c4472bb20c64df876069eb977d2b641b538ade87b534eb74100a45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3f7d33f96c4472bb20c64df876069eb977d2b641b538ade87b534eb74100a45->enter($__internal_f3f7d33f96c4472bb20c64df876069eb977d2b641b538ade87b534eb74100a45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_cede85be75925337b0fa1cf7d0f218c07c69a3e6b22d129371628c0613536182 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cede85be75925337b0fa1cf7d0f218c07c69a3e6b22d129371628c0613536182->enter($__internal_cede85be75925337b0fa1cf7d0f218c07c69a3e6b22d129371628c0613536182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_cede85be75925337b0fa1cf7d0f218c07c69a3e6b22d129371628c0613536182->leave($__internal_cede85be75925337b0fa1cf7d0f218c07c69a3e6b22d129371628c0613536182_prof);

        
        $__internal_f3f7d33f96c4472bb20c64df876069eb977d2b641b538ade87b534eb74100a45->leave($__internal_f3f7d33f96c4472bb20c64df876069eb977d2b641b538ade87b534eb74100a45_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:reset.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}

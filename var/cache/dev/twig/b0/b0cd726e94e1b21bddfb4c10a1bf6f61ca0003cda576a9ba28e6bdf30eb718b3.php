<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_2910cc1871175f297ed016c3e627ca337de36836c3ceffbe1334ff2718c1a2d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc28939c960faca90d584c45395092f91ca2a8f56237cad5c8be287d9221a7b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc28939c960faca90d584c45395092f91ca2a8f56237cad5c8be287d9221a7b6->enter($__internal_dc28939c960faca90d584c45395092f91ca2a8f56237cad5c8be287d9221a7b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_82b27eafedc5ac76eec5cf11c8b7b290cfe290c0cd2a1d35b96762b95f5be075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82b27eafedc5ac76eec5cf11c8b7b290cfe290c0cd2a1d35b96762b95f5be075->enter($__internal_82b27eafedc5ac76eec5cf11c8b7b290cfe290c0cd2a1d35b96762b95f5be075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dc28939c960faca90d584c45395092f91ca2a8f56237cad5c8be287d9221a7b6->leave($__internal_dc28939c960faca90d584c45395092f91ca2a8f56237cad5c8be287d9221a7b6_prof);

        
        $__internal_82b27eafedc5ac76eec5cf11c8b7b290cfe290c0cd2a1d35b96762b95f5be075->leave($__internal_82b27eafedc5ac76eec5cf11c8b7b290cfe290c0cd2a1d35b96762b95f5be075_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4665420712866d76bd2b6904ec09ba12e4d46dd5b3ac4d0f0d201789bb5636a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4665420712866d76bd2b6904ec09ba12e4d46dd5b3ac4d0f0d201789bb5636a3->enter($__internal_4665420712866d76bd2b6904ec09ba12e4d46dd5b3ac4d0f0d201789bb5636a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3c67db1250017a8f21de55e66e85939beceff3c96419d37ab8a662dc6f14b9e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c67db1250017a8f21de55e66e85939beceff3c96419d37ab8a662dc6f14b9e5->enter($__internal_3c67db1250017a8f21de55e66e85939beceff3c96419d37ab8a662dc6f14b9e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_3c67db1250017a8f21de55e66e85939beceff3c96419d37ab8a662dc6f14b9e5->leave($__internal_3c67db1250017a8f21de55e66e85939beceff3c96419d37ab8a662dc6f14b9e5_prof);

        
        $__internal_4665420712866d76bd2b6904ec09ba12e4d46dd5b3ac4d0f0d201789bb5636a3->leave($__internal_4665420712866d76bd2b6904ec09ba12e4d46dd5b3ac4d0f0d201789bb5636a3_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_8bd0413d0d5bb1323fdba29ac783933724cff26d6060a966f241011295b013cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bd0413d0d5bb1323fdba29ac783933724cff26d6060a966f241011295b013cc->enter($__internal_8bd0413d0d5bb1323fdba29ac783933724cff26d6060a966f241011295b013cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8fd050289736a758513b18c677d80d2dc4e7cd55805fba09d4d557b5615890d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fd050289736a758513b18c677d80d2dc4e7cd55805fba09d4d557b5615890d2->enter($__internal_8fd050289736a758513b18c677d80d2dc4e7cd55805fba09d4d557b5615890d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_8fd050289736a758513b18c677d80d2dc4e7cd55805fba09d4d557b5615890d2->leave($__internal_8fd050289736a758513b18c677d80d2dc4e7cd55805fba09d4d557b5615890d2_prof);

        
        $__internal_8bd0413d0d5bb1323fdba29ac783933724cff26d6060a966f241011295b013cc->leave($__internal_8bd0413d0d5bb1323fdba29ac783933724cff26d6060a966f241011295b013cc_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_49057b119451831995232504633335c7b7206588f5e5753c8560aab998b2bcea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49057b119451831995232504633335c7b7206588f5e5753c8560aab998b2bcea->enter($__internal_49057b119451831995232504633335c7b7206588f5e5753c8560aab998b2bcea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_73ab92437cd3c432835eb2f5f98b5a679b15a1687ae01c2889c2f645e18e4a05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73ab92437cd3c432835eb2f5f98b5a679b15a1687ae01c2889c2f645e18e4a05->enter($__internal_73ab92437cd3c432835eb2f5f98b5a679b15a1687ae01c2889c2f645e18e4a05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_73ab92437cd3c432835eb2f5f98b5a679b15a1687ae01c2889c2f645e18e4a05->leave($__internal_73ab92437cd3c432835eb2f5f98b5a679b15a1687ae01c2889c2f645e18e4a05_prof);

        
        $__internal_49057b119451831995232504633335c7b7206588f5e5753c8560aab998b2bcea->leave($__internal_49057b119451831995232504633335c7b7206588f5e5753c8560aab998b2bcea_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_time.html.twig */
class __TwigTemplate_8c090edfce119cf2e25897ee13f6dfc4afcf3c0fc7b7b3e16a4f343730e7721a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d72506adfb6ad46542400ca3300888bb1fae629c411a2f88eb1d4b0b13a35a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d72506adfb6ad46542400ca3300888bb1fae629c411a2f88eb1d4b0b13a35a4->enter($__internal_8d72506adfb6ad46542400ca3300888bb1fae629c411a2f88eb1d4b0b13a35a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_time.html.twig"));

        $__internal_9e76e7ded0ba7996aa3671ee916784c4a69b9d97b31bd8119d6233ce7b360ecd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e76e7ded0ba7996aa3671ee916784c4a69b9d97b31bd8119d6233ce7b360ecd->enter($__internal_9e76e7ded0ba7996aa3671ee916784c4a69b9d97b31bd8119d6233ce7b360ecd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_time.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_8d72506adfb6ad46542400ca3300888bb1fae629c411a2f88eb1d4b0b13a35a4->leave($__internal_8d72506adfb6ad46542400ca3300888bb1fae629c411a2f88eb1d4b0b13a35a4_prof);

        
        $__internal_9e76e7ded0ba7996aa3671ee916784c4a69b9d97b31bd8119d6233ce7b360ecd->leave($__internal_9e76e7ded0ba7996aa3671ee916784c4a69b9d97b31bd8119d6233ce7b360ecd_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_time.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_time.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_time.html.twig");
    }
}

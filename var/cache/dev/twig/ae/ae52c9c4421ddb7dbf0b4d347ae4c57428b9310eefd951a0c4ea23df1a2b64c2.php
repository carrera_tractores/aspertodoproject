<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_6d0af57b54410f459db9f734c3284952be0d91669a11922ab261915c9ae23fb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0eefe5785d6041acf6d65ee18a9c04b1108265b354fd0ba5277d5434c1f94f66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0eefe5785d6041acf6d65ee18a9c04b1108265b354fd0ba5277d5434c1f94f66->enter($__internal_0eefe5785d6041acf6d65ee18a9c04b1108265b354fd0ba5277d5434c1f94f66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_20b70d46e73382618fa331bff7fef6a5eb0eb238ee48735b81cb4dcb87f2d4ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_20b70d46e73382618fa331bff7fef6a5eb0eb238ee48735b81cb4dcb87f2d4ae->enter($__internal_20b70d46e73382618fa331bff7fef6a5eb0eb238ee48735b81cb4dcb87f2d4ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_0eefe5785d6041acf6d65ee18a9c04b1108265b354fd0ba5277d5434c1f94f66->leave($__internal_0eefe5785d6041acf6d65ee18a9c04b1108265b354fd0ba5277d5434c1f94f66_prof);

        
        $__internal_20b70d46e73382618fa331bff7fef6a5eb0eb238ee48735b81cb4dcb87f2d4ae->leave($__internal_20b70d46e73382618fa331bff7fef6a5eb0eb238ee48735b81cb4dcb87f2d4ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}

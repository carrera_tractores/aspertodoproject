<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_fb30797c7ae7cefb2ad9034a939a8701668ff0d737e78a35e73c844fb9f1cd06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f1e82909dfc4637e54294fd5a454386a1a04239ea702d5dd41f669716bb799f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f1e82909dfc4637e54294fd5a454386a1a04239ea702d5dd41f669716bb799f->enter($__internal_2f1e82909dfc4637e54294fd5a454386a1a04239ea702d5dd41f669716bb799f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_28a94a2b7613b4584c9dbdf23fc53d892c1040b8119b63c94c4647f0335040d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28a94a2b7613b4584c9dbdf23fc53d892c1040b8119b63c94c4647f0335040d8->enter($__internal_28a94a2b7613b4584c9dbdf23fc53d892c1040b8119b63c94c4647f0335040d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_2f1e82909dfc4637e54294fd5a454386a1a04239ea702d5dd41f669716bb799f->leave($__internal_2f1e82909dfc4637e54294fd5a454386a1a04239ea702d5dd41f669716bb799f_prof);

        
        $__internal_28a94a2b7613b4584c9dbdf23fc53d892c1040b8119b63c94c4647f0335040d8->leave($__internal_28a94a2b7613b4584c9dbdf23fc53d892c1040b8119b63c94c4647f0335040d8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}

<?php

/* new.html.twig~ */
class __TwigTemplate_73f3f1df1ea500952dcd8cae69bd588a11f6dd6ad0d8c2fdfc58fc5c6380ae9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3da3964d41c0959ada9b7fd163d1dacccc19db2603f46e19d8f26a2720abd4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3da3964d41c0959ada9b7fd163d1dacccc19db2603f46e19d8f26a2720abd4c->enter($__internal_e3da3964d41c0959ada9b7fd163d1dacccc19db2603f46e19d8f26a2720abd4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "new.html.twig~"));

        $__internal_209b8b18fc74bf9b2694008f372f39862ef276e93e10ab007c92be75695ca44a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_209b8b18fc74bf9b2694008f372f39862ef276e93e10ab007c92be75695ca44a->enter($__internal_209b8b18fc74bf9b2694008f372f39862ef276e93e10ab007c92be75695ca44a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "new.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
 \t <h3>Formulari de l'usuari</h3>
 \t 
    \t  ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), 'form_start');
        echo "
    \t  ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->getSourceContext()); })()), 'widget');
        echo "
    \t  ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 14, $this->getSourceContext()); })()), 'form_end');
        echo "
    \t  
        ";
        // line 16
        $this->displayBlock('body', $context, $blocks);
        // line 17
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 18
        echo "    </body>
</html>
";
        
        $__internal_e3da3964d41c0959ada9b7fd163d1dacccc19db2603f46e19d8f26a2720abd4c->leave($__internal_e3da3964d41c0959ada9b7fd163d1dacccc19db2603f46e19d8f26a2720abd4c_prof);

        
        $__internal_209b8b18fc74bf9b2694008f372f39862ef276e93e10ab007c92be75695ca44a->leave($__internal_209b8b18fc74bf9b2694008f372f39862ef276e93e10ab007c92be75695ca44a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6be7947b08d1ebff6b7578b7627652ec05d48344e16be076974af4b128084f1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6be7947b08d1ebff6b7578b7627652ec05d48344e16be076974af4b128084f1a->enter($__internal_6be7947b08d1ebff6b7578b7627652ec05d48344e16be076974af4b128084f1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_30db3f7b439b95b69b56b398d2ea16d0ab4a7315bd8c9dc77ebf19bf0c343aff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30db3f7b439b95b69b56b398d2ea16d0ab4a7315bd8c9dc77ebf19bf0c343aff->enter($__internal_30db3f7b439b95b69b56b398d2ea16d0ab4a7315bd8c9dc77ebf19bf0c343aff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Formulari!";
        
        $__internal_30db3f7b439b95b69b56b398d2ea16d0ab4a7315bd8c9dc77ebf19bf0c343aff->leave($__internal_30db3f7b439b95b69b56b398d2ea16d0ab4a7315bd8c9dc77ebf19bf0c343aff_prof);

        
        $__internal_6be7947b08d1ebff6b7578b7627652ec05d48344e16be076974af4b128084f1a->leave($__internal_6be7947b08d1ebff6b7578b7627652ec05d48344e16be076974af4b128084f1a_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d26d6fb1691c561f85a0465d6d585eadab8a20f3ac3922398c988f37662e6784 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d26d6fb1691c561f85a0465d6d585eadab8a20f3ac3922398c988f37662e6784->enter($__internal_d26d6fb1691c561f85a0465d6d585eadab8a20f3ac3922398c988f37662e6784_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e610aa9572c7ca97ba3aa987f6be397491e23fc4faba76d622a2f29c7cb39f0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e610aa9572c7ca97ba3aa987f6be397491e23fc4faba76d622a2f29c7cb39f0a->enter($__internal_e610aa9572c7ca97ba3aa987f6be397491e23fc4faba76d622a2f29c7cb39f0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_e610aa9572c7ca97ba3aa987f6be397491e23fc4faba76d622a2f29c7cb39f0a->leave($__internal_e610aa9572c7ca97ba3aa987f6be397491e23fc4faba76d622a2f29c7cb39f0a_prof);

        
        $__internal_d26d6fb1691c561f85a0465d6d585eadab8a20f3ac3922398c988f37662e6784->leave($__internal_d26d6fb1691c561f85a0465d6d585eadab8a20f3ac3922398c988f37662e6784_prof);

    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        $__internal_5a05aae7950ba35b9e902fa59d7bb59d8c420e8c24ae26648e0cc6e6b28a920c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a05aae7950ba35b9e902fa59d7bb59d8c420e8c24ae26648e0cc6e6b28a920c->enter($__internal_5a05aae7950ba35b9e902fa59d7bb59d8c420e8c24ae26648e0cc6e6b28a920c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5b4e0b4879fa119ae19941389bb9dd7215ef954c852be88cc954218c23fc4aa3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b4e0b4879fa119ae19941389bb9dd7215ef954c852be88cc954218c23fc4aa3->enter($__internal_5b4e0b4879fa119ae19941389bb9dd7215ef954c852be88cc954218c23fc4aa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5b4e0b4879fa119ae19941389bb9dd7215ef954c852be88cc954218c23fc4aa3->leave($__internal_5b4e0b4879fa119ae19941389bb9dd7215ef954c852be88cc954218c23fc4aa3_prof);

        
        $__internal_5a05aae7950ba35b9e902fa59d7bb59d8c420e8c24ae26648e0cc6e6b28a920c->leave($__internal_5a05aae7950ba35b9e902fa59d7bb59d8c420e8c24ae26648e0cc6e6b28a920c_prof);

    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d872cdfcce278f0262af266ec9b3fcd6b32829ea09076ef9e0bb12d15da4f65d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d872cdfcce278f0262af266ec9b3fcd6b32829ea09076ef9e0bb12d15da4f65d->enter($__internal_d872cdfcce278f0262af266ec9b3fcd6b32829ea09076ef9e0bb12d15da4f65d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_0b74ae79943b02b884a352094f45d1041acbfe10cf962c05d62a3304d51718b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b74ae79943b02b884a352094f45d1041acbfe10cf962c05d62a3304d51718b8->enter($__internal_0b74ae79943b02b884a352094f45d1041acbfe10cf962c05d62a3304d51718b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_0b74ae79943b02b884a352094f45d1041acbfe10cf962c05d62a3304d51718b8->leave($__internal_0b74ae79943b02b884a352094f45d1041acbfe10cf962c05d62a3304d51718b8_prof);

        
        $__internal_d872cdfcce278f0262af266ec9b3fcd6b32829ea09076ef9e0bb12d15da4f65d->leave($__internal_d872cdfcce278f0262af266ec9b3fcd6b32829ea09076ef9e0bb12d15da4f65d_prof);

    }

    public function getTemplateName()
    {
        return "new.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 17,  115 => 16,  98 => 6,  80 => 5,  68 => 18,  65 => 17,  63 => 16,  58 => 14,  54 => 13,  50 => 12,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Formulari!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
 \t <h3>Formulari de l'usuari</h3>
 \t 
    \t  {{ form_start(form) }}
    \t  {{ form_widget(form) }}
    \t  {{ form_end(form) }}
    \t  
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "new.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/new.html.twig~");
    }
}

<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_f1af8bb68eaf3685cfbca9f87652915e26029ab1d8c2eb006c8f81e54a794aa0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ff1174ca4d86bd006a1ba4ff6e293df5532b88eef4715f6ea6812acd99cd9b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ff1174ca4d86bd006a1ba4ff6e293df5532b88eef4715f6ea6812acd99cd9b4->enter($__internal_9ff1174ca4d86bd006a1ba4ff6e293df5532b88eef4715f6ea6812acd99cd9b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_6dd8b20d1926662cd723b2d1003ba72d47c6a29e66e05c334320d876545ca7f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6dd8b20d1926662cd723b2d1003ba72d47c6a29e66e05c334320d876545ca7f6->enter($__internal_6dd8b20d1926662cd723b2d1003ba72d47c6a29e66e05c334320d876545ca7f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()))));
        
        $__internal_9ff1174ca4d86bd006a1ba4ff6e293df5532b88eef4715f6ea6812acd99cd9b4->leave($__internal_9ff1174ca4d86bd006a1ba4ff6e293df5532b88eef4715f6ea6812acd99cd9b4_prof);

        
        $__internal_6dd8b20d1926662cd723b2d1003ba72d47c6a29e66e05c334320d876545ca7f6->leave($__internal_6dd8b20d1926662cd723b2d1003ba72d47c6a29e66e05c334320d876545ca7f6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.rdf.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}

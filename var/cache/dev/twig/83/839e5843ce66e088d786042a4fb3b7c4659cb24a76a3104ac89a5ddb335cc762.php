<?php

/* @EasyAdmin/default/edit.html.twig */
class __TwigTemplate_4aaa65bf1736268e733bc7f5948111a8340c210df0dd206c8fdd3087c8ed37bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'delete_form' => array($this, 'block_delete_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 8
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 8, $this->getSourceContext()); })()), "templates", array()), "layout", array()), "@EasyAdmin/default/edit.html.twig", 8);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c867455bde444d3628dc2288db9ae1dcf88b2fcb530afd6c2c64cd875467429f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c867455bde444d3628dc2288db9ae1dcf88b2fcb530afd6c2c64cd875467429f->enter($__internal_c867455bde444d3628dc2288db9ae1dcf88b2fcb530afd6c2c64cd875467429f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/edit.html.twig"));

        $__internal_a76bb46e7e0db4e70ca28735d3b14aeacee396ffa2d0a74d392af9583734b6b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a76bb46e7e0db4e70ca28735d3b14aeacee396ffa2d0a74d392af9583734b6b5->enter($__internal_a76bb46e7e0db4e70ca28735d3b14aeacee396ffa2d0a74d392af9583734b6b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/edit.html.twig"));

        // line 1
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 1, $this->getSourceContext()); })()), $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 3, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["_entity_id"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["entity"]) || array_key_exists("entity", $context) ? $context["entity"] : (function () { throw new Twig_Error_Runtime('Variable "entity" does not exist.', 4, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 4, $this->getSourceContext()); })()), "primary_key_field_name", array()));
        // line 5
        $context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->getSourceContext()); })()), "translation_domain", array());
        // line 6
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 6, $this->getSourceContext()); })()), "name", array()), array(),         // line 5
(isset($context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"]) || array_key_exists("__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723", $context) ? $context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723" does not exist.', 5, $this->getSourceContext()); })())), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(),         // line 6
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 6, $this->getSourceContext()); })()), "label", array()), array(),         // line 5
(isset($context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"]) || array_key_exists("__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723", $context) ? $context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723" does not exist.', 5, $this->getSourceContext()); })())), "%entity_id%" =>         // line 6
(isset($context["_entity_id"]) || array_key_exists("_entity_id", $context) ? $context["_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_id" does not exist.', 6, $this->getSourceContext()); })()));
        // line 8
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c867455bde444d3628dc2288db9ae1dcf88b2fcb530afd6c2c64cd875467429f->leave($__internal_c867455bde444d3628dc2288db9ae1dcf88b2fcb530afd6c2c64cd875467429f_prof);

        
        $__internal_a76bb46e7e0db4e70ca28735d3b14aeacee396ffa2d0a74d392af9583734b6b5->leave($__internal_a76bb46e7e0db4e70ca28735d3b14aeacee396ffa2d0a74d392af9583734b6b5_prof);

    }

    // line 10
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_1f62e54944a46b55141b885c26832b3a0660fcf288d15d0d2425ba21ea6eab35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f62e54944a46b55141b885c26832b3a0660fcf288d15d0d2425ba21ea6eab35->enter($__internal_1f62e54944a46b55141b885c26832b3a0660fcf288d15d0d2425ba21ea6eab35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_bd1a830bd2dcdae346b7e022dc66c771a5b48b45bd49d7f9c1c58b7cd7bcb458 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd1a830bd2dcdae346b7e022dc66c771a5b48b45bd49d7f9c1c58b7cd7bcb458->enter($__internal_bd1a830bd2dcdae346b7e022dc66c771a5b48b45bd49d7f9c1c58b7cd7bcb458_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ((("easyadmin-edit-" . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 10, $this->getSourceContext()); })()), "name", array())) . "-") . (isset($context["_entity_id"]) || array_key_exists("_entity_id", $context) ? $context["_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_id" does not exist.', 10, $this->getSourceContext()); })())), "html", null, true);
        
        $__internal_bd1a830bd2dcdae346b7e022dc66c771a5b48b45bd49d7f9c1c58b7cd7bcb458->leave($__internal_bd1a830bd2dcdae346b7e022dc66c771a5b48b45bd49d7f9c1c58b7cd7bcb458_prof);

        
        $__internal_1f62e54944a46b55141b885c26832b3a0660fcf288d15d0d2425ba21ea6eab35->leave($__internal_1f62e54944a46b55141b885c26832b3a0660fcf288d15d0d2425ba21ea6eab35_prof);

    }

    // line 11
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_7edd91e3fa6b134bca095320128e87413b6469fbd9854a22fcc89bbfeb1c5c45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7edd91e3fa6b134bca095320128e87413b6469fbd9854a22fcc89bbfeb1c5c45->enter($__internal_7edd91e3fa6b134bca095320128e87413b6469fbd9854a22fcc89bbfeb1c5c45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_a05dfbe7434e6f50744c79e913c9e217a6529c7397822dc0b270f11bb7309449 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a05dfbe7434e6f50744c79e913c9e217a6529c7397822dc0b270f11bb7309449->enter($__internal_a05dfbe7434e6f50744c79e913c9e217a6529c7397822dc0b270f11bb7309449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("edit edit-" . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 11, $this->getSourceContext()); })()), "name", array()))), "html", null, true);
        
        $__internal_a05dfbe7434e6f50744c79e913c9e217a6529c7397822dc0b270f11bb7309449->leave($__internal_a05dfbe7434e6f50744c79e913c9e217a6529c7397822dc0b270f11bb7309449_prof);

        
        $__internal_7edd91e3fa6b134bca095320128e87413b6469fbd9854a22fcc89bbfeb1c5c45->leave($__internal_7edd91e3fa6b134bca095320128e87413b6469fbd9854a22fcc89bbfeb1c5c45_prof);

    }

    // line 13
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_e4a6fcd544bce0103826673e81f77244400a9e3f6db2524ffdff7ad5625c1cb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4a6fcd544bce0103826673e81f77244400a9e3f6db2524ffdff7ad5625c1cb4->enter($__internal_e4a6fcd544bce0103826673e81f77244400a9e3f6db2524ffdff7ad5625c1cb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_3daf8da665aedf4071b5b5c5c7b0196b63e1069474825346f3ce4e22af4b7bd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3daf8da665aedf4071b5b5c5c7b0196b63e1069474825346f3ce4e22af4b7bd8->enter($__internal_3daf8da665aedf4071b5b5c5c7b0196b63e1069474825346f3ce4e22af4b7bd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 14
        ob_start();
        // line 15
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("edit.page_title", (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 15, $this->getSourceContext()); })()), "EasyAdminBundle");
        // line 16
        echo "    ";
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), "edit", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 16, $this->getSourceContext()); })()), "edit", array()), "title", array()), (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 16, $this->getSourceContext()); })()),         // line 5
(isset($context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"]) || array_key_exists("__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723", $context) ? $context["__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_6ff8f3c31b5201b06c3f45fc97ad94b4732644c7155fda8fac6ed57b518d4723" does not exist.', 5, $this->getSourceContext()); })()))) : (        // line 16
(isset($context["_default_title"]) || array_key_exists("_default_title", $context) ? $context["_default_title"] : (function () { throw new Twig_Error_Runtime('Variable "_default_title" does not exist.', 16, $this->getSourceContext()); })()))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_3daf8da665aedf4071b5b5c5c7b0196b63e1069474825346f3ce4e22af4b7bd8->leave($__internal_3daf8da665aedf4071b5b5c5c7b0196b63e1069474825346f3ce4e22af4b7bd8_prof);

        
        $__internal_e4a6fcd544bce0103826673e81f77244400a9e3f6db2524ffdff7ad5625c1cb4->leave($__internal_e4a6fcd544bce0103826673e81f77244400a9e3f6db2524ffdff7ad5625c1cb4_prof);

    }

    // line 20
    public function block_main($context, array $blocks = array())
    {
        $__internal_e5d441a41eb7c4b5147bb293a48da0eb8cd334e692b390a0ee902023dfb8deda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5d441a41eb7c4b5147bb293a48da0eb8cd334e692b390a0ee902023dfb8deda->enter($__internal_e5d441a41eb7c4b5147bb293a48da0eb8cd334e692b390a0ee902023dfb8deda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_ba0bba926a4ea41794c64acd89d5af2fec788f1c8f52c01c3006c14f6ac70dc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba0bba926a4ea41794c64acd89d5af2fec788f1c8f52c01c3006c14f6ac70dc3->enter($__internal_ba0bba926a4ea41794c64acd89d5af2fec788f1c8f52c01c3006c14f6ac70dc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 21
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        // line 24
        echo "
    ";
        // line 25
        $this->displayBlock('delete_form', $context, $blocks);
        
        $__internal_ba0bba926a4ea41794c64acd89d5af2fec788f1c8f52c01c3006c14f6ac70dc3->leave($__internal_ba0bba926a4ea41794c64acd89d5af2fec788f1c8f52c01c3006c14f6ac70dc3_prof);

        
        $__internal_e5d441a41eb7c4b5147bb293a48da0eb8cd334e692b390a0ee902023dfb8deda->leave($__internal_e5d441a41eb7c4b5147bb293a48da0eb8cd334e692b390a0ee902023dfb8deda_prof);

    }

    // line 21
    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_4a929023c90f21dca09e847a262cba27665c5e2655a3d02d83a6f55de4b1f04f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a929023c90f21dca09e847a262cba27665c5e2655a3d02d83a6f55de4b1f04f->enter($__internal_4a929023c90f21dca09e847a262cba27665c5e2655a3d02d83a6f55de4b1f04f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_ab2f11b7bf6c177f61cbf255d37082d1d91585792462b17903882cdd1d0ee26e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab2f11b7bf6c177f61cbf255d37082d1d91585792462b17903882cdd1d0ee26e->enter($__internal_ab2f11b7bf6c177f61cbf255d37082d1d91585792462b17903882cdd1d0ee26e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 22
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 22, $this->getSourceContext()); })()), 'form');
        echo "
    ";
        
        $__internal_ab2f11b7bf6c177f61cbf255d37082d1d91585792462b17903882cdd1d0ee26e->leave($__internal_ab2f11b7bf6c177f61cbf255d37082d1d91585792462b17903882cdd1d0ee26e_prof);

        
        $__internal_4a929023c90f21dca09e847a262cba27665c5e2655a3d02d83a6f55de4b1f04f->leave($__internal_4a929023c90f21dca09e847a262cba27665c5e2655a3d02d83a6f55de4b1f04f_prof);

    }

    // line 25
    public function block_delete_form($context, array $blocks = array())
    {
        $__internal_f0adc1335477e605497bfedf9d97f094834b591baf63ca21fbbbc80ae8fb4b47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0adc1335477e605497bfedf9d97f094834b591baf63ca21fbbbc80ae8fb4b47->enter($__internal_f0adc1335477e605497bfedf9d97f094834b591baf63ca21fbbbc80ae8fb4b47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        $__internal_16450b9374ba13b247876510d3a95de9b0202c25fbde6b3473769b0b92351dfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16450b9374ba13b247876510d3a95de9b0202c25fbde6b3473769b0b92351dfc->enter($__internal_16450b9374ba13b247876510d3a95de9b0202c25fbde6b3473769b0b92351dfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        // line 26
        echo "        ";
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_delete_form.html.twig", array("view" => "edit", "referer" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),         // line 28
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 28, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "referer", 1 => ""), "method"), "delete_form" =>         // line 29
(isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 29, $this->getSourceContext()); })()), "_translation_domain" => twig_get_attribute($this->env, $this->getSourceContext(),         // line 30
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 30, $this->getSourceContext()); })()), "translation_domain", array()), "_trans_parameters" =>         // line 31
(isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 31, $this->getSourceContext()); })()), "_entity_config" =>         // line 32
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 32, $this->getSourceContext()); })())), false);
        // line 33
        echo "
    ";
        
        $__internal_16450b9374ba13b247876510d3a95de9b0202c25fbde6b3473769b0b92351dfc->leave($__internal_16450b9374ba13b247876510d3a95de9b0202c25fbde6b3473769b0b92351dfc_prof);

        
        $__internal_f0adc1335477e605497bfedf9d97f094834b591baf63ca21fbbbc80ae8fb4b47->leave($__internal_f0adc1335477e605497bfedf9d97f094834b591baf63ca21fbbbc80ae8fb4b47_prof);

    }

    // line 37
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_bd879f8622d8656dc905bc823c835a835c7a3a7e95ec5ebef81cb483f333811b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd879f8622d8656dc905bc823c835a835c7a3a7e95ec5ebef81cb483f333811b->enter($__internal_bd879f8622d8656dc905bc823c835a835c7a3a7e95ec5ebef81cb483f333811b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_f54a82d2b27a4e1e89757966cdba61dff6a9238aa856d4dafb039e2ec617710b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f54a82d2b27a4e1e89757966cdba61dff6a9238aa856d4dafb039e2ec617710b->enter($__internal_f54a82d2b27a4e1e89757966cdba61dff6a9238aa856d4dafb039e2ec617710b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 38
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.edit-form').areYouSure({ 'message': '";
        // line 42
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();

            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>

    ";
        // line 58
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_f54a82d2b27a4e1e89757966cdba61dff6a9238aa856d4dafb039e2ec617710b->leave($__internal_f54a82d2b27a4e1e89757966cdba61dff6a9238aa856d4dafb039e2ec617710b_prof);

        
        $__internal_bd879f8622d8656dc905bc823c835a835c7a3a7e95ec5ebef81cb483f333811b->leave($__internal_bd879f8622d8656dc905bc823c835a835c7a3a7e95ec5ebef81cb483f333811b_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 58,  218 => 42,  210 => 38,  201 => 37,  190 => 33,  188 => 32,  187 => 31,  186 => 30,  185 => 29,  184 => 28,  182 => 26,  173 => 25,  160 => 22,  151 => 21,  141 => 25,  138 => 24,  135 => 21,  126 => 20,  113 => 16,  112 => 5,  110 => 16,  107 => 15,  105 => 14,  96 => 13,  78 => 11,  60 => 10,  50 => 8,  48 => 6,  47 => 5,  46 => 6,  45 => 5,  44 => 6,  42 => 5,  40 => 4,  38 => 3,  36 => 1,  24 => 8,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% form_theme form with easyadmin_config('design.form_theme') %}

{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% set _entity_id = attribute(entity, _entity_config.primary_key_field_name) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans, '%entity_id%': _entity_id } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-edit-' ~ _entity_config.name ~ '-' ~ _entity_id %}
{% block body_class 'edit edit-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'edit.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.edit.title is defined ? _entity_config.edit.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    {% block entity_form %}
        {{ form(form) }}
    {% endblock entity_form %}

    {% block delete_form %}
        {{ include('@EasyAdmin/default/includes/_delete_form.html.twig', {
            view: 'edit',
            referer: app.request.query.get('referer', ''),
            delete_form: delete_form,
            _translation_domain: _entity_config.translation_domain,
            _trans_parameters: _trans_parameters,
            _entity_config: _entity_config,
        }, with_context = false) }}
    {% endblock delete_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('.edit-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });

            \$('.form-actions').easyAdminSticky();

            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>

    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "@EasyAdmin/default/edit.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/edit.html.twig");
    }
}

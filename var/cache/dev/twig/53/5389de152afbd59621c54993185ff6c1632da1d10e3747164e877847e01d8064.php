<?php

/* EasyAdminBundle:default:field_date.html.twig */
class __TwigTemplate_08a1d2a75a294cb84e559439c129a6f14641ffd9c939d2b8daf3f5a4c9570298 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc92155c6a7a370f01696d705880033afd0f720401de9c9ca2eaaa16323280a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc92155c6a7a370f01696d705880033afd0f720401de9c9ca2eaaa16323280a7->enter($__internal_bc92155c6a7a370f01696d705880033afd0f720401de9c9ca2eaaa16323280a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_date.html.twig"));

        $__internal_f2fcb45dad214fbd3f7435d6119ccc7e799d5cceb52af183c0db34ddd4da7075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2fcb45dad214fbd3f7435d6119ccc7e799d5cceb52af183c0db34ddd4da7075->enter($__internal_f2fcb45dad214fbd3f7435d6119ccc7e799d5cceb52af183c0db34ddd4da7075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_date.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_bc92155c6a7a370f01696d705880033afd0f720401de9c9ca2eaaa16323280a7->leave($__internal_bc92155c6a7a370f01696d705880033afd0f720401de9c9ca2eaaa16323280a7_prof);

        
        $__internal_f2fcb45dad214fbd3f7435d6119ccc7e799d5cceb52af183c0db34ddd4da7075->leave($__internal_f2fcb45dad214fbd3f7435d6119ccc7e799d5cceb52af183c0db34ddd4da7075_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_date.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_date.html.twig");
    }
}

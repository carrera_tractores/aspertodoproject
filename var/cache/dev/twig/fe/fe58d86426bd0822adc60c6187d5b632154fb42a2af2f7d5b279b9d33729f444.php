<?php

/* proba.html.twig~ */
class __TwigTemplate_e39da34c556117c4e2db49cc3e2a0773c311bafe4ff9479ab2bf42f635af83aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_965dc853e1140cb23414ba9b14abb8fb84b3f76d10761f1a716b87851027fe39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_965dc853e1140cb23414ba9b14abb8fb84b3f76d10761f1a716b87851027fe39->enter($__internal_965dc853e1140cb23414ba9b14abb8fb84b3f76d10761f1a716b87851027fe39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "proba.html.twig~"));

        $__internal_734bfc3ab1cc48c24576f3bcc0980cae3e9b3e4bc36e8a265fda208ddb268f00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_734bfc3ab1cc48c24576f3bcc0980cae3e9b3e4bc36e8a265fda208ddb268f00->enter($__internal_734bfc3ab1cc48c24576f3bcc0980cae3e9b3e4bc36e8a265fda208ddb268f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "proba.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/calendari.css"), "html", null, true);
        echo "\" />
    </head>
    <body>
    \t";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "\t\t<header>
\t\t\t<h1 id=\"titol\">AsperToDo</h1>\t\t
        \t<div id=\"power\"> 
        \t\t<div id=\"imatge_power\"></div>
\t\t\t\tHola @Usuari!
\t\t\t</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"proba\">Calendari</a></li>
        <li><a href=\"proba\">Gestió d'usuaris</a></li>
        <li><a href=\"proba\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>CALENDARI</h1>
      <hr>
      <div id=\"calendar\">
      
   
    
      </div>
\t\t<div id=\"afegir_tasca\">
\t\t<div id=\"imatge_afegir\"></div>
\t\tAfegir tasca
\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>Footer Text</p>
</footer>
        
        
        
        ";
        // line 66
        $this->displayBlock('javascripts', $context, $blocks);
        // line 70
        echo "    </body>
</html>
";
        
        $__internal_965dc853e1140cb23414ba9b14abb8fb84b3f76d10761f1a716b87851027fe39->leave($__internal_965dc853e1140cb23414ba9b14abb8fb84b3f76d10761f1a716b87851027fe39_prof);

        
        $__internal_734bfc3ab1cc48c24576f3bcc0980cae3e9b3e4bc36e8a265fda208ddb268f00->leave($__internal_734bfc3ab1cc48c24576f3bcc0980cae3e9b3e4bc36e8a265fda208ddb268f00_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ad5f166d0ccd48e41cd631e12ae7dcf55856f9fc3b4db5716e5d4d7d9c0e09a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad5f166d0ccd48e41cd631e12ae7dcf55856f9fc3b4db5716e5d4d7d9c0e09a2->enter($__internal_ad5f166d0ccd48e41cd631e12ae7dcf55856f9fc3b4db5716e5d4d7d9c0e09a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6bab889213ca16cdef561eb93d015fa80ea698b8dfcdfee7b4a03494c725a5e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bab889213ca16cdef561eb93d015fa80ea698b8dfcdfee7b4a03494c725a5e1->enter($__internal_6bab889213ca16cdef561eb93d015fa80ea698b8dfcdfee7b4a03494c725a5e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Calendari";
        
        $__internal_6bab889213ca16cdef561eb93d015fa80ea698b8dfcdfee7b4a03494c725a5e1->leave($__internal_6bab889213ca16cdef561eb93d015fa80ea698b8dfcdfee7b4a03494c725a5e1_prof);

        
        $__internal_ad5f166d0ccd48e41cd631e12ae7dcf55856f9fc3b4db5716e5d4d7d9c0e09a2->leave($__internal_ad5f166d0ccd48e41cd631e12ae7dcf55856f9fc3b4db5716e5d4d7d9c0e09a2_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b1b7d7a9a6a091b9ab69437c2f7ec1f085314052085660cc2b2ed8e1359cedd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b1b7d7a9a6a091b9ab69437c2f7ec1f085314052085660cc2b2ed8e1359cedd5->enter($__internal_b1b7d7a9a6a091b9ab69437c2f7ec1f085314052085660cc2b2ed8e1359cedd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_02cb497ceeb127983ace03f0a8ee1816bc59507947a5abd883f8e079709d8f50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02cb497ceeb127983ace03f0a8ee1816bc59507947a5abd883f8e079709d8f50->enter($__internal_02cb497ceeb127983ace03f0a8ee1816bc59507947a5abd883f8e079709d8f50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_02cb497ceeb127983ace03f0a8ee1816bc59507947a5abd883f8e079709d8f50->leave($__internal_02cb497ceeb127983ace03f0a8ee1816bc59507947a5abd883f8e079709d8f50_prof);

        
        $__internal_b1b7d7a9a6a091b9ab69437c2f7ec1f085314052085660cc2b2ed8e1359cedd5->leave($__internal_b1b7d7a9a6a091b9ab69437c2f7ec1f085314052085660cc2b2ed8e1359cedd5_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_7100ed52c12921afc19e506e6c4bc1df38ac1f2d54fba950d30a6361a84f45d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7100ed52c12921afc19e506e6c4bc1df38ac1f2d54fba950d30a6361a84f45d6->enter($__internal_7100ed52c12921afc19e506e6c4bc1df38ac1f2d54fba950d30a6361a84f45d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2fceb2615156b242a8ce88b2ebc0ba24d2f925aad8708dfaab0cc0583beebcae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fceb2615156b242a8ce88b2ebc0ba24d2f925aad8708dfaab0cc0583beebcae->enter($__internal_2fceb2615156b242a8ce88b2ebc0ba24d2f925aad8708dfaab0cc0583beebcae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_2fceb2615156b242a8ce88b2ebc0ba24d2f925aad8708dfaab0cc0583beebcae->leave($__internal_2fceb2615156b242a8ce88b2ebc0ba24d2f925aad8708dfaab0cc0583beebcae_prof);

        
        $__internal_7100ed52c12921afc19e506e6c4bc1df38ac1f2d54fba950d30a6361a84f45d6->leave($__internal_7100ed52c12921afc19e506e6c4bc1df38ac1f2d54fba950d30a6361a84f45d6_prof);

    }

    // line 66
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_401fd895cfa73a39c5a29b6ee4971dc2251f55ca70c587ae18f941fd9fdb0f61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_401fd895cfa73a39c5a29b6ee4971dc2251f55ca70c587ae18f941fd9fdb0f61->enter($__internal_401fd895cfa73a39c5a29b6ee4971dc2251f55ca70c587ae18f941fd9fdb0f61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d4ea66a6f86aa5e5f3861907b76f706531b8f48596fde48e5277b59a5c2f1800 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4ea66a6f86aa5e5f3861907b76f706531b8f48596fde48e5277b59a5c2f1800->enter($__internal_d4ea66a6f86aa5e5f3861907b76f706531b8f48596fde48e5277b59a5c2f1800_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 67
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>            
        ";
        
        $__internal_d4ea66a6f86aa5e5f3861907b76f706531b8f48596fde48e5277b59a5c2f1800->leave($__internal_d4ea66a6f86aa5e5f3861907b76f706531b8f48596fde48e5277b59a5c2f1800_prof);

        
        $__internal_401fd895cfa73a39c5a29b6ee4971dc2251f55ca70c587ae18f941fd9fdb0f61->leave($__internal_401fd895cfa73a39c5a29b6ee4971dc2251f55ca70c587ae18f941fd9fdb0f61_prof);

    }

    public function getTemplateName()
    {
        return "proba.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 68,  191 => 67,  182 => 66,  165 => 14,  148 => 6,  130 => 5,  118 => 70,  116 => 66,  63 => 15,  61 => 14,  55 => 11,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Calendari{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/calendari.css') }}\" />
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<h1 id=\"titol\">AsperToDo</h1>\t\t
        \t<div id=\"power\"> 
        \t\t<div id=\"imatge_power\"></div>
\t\t\t\tHola @Usuari!
\t\t\t</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"proba\">Calendari</a></li>
        <li><a href=\"proba\">Gestió d'usuaris</a></li>
        <li><a href=\"proba\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>CALENDARI</h1>
      <hr>
      <div id=\"calendar\">
      
   
    
      </div>
\t\t<div id=\"afegir_tasca\">
\t\t<div id=\"imatge_afegir\"></div>
\t\tAfegir tasca
\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>Footer Text</p>
</footer>
        
        
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>            
        {% endblock %}
    </body>
</html>
", "proba.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/proba.html.twig~");
    }
}

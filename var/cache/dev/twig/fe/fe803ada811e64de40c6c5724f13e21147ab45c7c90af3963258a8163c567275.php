<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_5ad52dd7c42cde65a8c96e59497849337d96f0084fbf0fefb2a32b079a1fb784 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6952bcca55789480ecb773c91272a84e9ab62996ab0636e5ecab96da7199ab90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6952bcca55789480ecb773c91272a84e9ab62996ab0636e5ecab96da7199ab90->enter($__internal_6952bcca55789480ecb773c91272a84e9ab62996ab0636e5ecab96da7199ab90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_00b7eab74f6a1337b1dcf2ad4fddd601fdf90448771e61f2bbde41803eb209f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00b7eab74f6a1337b1dcf2ad4fddd601fdf90448771e61f2bbde41803eb209f0->enter($__internal_00b7eab74f6a1337b1dcf2ad4fddd601fdf90448771e61f2bbde41803eb209f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_6952bcca55789480ecb773c91272a84e9ab62996ab0636e5ecab96da7199ab90->leave($__internal_6952bcca55789480ecb773c91272a84e9ab62996ab0636e5ecab96da7199ab90_prof);

        
        $__internal_00b7eab74f6a1337b1dcf2ad4fddd601fdf90448771e61f2bbde41803eb209f0->leave($__internal_00b7eab74f6a1337b1dcf2ad4fddd601fdf90448771e61f2bbde41803eb209f0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}

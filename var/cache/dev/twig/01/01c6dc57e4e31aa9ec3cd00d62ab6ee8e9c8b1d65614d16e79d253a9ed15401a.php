<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_b81c61668e0043650300a51b612efc93c3232cf357bb0474e401f3cc8c660857 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a71164c4a70932bb8d8ad2f57173f1bed062e2b78a320cd58f6e190820741106 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a71164c4a70932bb8d8ad2f57173f1bed062e2b78a320cd58f6e190820741106->enter($__internal_a71164c4a70932bb8d8ad2f57173f1bed062e2b78a320cd58f6e190820741106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_1be28c15f47707b6a17fdbe8058cf5c725b9597d71b1a1a19c29f4924a72cade = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1be28c15f47707b6a17fdbe8058cf5c725b9597d71b1a1a19c29f4924a72cade->enter($__internal_1be28c15f47707b6a17fdbe8058cf5c725b9597d71b1a1a19c29f4924a72cade_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_a71164c4a70932bb8d8ad2f57173f1bed062e2b78a320cd58f6e190820741106->leave($__internal_a71164c4a70932bb8d8ad2f57173f1bed062e2b78a320cd58f6e190820741106_prof);

        
        $__internal_1be28c15f47707b6a17fdbe8058cf5c725b9597d71b1a1a19c29f4924a72cade->leave($__internal_1be28c15f47707b6a17fdbe8058cf5c725b9597d71b1a1a19c29f4924a72cade_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_a6f5610122c414bbbe3524d998d30140acb40ae97e13ebcc766289e33d52ff44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6f5610122c414bbbe3524d998d30140acb40ae97e13ebcc766289e33d52ff44->enter($__internal_a6f5610122c414bbbe3524d998d30140acb40ae97e13ebcc766289e33d52ff44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_2be6a5f9309521192fa79094a6f5de0f360ff4624c461947291e761a726b92ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2be6a5f9309521192fa79094a6f5de0f360ff4624c461947291e761a726b92ef->enter($__internal_2be6a5f9309521192fa79094a6f5de0f360ff4624c461947291e761a726b92ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 4, $this->getSourceContext()); })())), "FOSUserBundle");
        
        $__internal_2be6a5f9309521192fa79094a6f5de0f360ff4624c461947291e761a726b92ef->leave($__internal_2be6a5f9309521192fa79094a6f5de0f360ff4624c461947291e761a726b92ef_prof);

        
        $__internal_a6f5610122c414bbbe3524d998d30140acb40ae97e13ebcc766289e33d52ff44->leave($__internal_a6f5610122c414bbbe3524d998d30140acb40ae97e13ebcc766289e33d52ff44_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_9b7628c0df4a76c57642ab5c40124fdc4d0eafb07ed8c57b3723eb8f5353bfe1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b7628c0df4a76c57642ab5c40124fdc4d0eafb07ed8c57b3723eb8f5353bfe1->enter($__internal_9b7628c0df4a76c57642ab5c40124fdc4d0eafb07ed8c57b3723eb8f5353bfe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_c08186970e03be3eefdadeee6845fbef4ca9e2b389f41944ff8761ff370e6bc0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c08186970e03be3eefdadeee6845fbef4ca9e2b389f41944ff8761ff370e6bc0->enter($__internal_c08186970e03be3eefdadeee6845fbef4ca9e2b389f41944ff8761ff370e6bc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_c08186970e03be3eefdadeee6845fbef4ca9e2b389f41944ff8761ff370e6bc0->leave($__internal_c08186970e03be3eefdadeee6845fbef4ca9e2b389f41944ff8761ff370e6bc0_prof);

        
        $__internal_9b7628c0df4a76c57642ab5c40124fdc4d0eafb07ed8c57b3723eb8f5353bfe1->leave($__internal_9b7628c0df4a76c57642ab5c40124fdc4d0eafb07ed8c57b3723eb8f5353bfe1_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_c258f80c07caad150b9dd70f94d23c8977de26ac17020380b9a1552ce9cbecee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c258f80c07caad150b9dd70f94d23c8977de26ac17020380b9a1552ce9cbecee->enter($__internal_c258f80c07caad150b9dd70f94d23c8977de26ac17020380b9a1552ce9cbecee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_61c7326fbbd47568dc63a20fb5cce6f6355b497358d8ea421de570391be5634e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61c7326fbbd47568dc63a20fb5cce6f6355b497358d8ea421de570391be5634e->enter($__internal_61c7326fbbd47568dc63a20fb5cce6f6355b497358d8ea421de570391be5634e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_61c7326fbbd47568dc63a20fb5cce6f6355b497358d8ea421de570391be5634e->leave($__internal_61c7326fbbd47568dc63a20fb5cce6f6355b497358d8ea421de570391be5634e_prof);

        
        $__internal_c258f80c07caad150b9dd70f94d23c8977de26ac17020380b9a1552ce9cbecee->leave($__internal_c258f80c07caad150b9dd70f94d23c8977de26ac17020380b9a1552ce9cbecee_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/email.txt.twig");
    }
}

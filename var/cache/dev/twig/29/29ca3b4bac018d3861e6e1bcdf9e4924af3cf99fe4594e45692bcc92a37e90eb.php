<?php

/* FOSUserBundle:Group:new_content.html.twig */
class __TwigTemplate_29b02bae6a31fc5b40b09902e56311133ac7c77b91fd30319dd59c6cd58e4d28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bdd4b3b13432795e1e32eec5977a5ab78a8f18e48079bb5f9ca11caea642416 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bdd4b3b13432795e1e32eec5977a5ab78a8f18e48079bb5f9ca11caea642416->enter($__internal_0bdd4b3b13432795e1e32eec5977a5ab78a8f18e48079bb5f9ca11caea642416_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        $__internal_cb2149df4ec9074236b5bd5b1cdc8c04dcf46c04a2dbaa3bf1938d51d230e1fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb2149df4ec9074236b5bd5b1cdc8c04dcf46c04a2dbaa3bf1938d51d230e1fb->enter($__internal_cb2149df4ec9074236b5bd5b1cdc8c04dcf46c04a2dbaa3bf1938d51d230e1fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new_content.html.twig"));

        // line 2
        echo "
";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'form_start', array("action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_new"), "attr" => array("class" => "fos_user_group_new")));
        echo "
    ";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    <div>
        <input type=\"submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.new.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'form_end');
        echo "
";
        
        $__internal_0bdd4b3b13432795e1e32eec5977a5ab78a8f18e48079bb5f9ca11caea642416->leave($__internal_0bdd4b3b13432795e1e32eec5977a5ab78a8f18e48079bb5f9ca11caea642416_prof);

        
        $__internal_cb2149df4ec9074236b5bd5b1cdc8c04dcf46c04a2dbaa3bf1938d51d230e1fb->leave($__internal_cb2149df4ec9074236b5bd5b1cdc8c04dcf46c04a2dbaa3bf1938d51d230e1fb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 8,  37 => 6,  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

{{ form_start(form, { 'action': path('fos_user_group_new'), 'attr': { 'class': 'fos_user_group_new' } }) }}
    {{ form_widget(form) }}
    <div>
        <input type=\"submit\" value=\"{{ 'group.new.submit'|trans }}\" />
    </div>
{{ form_end(form) }}
", "FOSUserBundle:Group:new_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new_content.html.twig");
    }
}

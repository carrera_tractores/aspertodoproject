<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_463436263c810ccde11d46812a37df1640b50a507cae0fe2034b6d8b5855bc01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4cc722a4ad9c2646710cadf669e6619f0d28a241e9b708c1927e918e8900709 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4cc722a4ad9c2646710cadf669e6619f0d28a241e9b708c1927e918e8900709->enter($__internal_c4cc722a4ad9c2646710cadf669e6619f0d28a241e9b708c1927e918e8900709_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_820be0a4a8eff56ab1d1aaaf7b9875f9e4d98e72b6d440266cc4baf24a579de9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_820be0a4a8eff56ab1d1aaaf7b9875f9e4d98e72b6d440266cc4baf24a579de9->enter($__internal_820be0a4a8eff56ab1d1aaaf7b9875f9e4d98e72b6d440266cc4baf24a579de9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_c4cc722a4ad9c2646710cadf669e6619f0d28a241e9b708c1927e918e8900709->leave($__internal_c4cc722a4ad9c2646710cadf669e6619f0d28a241e9b708c1927e918e8900709_prof);

        
        $__internal_820be0a4a8eff56ab1d1aaaf7b9875f9e4d98e72b6d440266cc4baf24a579de9->leave($__internal_820be0a4a8eff56ab1d1aaaf7b9875f9e4d98e72b6d440266cc4baf24a579de9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}

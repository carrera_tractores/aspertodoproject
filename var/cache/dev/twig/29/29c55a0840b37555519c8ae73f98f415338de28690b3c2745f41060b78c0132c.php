<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_909c25f57be03255884cfff04258fe876044441358f3b76133d554fc486be0da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd6e784256aa70ba18d246951b49fb597f25581635199b976fbc68eb58019ed4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd6e784256aa70ba18d246951b49fb597f25581635199b976fbc68eb58019ed4->enter($__internal_bd6e784256aa70ba18d246951b49fb597f25581635199b976fbc68eb58019ed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request_content.html.twig"));

        $__internal_d8a9ea6f13e0405734197c30fcd3e75a555d142a852ae22f57ff26420805fdcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8a9ea6f13e0405734197c30fcd3e75a555d142a852ae22f57ff26420805fdcc->enter($__internal_d8a9ea6f13e0405734197c30fcd3e75a555d142a852ae22f57ff26420805fdcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request_content.html.twig"));

        // line 2
        echo "
<form action=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
    </div>
</form>
";
        
        $__internal_bd6e784256aa70ba18d246951b49fb597f25581635199b976fbc68eb58019ed4->leave($__internal_bd6e784256aa70ba18d246951b49fb597f25581635199b976fbc68eb58019ed4_prof);

        
        $__internal_d8a9ea6f13e0405734197c30fcd3e75a555d142a852ae22f57ff26420805fdcc->leave($__internal_d8a9ea6f13e0405734197c30fcd3e75a555d142a852ae22f57ff26420805fdcc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 9,  33 => 5,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<form action=\"{{ path('fos_user_resetting_send_email') }}\" method=\"POST\" class=\"fos_user_resetting_request\">
    <div>
        <label for=\"username\">{{ 'resetting.request.username'|trans }}</label>
        <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" />
    </div>
    <div>
        <input type=\"submit\" value=\"{{ 'resetting.request.submit'|trans }}\" />
    </div>
</form>
", "FOSUserBundle:Resetting:request_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request_content.html.twig");
    }
}

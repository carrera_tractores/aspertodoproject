<?php

/* EasyAdminBundle:default:field_guid.html.twig */
class __TwigTemplate_de04efc2bc854ec07e2d8bd006d90d670141500b3002f526c56cb2ff3601d0f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6ef8740d16e0744ad6223a2d99b07e44b4d235191a8abefc3674afe1e447f27 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6ef8740d16e0744ad6223a2d99b07e44b4d235191a8abefc3674afe1e447f27->enter($__internal_e6ef8740d16e0744ad6223a2d99b07e44b4d235191a8abefc3674afe1e447f27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_guid.html.twig"));

        $__internal_1a041a1291a66c217fdab1f64ace7a4bcbdf7d8792e24e790750c78f2f1ec9cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a041a1291a66c217fdab1f64ace7a4bcbdf7d8792e24e790750c78f2f1ec9cf->enter($__internal_1a041a1291a66c217fdab1f64ace7a4bcbdf7d8792e24e790750c78f2f1ec9cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_guid.html.twig"));

        // line 1
        if (((isset($context["view"]) || array_key_exists("view", $context) ? $context["view"] : (function () { throw new Twig_Error_Runtime('Variable "view" does not exist.', 1, $this->getSourceContext()); })()) == "show")) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })()), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })()), 7), "html", null, true);
            echo "
";
        }
        
        $__internal_e6ef8740d16e0744ad6223a2d99b07e44b4d235191a8abefc3674afe1e447f27->leave($__internal_e6ef8740d16e0744ad6223a2d99b07e44b4d235191a8abefc3674afe1e447f27_prof);

        
        $__internal_1a041a1291a66c217fdab1f64ace7a4bcbdf7d8792e24e790750c78f2f1ec9cf->leave($__internal_1a041a1291a66c217fdab1f64ace7a4bcbdf7d8792e24e790750c78f2f1ec9cf_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_guid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value }}
{% else %}
    {{ value|easyadmin_truncate(7) }}
{% endif %}
", "EasyAdminBundle:default:field_guid.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_guid.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_integer.html.twig */
class __TwigTemplate_66b1f5c07809cf19e704af8afe080559e60beb0d133682dc6e5a6445b1af5182 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c090aa27dd6dfce360a8db82a9096325984cfeaa4cf81a2d58c36668f86fae5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c090aa27dd6dfce360a8db82a9096325984cfeaa4cf81a2d58c36668f86fae5d->enter($__internal_c090aa27dd6dfce360a8db82a9096325984cfeaa4cf81a2d58c36668f86fae5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_integer.html.twig"));

        $__internal_14c12dcf72c6411f4a62c329d82eeeaac280a80c6f0cf3e469417bca81fc2bbd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14c12dcf72c6411f4a62c329d82eeeaac280a80c6f0cf3e469417bca81fc2bbd->enter($__internal_14c12dcf72c6411f4a62c329d82eeeaac280a80c6f0cf3e469417bca81fc2bbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_integer.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 2, $this->getSourceContext()); })()), "format", array()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        }
        
        $__internal_c090aa27dd6dfce360a8db82a9096325984cfeaa4cf81a2d58c36668f86fae5d->leave($__internal_c090aa27dd6dfce360a8db82a9096325984cfeaa4cf81a2d58c36668f86fae5d_prof);

        
        $__internal_14c12dcf72c6411f4a62c329d82eeeaac280a80c6f0cf3e469417bca81fc2bbd->leave($__internal_14c12dcf72c6411f4a62c329d82eeeaac280a80c6f0cf3e469417bca81fc2bbd_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_integer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_integer.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_integer.html.twig");
    }
}

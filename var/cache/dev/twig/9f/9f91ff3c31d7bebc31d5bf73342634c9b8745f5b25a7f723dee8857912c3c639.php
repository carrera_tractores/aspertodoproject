<?php

/* afegirtasca.html.twig~ */
class __TwigTemplate_18acc8d40d93609eb4f0d603fd04048d19502c0cccc9211cfc60ddd4d3b4f117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_241196d0145d656177cfeb940972d84572f6900d0821408a818e6df0a64d7e69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_241196d0145d656177cfeb940972d84572f6900d0821408a818e6df0a64d7e69->enter($__internal_241196d0145d656177cfeb940972d84572f6900d0821408a818e6df0a64d7e69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirtasca.html.twig~"));

        $__internal_2c74dc4403ec57f21c2f59c57f79b95428973a5fc506ff1fdc489009665c1355 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c74dc4403ec57f21c2f59c57f79b95428973a5fc506ff1fdc489009665c1355->enter($__internal_2c74dc4403ec57f21c2f59c57f79b95428973a5fc506ff1fdc489009665c1355_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirtasca.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />

        
    </head>
    <body>
    \t";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola ";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir tasca</h1>
      <hr>
      
\t\t<div id=\"formulari\">
\t\t\t<input type=\"hidden\" name=\"\" value=\"\" />

    \t\t<label for=\"Nom\">Nom</label>
    \t\t<input type=\"text\" id=\"nom\" name=\"nom\" required=\"required\" />

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        ";
        // line 67
        $this->displayBlock('javascripts', $context, $blocks);
        // line 72
        echo "    </body>
</html>
";
        
        $__internal_241196d0145d656177cfeb940972d84572f6900d0821408a818e6df0a64d7e69->leave($__internal_241196d0145d656177cfeb940972d84572f6900d0821408a818e6df0a64d7e69_prof);

        
        $__internal_2c74dc4403ec57f21c2f59c57f79b95428973a5fc506ff1fdc489009665c1355->leave($__internal_2c74dc4403ec57f21c2f59c57f79b95428973a5fc506ff1fdc489009665c1355_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_d7c7b4eb3a219121266e62332fc1a1b6ce55f363dad5e3f2387e948830095dec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7c7b4eb3a219121266e62332fc1a1b6ce55f363dad5e3f2387e948830095dec->enter($__internal_d7c7b4eb3a219121266e62332fc1a1b6ce55f363dad5e3f2387e948830095dec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_44ae757f1bb7c23698d72813eed4f516f2bf13e7988b6974191bbe77ce3cdb80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44ae757f1bb7c23698d72813eed4f516f2bf13e7988b6974191bbe77ce3cdb80->enter($__internal_44ae757f1bb7c23698d72813eed4f516f2bf13e7988b6974191bbe77ce3cdb80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Afegir tasca";
        
        $__internal_44ae757f1bb7c23698d72813eed4f516f2bf13e7988b6974191bbe77ce3cdb80->leave($__internal_44ae757f1bb7c23698d72813eed4f516f2bf13e7988b6974191bbe77ce3cdb80_prof);

        
        $__internal_d7c7b4eb3a219121266e62332fc1a1b6ce55f363dad5e3f2387e948830095dec->leave($__internal_d7c7b4eb3a219121266e62332fc1a1b6ce55f363dad5e3f2387e948830095dec_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c9976257ee9bf1e88718e0019fbb1f6cc3490e28804ecf8f40b17f66fd0e14b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9976257ee9bf1e88718e0019fbb1f6cc3490e28804ecf8f40b17f66fd0e14b5->enter($__internal_c9976257ee9bf1e88718e0019fbb1f6cc3490e28804ecf8f40b17f66fd0e14b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_4093d5a2f397aa9b19bde00d828c97626c79713f44cab441f97fb20372fd7759 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4093d5a2f397aa9b19bde00d828c97626c79713f44cab441f97fb20372fd7759->enter($__internal_4093d5a2f397aa9b19bde00d828c97626c79713f44cab441f97fb20372fd7759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_4093d5a2f397aa9b19bde00d828c97626c79713f44cab441f97fb20372fd7759->leave($__internal_4093d5a2f397aa9b19bde00d828c97626c79713f44cab441f97fb20372fd7759_prof);

        
        $__internal_c9976257ee9bf1e88718e0019fbb1f6cc3490e28804ecf8f40b17f66fd0e14b5->leave($__internal_c9976257ee9bf1e88718e0019fbb1f6cc3490e28804ecf8f40b17f66fd0e14b5_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_4251d5f0ef7b2909c194d8e4760e2699a933be562fffa89148502153ece7a2eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4251d5f0ef7b2909c194d8e4760e2699a933be562fffa89148502153ece7a2eb->enter($__internal_4251d5f0ef7b2909c194d8e4760e2699a933be562fffa89148502153ece7a2eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b52fc002056372caf6eed5c70d20266a938605c405360ed6c9751a8868ef5274 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b52fc002056372caf6eed5c70d20266a938605c405360ed6c9751a8868ef5274->enter($__internal_b52fc002056372caf6eed5c70d20266a938605c405360ed6c9751a8868ef5274_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b52fc002056372caf6eed5c70d20266a938605c405360ed6c9751a8868ef5274->leave($__internal_b52fc002056372caf6eed5c70d20266a938605c405360ed6c9751a8868ef5274_prof);

        
        $__internal_4251d5f0ef7b2909c194d8e4760e2699a933be562fffa89148502153ece7a2eb->leave($__internal_4251d5f0ef7b2909c194d8e4760e2699a933be562fffa89148502153ece7a2eb_prof);

    }

    // line 67
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d3d1e8d59760fa69c8168420160e1df5d25a87983e71beb30f2533609874465c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3d1e8d59760fa69c8168420160e1df5d25a87983e71beb30f2533609874465c->enter($__internal_d3d1e8d59760fa69c8168420160e1df5d25a87983e71beb30f2533609874465c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_30c735d9d9502c8581ce126d135a08c53c3c2f007a1b1e086d5fc7dbb37e1d94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30c735d9d9502c8581ce126d135a08c53c3c2f007a1b1e086d5fc7dbb37e1d94->enter($__internal_30c735d9d9502c8581ce126d135a08c53c3c2f007a1b1e086d5fc7dbb37e1d94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 68
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_30c735d9d9502c8581ce126d135a08c53c3c2f007a1b1e086d5fc7dbb37e1d94->leave($__internal_30c735d9d9502c8581ce126d135a08c53c3c2f007a1b1e086d5fc7dbb37e1d94_prof);

        
        $__internal_d3d1e8d59760fa69c8168420160e1df5d25a87983e71beb30f2533609874465c->leave($__internal_d3d1e8d59760fa69c8168420160e1df5d25a87983e71beb30f2533609874465c_prof);

    }

    public function getTemplateName()
    {
        return "afegirtasca.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 69,  195 => 68,  186 => 67,  169 => 15,  152 => 6,  134 => 5,  122 => 72,  120 => 67,  70 => 20,  66 => 19,  61 => 16,  59 => 15,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Afegir tasca{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />

        
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"{{ path('fos_user_security_logout') }}\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola {{ app.user.username }}!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir tasca</h1>
      <hr>
      
\t\t<div id=\"formulari\">
\t\t\t<input type=\"hidden\" name=\"\" value=\"\" />

    \t\t<label for=\"Nom\">Nom</label>
    \t\t<input type=\"text\" id=\"nom\" name=\"nom\" required=\"required\" />

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>

        {% endblock %}
    </body>
</html>
", "afegirtasca.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/afegirtasca.html.twig~");
    }
}

<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_7c685f5ebf1d334682ab3b697792e97200a944d5e71b8de1d44ab0df841bd4af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e61022b0e2502621a93fbf456a01c3f0b466c6c52dc4650fdad0b7d33fde0dba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e61022b0e2502621a93fbf456a01c3f0b466c6c52dc4650fdad0b7d33fde0dba->enter($__internal_e61022b0e2502621a93fbf456a01c3f0b466c6c52dc4650fdad0b7d33fde0dba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_b55db8f9c3dd87267b28093f6d99b4fc8e36d07893c8f37bf6a9ba533cd75157 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b55db8f9c3dd87267b28093f6d99b4fc8e36d07893c8f37bf6a9ba533cd75157->enter($__internal_b55db8f9c3dd87267b28093f6d99b4fc8e36d07893c8f37bf6a9ba533cd75157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_e61022b0e2502621a93fbf456a01c3f0b466c6c52dc4650fdad0b7d33fde0dba->leave($__internal_e61022b0e2502621a93fbf456a01c3f0b466c6c52dc4650fdad0b7d33fde0dba_prof);

        
        $__internal_b55db8f9c3dd87267b28093f6d99b4fc8e36d07893c8f37bf6a9ba533cd75157->leave($__internal_b55db8f9c3dd87267b28093f6d99b4fc8e36d07893c8f37bf6a9ba533cd75157_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}

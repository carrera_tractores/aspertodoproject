<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_939d4e5378746e7a86f60e340a30f69f24ec7e0d012551458a4e2033cc67c5d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_046f16a6c06eef4fd752fc3b23c1f8d88c5dd7f2a79da1588c307f74ec8f6a72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_046f16a6c06eef4fd752fc3b23c1f8d88c5dd7f2a79da1588c307f74ec8f6a72->enter($__internal_046f16a6c06eef4fd752fc3b23c1f8d88c5dd7f2a79da1588c307f74ec8f6a72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_d36dc970cde4c6785a4cf8d52e0db9b636638bad4a1749495025ca0df15e4e6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d36dc970cde4c6785a4cf8d52e0db9b636638bad4a1749495025ca0df15e4e6c->enter($__internal_d36dc970cde4c6785a4cf8d52e0db9b636638bad4a1749495025ca0df15e4e6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_046f16a6c06eef4fd752fc3b23c1f8d88c5dd7f2a79da1588c307f74ec8f6a72->leave($__internal_046f16a6c06eef4fd752fc3b23c1f8d88c5dd7f2a79da1588c307f74ec8f6a72_prof);

        
        $__internal_d36dc970cde4c6785a4cf8d52e0db9b636638bad4a1749495025ca0df15e4e6c->leave($__internal_d36dc970cde4c6785a4cf8d52e0db9b636638bad4a1749495025ca0df15e4e6c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}

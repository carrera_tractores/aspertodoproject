<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_ca093947318a117850c9a707bf3d5eba789c12f07b46e8ab7919272417a8785e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf3e415ad29486623772558dcb02af9867c193a7d67c8a05fbee29aee7ea68a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf3e415ad29486623772558dcb02af9867c193a7d67c8a05fbee29aee7ea68a6->enter($__internal_cf3e415ad29486623772558dcb02af9867c193a7d67c8a05fbee29aee7ea68a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_73effbfab449b7122017a795c4f466d2b536cce9b1c91740120393a682f24ff1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73effbfab449b7122017a795c4f466d2b536cce9b1c91740120393a682f24ff1->enter($__internal_73effbfab449b7122017a795c4f466d2b536cce9b1c91740120393a682f24ff1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_cf3e415ad29486623772558dcb02af9867c193a7d67c8a05fbee29aee7ea68a6->leave($__internal_cf3e415ad29486623772558dcb02af9867c193a7d67c8a05fbee29aee7ea68a6_prof);

        
        $__internal_73effbfab449b7122017a795c4f466d2b536cce9b1c91740120393a682f24ff1->leave($__internal_73effbfab449b7122017a795c4f466d2b536cce9b1c91740120393a682f24ff1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}

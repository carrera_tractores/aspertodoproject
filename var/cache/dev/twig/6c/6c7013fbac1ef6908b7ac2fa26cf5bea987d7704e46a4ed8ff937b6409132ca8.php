<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_17cd172386597bb44eabf5054f7ddf0545f18d80e4b8cae65278430e6b6aa366 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6c8bb22624c750b29a287d081bba73604f79b9fb582a895cb5775d8e72b424b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6c8bb22624c750b29a287d081bba73604f79b9fb582a895cb5775d8e72b424b->enter($__internal_d6c8bb22624c750b29a287d081bba73604f79b9fb582a895cb5775d8e72b424b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_3deea73bbdcae79c2fc515a574114d1e227864ec0d8c6b11ff5bfc8fe37fc000 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3deea73bbdcae79c2fc515a574114d1e227864ec0d8c6b11ff5bfc8fe37fc000->enter($__internal_3deea73bbdcae79c2fc515a574114d1e227864ec0d8c6b11ff5bfc8fe37fc000_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 1
        echo "<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
    \t<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
\t</head>

\t<body>

\t\t";
        // line 11
        echo "
\t\t";
        // line 12
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })())) {
            // line 13
            echo "    \t<div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 13, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 13, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
\t\t";
        }
        // line 15
        echo "
\t\t\t<form action=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    \t\t";
        // line 17
        if ((isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 17, $this->getSourceContext()); })())) {
            // line 18
            echo "
        \t\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"";
            // line 19
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 19, $this->getSourceContext()); })()), "html", null, true);
            echo "\" />
    \t\t";
        }
        // line 21
        echo "
    \t\t\t\t<label for=\"username\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    \t\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 23, $this->getSourceContext()); })()), "html", null, true);
        echo "\" required=\"required\" />

    \t\t\t\t<label for=\"password\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    \t\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

\t\t\t\t\t<label for=\"remember_me\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>    \t\t\t
    \t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    \t\t
   \t\t<div id=\"caixa\">
   \t\t\t<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
   \t\t\t<a  id=\"registre\" href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
        echo "</a>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t\t
\t\t";
        // line 38
        $this->displayBlock('javascripts', $context, $blocks);
        // line 42
        echo "    
\t</body>

</html>
";
        
        $__internal_d6c8bb22624c750b29a287d081bba73604f79b9fb582a895cb5775d8e72b424b->leave($__internal_d6c8bb22624c750b29a287d081bba73604f79b9fb582a895cb5775d8e72b424b_prof);

        
        $__internal_3deea73bbdcae79c2fc515a574114d1e227864ec0d8c6b11ff5bfc8fe37fc000->leave($__internal_3deea73bbdcae79c2fc515a574114d1e227864ec0d8c6b11ff5bfc8fe37fc000_prof);

    }

    // line 38
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_96a48206403e694af1468979c7ab6dfaf4e366ab8052d332edddcb7fedcfc6e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96a48206403e694af1468979c7ab6dfaf4e366ab8052d332edddcb7fedcfc6e4->enter($__internal_96a48206403e694af1468979c7ab6dfaf4e366ab8052d332edddcb7fedcfc6e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_7cdad767d5c73b79d5e07c5b07164f6d7fd16376d4ee1745ff1ad008a1dcfdc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7cdad767d5c73b79d5e07c5b07164f6d7fd16376d4ee1745ff1ad008a1dcfdc1->enter($__internal_7cdad767d5c73b79d5e07c5b07164f6d7fd16376d4ee1745ff1ad008a1dcfdc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 39
        echo "      \t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" />
        \t<script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\" />
    \t";
        
        $__internal_7cdad767d5c73b79d5e07c5b07164f6d7fd16376d4ee1745ff1ad008a1dcfdc1->leave($__internal_7cdad767d5c73b79d5e07c5b07164f6d7fd16376d4ee1745ff1ad008a1dcfdc1_prof);

        
        $__internal_96a48206403e694af1468979c7ab6dfaf4e366ab8052d332edddcb7fedcfc6e4->leave($__internal_96a48206403e694af1468979c7ab6dfaf4e366ab8052d332edddcb7fedcfc6e4_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 40,  135 => 39,  126 => 38,  112 => 42,  110 => 38,  100 => 33,  96 => 32,  89 => 28,  83 => 25,  78 => 23,  74 => 22,  71 => 21,  66 => 19,  63 => 18,  61 => 17,  57 => 16,  54 => 15,  48 => 13,  46 => 12,  43 => 11,  35 => 5,  31 => 4,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
    \t<link rel=\"stylesheet\" href=\"{{ asset('css/login.css') }}\" />
\t</head>

\t<body>

\t\t{% trans_default_domain 'FOSUserBundle' %}

\t\t{% if error %}
    \t<div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
\t\t{% endif %}

\t\t\t<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
    \t\t{% if csrf_token %}

        \t\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    \t\t{% endif %}

    \t\t\t\t<label for=\"username\">{{ 'security.login.username'|trans }}</label>
    \t\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />

    \t\t\t\t<label for=\"password\">{{ 'security.login.password'|trans }}</label>
    \t\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

\t\t\t\t\t<label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>    \t\t\t
    \t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    \t\t
   \t\t<div id=\"caixa\">
   \t\t\t<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" />
   \t\t\t<a  id=\"registre\" href=\"{{ path('fos_user_registration_register') }}\">{{ 'layout.register'|trans({}, 'FOSUserBundle') }}</a>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t\t
\t\t{% block javascripts %}
      \t<script src=\"{{ asset('js/bootstrap.min.js') }}\" />
        \t<script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\" />
    \t{% endblock %}
    
\t</body>

</html>
", "@FOSUser/Security/login_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login_content.html.twig");
    }
}

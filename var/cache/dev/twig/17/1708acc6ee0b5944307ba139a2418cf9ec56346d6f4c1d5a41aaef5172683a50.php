<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_f290ca6ed9490fbea71bc03d9de57381638e75d10076cbe57c5495180f38ec08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cccae70b3d889b2b999cdaa7b0492de96f8fba73874b2ef7ba549cf77e652b04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cccae70b3d889b2b999cdaa7b0492de96f8fba73874b2ef7ba549cf77e652b04->enter($__internal_cccae70b3d889b2b999cdaa7b0492de96f8fba73874b2ef7ba549cf77e652b04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_4343bc78953817e7af71dd3d3c4823ef7586e5c30a6378b6a29ff43b10867a54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4343bc78953817e7af71dd3d3c4823ef7586e5c30a6378b6a29ff43b10867a54->enter($__internal_4343bc78953817e7af71dd3d3c4823ef7586e5c30a6378b6a29ff43b10867a54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_cccae70b3d889b2b999cdaa7b0492de96f8fba73874b2ef7ba549cf77e652b04->leave($__internal_cccae70b3d889b2b999cdaa7b0492de96f8fba73874b2ef7ba549cf77e652b04_prof);

        
        $__internal_4343bc78953817e7af71dd3d3c4823ef7586e5c30a6378b6a29ff43b10867a54->leave($__internal_4343bc78953817e7af71dd3d3c4823ef7586e5c30a6378b6a29ff43b10867a54_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}

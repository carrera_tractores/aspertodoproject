<?php

/* layout.html.twig */
class __TwigTemplate_ee996aefe05c0a0e0b8d1f6ba863abbad2f6d315e267124638677e966a2fe473 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'head_favicon' => array($this, 'block_head_favicon'),
            'head_javascript' => array($this, 'block_head_javascript'),
            'adminlte_options' => array($this, 'block_adminlte_options'),
            'body' => array($this, 'block_body'),
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'wrapper' => array($this, 'block_wrapper'),
            'header' => array($this, 'block_header'),
            'header_logo' => array($this, 'block_header_logo'),
            'header_custom_menu' => array($this, 'block_header_custom_menu'),
            'user_menu' => array($this, 'block_user_menu'),
            'user_menu_dropdown' => array($this, 'block_user_menu_dropdown'),
            'sidebar' => array($this, 'block_sidebar'),
            'main_menu_wrapper' => array($this, 'block_main_menu_wrapper'),
            'content' => array($this, 'block_content'),
            'flash_messages' => array($this, 'block_flash_messages'),
            'content_header' => array($this, 'block_content_header'),
            'content_title' => array($this, 'block_content_title'),
            'content_help' => array($this, 'block_content_help'),
            'main' => array($this, 'block_main'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffae9f130e3e12de7965cf61ca06b8c60b06cf0080a795b3e2444a6eae58d608 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffae9f130e3e12de7965cf61ca06b8c60b06cf0080a795b3e2444a6eae58d608->enter($__internal_ffae9f130e3e12de7965cf61ca06b8c60b06cf0080a795b3e2444a6eae58d608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layout.html.twig"));

        $__internal_ae3037b7b3e2ebcb67e262e02d92f7c6ac5f6e2aaa48830e99eaac74b79c5c91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae3037b7b3e2ebcb67e262e02d92f7c6ac5f6e2aaa48830e99eaac74b79c5c91->enter($__internal_ae3037b7b3e2ebcb67e262e02d92f7c6ac5f6e2aaa48830e99eaac74b79c5c91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, _twig_default_filter(twig_first($this->env, twig_split_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 2, $this->getSourceContext()); })()), "request", array()), "locale", array()), "_")), "en"), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>";
        // line 10
        $this->displayBlock('page_title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.css"));
        foreach ($context['_seq'] as $context["_key"] => $context["css_asset"]) {
            // line 20
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["css_asset"]), "html", null, true);
            echo "\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['css_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
        ";
        // line 23
        $this->displayBlock('head_favicon', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        $this->displayBlock('head_javascript', $context, $blocks);
        // line 45
        echo "
        ";
        // line 46
        if ($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.rtl")) {
            // line 47
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/bootstrap-rtl.min.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/adminlte-rtl.min.css"), "html", null, true);
            echo "\">
        ";
        }
        // line 50
        echo "
        <!--[if lt IE 9]>
            <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/html5shiv.min.css"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/respond.min.css"), "html", null, true);
        echo "\"></script>
        <![endif]-->
    </head>

    ";
        // line 57
        $this->displayBlock('body', $context, $blocks);
        // line 167
        echo "</html>
";
        
        $__internal_ffae9f130e3e12de7965cf61ca06b8c60b06cf0080a795b3e2444a6eae58d608->leave($__internal_ffae9f130e3e12de7965cf61ca06b8c60b06cf0080a795b3e2444a6eae58d608_prof);

        
        $__internal_ae3037b7b3e2ebcb67e262e02d92f7c6ac5f6e2aaa48830e99eaac74b79c5c91->leave($__internal_ae3037b7b3e2ebcb67e262e02d92f7c6ac5f6e2aaa48830e99eaac74b79c5c91_prof);

    }

    // line 10
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_1e43522537b62278143ca49afe4221c848fb285f76002e0ab2d9135cda3760df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e43522537b62278143ca49afe4221c848fb285f76002e0ab2d9135cda3760df->enter($__internal_1e43522537b62278143ca49afe4221c848fb285f76002e0ab2d9135cda3760df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_57d13945e4738af3fabaa6acbfe938a1107be4097480c4e34e1f02a0a8e066e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57d13945e4738af3fabaa6acbfe938a1107be4097480c4e34e1f02a0a8e066e7->enter($__internal_57d13945e4738af3fabaa6acbfe938a1107be4097480c4e34e1f02a0a8e066e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo strip_tags(        $this->renderBlock("content_title", $context, $blocks));
        
        $__internal_57d13945e4738af3fabaa6acbfe938a1107be4097480c4e34e1f02a0a8e066e7->leave($__internal_57d13945e4738af3fabaa6acbfe938a1107be4097480c4e34e1f02a0a8e066e7_prof);

        
        $__internal_1e43522537b62278143ca49afe4221c848fb285f76002e0ab2d9135cda3760df->leave($__internal_1e43522537b62278143ca49afe4221c848fb285f76002e0ab2d9135cda3760df_prof);

    }

    // line 12
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_9205be0e7f0a00f0bfc7db06af7fd7526bc0495a7af0c9843ae29b191c1165b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9205be0e7f0a00f0bfc7db06af7fd7526bc0495a7af0c9843ae29b191c1165b1->enter($__internal_9205be0e7f0a00f0bfc7db06af7fd7526bc0495a7af0c9843ae29b191c1165b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_d6d2152d526e8ddb2b071665de260b301646d42db743417a7d258565c028a84e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6d2152d526e8ddb2b071665de260b301646d42db743417a7d258565c028a84e->enter($__internal_d6d2152d526e8ddb2b071665de260b301646d42db743417a7d258565c028a84e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 13
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/easyadmin-all.min.css"), "html", null, true);
        echo "\">
            <style>
                ";
        // line 15
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("_internal.custom_css");
        echo "
            </style>
        ";
        
        $__internal_d6d2152d526e8ddb2b071665de260b301646d42db743417a7d258565c028a84e->leave($__internal_d6d2152d526e8ddb2b071665de260b301646d42db743417a7d258565c028a84e_prof);

        
        $__internal_9205be0e7f0a00f0bfc7db06af7fd7526bc0495a7af0c9843ae29b191c1165b1->leave($__internal_9205be0e7f0a00f0bfc7db06af7fd7526bc0495a7af0c9843ae29b191c1165b1_prof);

    }

    // line 23
    public function block_head_favicon($context, array $blocks = array())
    {
        $__internal_bc322bb606919400d2ce3b42b2816874f12eeece0e941972491e2208973c7060 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc322bb606919400d2ce3b42b2816874f12eeece0e941972491e2208973c7060->enter($__internal_bc322bb606919400d2ce3b42b2816874f12eeece0e941972491e2208973c7060_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        $__internal_f8c5d4ef15e534c3dd0842015be958c75b0d754016ed7002fe79abe91d8d4b9e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8c5d4ef15e534c3dd0842015be958c75b0d754016ed7002fe79abe91d8d4b9e->enter($__internal_f8c5d4ef15e534c3dd0842015be958c75b0d754016ed7002fe79abe91d8d4b9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        // line 24
        echo "            ";
        $context["favicon"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.favicon");
        // line 25
        echo "            <link rel=\"icon\" type=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["favicon"]) || array_key_exists("favicon", $context) ? $context["favicon"] : (function () { throw new Twig_Error_Runtime('Variable "favicon" does not exist.', 25, $this->getSourceContext()); })()), "mime_type", array()), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["favicon"]) || array_key_exists("favicon", $context) ? $context["favicon"] : (function () { throw new Twig_Error_Runtime('Variable "favicon" does not exist.', 25, $this->getSourceContext()); })()), "path", array())), "html", null, true);
        echo "\" />
        ";
        
        $__internal_f8c5d4ef15e534c3dd0842015be958c75b0d754016ed7002fe79abe91d8d4b9e->leave($__internal_f8c5d4ef15e534c3dd0842015be958c75b0d754016ed7002fe79abe91d8d4b9e_prof);

        
        $__internal_bc322bb606919400d2ce3b42b2816874f12eeece0e941972491e2208973c7060->leave($__internal_bc322bb606919400d2ce3b42b2816874f12eeece0e941972491e2208973c7060_prof);

    }

    // line 28
    public function block_head_javascript($context, array $blocks = array())
    {
        $__internal_212015e17530abbd7c861e4b03d400cbd279bd4e5abf776f9ef76d5744600a06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_212015e17530abbd7c861e4b03d400cbd279bd4e5abf776f9ef76d5744600a06->enter($__internal_212015e17530abbd7c861e4b03d400cbd279bd4e5abf776f9ef76d5744600a06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        $__internal_d6ea521b25d4d84dd665cdf9b917a984e37b386e022926d70bc92b6f41eff07e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6ea521b25d4d84dd665cdf9b917a984e37b386e022926d70bc92b6f41eff07e->enter($__internal_d6ea521b25d4d84dd665cdf9b917a984e37b386e022926d70bc92b6f41eff07e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        // line 29
        echo "            ";
        $this->displayBlock('adminlte_options', $context, $blocks);
        // line 42
        echo "
            <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/javascript/easyadmin-all.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_d6ea521b25d4d84dd665cdf9b917a984e37b386e022926d70bc92b6f41eff07e->leave($__internal_d6ea521b25d4d84dd665cdf9b917a984e37b386e022926d70bc92b6f41eff07e_prof);

        
        $__internal_212015e17530abbd7c861e4b03d400cbd279bd4e5abf776f9ef76d5744600a06->leave($__internal_212015e17530abbd7c861e4b03d400cbd279bd4e5abf776f9ef76d5744600a06_prof);

    }

    // line 29
    public function block_adminlte_options($context, array $blocks = array())
    {
        $__internal_90d1607c1fe02d9fb665749eaf9eb024603dff79758b1693a076997b9f123b5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90d1607c1fe02d9fb665749eaf9eb024603dff79758b1693a076997b9f123b5e->enter($__internal_90d1607c1fe02d9fb665749eaf9eb024603dff79758b1693a076997b9f123b5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        $__internal_b222e1dfcb493c02b0a5405bb111d34f010ab1fc9c3ab1ea1360c0a7d3143cc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b222e1dfcb493c02b0a5405bb111d34f010ab1fc9c3ab1ea1360c0a7d3143cc4->enter($__internal_b222e1dfcb493c02b0a5405bb111d34f010ab1fc9c3ab1ea1360c0a7d3143cc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        // line 30
        echo "                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            ";
        
        $__internal_b222e1dfcb493c02b0a5405bb111d34f010ab1fc9c3ab1ea1360c0a7d3143cc4->leave($__internal_b222e1dfcb493c02b0a5405bb111d34f010ab1fc9c3ab1ea1360c0a7d3143cc4_prof);

        
        $__internal_90d1607c1fe02d9fb665749eaf9eb024603dff79758b1693a076997b9f123b5e->leave($__internal_90d1607c1fe02d9fb665749eaf9eb024603dff79758b1693a076997b9f123b5e_prof);

    }

    // line 57
    public function block_body($context, array $blocks = array())
    {
        $__internal_95d21c68a19424101f2d1a0974bf9c445673dd70b4b63286e710faf157160be2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95d21c68a19424101f2d1a0974bf9c445673dd70b4b63286e710faf157160be2->enter($__internal_95d21c68a19424101f2d1a0974bf9c445673dd70b4b63286e710faf157160be2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7e6ebf0f470bc8bcdb58340444ff3e23206227796c64d7d21d1bd07065c9d485 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e6ebf0f470bc8bcdb58340444ff3e23206227796c64d7d21d1bd07065c9d485->enter($__internal_7e6ebf0f470bc8bcdb58340444ff3e23206227796c64d7d21d1bd07065c9d485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 58
        echo "    <body id=\"";
        $this->displayBlock('body_id', $context, $blocks);
        echo "\" class=\"easyadmin sidebar-mini ";
        $this->displayBlock('body_class', $context, $blocks);
        echo " ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 58, $this->getSourceContext()); })()), "request", array()), "cookies", array()), "has", array(0 => "_easyadmin_navigation_iscollapsed"), "method")) ? ("sidebar-collapse") : (""));
        echo "\">
        <div class=\"wrapper\">
        ";
        // line 60
        $this->displayBlock('wrapper', $context, $blocks);
        // line 158
        echo "        </div>

        ";
        // line 160
        $this->displayBlock('body_javascript', $context, $blocks);
        // line 161
        echo "
        ";
        // line 162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.js"));
        foreach ($context['_seq'] as $context["_key"] => $context["js_asset"]) {
            // line 163
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["js_asset"]), "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 165
        echo "    </body>
    ";
        
        $__internal_7e6ebf0f470bc8bcdb58340444ff3e23206227796c64d7d21d1bd07065c9d485->leave($__internal_7e6ebf0f470bc8bcdb58340444ff3e23206227796c64d7d21d1bd07065c9d485_prof);

        
        $__internal_95d21c68a19424101f2d1a0974bf9c445673dd70b4b63286e710faf157160be2->leave($__internal_95d21c68a19424101f2d1a0974bf9c445673dd70b4b63286e710faf157160be2_prof);

    }

    // line 58
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_db28467a4b7334af9ca91892852be2e42803422745f05753a2332cd24a71f57e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db28467a4b7334af9ca91892852be2e42803422745f05753a2332cd24a71f57e->enter($__internal_db28467a4b7334af9ca91892852be2e42803422745f05753a2332cd24a71f57e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_aafe92fd48f77fe1eeb1226b2726c735dc6aa39b64b596cb82d8eabdd0e309aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aafe92fd48f77fe1eeb1226b2726c735dc6aa39b64b596cb82d8eabdd0e309aa->enter($__internal_aafe92fd48f77fe1eeb1226b2726c735dc6aa39b64b596cb82d8eabdd0e309aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_aafe92fd48f77fe1eeb1226b2726c735dc6aa39b64b596cb82d8eabdd0e309aa->leave($__internal_aafe92fd48f77fe1eeb1226b2726c735dc6aa39b64b596cb82d8eabdd0e309aa_prof);

        
        $__internal_db28467a4b7334af9ca91892852be2e42803422745f05753a2332cd24a71f57e->leave($__internal_db28467a4b7334af9ca91892852be2e42803422745f05753a2332cd24a71f57e_prof);

    }

    public function block_body_class($context, array $blocks = array())
    {
        $__internal_0563b6d97530cb52b561209a936700a0cf99d5650dc29e46d4499faa3687c534 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0563b6d97530cb52b561209a936700a0cf99d5650dc29e46d4499faa3687c534->enter($__internal_0563b6d97530cb52b561209a936700a0cf99d5650dc29e46d4499faa3687c534_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_e30e586f892f801b3a6a4442fd6e0e38b876810b9ea259c821666e7a9948d475 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e30e586f892f801b3a6a4442fd6e0e38b876810b9ea259c821666e7a9948d475->enter($__internal_e30e586f892f801b3a6a4442fd6e0e38b876810b9ea259c821666e7a9948d475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        
        $__internal_e30e586f892f801b3a6a4442fd6e0e38b876810b9ea259c821666e7a9948d475->leave($__internal_e30e586f892f801b3a6a4442fd6e0e38b876810b9ea259c821666e7a9948d475_prof);

        
        $__internal_0563b6d97530cb52b561209a936700a0cf99d5650dc29e46d4499faa3687c534->leave($__internal_0563b6d97530cb52b561209a936700a0cf99d5650dc29e46d4499faa3687c534_prof);

    }

    // line 60
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_7f4e9ff1d5076de27b7b106a03165f35165d293994f14407d5199a47537991a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f4e9ff1d5076de27b7b106a03165f35165d293994f14407d5199a47537991a6->enter($__internal_7f4e9ff1d5076de27b7b106a03165f35165d293994f14407d5199a47537991a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        $__internal_c61f69a35291bc062221f3a0625f38764e7a3c3508a6e45a9d837621b11722a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c61f69a35291bc062221f3a0625f38764e7a3c3508a6e45a9d837621b11722a8->enter($__internal_c61f69a35291bc062221f3a0625f38764e7a3c3508a6e45a9d837621b11722a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 61
        echo "            <header class=\"main-header\">
            ";
        // line 62
        $this->displayBlock('header', $context, $blocks);
        // line 115
        echo "            </header>

            <aside class=\"main-sidebar\">
            ";
        // line 118
        $this->displayBlock('sidebar', $context, $blocks);
        // line 129
        echo "            </aside>

            <div class=\"content-wrapper\">
            ";
        // line 132
        $this->displayBlock('content', $context, $blocks);
        // line 156
        echo "            </div>
        ";
        
        $__internal_c61f69a35291bc062221f3a0625f38764e7a3c3508a6e45a9d837621b11722a8->leave($__internal_c61f69a35291bc062221f3a0625f38764e7a3c3508a6e45a9d837621b11722a8_prof);

        
        $__internal_7f4e9ff1d5076de27b7b106a03165f35165d293994f14407d5199a47537991a6->leave($__internal_7f4e9ff1d5076de27b7b106a03165f35165d293994f14407d5199a47537991a6_prof);

    }

    // line 62
    public function block_header($context, array $blocks = array())
    {
        $__internal_6daf4fabb3e3b95c663a5676cf62de8dd02fa620f284cf54f2937180fec971b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6daf4fabb3e3b95c663a5676cf62de8dd02fa620f284cf54f2937180fec971b9->enter($__internal_6daf4fabb3e3b95c663a5676cf62de8dd02fa620f284cf54f2937180fec971b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_0a252b3d3b95acd1d74f8990e28b08ff8c0f324ddde440004b1a505e461a70db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a252b3d3b95acd1d74f8990e28b08ff8c0f324ddde440004b1a505e461a70db->enter($__internal_0a252b3d3b95acd1d74f8990e28b08ff8c0f324ddde440004b1a505e461a70db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 63
        echo "                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("toggle_navigation", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
                    </a>

                    <div id=\"header-logo\">
                        ";
        // line 69
        $this->displayBlock('header_logo', $context, $blocks);
        // line 74
        echo "                    </div>

                    <div class=\"navbar-custom-menu\">
                    ";
        // line 77
        $this->displayBlock('header_custom_menu', $context, $blocks);
        // line 112
        echo "                    </div>
                </nav>
            ";
        
        $__internal_0a252b3d3b95acd1d74f8990e28b08ff8c0f324ddde440004b1a505e461a70db->leave($__internal_0a252b3d3b95acd1d74f8990e28b08ff8c0f324ddde440004b1a505e461a70db_prof);

        
        $__internal_6daf4fabb3e3b95c663a5676cf62de8dd02fa620f284cf54f2937180fec971b9->leave($__internal_6daf4fabb3e3b95c663a5676cf62de8dd02fa620f284cf54f2937180fec971b9_prof);

    }

    // line 69
    public function block_header_logo($context, array $blocks = array())
    {
        $__internal_0847b975d8c006f0e4bb650abce27c6a525829089bef9a060ca5bb2e59496b44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0847b975d8c006f0e4bb650abce27c6a525829089bef9a060ca5bb2e59496b44->enter($__internal_0847b975d8c006f0e4bb650abce27c6a525829089bef9a060ca5bb2e59496b44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        $__internal_433f686e0f6c398e1c13d0c760deb5a9d6293dc5d139a5e770f9442d9cecae7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_433f686e0f6c398e1c13d0c760deb5a9d6293dc5d139a5e770f9442d9cecae7d->enter($__internal_433f686e0f6c398e1c13d0c760deb5a9d6293dc5d139a5e770f9442d9cecae7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        // line 70
        echo "                            <a class=\"logo ";
        echo (((twig_length_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")) > 14)) ? ("logo-long") : (""));
        echo "\" title=\"";
        echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")), "html", null, true);
        echo "\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("easyadmin");
        echo "\">
                                ";
        // line 71
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name");
        echo "
                            </a>
                        ";
        
        $__internal_433f686e0f6c398e1c13d0c760deb5a9d6293dc5d139a5e770f9442d9cecae7d->leave($__internal_433f686e0f6c398e1c13d0c760deb5a9d6293dc5d139a5e770f9442d9cecae7d_prof);

        
        $__internal_0847b975d8c006f0e4bb650abce27c6a525829089bef9a060ca5bb2e59496b44->leave($__internal_0847b975d8c006f0e4bb650abce27c6a525829089bef9a060ca5bb2e59496b44_prof);

    }

    // line 77
    public function block_header_custom_menu($context, array $blocks = array())
    {
        $__internal_392572070f6fe2d2252921f6de63f31ab174cc3b4b21264ea5d3e83af65837c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_392572070f6fe2d2252921f6de63f31ab174cc3b4b21264ea5d3e83af65837c0->enter($__internal_392572070f6fe2d2252921f6de63f31ab174cc3b4b21264ea5d3e83af65837c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        $__internal_3696de6d325feb821152cab032062fa774c1912d9d374b5b67f171aeb74f5af1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3696de6d325feb821152cab032062fa774c1912d9d374b5b67f171aeb74f5af1->enter($__internal_3696de6d325feb821152cab032062fa774c1912d9d374b5b67f171aeb74f5af1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        // line 78
        echo "                        ";
        $context["_logout_path"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getLogoutPath();
        // line 79
        echo "                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                ";
        // line 81
        $this->displayBlock('user_menu', $context, $blocks);
        // line 109
        echo "                            </li>
                        </ul>
                    ";
        
        $__internal_3696de6d325feb821152cab032062fa774c1912d9d374b5b67f171aeb74f5af1->leave($__internal_3696de6d325feb821152cab032062fa774c1912d9d374b5b67f171aeb74f5af1_prof);

        
        $__internal_392572070f6fe2d2252921f6de63f31ab174cc3b4b21264ea5d3e83af65837c0->leave($__internal_392572070f6fe2d2252921f6de63f31ab174cc3b4b21264ea5d3e83af65837c0_prof);

    }

    // line 81
    public function block_user_menu($context, array $blocks = array())
    {
        $__internal_1f8760dfb2b7fe8e3f6e0eaa88983623b15b7c75ad58e403f62b0bccaa0817a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f8760dfb2b7fe8e3f6e0eaa88983623b15b7c75ad58e403f62b0bccaa0817a8->enter($__internal_1f8760dfb2b7fe8e3f6e0eaa88983623b15b7c75ad58e403f62b0bccaa0817a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        $__internal_a7927c437a1243ccfbbf4229e71a3a9781bb9cf505a896764dc9fdfbaf9dcc12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7927c437a1243ccfbbf4229e71a3a9781bb9cf505a896764dc9fdfbaf9dcc12->enter($__internal_a7927c437a1243ccfbbf4229e71a3a9781bb9cf505a896764dc9fdfbaf9dcc12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        // line 82
        echo "                                    <span class=\"sr-only\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.logged_in_as", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>

                                    ";
        // line 84
        if ((((twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array()), false)) : (false)) == false)) {
            // line 85
            echo "                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        ";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.anonymous", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                                    ";
        } elseif ( !        // line 87
(isset($context["_logout_path"]) || array_key_exists("_logout_path", $context) ? $context["_logout_path"] : (function () { throw new Twig_Error_Runtime('Variable "_logout_path" does not exist.', 87, $this->getSourceContext()); })())) {
            // line 88
            echo "                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        ";
            // line 89
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                    ";
        } else {
            // line 91
            echo "                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                ";
            // line 94
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                ";
            // line 100
            $this->displayBlock('user_menu_dropdown', $context, $blocks);
            // line 105
            echo "                                            </ul>
                                        </div>
                                    ";
        }
        // line 108
        echo "                                ";
        
        $__internal_a7927c437a1243ccfbbf4229e71a3a9781bb9cf505a896764dc9fdfbaf9dcc12->leave($__internal_a7927c437a1243ccfbbf4229e71a3a9781bb9cf505a896764dc9fdfbaf9dcc12_prof);

        
        $__internal_1f8760dfb2b7fe8e3f6e0eaa88983623b15b7c75ad58e403f62b0bccaa0817a8->leave($__internal_1f8760dfb2b7fe8e3f6e0eaa88983623b15b7c75ad58e403f62b0bccaa0817a8_prof);

    }

    // line 100
    public function block_user_menu_dropdown($context, array $blocks = array())
    {
        $__internal_9ecae6b859f6355fbc6181499ec722b48cd99fb607a169c0224797490fdbb3c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ecae6b859f6355fbc6181499ec722b48cd99fb607a169c0224797490fdbb3c6->enter($__internal_9ecae6b859f6355fbc6181499ec722b48cd99fb607a169c0224797490fdbb3c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        $__internal_0b99b1c7fde5f48c7d3282f2b2e831937fb09e106ec27bb457d3d923a3467936 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b99b1c7fde5f48c7d3282f2b2e831937fb09e106ec27bb457d3d923a3467936->enter($__internal_0b99b1c7fde5f48c7d3282f2b2e831937fb09e106ec27bb457d3d923a3467936_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        // line 101
        echo "                                                <li>
                                                    <a href=\"";
        // line 102
        echo twig_escape_filter($this->env, (isset($context["_logout_path"]) || array_key_exists("_logout_path", $context) ? $context["_logout_path"] : (function () { throw new Twig_Error_Runtime('Variable "_logout_path" does not exist.', 102, $this->getSourceContext()); })()), "html", null, true);
        echo "\"><i class=\"fa fa-sign-out\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.signout", array(), "EasyAdminBundle"), "html", null, true);
        echo "</a>
                                                </li>
                                                ";
        
        $__internal_0b99b1c7fde5f48c7d3282f2b2e831937fb09e106ec27bb457d3d923a3467936->leave($__internal_0b99b1c7fde5f48c7d3282f2b2e831937fb09e106ec27bb457d3d923a3467936_prof);

        
        $__internal_9ecae6b859f6355fbc6181499ec722b48cd99fb607a169c0224797490fdbb3c6->leave($__internal_9ecae6b859f6355fbc6181499ec722b48cd99fb607a169c0224797490fdbb3c6_prof);

    }

    // line 118
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_503fb3661c2590ab9180b70b0b193b64d28df4e983380864c3103e84b311dba1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_503fb3661c2590ab9180b70b0b193b64d28df4e983380864c3103e84b311dba1->enter($__internal_503fb3661c2590ab9180b70b0b193b64d28df4e983380864c3103e84b311dba1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_f6d7d76aefa1acb937ea52ec4f06bdc3fb52671f0536981786d0323de1fcdd58 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6d7d76aefa1acb937ea52ec4f06bdc3fb52671f0536981786d0323de1fcdd58->enter($__internal_f6d7d76aefa1acb937ea52ec4f06bdc3fb52671f0536981786d0323de1fcdd58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 119
        echo "                <section class=\"sidebar\">
                    ";
        // line 120
        $this->displayBlock('main_menu_wrapper', $context, $blocks);
        // line 127
        echo "                </section>
            ";
        
        $__internal_f6d7d76aefa1acb937ea52ec4f06bdc3fb52671f0536981786d0323de1fcdd58->leave($__internal_f6d7d76aefa1acb937ea52ec4f06bdc3fb52671f0536981786d0323de1fcdd58_prof);

        
        $__internal_503fb3661c2590ab9180b70b0b193b64d28df4e983380864c3103e84b311dba1->leave($__internal_503fb3661c2590ab9180b70b0b193b64d28df4e983380864c3103e84b311dba1_prof);

    }

    // line 120
    public function block_main_menu_wrapper($context, array $blocks = array())
    {
        $__internal_cea1ddc585c064013ad2273e90cf8d9bf2abad6db1f58376db45236bdee2b274 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cea1ddc585c064013ad2273e90cf8d9bf2abad6db1f58376db45236bdee2b274->enter($__internal_cea1ddc585c064013ad2273e90cf8d9bf2abad6db1f58376db45236bdee2b274_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        $__internal_f12ae78004f652e40fc0dcaad0275175c211525ed07b3fa1ac2819a73212c0ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f12ae78004f652e40fc0dcaad0275175c211525ed07b3fa1ac2819a73212c0ce->enter($__internal_f12ae78004f652e40fc0dcaad0275175c211525ed07b3fa1ac2819a73212c0ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        // line 121
        echo "                        ";
        echo twig_include($this->env, $context, array(0 => ((        // line 122
array_key_exists("_entity_config", $context)) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 122, $this->getSourceContext()); })()), "templates", array()), "menu", array())) : ("")), 1 => $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.templates.menu"), 2 => "@EasyAdmin/default/menu.html.twig"));
        // line 125
        echo "
                    ";
        
        $__internal_f12ae78004f652e40fc0dcaad0275175c211525ed07b3fa1ac2819a73212c0ce->leave($__internal_f12ae78004f652e40fc0dcaad0275175c211525ed07b3fa1ac2819a73212c0ce_prof);

        
        $__internal_cea1ddc585c064013ad2273e90cf8d9bf2abad6db1f58376db45236bdee2b274->leave($__internal_cea1ddc585c064013ad2273e90cf8d9bf2abad6db1f58376db45236bdee2b274_prof);

    }

    // line 132
    public function block_content($context, array $blocks = array())
    {
        $__internal_5f7221eb9fbc60d9d695f37333bfadaee33a30b1502f22668878a1e6dc34c9bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f7221eb9fbc60d9d695f37333bfadaee33a30b1502f22668878a1e6dc34c9bd->enter($__internal_5f7221eb9fbc60d9d695f37333bfadaee33a30b1502f22668878a1e6dc34c9bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_f7327cea9c50d7ff7135b8aef90311fefb27bd81f322f5046a58fb6c53238c86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7327cea9c50d7ff7135b8aef90311fefb27bd81f322f5046a58fb6c53238c86->enter($__internal_f7327cea9c50d7ff7135b8aef90311fefb27bd81f322f5046a58fb6c53238c86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 133
        echo "                ";
        $this->displayBlock('flash_messages', $context, $blocks);
        // line 136
        echo "
                <section class=\"content-header\">
                ";
        // line 138
        $this->displayBlock('content_header', $context, $blocks);
        // line 141
        echo "                ";
        $this->displayBlock('content_help', $context, $blocks);
        // line 150
        echo "                </section>

                <section id=\"main\" class=\"content\">
                    ";
        // line 153
        $this->displayBlock('main', $context, $blocks);
        // line 154
        echo "                </section>
            ";
        
        $__internal_f7327cea9c50d7ff7135b8aef90311fefb27bd81f322f5046a58fb6c53238c86->leave($__internal_f7327cea9c50d7ff7135b8aef90311fefb27bd81f322f5046a58fb6c53238c86_prof);

        
        $__internal_5f7221eb9fbc60d9d695f37333bfadaee33a30b1502f22668878a1e6dc34c9bd->leave($__internal_5f7221eb9fbc60d9d695f37333bfadaee33a30b1502f22668878a1e6dc34c9bd_prof);

    }

    // line 133
    public function block_flash_messages($context, array $blocks = array())
    {
        $__internal_b9c64b3f0cfa811039675b70b5c2c1c91a705c7322545fe0e935bba498351d56 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9c64b3f0cfa811039675b70b5c2c1c91a705c7322545fe0e935bba498351d56->enter($__internal_b9c64b3f0cfa811039675b70b5c2c1c91a705c7322545fe0e935bba498351d56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        $__internal_c44d79db0f2385c64af7b63a26fd17684996338fd5dd1353392f8f51ef47e194 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c44d79db0f2385c64af7b63a26fd17684996338fd5dd1353392f8f51ef47e194->enter($__internal_c44d79db0f2385c64af7b63a26fd17684996338fd5dd1353392f8f51ef47e194_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        // line 134
        echo "                    ";
        echo twig_include($this->env, $context, ((array_key_exists("_entity_config", $context)) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 134, $this->getSourceContext()); })()), "templates", array()), "flash_messages", array())) : ("@EasyAdmin/default/flash_messages.html.twig")));
        echo "
                ";
        
        $__internal_c44d79db0f2385c64af7b63a26fd17684996338fd5dd1353392f8f51ef47e194->leave($__internal_c44d79db0f2385c64af7b63a26fd17684996338fd5dd1353392f8f51ef47e194_prof);

        
        $__internal_b9c64b3f0cfa811039675b70b5c2c1c91a705c7322545fe0e935bba498351d56->leave($__internal_b9c64b3f0cfa811039675b70b5c2c1c91a705c7322545fe0e935bba498351d56_prof);

    }

    // line 138
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_ad63c85aa7836d03f61568bd2d0d65fe6f35f146e74a9dcab0e2b524d7838a0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad63c85aa7836d03f61568bd2d0d65fe6f35f146e74a9dcab0e2b524d7838a0a->enter($__internal_ad63c85aa7836d03f61568bd2d0d65fe6f35f146e74a9dcab0e2b524d7838a0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_c75610890bd40d588a96811fbad7f0331e32665964e49e8e91786b9db3e53306 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c75610890bd40d588a96811fbad7f0331e32665964e49e8e91786b9db3e53306->enter($__internal_c75610890bd40d588a96811fbad7f0331e32665964e49e8e91786b9db3e53306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 139
        echo "                    <h1 class=\"title\">";
        $this->displayBlock('content_title', $context, $blocks);
        echo "</h1>
                ";
        
        $__internal_c75610890bd40d588a96811fbad7f0331e32665964e49e8e91786b9db3e53306->leave($__internal_c75610890bd40d588a96811fbad7f0331e32665964e49e8e91786b9db3e53306_prof);

        
        $__internal_ad63c85aa7836d03f61568bd2d0d65fe6f35f146e74a9dcab0e2b524d7838a0a->leave($__internal_ad63c85aa7836d03f61568bd2d0d65fe6f35f146e74a9dcab0e2b524d7838a0a_prof);

    }

    public function block_content_title($context, array $blocks = array())
    {
        $__internal_6a13b9348c595a6c3fb50f4b01bffae75650b1fa14fd48b659da5a5b2a1eee0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a13b9348c595a6c3fb50f4b01bffae75650b1fa14fd48b659da5a5b2a1eee0e->enter($__internal_6a13b9348c595a6c3fb50f4b01bffae75650b1fa14fd48b659da5a5b2a1eee0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_a5d6a7578ad3f412517365a2f7d9942ec0d18d72dd729d803293c56621c3fe46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5d6a7578ad3f412517365a2f7d9942ec0d18d72dd729d803293c56621c3fe46->enter($__internal_a5d6a7578ad3f412517365a2f7d9942ec0d18d72dd729d803293c56621c3fe46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        
        $__internal_a5d6a7578ad3f412517365a2f7d9942ec0d18d72dd729d803293c56621c3fe46->leave($__internal_a5d6a7578ad3f412517365a2f7d9942ec0d18d72dd729d803293c56621c3fe46_prof);

        
        $__internal_6a13b9348c595a6c3fb50f4b01bffae75650b1fa14fd48b659da5a5b2a1eee0e->leave($__internal_6a13b9348c595a6c3fb50f4b01bffae75650b1fa14fd48b659da5a5b2a1eee0e_prof);

    }

    // line 141
    public function block_content_help($context, array $blocks = array())
    {
        $__internal_79d33ba0e85281666e50566fe5d0abbf52ef097472ed7affb694a3f5cb8e1142 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79d33ba0e85281666e50566fe5d0abbf52ef097472ed7affb694a3f5cb8e1142->enter($__internal_79d33ba0e85281666e50566fe5d0abbf52ef097472ed7affb694a3f5cb8e1142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        $__internal_24c1d55faae712794890e4f535cca39f58c4a8c4298e1810a6e865693c1ff7ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24c1d55faae712794890e4f535cca39f58c4a8c4298e1810a6e865693c1ff7ea->enter($__internal_24c1d55faae712794890e4f535cca39f58c4a8c4298e1810a6e865693c1ff7ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        // line 142
        echo "                    ";
        if ((array_key_exists("_entity_config", $context) && ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 142, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 142, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array"), false)) : (false)))) {
            // line 143
            echo "                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                ";
            // line 145
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 145, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 145, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array"), "help", array(), "array"));
            echo "
                            </div>
                        </div>
                    ";
        }
        // line 149
        echo "                ";
        
        $__internal_24c1d55faae712794890e4f535cca39f58c4a8c4298e1810a6e865693c1ff7ea->leave($__internal_24c1d55faae712794890e4f535cca39f58c4a8c4298e1810a6e865693c1ff7ea_prof);

        
        $__internal_79d33ba0e85281666e50566fe5d0abbf52ef097472ed7affb694a3f5cb8e1142->leave($__internal_79d33ba0e85281666e50566fe5d0abbf52ef097472ed7affb694a3f5cb8e1142_prof);

    }

    // line 153
    public function block_main($context, array $blocks = array())
    {
        $__internal_168565f026bbc72daadd55ccd48bb5414ffa6740d1729d262f0285c21ee46b26 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_168565f026bbc72daadd55ccd48bb5414ffa6740d1729d262f0285c21ee46b26->enter($__internal_168565f026bbc72daadd55ccd48bb5414ffa6740d1729d262f0285c21ee46b26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_ee860c6be4f866bfbdf47da2881e707ea97f808e6cc574b2756352b88816021c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee860c6be4f866bfbdf47da2881e707ea97f808e6cc574b2756352b88816021c->enter($__internal_ee860c6be4f866bfbdf47da2881e707ea97f808e6cc574b2756352b88816021c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_ee860c6be4f866bfbdf47da2881e707ea97f808e6cc574b2756352b88816021c->leave($__internal_ee860c6be4f866bfbdf47da2881e707ea97f808e6cc574b2756352b88816021c_prof);

        
        $__internal_168565f026bbc72daadd55ccd48bb5414ffa6740d1729d262f0285c21ee46b26->leave($__internal_168565f026bbc72daadd55ccd48bb5414ffa6740d1729d262f0285c21ee46b26_prof);

    }

    // line 160
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_0d66dc70c5f4ec4029db07064ae28374bdb4286e7e493d60f3e1c48982c5eed7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d66dc70c5f4ec4029db07064ae28374bdb4286e7e493d60f3e1c48982c5eed7->enter($__internal_0d66dc70c5f4ec4029db07064ae28374bdb4286e7e493d60f3e1c48982c5eed7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_7794142c18df0046d913a1374c5da23892511cda8f616929fed969d8329878ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7794142c18df0046d913a1374c5da23892511cda8f616929fed969d8329878ac->enter($__internal_7794142c18df0046d913a1374c5da23892511cda8f616929fed969d8329878ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        
        $__internal_7794142c18df0046d913a1374c5da23892511cda8f616929fed969d8329878ac->leave($__internal_7794142c18df0046d913a1374c5da23892511cda8f616929fed969d8329878ac_prof);

        
        $__internal_0d66dc70c5f4ec4029db07064ae28374bdb4286e7e493d60f3e1c48982c5eed7->leave($__internal_0d66dc70c5f4ec4029db07064ae28374bdb4286e7e493d60f3e1c48982c5eed7_prof);

    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  786 => 160,  769 => 153,  759 => 149,  752 => 145,  748 => 143,  745 => 142,  736 => 141,  707 => 139,  698 => 138,  685 => 134,  676 => 133,  665 => 154,  663 => 153,  658 => 150,  655 => 141,  653 => 138,  649 => 136,  646 => 133,  637 => 132,  626 => 125,  624 => 122,  622 => 121,  613 => 120,  602 => 127,  600 => 120,  597 => 119,  588 => 118,  573 => 102,  570 => 101,  561 => 100,  551 => 108,  546 => 105,  544 => 100,  535 => 94,  530 => 91,  525 => 89,  522 => 88,  520 => 87,  516 => 86,  513 => 85,  511 => 84,  505 => 82,  496 => 81,  484 => 109,  482 => 81,  478 => 79,  475 => 78,  466 => 77,  453 => 71,  444 => 70,  435 => 69,  423 => 112,  421 => 77,  416 => 74,  414 => 69,  407 => 65,  403 => 63,  394 => 62,  383 => 156,  381 => 132,  376 => 129,  374 => 118,  369 => 115,  367 => 62,  364 => 61,  355 => 60,  322 => 58,  311 => 165,  302 => 163,  298 => 162,  295 => 161,  293 => 160,  289 => 158,  287 => 60,  277 => 58,  268 => 57,  247 => 30,  238 => 29,  226 => 43,  223 => 42,  220 => 29,  211 => 28,  196 => 25,  193 => 24,  184 => 23,  171 => 15,  165 => 13,  156 => 12,  138 => 10,  127 => 167,  125 => 57,  118 => 53,  114 => 52,  110 => 50,  105 => 48,  100 => 47,  98 => 46,  95 => 45,  93 => 28,  90 => 27,  88 => 23,  85 => 22,  76 => 20,  72 => 19,  69 => 18,  67 => 12,  62 => 10,  51 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale|split('_')|first|default('en') }}\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>{% block page_title %}{{ block('content_title')|striptags|raw }}{% endblock %}</title>

        {% block head_stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/easyadmin-all.min.css') }}\">
            <style>
                {{ easyadmin_config('_internal.custom_css')|raw }}
            </style>
        {% endblock %}

        {% for css_asset in easyadmin_config('design.assets.css') %}
            <link rel=\"stylesheet\" href=\"{{ asset(css_asset) }}\">
        {% endfor %}

        {% block head_favicon %}
            {% set favicon = easyadmin_config('design.assets.favicon') %}
            <link rel=\"icon\" type=\"{{ favicon.mime_type }}\" href=\"{{ asset(favicon.path) }}\" />
        {% endblock %}

        {% block head_javascript %}
            {% block adminlte_options %}
                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            {% endblock %}

            <script src=\"{{ asset('bundles/easyadmin/javascript/easyadmin-all.min.js') }}\"></script>
        {% endblock head_javascript %}

        {% if easyadmin_config('design.rtl') %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/bootstrap-rtl.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/adminlte-rtl.min.css') }}\">
        {% endif %}

        <!--[if lt IE 9]>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/html5shiv.min.css') }}\"></script>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/respond.min.css') }}\"></script>
        <![endif]-->
    </head>

    {% block body %}
    <body id=\"{% block body_id %}{% endblock %}\" class=\"easyadmin sidebar-mini {% block body_class %}{% endblock %} {{ app.request.cookies.has('_easyadmin_navigation_iscollapsed') ? 'sidebar-collapse' }}\">
        <div class=\"wrapper\">
        {% block wrapper %}
            <header class=\"main-header\">
            {% block header %}
                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">{{ 'toggle_navigation'|trans(domain = 'EasyAdminBundle') }}</span>
                    </a>

                    <div id=\"header-logo\">
                        {% block header_logo %}
                            <a class=\"logo {{ easyadmin_config('site_name')|length > 14 ? 'logo-long' }}\" title=\"{{ easyadmin_config('site_name')|striptags }}\" href=\"{{ path('easyadmin') }}\">
                                {{ easyadmin_config('site_name')|raw }}
                            </a>
                        {% endblock header_logo %}
                    </div>

                    <div class=\"navbar-custom-menu\">
                    {% block header_custom_menu %}
                        {% set _logout_path = easyadmin_logout_path() %}
                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                {% block user_menu %}
                                    <span class=\"sr-only\">{{ 'user.logged_in_as'|trans(domain = 'EasyAdminBundle') }}</span>

                                    {% if app.user|default(false) == false %}
                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        {{ 'user.anonymous'|trans(domain = 'EasyAdminBundle') }}
                                    {% elseif not _logout_path %}
                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                    {% else %}
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                {% block user_menu_dropdown %}
                                                <li>
                                                    <a href=\"{{ _logout_path }}\"><i class=\"fa fa-sign-out\"></i> {{ 'user.signout'|trans(domain = 'EasyAdminBundle') }}</a>
                                                </li>
                                                {% endblock user_menu_dropdown %}
                                            </ul>
                                        </div>
                                    {% endif %}
                                {% endblock user_menu %}
                            </li>
                        </ul>
                    {% endblock header_custom_menu %}
                    </div>
                </nav>
            {% endblock header %}
            </header>

            <aside class=\"main-sidebar\">
            {% block sidebar %}
                <section class=\"sidebar\">
                    {% block main_menu_wrapper %}
                        {{ include([
                            _entity_config is defined ? _entity_config.templates.menu,
                            easyadmin_config('design.templates.menu'),
                            '@EasyAdmin/default/menu.html.twig'
                        ]) }}
                    {% endblock main_menu_wrapper %}
                </section>
            {% endblock sidebar %}
            </aside>

            <div class=\"content-wrapper\">
            {% block content %}
                {% block flash_messages %}
                    {{ include(_entity_config is defined ? _entity_config.templates.flash_messages : '@EasyAdmin/default/flash_messages.html.twig') }}
                {% endblock flash_messages %}

                <section class=\"content-header\">
                {% block content_header %}
                    <h1 class=\"title\">{% block content_title %}{% endblock %}</h1>
                {% endblock content_header %}
                {% block content_help %}
                    {% if _entity_config is defined and _entity_config[app.request.query.get('action')]['help']|default(false) %}
                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                {{ _entity_config[app.request.query.get('action')]['help']|trans|raw }}
                            </div>
                        </div>
                    {% endif %}
                {% endblock content_help %}
                </section>

                <section id=\"main\" class=\"content\">
                    {% block main %}{% endblock %}
                </section>
            {% endblock content %}
            </div>
        {% endblock wrapper %}
        </div>

        {% block body_javascript %}{% endblock body_javascript %}

        {% for js_asset in easyadmin_config('design.assets.js') %}
            <script src=\"{{ asset(js_asset) }}\"></script>
        {% endfor %}
    </body>
    {% endblock body %}
</html>
", "layout.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bridge/Twig/Resources/views/Form/layout.html.twig");
    }
}

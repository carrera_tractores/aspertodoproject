<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_0f807871f9f13873e56bc1043b890f4d7989278d2915f9b0991d1bd3af0c49b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4970784ac2a298e36e0cf06f300a8f005a8530b8b0c167b9fa02809c48cf7ca0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4970784ac2a298e36e0cf06f300a8f005a8530b8b0c167b9fa02809c48cf7ca0->enter($__internal_4970784ac2a298e36e0cf06f300a8f005a8530b8b0c167b9fa02809c48cf7ca0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_c8ada18e2ce73a1e36e1049d41e5b17e8046633c0f3d3fc6b0364e447f07142e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8ada18e2ce73a1e36e1049d41e5b17e8046633c0f3d3fc6b0364e447f07142e->enter($__internal_c8ada18e2ce73a1e36e1049d41e5b17e8046633c0f3d3fc6b0364e447f07142e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 11, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 12, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 12, $this->getSourceContext()); })()), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4970784ac2a298e36e0cf06f300a8f005a8530b8b0c167b9fa02809c48cf7ca0->leave($__internal_4970784ac2a298e36e0cf06f300a8f005a8530b8b0c167b9fa02809c48cf7ca0_prof);

        
        $__internal_c8ada18e2ce73a1e36e1049d41e5b17e8046633c0f3d3fc6b0364e447f07142e->leave($__internal_c8ada18e2ce73a1e36e1049d41e5b17e8046633c0f3d3fc6b0364e447f07142e_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_622bb61283a7f1c94fbcaf291e5228282726128b7b093447e0b757f0cedc9419 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_622bb61283a7f1c94fbcaf291e5228282726128b7b093447e0b757f0cedc9419->enter($__internal_622bb61283a7f1c94fbcaf291e5228282726128b7b093447e0b757f0cedc9419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_b180880cfc286e950bf0c44fabb02323e2d2f6e1aebce1a1af702db042a6d16c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b180880cfc286e950bf0c44fabb02323e2d2f6e1aebce1a1af702db042a6d16c->enter($__internal_b180880cfc286e950bf0c44fabb02323e2d2f6e1aebce1a1af702db042a6d16c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 27, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 27, $this->getSourceContext()); })()), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 29, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 29, $this->getSourceContext()); })()), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_b180880cfc286e950bf0c44fabb02323e2d2f6e1aebce1a1af702db042a6d16c->leave($__internal_b180880cfc286e950bf0c44fabb02323e2d2f6e1aebce1a1af702db042a6d16c_prof);

        
        $__internal_622bb61283a7f1c94fbcaf291e5228282726128b7b093447e0b757f0cedc9419->leave($__internal_622bb61283a7f1c94fbcaf291e5228282726128b7b093447e0b757f0cedc9419_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f2701c35ea4110f1adf6107452902d694e0a70ae9d00713a688b2e10184d86ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2701c35ea4110f1adf6107452902d694e0a70ae9d00713a688b2e10184d86ff->enter($__internal_f2701c35ea4110f1adf6107452902d694e0a70ae9d00713a688b2e10184d86ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_a6a3b4dab8b0cff85175e8e528e5bf53a86c3931c05e0735e3125118ca87a9cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6a3b4dab8b0cff85175e8e528e5bf53a86c3931c05e0735e3125118ca87a9cf->enter($__internal_a6a3b4dab8b0cff85175e8e528e5bf53a86c3931c05e0735e3125118ca87a9cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 35, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 35, $this->getSourceContext()); })()), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 36, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 36, $this->getSourceContext()); })()), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_a6a3b4dab8b0cff85175e8e528e5bf53a86c3931c05e0735e3125118ca87a9cf->leave($__internal_a6a3b4dab8b0cff85175e8e528e5bf53a86c3931c05e0735e3125118ca87a9cf_prof);

        
        $__internal_f2701c35ea4110f1adf6107452902d694e0a70ae9d00713a688b2e10184d86ff->leave($__internal_f2701c35ea4110f1adf6107452902d694e0a70ae9d00713a688b2e10184d86ff_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}

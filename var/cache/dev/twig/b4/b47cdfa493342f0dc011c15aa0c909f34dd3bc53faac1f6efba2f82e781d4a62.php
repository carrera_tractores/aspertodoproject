<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_957931083f2a76b6e3c96c62803bcdf5d7da47f9166dcaea9320bf0518fdd632 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41ab43ee1554816cb4fdc7a5af511d25078af51f66812b3dbd0328a88890d583 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41ab43ee1554816cb4fdc7a5af511d25078af51f66812b3dbd0328a88890d583->enter($__internal_41ab43ee1554816cb4fdc7a5af511d25078af51f66812b3dbd0328a88890d583_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_0ab7f9e34c8d76c5b88c070ef59043afcf550bdaff33c4770cee3688f7dda817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ab7f9e34c8d76c5b88c070ef59043afcf550bdaff33c4770cee3688f7dda817->enter($__internal_0ab7f9e34c8d76c5b88c070ef59043afcf550bdaff33c4770cee3688f7dda817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_41ab43ee1554816cb4fdc7a5af511d25078af51f66812b3dbd0328a88890d583->leave($__internal_41ab43ee1554816cb4fdc7a5af511d25078af51f66812b3dbd0328a88890d583_prof);

        
        $__internal_0ab7f9e34c8d76c5b88c070ef59043afcf550bdaff33c4770cee3688f7dda817->leave($__internal_0ab7f9e34c8d76c5b88c070ef59043afcf550bdaff33c4770cee3688f7dda817_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}

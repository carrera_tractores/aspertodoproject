<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_21318912110d77b97911cbd23b2a84d3428bb8176b161bbcbe22d5fbc6b2bbac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9585861710d91c0d95c07745b2ce736d36a912022ec94151f43c01cdc2597de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9585861710d91c0d95c07745b2ce736d36a912022ec94151f43c01cdc2597de->enter($__internal_c9585861710d91c0d95c07745b2ce736d36a912022ec94151f43c01cdc2597de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_16a30bad6a62ba1f6c83ebad103e8c2d4381147872c300a3e1b78633de0a5ae6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16a30bad6a62ba1f6c83ebad103e8c2d4381147872c300a3e1b78633de0a5ae6->enter($__internal_16a30bad6a62ba1f6c83ebad103e8c2d4381147872c300a3e1b78633de0a5ae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_c9585861710d91c0d95c07745b2ce736d36a912022ec94151f43c01cdc2597de->leave($__internal_c9585861710d91c0d95c07745b2ce736d36a912022ec94151f43c01cdc2597de_prof);

        
        $__internal_16a30bad6a62ba1f6c83ebad103e8c2d4381147872c300a3e1b78633de0a5ae6->leave($__internal_16a30bad6a62ba1f6c83ebad103e8c2d4381147872c300a3e1b78633de0a5ae6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}

<?php

/* EasyAdminBundle:default:label_empty.html.twig */
class __TwigTemplate_12db9ca5e1800d3bc7cc18fe95fc87e5deff1848d5f56fb524de02f8b32ae77b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d772514a7a0015c18d4bed991a937f257503f8fa553dae05af7d274541fa0643 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d772514a7a0015c18d4bed991a937f257503f8fa553dae05af7d274541fa0643->enter($__internal_d772514a7a0015c18d4bed991a937f257503f8fa553dae05af7d274541fa0643_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_empty.html.twig"));

        $__internal_4408491a2d85c58c72024584c8fd91e3da51b999a687d3dddbcfc4153e25b3b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4408491a2d85c58c72024584c8fd91e3da51b999a687d3dddbcfc4153e25b3b1->enter($__internal_4408491a2d85c58c72024584c8fd91e3da51b999a687d3dddbcfc4153e25b3b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_empty.html.twig"));

        // line 1
        echo "<span class=\"label label-empty\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.empty", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_d772514a7a0015c18d4bed991a937f257503f8fa553dae05af7d274541fa0643->leave($__internal_d772514a7a0015c18d4bed991a937f257503f8fa553dae05af7d274541fa0643_prof);

        
        $__internal_4408491a2d85c58c72024584c8fd91e3da51b999a687d3dddbcfc4153e25b3b1->leave($__internal_4408491a2d85c58c72024584c8fd91e3da51b999a687d3dddbcfc4153e25b3b1_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_empty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label label-empty\">{{ 'label.empty'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:label_empty.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_empty.html.twig");
    }
}

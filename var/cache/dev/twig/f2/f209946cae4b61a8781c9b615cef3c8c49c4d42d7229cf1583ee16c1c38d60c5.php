<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_a88a62a5742195583b873170b09ee5907f9333c5f6ea3f12bf345e1f0ba2dc05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_420120ef9a0be58c68eda03b3b3f0dcdd46556a6edbc87de93c053bfdff585d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_420120ef9a0be58c68eda03b3b3f0dcdd46556a6edbc87de93c053bfdff585d2->enter($__internal_420120ef9a0be58c68eda03b3b3f0dcdd46556a6edbc87de93c053bfdff585d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_51754887abc3b1b7ac36b2c9a8c1fce3c59af23b37ffd2cfd8dd89916d048e34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51754887abc3b1b7ac36b2c9a8c1fce3c59af23b37ffd2cfd8dd89916d048e34->enter($__internal_51754887abc3b1b7ac36b2c9a8c1fce3c59af23b37ffd2cfd8dd89916d048e34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo json_encode(array("error" => array("code" => (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 1, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_420120ef9a0be58c68eda03b3b3f0dcdd46556a6edbc87de93c053bfdff585d2->leave($__internal_420120ef9a0be58c68eda03b3b3f0dcdd46556a6edbc87de93c053bfdff585d2_prof);

        
        $__internal_51754887abc3b1b7ac36b2c9a8c1fce3c59af23b37ffd2cfd8dd89916d048e34->leave($__internal_51754887abc3b1b7ac36b2c9a8c1fce3c59af23b37ffd2cfd8dd89916d048e34_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.json.twig");
    }
}

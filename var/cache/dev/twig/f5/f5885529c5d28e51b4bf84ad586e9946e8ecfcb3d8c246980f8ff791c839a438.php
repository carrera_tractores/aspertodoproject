<?php

/* afegirtascaextra.html.twig~ */
class __TwigTemplate_7d313a7bb4e43bd8d40c1fd5ae1bcf8def6bd1498b57b1b67cedc5c5ae535370 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7db66a56a5e2d942fc413e01db0f27c5783e017193a79f15a253df2e16f3c477 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7db66a56a5e2d942fc413e01db0f27c5783e017193a79f15a253df2e16f3c477->enter($__internal_7db66a56a5e2d942fc413e01db0f27c5783e017193a79f15a253df2e16f3c477_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirtascaextra.html.twig~"));

        $__internal_b9640247ba703db5d356f63f246019c36a2a610442d2e1de9e159871a2e5c826 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9640247ba703db5d356f63f246019c36a2a610442d2e1de9e159871a2e5c826->enter($__internal_b9640247ba703db5d356f63f246019c36a2a610442d2e1de9e159871a2e5c826_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirtascaextra.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />

        
    </head>
    <body>
    \t";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola ";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir Usuari</h1>
      <hr>
      
\t\t<div id=\"formulari\">
\t\t\t<input type=\"hidden\" name=\"\" value=\"\" />

    \t\t<label for=\"Nom\">Nom</label>
    \t\t<input type=\"text\" id=\"nom\" name=\"nom\" required=\"required\" />

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        ";
        // line 67
        $this->displayBlock('javascripts', $context, $blocks);
        // line 72
        echo "    </body>
</html>
";
        
        $__internal_7db66a56a5e2d942fc413e01db0f27c5783e017193a79f15a253df2e16f3c477->leave($__internal_7db66a56a5e2d942fc413e01db0f27c5783e017193a79f15a253df2e16f3c477_prof);

        
        $__internal_b9640247ba703db5d356f63f246019c36a2a610442d2e1de9e159871a2e5c826->leave($__internal_b9640247ba703db5d356f63f246019c36a2a610442d2e1de9e159871a2e5c826_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_95c04170858dedd2797e8d1fc2da7dd0532a5eb54cbe600abb4b1cc4b69547cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95c04170858dedd2797e8d1fc2da7dd0532a5eb54cbe600abb4b1cc4b69547cd->enter($__internal_95c04170858dedd2797e8d1fc2da7dd0532a5eb54cbe600abb4b1cc4b69547cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_123936c354cfdb8f3000719428369974ae320dcfd418134f8473d80a0f404deb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_123936c354cfdb8f3000719428369974ae320dcfd418134f8473d80a0f404deb->enter($__internal_123936c354cfdb8f3000719428369974ae320dcfd418134f8473d80a0f404deb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Afegir usuari";
        
        $__internal_123936c354cfdb8f3000719428369974ae320dcfd418134f8473d80a0f404deb->leave($__internal_123936c354cfdb8f3000719428369974ae320dcfd418134f8473d80a0f404deb_prof);

        
        $__internal_95c04170858dedd2797e8d1fc2da7dd0532a5eb54cbe600abb4b1cc4b69547cd->leave($__internal_95c04170858dedd2797e8d1fc2da7dd0532a5eb54cbe600abb4b1cc4b69547cd_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_12b1bebb64a1996ad4b937cf88a92723be1c83e7d4e0d25089bb070ab53876d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12b1bebb64a1996ad4b937cf88a92723be1c83e7d4e0d25089bb070ab53876d3->enter($__internal_12b1bebb64a1996ad4b937cf88a92723be1c83e7d4e0d25089bb070ab53876d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_584b9208f3c85fc46a664bb9ed1e77eac0cc6b3085e2ccadda02d075226e4ede = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_584b9208f3c85fc46a664bb9ed1e77eac0cc6b3085e2ccadda02d075226e4ede->enter($__internal_584b9208f3c85fc46a664bb9ed1e77eac0cc6b3085e2ccadda02d075226e4ede_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_584b9208f3c85fc46a664bb9ed1e77eac0cc6b3085e2ccadda02d075226e4ede->leave($__internal_584b9208f3c85fc46a664bb9ed1e77eac0cc6b3085e2ccadda02d075226e4ede_prof);

        
        $__internal_12b1bebb64a1996ad4b937cf88a92723be1c83e7d4e0d25089bb070ab53876d3->leave($__internal_12b1bebb64a1996ad4b937cf88a92723be1c83e7d4e0d25089bb070ab53876d3_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_73a0bacdd519283a03d5a69af8b2bfaff7b40306e8bc9c3c8ec45a65fa3081b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73a0bacdd519283a03d5a69af8b2bfaff7b40306e8bc9c3c8ec45a65fa3081b4->enter($__internal_73a0bacdd519283a03d5a69af8b2bfaff7b40306e8bc9c3c8ec45a65fa3081b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_de9dcbba04223bfd2350ea26146bc3cbdf5bfd17a9494b5f0755e4598f9445c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de9dcbba04223bfd2350ea26146bc3cbdf5bfd17a9494b5f0755e4598f9445c3->enter($__internal_de9dcbba04223bfd2350ea26146bc3cbdf5bfd17a9494b5f0755e4598f9445c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_de9dcbba04223bfd2350ea26146bc3cbdf5bfd17a9494b5f0755e4598f9445c3->leave($__internal_de9dcbba04223bfd2350ea26146bc3cbdf5bfd17a9494b5f0755e4598f9445c3_prof);

        
        $__internal_73a0bacdd519283a03d5a69af8b2bfaff7b40306e8bc9c3c8ec45a65fa3081b4->leave($__internal_73a0bacdd519283a03d5a69af8b2bfaff7b40306e8bc9c3c8ec45a65fa3081b4_prof);

    }

    // line 67
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_30d28f2f40cbed75c46d2480ef755076b641346f925a84a3a00468e369be13ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30d28f2f40cbed75c46d2480ef755076b641346f925a84a3a00468e369be13ba->enter($__internal_30d28f2f40cbed75c46d2480ef755076b641346f925a84a3a00468e369be13ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_f8f893f013999890e88d04177ee3f276cc1e8a01e7d66920865be8472d9f8905 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8f893f013999890e88d04177ee3f276cc1e8a01e7d66920865be8472d9f8905->enter($__internal_f8f893f013999890e88d04177ee3f276cc1e8a01e7d66920865be8472d9f8905_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 68
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_f8f893f013999890e88d04177ee3f276cc1e8a01e7d66920865be8472d9f8905->leave($__internal_f8f893f013999890e88d04177ee3f276cc1e8a01e7d66920865be8472d9f8905_prof);

        
        $__internal_30d28f2f40cbed75c46d2480ef755076b641346f925a84a3a00468e369be13ba->leave($__internal_30d28f2f40cbed75c46d2480ef755076b641346f925a84a3a00468e369be13ba_prof);

    }

    public function getTemplateName()
    {
        return "afegirtascaextra.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 69,  195 => 68,  186 => 67,  169 => 15,  152 => 6,  134 => 5,  122 => 72,  120 => 67,  70 => 20,  66 => 19,  61 => 16,  59 => 15,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Afegir usuari{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />

        
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"{{ path('fos_user_security_logout') }}\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola {{ app.user.username }}!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir Usuari</h1>
      <hr>
      
\t\t<div id=\"formulari\">
\t\t\t<input type=\"hidden\" name=\"\" value=\"\" />

    \t\t<label for=\"Nom\">Nom</label>
    \t\t<input type=\"text\" id=\"nom\" name=\"nom\" required=\"required\" />

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>

        {% endblock %}
    </body>
</html>
", "afegirtascaextra.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/afegirtascaextra.html.twig~");
    }
}

<?php

/* @Framework/Form/form_start.html.php */
class __TwigTemplate_0d54162930210eba02fc05b164da3c6ea42ad68b71a8557efaaa6a6da6c7ef75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9dd967fb2306304aa436d2d6a574268e05d6841c6fc590598bf8194b60193fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9dd967fb2306304aa436d2d6a574268e05d6841c6fc590598bf8194b60193fd->enter($__internal_d9dd967fb2306304aa436d2d6a574268e05d6841c6fc590598bf8194b60193fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        $__internal_1d7c5962f2cb99b64dba5027b1d37d37507990a3f52ab520df9e16f716b44849 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d7c5962f2cb99b64dba5027b1d37d37507990a3f52ab520df9e16f716b44849->enter($__internal_1d7c5962f2cb99b64dba5027b1d37d37507990a3f52ab520df9e16f716b44849_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_start.html.php"));

        // line 1
        echo "<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
";
        
        $__internal_d9dd967fb2306304aa436d2d6a574268e05d6841c6fc590598bf8194b60193fd->leave($__internal_d9dd967fb2306304aa436d2d6a574268e05d6841c6fc590598bf8194b60193fd_prof);

        
        $__internal_1d7c5962f2cb99b64dba5027b1d37d37507990a3f52ab520df9e16f716b44849->leave($__internal_1d7c5962f2cb99b64dba5027b1d37d37507990a3f52ab520df9e16f716b44849_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_start.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php \$method = strtoupper(\$method) ?>
<?php \$form_method = \$method === 'GET' || \$method === 'POST' ? \$method : 'POST' ?>
<form name=\"<?php echo \$name ?>\" method=\"<?php echo strtolower(\$form_method) ?>\"<?php if (\$action !== ''): ?> action=\"<?php echo \$action ?>\"<?php endif ?><?php foreach (\$attr as \$k => \$v) { printf(' %s=\"%s\"', \$view->escape(\$k), \$view->escape(\$v)); } ?><?php if (\$multipart): ?> enctype=\"multipart/form-data\"<?php endif ?>>
<?php if (\$form_method !== \$method): ?>
    <input type=\"hidden\" name=\"_method\" value=\"<?php echo \$method ?>\" />
<?php endif ?>
", "@Framework/Form/form_start.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_start.html.php");
    }
}

<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_c4063acbad8726806bc1304a8520136ad405362e4b69f4d670a483f9a93b725e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffd13aecefe44b9b43c4ae0c2fe2ce20f9e8dee54d65f2d161f182de21cbe917 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffd13aecefe44b9b43c4ae0c2fe2ce20f9e8dee54d65f2d161f182de21cbe917->enter($__internal_ffd13aecefe44b9b43c4ae0c2fe2ce20f9e8dee54d65f2d161f182de21cbe917_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_5ba8cc940a5e75eb771fb18f7d0644a8ee4d19da31c5e5579a4c8ddad3b62944 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ba8cc940a5e75eb771fb18f7d0644a8ee4d19da31c5e5579a4c8ddad3b62944->enter($__internal_5ba8cc940a5e75eb771fb18f7d0644a8ee4d19da31c5e5579a4c8ddad3b62944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_ffd13aecefe44b9b43c4ae0c2fe2ce20f9e8dee54d65f2d161f182de21cbe917->leave($__internal_ffd13aecefe44b9b43c4ae0c2fe2ce20f9e8dee54d65f2d161f182de21cbe917_prof);

        
        $__internal_5ba8cc940a5e75eb771fb18f7d0644a8ee4d19da31c5e5579a4c8ddad3b62944->leave($__internal_5ba8cc940a5e75eb771fb18f7d0644a8ee4d19da31c5e5579a4c8ddad3b62944_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}

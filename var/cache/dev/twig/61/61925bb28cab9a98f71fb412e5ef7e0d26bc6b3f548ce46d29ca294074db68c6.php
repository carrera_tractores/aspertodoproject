<?php

/* @FOSUser/Registration/register_content.html.twig~ */
class __TwigTemplate_9bc7109b9cd50bc9af849bd76ee66d59d0072bce7d0c166d5c54388e01db9140 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_070ec141328a3ad2b8dcc8f51a2c1aa4e1bd507ea04938380c39de3f45d58348 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_070ec141328a3ad2b8dcc8f51a2c1aa4e1bd507ea04938380c39de3f45d58348->enter($__internal_070ec141328a3ad2b8dcc8f51a2c1aa4e1bd507ea04938380c39de3f45d58348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig~"));

        $__internal_09f530c74cef2f5e219269ef4c05a3b89c45d3e59eeb0050fcb1bca5fbc52597 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09f530c74cef2f5e219269ef4c05a3b89c45d3e59eeb0050fcb1bca5fbc52597->enter($__internal_09f530c74cef2f5e219269ef4c05a3b89c45d3e59eeb0050fcb1bca5fbc52597_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig~"));

        // line 1
        echo "<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
    \t<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
\t</head>

\t<body>

\t\t";
        // line 11
        echo "
\t\t";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    \t\t";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->getSourceContext()); })()), 'widget');
        echo "
    \t\t<div>
        \t\t<input type=\"submit\" id=\"_submit\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
   \t\t</div>
\t\t";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 17, $this->getSourceContext()); })()), 'form_end');
        echo "

\t</body>

</html>";
        
        $__internal_070ec141328a3ad2b8dcc8f51a2c1aa4e1bd507ea04938380c39de3f45d58348->leave($__internal_070ec141328a3ad2b8dcc8f51a2c1aa4e1bd507ea04938380c39de3f45d58348_prof);

        
        $__internal_09f530c74cef2f5e219269ef4c05a3b89c45d3e59eeb0050fcb1bca5fbc52597->leave($__internal_09f530c74cef2f5e219269ef4c05a3b89c45d3e59eeb0050fcb1bca5fbc52597_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 17,  54 => 15,  49 => 13,  45 => 12,  42 => 11,  34 => 5,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
    \t<link rel=\"stylesheet\" href=\"{{ asset('css/login.css') }}\" />
\t</head>

\t<body>

\t\t{% trans_default_domain 'FOSUserBundle' %}

\t\t{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
    \t\t{{ form_widget(form) }}
    \t\t<div>
        \t\t<input type=\"submit\" id=\"_submit\" value=\"{{ 'registration.submit'|trans }}\" />
   \t\t</div>
\t\t{{ form_end(form) }}

\t</body>

</html>", "@FOSUser/Registration/register_content.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig~");
    }
}

<?php

/* EasyAdminBundle:default:show.html.twig */
class __TwigTemplate_c4f6c70b649e0d54adf19ebbf2f1632cfe9c7149b6b7c48bf0c71b0022e3dae8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'item_actions' => array($this, 'block_item_actions'),
            'delete_form' => array($this, 'block_delete_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 7, $this->getSourceContext()); })()), "templates", array()), "layout", array()), "EasyAdminBundle:default:show.html.twig", 7);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f49c5f9b7bb3b290384a2ecd9c05fd4597ca6163627c360df7b9186ff8fa5e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f49c5f9b7bb3b290384a2ecd9c05fd4597ca6163627c360df7b9186ff8fa5e4->enter($__internal_4f49c5f9b7bb3b290384a2ecd9c05fd4597ca6163627c360df7b9186ff8fa5e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:show.html.twig"));

        $__internal_18178d82d726c821f8434209d702ef62fd8ed56d9597bb7ab2aed5fdd4b3fa7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18178d82d726c821f8434209d702ef62fd8ed56d9597bb7ab2aed5fdd4b3fa7f->enter($__internal_18178d82d726c821f8434209d702ef62fd8ed56d9597bb7ab2aed5fdd4b3fa7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:show.html.twig"));

        // line 1
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 1, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 3
        $context["_entity_id"] = ("" . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["entity"]) || array_key_exists("entity", $context) ? $context["entity"] : (function () { throw new Twig_Error_Runtime('Variable "entity" does not exist.', 3, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 3, $this->getSourceContext()); })()), "primary_key_field_name", array())));
        // line 4
        $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 4, $this->getSourceContext()); })()), "translation_domain", array());
        // line 5
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->getSourceContext()); })()), "name", array()), array(),         // line 4
(isset($context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"]) || array_key_exists("__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a", $context) ? $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a" does not exist.', 4, $this->getSourceContext()); })())), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(),         // line 5
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->getSourceContext()); })()), "label", array()), array(),         // line 4
(isset($context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"]) || array_key_exists("__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a", $context) ? $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a" does not exist.', 4, $this->getSourceContext()); })())), "%entity_id%" =>         // line 5
(isset($context["_entity_id"]) || array_key_exists("_entity_id", $context) ? $context["_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_id" does not exist.', 5, $this->getSourceContext()); })()));
        // line 7
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4f49c5f9b7bb3b290384a2ecd9c05fd4597ca6163627c360df7b9186ff8fa5e4->leave($__internal_4f49c5f9b7bb3b290384a2ecd9c05fd4597ca6163627c360df7b9186ff8fa5e4_prof);

        
        $__internal_18178d82d726c821f8434209d702ef62fd8ed56d9597bb7ab2aed5fdd4b3fa7f->leave($__internal_18178d82d726c821f8434209d702ef62fd8ed56d9597bb7ab2aed5fdd4b3fa7f_prof);

    }

    // line 9
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_2474226c2547009eb79323f043cef935679c41fcd6af46d73395b8d694c29ad6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2474226c2547009eb79323f043cef935679c41fcd6af46d73395b8d694c29ad6->enter($__internal_2474226c2547009eb79323f043cef935679c41fcd6af46d73395b8d694c29ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_fcbeb347da9da4b737eeef0d695b105b6a357196bf44c0b65c8fcdf1491bfe95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcbeb347da9da4b737eeef0d695b105b6a357196bf44c0b65c8fcdf1491bfe95->enter($__internal_fcbeb347da9da4b737eeef0d695b105b6a357196bf44c0b65c8fcdf1491bfe95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ((("easyadmin-show-" . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 9, $this->getSourceContext()); })()), "name", array())) . "-") . (isset($context["_entity_id"]) || array_key_exists("_entity_id", $context) ? $context["_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_id" does not exist.', 9, $this->getSourceContext()); })())), "html", null, true);
        
        $__internal_fcbeb347da9da4b737eeef0d695b105b6a357196bf44c0b65c8fcdf1491bfe95->leave($__internal_fcbeb347da9da4b737eeef0d695b105b6a357196bf44c0b65c8fcdf1491bfe95_prof);

        
        $__internal_2474226c2547009eb79323f043cef935679c41fcd6af46d73395b8d694c29ad6->leave($__internal_2474226c2547009eb79323f043cef935679c41fcd6af46d73395b8d694c29ad6_prof);

    }

    // line 10
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_2247ecb2b835f90674fb5fb696aef06a857ac67fc5e7c97d0536f1e1266db97c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2247ecb2b835f90674fb5fb696aef06a857ac67fc5e7c97d0536f1e1266db97c->enter($__internal_2247ecb2b835f90674fb5fb696aef06a857ac67fc5e7c97d0536f1e1266db97c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_748b64c4307a1289516b178aed158f8dd99b3ffab171e4a91eb6e879cbb4a89c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_748b64c4307a1289516b178aed158f8dd99b3ffab171e4a91eb6e879cbb4a89c->enter($__internal_748b64c4307a1289516b178aed158f8dd99b3ffab171e4a91eb6e879cbb4a89c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("show show-" . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 10, $this->getSourceContext()); })()), "name", array()))), "html", null, true);
        
        $__internal_748b64c4307a1289516b178aed158f8dd99b3ffab171e4a91eb6e879cbb4a89c->leave($__internal_748b64c4307a1289516b178aed158f8dd99b3ffab171e4a91eb6e879cbb4a89c_prof);

        
        $__internal_2247ecb2b835f90674fb5fb696aef06a857ac67fc5e7c97d0536f1e1266db97c->leave($__internal_2247ecb2b835f90674fb5fb696aef06a857ac67fc5e7c97d0536f1e1266db97c_prof);

    }

    // line 12
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_41444fe60438d0070945ac92ddc82fdfd773c8bccc266fff80dcb8826355312c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41444fe60438d0070945ac92ddc82fdfd773c8bccc266fff80dcb8826355312c->enter($__internal_41444fe60438d0070945ac92ddc82fdfd773c8bccc266fff80dcb8826355312c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_beb7a903022a3be46333ddf1c29d56e77dfa7be7fc5da70bd2e9ab8c3f430253 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_beb7a903022a3be46333ddf1c29d56e77dfa7be7fc5da70bd2e9ab8c3f430253->enter($__internal_beb7a903022a3be46333ddf1c29d56e77dfa7be7fc5da70bd2e9ab8c3f430253_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 13
        ob_start();
        // line 14
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("show.page_title", (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 14, $this->getSourceContext()); })()), "EasyAdminBundle");
        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), "show", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 15, $this->getSourceContext()); })()), "show", array()), "title", array()), (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 15, $this->getSourceContext()); })()),         // line 4
(isset($context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"]) || array_key_exists("__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a", $context) ? $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a" does not exist.', 4, $this->getSourceContext()); })()))) : (        // line 15
(isset($context["_default_title"]) || array_key_exists("_default_title", $context) ? $context["_default_title"] : (function () { throw new Twig_Error_Runtime('Variable "_default_title" does not exist.', 15, $this->getSourceContext()); })()))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_beb7a903022a3be46333ddf1c29d56e77dfa7be7fc5da70bd2e9ab8c3f430253->leave($__internal_beb7a903022a3be46333ddf1c29d56e77dfa7be7fc5da70bd2e9ab8c3f430253_prof);

        
        $__internal_41444fe60438d0070945ac92ddc82fdfd773c8bccc266fff80dcb8826355312c->leave($__internal_41444fe60438d0070945ac92ddc82fdfd773c8bccc266fff80dcb8826355312c_prof);

    }

    // line 19
    public function block_main($context, array $blocks = array())
    {
        $__internal_9f07047320c189e4e0dc92df5c435a23b00a5115fdcc05b0194ca232bd450b57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f07047320c189e4e0dc92df5c435a23b00a5115fdcc05b0194ca232bd450b57->enter($__internal_9f07047320c189e4e0dc92df5c435a23b00a5115fdcc05b0194ca232bd450b57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_d8282e186914ad48f6d661e100ee4e5996fd38bc01dc4f06386a48875f4e7ac6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8282e186914ad48f6d661e100ee4e5996fd38bc01dc4f06386a48875f4e7ac6->enter($__internal_d8282e186914ad48f6d661e100ee4e5996fd38bc01dc4f06386a48875f4e7ac6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 20
        echo "    <div class=\"form-horizontal\">
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) || array_key_exists("fields", $context) ? $context["fields"] : (function () { throw new Twig_Error_Runtime('Variable "fields" does not exist.', 21, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["field"] => $context["metadata"]) {
            // line 22
            echo "            <div class=\"form-group field-";
            echo twig_escape_filter($this->env, twig_lower_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "type", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "type", array()), "default")) : ("default"))), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "css_class", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "css_class", array()), "")) : ("")), "html", null, true);
            echo "\">
                <label class=\"col-sm-2 control-label\">
                    ";
            // line 24
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(((twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "label", array())) ? (twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "label", array())) : ($this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize($context["field"]))), (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 24, $this->getSourceContext()); })()),             // line 4
(isset($context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"]) || array_key_exists("__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a", $context) ? $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a" does not exist.', 4, $this->getSourceContext()); })()));
            // line 24
            echo "
                </label>
                <div class=\"col-sm-10\">
                    <div class=\"form-control\">
                        ";
            // line 28
            echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->renderEntityField($this->env, "show", twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 28, $this->getSourceContext()); })()), "name", array()), (isset($context["entity"]) || array_key_exists("entity", $context) ? $context["entity"] : (function () { throw new Twig_Error_Runtime('Variable "entity" does not exist.', 28, $this->getSourceContext()); })()), $context["metadata"]);
            echo "
                    </div>

                    ";
            // line 31
            if ((((twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "help", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "help", array()), "")) : ("")) != "")) {
                // line 32
                echo "                        <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> ";
                echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), $context["metadata"], "help", array()), array(),                 // line 4
(isset($context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"]) || array_key_exists("__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a", $context) ? $context["__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_5da98115df28b149900df99dd90412c4a47f1c223cbd41750a03513e8985ec7a" does not exist.', 4, $this->getSourceContext()); })()));
                // line 32
                echo "</span>
                    ";
            }
            // line 34
            echo "                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field'], $context['metadata'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
        <div class=\"form-group form-actions\">
            <div class=\"col-sm-10 col-sm-offset-2\">
            ";
        // line 40
        $this->displayBlock('item_actions', $context, $blocks);
        // line 52
        echo "            </div>
        </div>
    </div>

    ";
        // line 56
        $this->displayBlock('delete_form', $context, $blocks);
        
        $__internal_d8282e186914ad48f6d661e100ee4e5996fd38bc01dc4f06386a48875f4e7ac6->leave($__internal_d8282e186914ad48f6d661e100ee4e5996fd38bc01dc4f06386a48875f4e7ac6_prof);

        
        $__internal_9f07047320c189e4e0dc92df5c435a23b00a5115fdcc05b0194ca232bd450b57->leave($__internal_9f07047320c189e4e0dc92df5c435a23b00a5115fdcc05b0194ca232bd450b57_prof);

    }

    // line 40
    public function block_item_actions($context, array $blocks = array())
    {
        $__internal_2954d864afd3aa26870919d7f143aef4feeb681a39ac641623327beed54d5bfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2954d864afd3aa26870919d7f143aef4feeb681a39ac641623327beed54d5bfd->enter($__internal_2954d864afd3aa26870919d7f143aef4feeb681a39ac641623327beed54d5bfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item_actions"));

        $__internal_8dd884f7b38daa7ef82d499269db6bc947cc90d788665076ca634fe07ddad264 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dd884f7b38daa7ef82d499269db6bc947cc90d788665076ca634fe07ddad264->enter($__internal_8dd884f7b38daa7ef82d499269db6bc947cc90d788665076ca634fe07ddad264_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item_actions"));

        // line 41
        echo "                ";
        $context["_show_actions"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getActionsForItem("show", twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 41, $this->getSourceContext()); })()), "name", array()));
        // line 42
        echo "                ";
        $context["_request_parameters"] = array("entity" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 42, $this->getSourceContext()); })()), "name", array()), "referer" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 42, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "referer"), "method"));
        // line 43
        echo "
                ";
        // line 44
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_actions.html.twig", array("actions" =>         // line 45
(isset($context["_show_actions"]) || array_key_exists("_show_actions", $context) ? $context["_show_actions"] : (function () { throw new Twig_Error_Runtime('Variable "_show_actions" does not exist.', 45, $this->getSourceContext()); })()), "request_parameters" =>         // line 46
(isset($context["_request_parameters"]) || array_key_exists("_request_parameters", $context) ? $context["_request_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_request_parameters" does not exist.', 46, $this->getSourceContext()); })()), "translation_domain" => twig_get_attribute($this->env, $this->getSourceContext(),         // line 47
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 47, $this->getSourceContext()); })()), "translation_domain", array()), "trans_parameters" =>         // line 48
(isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 48, $this->getSourceContext()); })()), "item_id" =>         // line 49
(isset($context["_entity_id"]) || array_key_exists("_entity_id", $context) ? $context["_entity_id"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_id" does not exist.', 49, $this->getSourceContext()); })())), false);
        // line 50
        echo "
            ";
        
        $__internal_8dd884f7b38daa7ef82d499269db6bc947cc90d788665076ca634fe07ddad264->leave($__internal_8dd884f7b38daa7ef82d499269db6bc947cc90d788665076ca634fe07ddad264_prof);

        
        $__internal_2954d864afd3aa26870919d7f143aef4feeb681a39ac641623327beed54d5bfd->leave($__internal_2954d864afd3aa26870919d7f143aef4feeb681a39ac641623327beed54d5bfd_prof);

    }

    // line 56
    public function block_delete_form($context, array $blocks = array())
    {
        $__internal_e2b58acca314ab780b9eed627eb0654aa31f30e64f0484777acddfdc07c66b7d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2b58acca314ab780b9eed627eb0654aa31f30e64f0484777acddfdc07c66b7d->enter($__internal_e2b58acca314ab780b9eed627eb0654aa31f30e64f0484777acddfdc07c66b7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        $__internal_d730a01c878c7f611d370cfff91df7319f8b6a12e88f8b2c6cda7bdc1c7c476c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d730a01c878c7f611d370cfff91df7319f8b6a12e88f8b2c6cda7bdc1c7c476c->enter($__internal_d730a01c878c7f611d370cfff91df7319f8b6a12e88f8b2c6cda7bdc1c7c476c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "delete_form"));

        // line 57
        echo "        ";
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_delete_form.html.twig", array("view" => "show", "referer" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(),         // line 59
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 59, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "referer", 1 => ""), "method"), "delete_form" =>         // line 60
(isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 60, $this->getSourceContext()); })()), "_translation_domain" => twig_get_attribute($this->env, $this->getSourceContext(),         // line 61
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 61, $this->getSourceContext()); })()), "translation_domain", array()), "_trans_parameters" =>         // line 62
(isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 62, $this->getSourceContext()); })()), "_entity_config" =>         // line 63
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 63, $this->getSourceContext()); })())), false);
        // line 64
        echo "
    ";
        
        $__internal_d730a01c878c7f611d370cfff91df7319f8b6a12e88f8b2c6cda7bdc1c7c476c->leave($__internal_d730a01c878c7f611d370cfff91df7319f8b6a12e88f8b2c6cda7bdc1c7c476c_prof);

        
        $__internal_e2b58acca314ab780b9eed627eb0654aa31f30e64f0484777acddfdc07c66b7d->leave($__internal_e2b58acca314ab780b9eed627eb0654aa31f30e64f0484777acddfdc07c66b7d_prof);

    }

    // line 68
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_5c86798e61390d99c32022e1f71aa882312cfe98ae611bc1af7867ee9441153c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c86798e61390d99c32022e1f71aa882312cfe98ae611bc1af7867ee9441153c->enter($__internal_5c86798e61390d99c32022e1f71aa882312cfe98ae611bc1af7867ee9441153c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_9093be038814c2cc8d663e341f8086e047dd02b984ddaab8c7ec97560887a05f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9093be038814c2cc8d663e341f8086e047dd02b984ddaab8c7ec97560887a05f->enter($__internal_9093be038814c2cc8d663e341f8086e047dd02b984ddaab8c7ec97560887a05f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 69
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>
";
        
        $__internal_9093be038814c2cc8d663e341f8086e047dd02b984ddaab8c7ec97560887a05f->leave($__internal_9093be038814c2cc8d663e341f8086e047dd02b984ddaab8c7ec97560887a05f_prof);

        
        $__internal_5c86798e61390d99c32022e1f71aa882312cfe98ae611bc1af7867ee9441153c->leave($__internal_5c86798e61390d99c32022e1f71aa882312cfe98ae611bc1af7867ee9441153c_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  277 => 69,  268 => 68,  257 => 64,  255 => 63,  254 => 62,  253 => 61,  252 => 60,  251 => 59,  249 => 57,  240 => 56,  229 => 50,  227 => 49,  226 => 48,  225 => 47,  224 => 46,  223 => 45,  222 => 44,  219 => 43,  216 => 42,  213 => 41,  204 => 40,  194 => 56,  188 => 52,  186 => 40,  181 => 37,  173 => 34,  169 => 32,  167 => 4,  165 => 32,  163 => 31,  157 => 28,  151 => 24,  149 => 4,  148 => 24,  140 => 22,  136 => 21,  133 => 20,  124 => 19,  111 => 15,  110 => 4,  108 => 15,  105 => 14,  103 => 13,  94 => 12,  76 => 10,  58 => 9,  48 => 7,  46 => 5,  45 => 4,  44 => 5,  43 => 4,  42 => 5,  40 => 4,  38 => 3,  36 => 1,  24 => 7,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{# the empty string concatenation is needed when the primary key is an object (e.g. an Uuid object) #}
{% set _entity_id = '' ~ attribute(entity, _entity_config.primary_key_field_name) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans, '%entity_id%': _entity_id } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-show-' ~ _entity_config.name ~ '-' ~ _entity_id %}
{% block body_class 'show show-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'show.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.show.title is defined ? _entity_config.show.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    <div class=\"form-horizontal\">
        {% for field, metadata in fields %}
            <div class=\"form-group field-{{ metadata.type|default('default')|lower }} {{ metadata.css_class|default('') }}\">
                <label class=\"col-sm-2 control-label\">
                    {{ (metadata.label ?: field|humanize)|trans(_trans_parameters)|raw }}
                </label>
                <div class=\"col-sm-10\">
                    <div class=\"form-control\">
                        {{ easyadmin_render_field_for_show_view(_entity_config.name, entity, metadata) }}
                    </div>

                    {% if metadata.help|default('') != '' %}
                        <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> {{ metadata.help|trans|raw }}</span>
                    {% endif %}
                </div>
            </div>
        {% endfor %}

        <div class=\"form-group form-actions\">
            <div class=\"col-sm-10 col-sm-offset-2\">
            {% block item_actions %}
                {% set _show_actions = easyadmin_get_actions_for_show_item(_entity_config.name) %}
                {% set _request_parameters = { entity: _entity_config.name, referer: app.request.query.get('referer') } %}

                {{ include('@EasyAdmin/default/includes/_actions.html.twig', {
                    actions: _show_actions,
                    request_parameters: _request_parameters,
                    translation_domain: _entity_config.translation_domain,
                    trans_parameters: _trans_parameters,
                    item_id: _entity_id
                }, with_context = false) }}
            {% endblock item_actions %}
            </div>
        </div>
    </div>

    {% block delete_form %}
        {{ include('@EasyAdmin/default/includes/_delete_form.html.twig', {
            view: 'show',
            referer: app.request.query.get('referer', ''),
            delete_form: delete_form,
            _translation_domain: _entity_config.translation_domain,
            _trans_parameters: _trans_parameters,
            _entity_config: _entity_config,
        }, with_context = false) }}
    {% endblock delete_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('a.action-delete').on('click', function(e) {
                e.preventDefault();

                \$('#modal-delete').modal({ backdrop: true, keyboard: true })
                    .off('click', '#modal-delete-button')
                    .on('click', '#modal-delete-button', function () {
                        \$('#delete-form').trigger('submit');
                    });
            });
        });
    </script>
{% endblock %}
", "EasyAdminBundle:default:show.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/show.html.twig");
    }
}

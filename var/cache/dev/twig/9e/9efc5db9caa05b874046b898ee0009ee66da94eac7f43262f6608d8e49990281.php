<?php

/* EasyAdminBundle:default:field_float.html.twig */
class __TwigTemplate_1ece9fefb3259ef4a2cb64cae138830306396145a9a0a708d1ceabe0c2e31179 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f32bd31d559041e57ddd1714242c489d6c5217394e3c724dde533d3f71f7fa3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f32bd31d559041e57ddd1714242c489d6c5217394e3c724dde533d3f71f7fa3->enter($__internal_7f32bd31d559041e57ddd1714242c489d6c5217394e3c724dde533d3f71f7fa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_float.html.twig"));

        $__internal_bfaebd348d9e5de3ef759d09dfec6c33bf50f50156c18a353d340754b47162b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfaebd348d9e5de3ef759d09dfec6c33bf50f50156c18a353d340754b47162b3->enter($__internal_bfaebd348d9e5de3ef759d09dfec6c33bf50f50156c18a353d340754b47162b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_float.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 2, $this->getSourceContext()); })()), "format", array()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })()), 2), "html", null, true);
            echo "
";
        }
        
        $__internal_7f32bd31d559041e57ddd1714242c489d6c5217394e3c724dde533d3f71f7fa3->leave($__internal_7f32bd31d559041e57ddd1714242c489d6c5217394e3c724dde533d3f71f7fa3_prof);

        
        $__internal_bfaebd348d9e5de3ef759d09dfec6c33bf50f50156c18a353d340754b47162b3->leave($__internal_bfaebd348d9e5de3ef759d09dfec6c33bf50f50156c18a353d340754b47162b3_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_float.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format(2) }}
{% endif %}
", "EasyAdminBundle:default:field_float.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_float.html.twig");
    }
}

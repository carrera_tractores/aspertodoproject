<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_887be3c0bd6d151476a6a3600d9fe8f0c074161870757444c3ea6091dc5910cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_882fbd1416ca7734913627de47c526cb66709ce3a0b2952c2d2c7d861a9290e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_882fbd1416ca7734913627de47c526cb66709ce3a0b2952c2d2c7d861a9290e2->enter($__internal_882fbd1416ca7734913627de47c526cb66709ce3a0b2952c2d2c7d861a9290e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_9fcf9a9f87bc174c123e1a3fdaeea5c319757342753b168aa6c303c4e54c0bb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fcf9a9f87bc174c123e1a3fdaeea5c319757342753b168aa6c303c4e54c0bb5->enter($__internal_9fcf9a9f87bc174c123e1a3fdaeea5c319757342753b168aa6c303c4e54c0bb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_882fbd1416ca7734913627de47c526cb66709ce3a0b2952c2d2c7d861a9290e2->leave($__internal_882fbd1416ca7734913627de47c526cb66709ce3a0b2952c2d2c7d861a9290e2_prof);

        
        $__internal_9fcf9a9f87bc174c123e1a3fdaeea5c319757342753b168aa6c303c4e54c0bb5->leave($__internal_9fcf9a9f87bc174c123e1a3fdaeea5c319757342753b168aa6c303c4e54c0bb5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}

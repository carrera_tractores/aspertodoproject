<?php

/* EasyAdminBundle:default:label_null.html.twig */
class __TwigTemplate_174509fd72f272f086858c0f374ebfe9c4eeb854a4fbc9fd4c43b9aad1c6a8b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72c4fd2744021a5adc96ed42bf11f63f3967359e6e4adcba5811d06f2bde5473 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72c4fd2744021a5adc96ed42bf11f63f3967359e6e4adcba5811d06f2bde5473->enter($__internal_72c4fd2744021a5adc96ed42bf11f63f3967359e6e4adcba5811d06f2bde5473_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_null.html.twig"));

        $__internal_4e194ecf4b578e88c293f786f115142712407ea52fe2d4eadb324b12db5a3237 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e194ecf4b578e88c293f786f115142712407ea52fe2d4eadb324b12db5a3237->enter($__internal_4e194ecf4b578e88c293f786f115142712407ea52fe2d4eadb324b12db5a3237_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_null.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.null", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_72c4fd2744021a5adc96ed42bf11f63f3967359e6e4adcba5811d06f2bde5473->leave($__internal_72c4fd2744021a5adc96ed42bf11f63f3967359e6e4adcba5811d06f2bde5473_prof);

        
        $__internal_4e194ecf4b578e88c293f786f115142712407ea52fe2d4eadb324b12db5a3237->leave($__internal_4e194ecf4b578e88c293f786f115142712407ea52fe2d4eadb324b12db5a3237_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_null.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label\">{{ 'label.null'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:label_null.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_null.html.twig");
    }
}

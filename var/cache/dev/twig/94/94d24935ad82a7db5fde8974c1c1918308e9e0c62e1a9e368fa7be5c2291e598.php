<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_129e5f0b4db0b2eea085e9bc604c2dda8d0ec0a1c79e334515c8b1d82097d66d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9e6bbefc048a220c85058fc3fed7cbe413db7c55a72ef846f001fd6bee5c6f7d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e6bbefc048a220c85058fc3fed7cbe413db7c55a72ef846f001fd6bee5c6f7d->enter($__internal_9e6bbefc048a220c85058fc3fed7cbe413db7c55a72ef846f001fd6bee5c6f7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        $__internal_115772b9848d8710d5f3351ba21bb5b3af2c90b7fd12377a3a86df8e49ee4b49 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_115772b9848d8710d5f3351ba21bb5b3af2c90b7fd12377a3a86df8e49ee4b49->enter($__internal_115772b9848d8710d5f3351ba21bb5b3af2c90b7fd12377a3a86df8e49ee4b49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo json_encode(array("error" => array("code" => (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 1, $this->getSourceContext()); })()), "message" => (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 1, $this->getSourceContext()); })()), "exception" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()), "toarray", array()))));
        echo "
";
        
        $__internal_9e6bbefc048a220c85058fc3fed7cbe413db7c55a72ef846f001fd6bee5c6f7d->leave($__internal_9e6bbefc048a220c85058fc3fed7cbe413db7c55a72ef846f001fd6bee5c6f7d_prof);

        
        $__internal_115772b9848d8710d5f3351ba21bb5b3af2c90b7fd12377a3a86df8e49ee4b49->leave($__internal_115772b9848d8710d5f3351ba21bb5b3af2c90b7fd12377a3a86df8e49ee4b49_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "@Twig/Exception/exception.json.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}

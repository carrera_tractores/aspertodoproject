<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_fc6ccb561475c2e59dfc9e771e5e68a1e36b0898f6cbaff59508c787bdcf89cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cf65ea0a3da0f104669aaa3cfdfadf671669e7475c0ad59df9493f860fa18d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8cf65ea0a3da0f104669aaa3cfdfadf671669e7475c0ad59df9493f860fa18d4->enter($__internal_8cf65ea0a3da0f104669aaa3cfdfadf671669e7475c0ad59df9493f860fa18d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_d67e9d10995a458bddf6e60e82c47e86c96db0050dcdd1e57a9a6f3f3bbe161f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d67e9d10995a458bddf6e60e82c47e86c96db0050dcdd1e57a9a6f3f3bbe161f->enter($__internal_d67e9d10995a458bddf6e60e82c47e86c96db0050dcdd1e57a9a6f3f3bbe161f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8cf65ea0a3da0f104669aaa3cfdfadf671669e7475c0ad59df9493f860fa18d4->leave($__internal_8cf65ea0a3da0f104669aaa3cfdfadf671669e7475c0ad59df9493f860fa18d4_prof);

        
        $__internal_d67e9d10995a458bddf6e60e82c47e86c96db0050dcdd1e57a9a6f3f3bbe161f->leave($__internal_d67e9d10995a458bddf6e60e82c47e86c96db0050dcdd1e57a9a6f3f3bbe161f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e83c6877f84953a4b37201aad4bdccbb501dbf2863524a7a4fd8c81a499f8ea4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e83c6877f84953a4b37201aad4bdccbb501dbf2863524a7a4fd8c81a499f8ea4->enter($__internal_e83c6877f84953a4b37201aad4bdccbb501dbf2863524a7a4fd8c81a499f8ea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_55890ba5cc8331946230656e9ea4adacf744ce23ec9a65a7d68ffd3ebf50e2a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55890ba5cc8331946230656e9ea4adacf744ce23ec9a65a7d68ffd3ebf50e2a8->enter($__internal_55890ba5cc8331946230656e9ea4adacf744ce23ec9a65a7d68ffd3ebf50e2a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_55890ba5cc8331946230656e9ea4adacf744ce23ec9a65a7d68ffd3ebf50e2a8->leave($__internal_55890ba5cc8331946230656e9ea4adacf744ce23ec9a65a7d68ffd3ebf50e2a8_prof);

        
        $__internal_e83c6877f84953a4b37201aad4bdccbb501dbf2863524a7a4fd8c81a499f8ea4->leave($__internal_e83c6877f84953a4b37201aad4bdccbb501dbf2863524a7a4fd8c81a499f8ea4_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login.html.twig");
    }
}

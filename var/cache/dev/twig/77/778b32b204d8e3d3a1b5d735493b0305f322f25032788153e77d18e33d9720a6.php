<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_f975041418afa65bf31af62b3e19565f240608314d7ec5fb00122815a74dbd14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9773d657a405ef78ecc5e6b98315d53b7c60cda816c5b4ba0f368fff94aa98e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9773d657a405ef78ecc5e6b98315d53b7c60cda816c5b4ba0f368fff94aa98e5->enter($__internal_9773d657a405ef78ecc5e6b98315d53b7c60cda816c5b4ba0f368fff94aa98e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_5eb344ef314f8870074bfa9f61891307ed1a794e3562f1a461386d27eee6b55d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5eb344ef314f8870074bfa9f61891307ed1a794e3562f1a461386d27eee6b55d->enter($__internal_5eb344ef314f8870074bfa9f61891307ed1a794e3562f1a461386d27eee6b55d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9773d657a405ef78ecc5e6b98315d53b7c60cda816c5b4ba0f368fff94aa98e5->leave($__internal_9773d657a405ef78ecc5e6b98315d53b7c60cda816c5b4ba0f368fff94aa98e5_prof);

        
        $__internal_5eb344ef314f8870074bfa9f61891307ed1a794e3562f1a461386d27eee6b55d->leave($__internal_5eb344ef314f8870074bfa9f61891307ed1a794e3562f1a461386d27eee6b55d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_247aa6500dbd39280e1cae0ab75fe6ff5f3a2351c26c55bd3a10e6c4a90a442b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_247aa6500dbd39280e1cae0ab75fe6ff5f3a2351c26c55bd3a10e6c4a90a442b->enter($__internal_247aa6500dbd39280e1cae0ab75fe6ff5f3a2351c26c55bd3a10e6c4a90a442b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1e5e3366ebbc85d9f3bd469364442de96f3e1d57d5b80a868a8dc303cc6e8f81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e5e3366ebbc85d9f3bd469364442de96f3e1d57d5b80a868a8dc303cc6e8f81->enter($__internal_1e5e3366ebbc85d9f3bd469364442de96f3e1d57d5b80a868a8dc303cc6e8f81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_1e5e3366ebbc85d9f3bd469364442de96f3e1d57d5b80a868a8dc303cc6e8f81->leave($__internal_1e5e3366ebbc85d9f3bd469364442de96f3e1d57d5b80a868a8dc303cc6e8f81_prof);

        
        $__internal_247aa6500dbd39280e1cae0ab75fe6ff5f3a2351c26c55bd3a10e6c4a90a442b->leave($__internal_247aa6500dbd39280e1cae0ab75fe6ff5f3a2351c26c55bd3a10e6c4a90a442b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}

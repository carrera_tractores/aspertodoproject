<?php

/* @FOSUser/Security/login_content.html.twig~ */
class __TwigTemplate_438533606c5268d8581829f938f2a8b4d26ff38bb8e1a3985156863d22246a42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17b7902c6f6bc7a9b82e78353df638cc03accb6ae85608ca23d1d43a5fde607b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17b7902c6f6bc7a9b82e78353df638cc03accb6ae85608ca23d1d43a5fde607b->enter($__internal_17b7902c6f6bc7a9b82e78353df638cc03accb6ae85608ca23d1d43a5fde607b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig~"));

        $__internal_317d9b2a5e97280d9f6629ed9256480e2f4791c5845a08e5fc4b5fbf43c2bd59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_317d9b2a5e97280d9f6629ed9256480e2f4791c5845a08e5fc4b5fbf43c2bd59->enter($__internal_317d9b2a5e97280d9f6629ed9256480e2f4791c5845a08e5fc4b5fbf43c2bd59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig~"));

        // line 1
        echo "<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
\t</head>

\t<body>

\t\t";
        // line 10
        echo "
\t\t";
        // line 11
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 11, $this->getSourceContext()); })())) {
            // line 12
            echo "    \t<div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 12, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
\t\t";
        }
        // line 14
        echo "
\t\t\t<form action=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    \t\t";
        // line 16
        if ((isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 16, $this->getSourceContext()); })())) {
            // line 17
            echo "
        \t\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"";
            // line 18
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 18, $this->getSourceContext()); })()), "html", null, true);
            echo "\" />
    \t\t";
        }
        // line 20
        echo "
    \t\t\t\t<label for=\"username\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    \t\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 22
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 22, $this->getSourceContext()); })()), "html", null, true);
        echo "\" required=\"required\" />

    \t\t\t\t<label for=\"password\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
    \t\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

\t\t\t\t\t<label for=\"remember_me\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>    \t\t\t
    \t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    \t\t
   \t\t<div id=\"caixa\">
   \t\t\t<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
   \t\t\t<a  id=\"registre\" href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.register", array(), "FOSUserBundle"), "html", null, true);
        echo "</a>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t\t
\t\t";
        // line 37
        $this->displayBlock('javascripts', $context, $blocks);
        // line 41
        echo "    
\t</body>

</html>
";
        
        $__internal_17b7902c6f6bc7a9b82e78353df638cc03accb6ae85608ca23d1d43a5fde607b->leave($__internal_17b7902c6f6bc7a9b82e78353df638cc03accb6ae85608ca23d1d43a5fde607b_prof);

        
        $__internal_317d9b2a5e97280d9f6629ed9256480e2f4791c5845a08e5fc4b5fbf43c2bd59->leave($__internal_317d9b2a5e97280d9f6629ed9256480e2f4791c5845a08e5fc4b5fbf43c2bd59_prof);

    }

    // line 37
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_423d2753bb006dd2be364028473b908a73a8dae9493037ae4ecda4ece6a67392 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_423d2753bb006dd2be364028473b908a73a8dae9493037ae4ecda4ece6a67392->enter($__internal_423d2753bb006dd2be364028473b908a73a8dae9493037ae4ecda4ece6a67392_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_4ca86bddc03ad83c8183ffc2c44b25438db2d3e5f9f925bb6cb0d3eedac66ed6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ca86bddc03ad83c8183ffc2c44b25438db2d3e5f9f925bb6cb0d3eedac66ed6->enter($__internal_4ca86bddc03ad83c8183ffc2c44b25438db2d3e5f9f925bb6cb0d3eedac66ed6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 38
        echo "      \t<script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" />
        \t<script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\" />
    \t";
        
        $__internal_4ca86bddc03ad83c8183ffc2c44b25438db2d3e5f9f925bb6cb0d3eedac66ed6->leave($__internal_4ca86bddc03ad83c8183ffc2c44b25438db2d3e5f9f925bb6cb0d3eedac66ed6_prof);

        
        $__internal_423d2753bb006dd2be364028473b908a73a8dae9493037ae4ecda4ece6a67392->leave($__internal_423d2753bb006dd2be364028473b908a73a8dae9493037ae4ecda4ece6a67392_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 39,  131 => 38,  122 => 37,  108 => 41,  106 => 37,  96 => 32,  92 => 31,  85 => 27,  79 => 24,  74 => 22,  70 => 21,  67 => 20,  62 => 18,  59 => 17,  57 => 16,  53 => 15,  50 => 14,  44 => 12,  42 => 11,  39 => 10,  31 => 4,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<html>
\t<head>
\t\t<meta charset=\"UTF-8\" />       
    \t<link rel=\"stylesheet\" href=\"{{ asset('css/login.css') }}\" />
\t</head>

\t<body>

\t\t{% trans_default_domain 'FOSUserBundle' %}

\t\t{% if error %}
    \t<div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
\t\t{% endif %}

\t\t\t<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
    \t\t{% if csrf_token %}

        \t\t\t<input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    \t\t{% endif %}

    \t\t\t\t<label for=\"username\">{{ 'security.login.username'|trans }}</label>
    \t\t\t\t<input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />

    \t\t\t\t<label for=\"password\">{{ 'security.login.password'|trans }}</label>
    \t\t\t\t<input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />

\t\t\t\t\t<label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>    \t\t\t
    \t\t\t\t<input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
    \t\t
   \t\t<div id=\"caixa\">
   \t\t\t<input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" />
   \t\t\t<a  id=\"registre\" href=\"{{ path('fos_user_registration_register') }}\">{{ 'layout.register'|trans({}, 'FOSUserBundle') }}</a>
\t\t\t</div>
\t\t\t
\t\t\t</form>
\t\t\t
\t\t{% block javascripts %}
      \t<script src=\"{{ asset('js/bootstrap.min.js') }}\" />
        \t<script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\" />
    \t{% endblock %}
    
\t</body>

</html>
", "@FOSUser/Security/login_content.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Security/login_content.html.twig~");
    }
}

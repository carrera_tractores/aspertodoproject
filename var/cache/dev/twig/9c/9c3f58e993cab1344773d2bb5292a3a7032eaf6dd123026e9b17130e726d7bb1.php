<?php

/* @EasyAdmin/default/field_image.html.twig */
class __TwigTemplate_dc5bbac6f1d6af526e7c1f69e2fb7e8a04f2d745527a381426c172987735e928 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2350b52b6fb22e8d1ad5469d79c48d0187ca9fce37361b457fb7346efd6e7761 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2350b52b6fb22e8d1ad5469d79c48d0187ca9fce37361b457fb7346efd6e7761->enter($__internal_2350b52b6fb22e8d1ad5469d79c48d0187ca9fce37361b457fb7346efd6e7761_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_image.html.twig"));

        $__internal_c2efda80713ba455ded4c28eae1b584c00612cfb6256a24b504e51771e0ccec4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c2efda80713ba455ded4c28eae1b584c00612cfb6256a24b504e51771e0ccec4->enter($__internal_c2efda80713ba455ded4c28eae1b584c00612cfb6256a24b504e51771e0ccec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_image.html.twig"));

        // line 1
        echo "<a href=\"#\" class=\"easyadmin-thumbnail\" data-featherlight=\"#easyadmin-lightbox-";
        echo twig_escape_filter($this->env, (isset($context["uuid"]) || array_key_exists("uuid", $context) ? $context["uuid"] : (function () { throw new Twig_Error_Runtime('Variable "uuid" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "\" data-featherlight-close-on-click=\"anywhere\">
    <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
        echo "\">
</a>

<div id=\"easyadmin-lightbox-";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["uuid"]) || array_key_exists("uuid", $context) ? $context["uuid"] : (function () { throw new Twig_Error_Runtime('Variable "uuid" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "\" class=\"easyadmin-lightbox\">
    <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 6, $this->getSourceContext()); })())), "html", null, true);
        echo "\">
</div>
";
        
        $__internal_2350b52b6fb22e8d1ad5469d79c48d0187ca9fce37361b457fb7346efd6e7761->leave($__internal_2350b52b6fb22e8d1ad5469d79c48d0187ca9fce37361b457fb7346efd6e7761_prof);

        
        $__internal_c2efda80713ba455ded4c28eae1b584c00612cfb6256a24b504e51771e0ccec4->leave($__internal_c2efda80713ba455ded4c28eae1b584c00612cfb6256a24b504e51771e0ccec4_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  36 => 5,  30 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a href=\"#\" class=\"easyadmin-thumbnail\" data-featherlight=\"#easyadmin-lightbox-{{ uuid }}\" data-featherlight-close-on-click=\"anywhere\">
    <img src=\"{{ asset(value) }}\">
</a>

<div id=\"easyadmin-lightbox-{{ uuid }}\" class=\"easyadmin-lightbox\">
    <img src=\"{{ asset(value) }}\">
</div>
", "@EasyAdmin/default/field_image.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_image.html.twig");
    }
}

<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_d956469a1391d1299249fbc7f4d7f0dcab306db70e6224c30b8e4b5dd9b16a09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16dc9207537c35c7c1c929b399fd49587f036885f5dbf3f8658905864faed308 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16dc9207537c35c7c1c929b399fd49587f036885f5dbf3f8658905864faed308->enter($__internal_16dc9207537c35c7c1c929b399fd49587f036885f5dbf3f8658905864faed308_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_f1ee5181b8534aec76e3f210151983ba8a40de267e60cc1fa095b2a4fef4a955 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1ee5181b8534aec76e3f210151983ba8a40de267e60cc1fa095b2a4fef4a955->enter($__internal_f1ee5181b8534aec76e3f210151983ba8a40de267e60cc1fa095b2a4fef4a955_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_16dc9207537c35c7c1c929b399fd49587f036885f5dbf3f8658905864faed308->leave($__internal_16dc9207537c35c7c1c929b399fd49587f036885f5dbf3f8658905864faed308_prof);

        
        $__internal_f1ee5181b8534aec76e3f210151983ba8a40de267e60cc1fa095b2a4fef4a955->leave($__internal_f1ee5181b8534aec76e3f210151983ba8a40de267e60cc1fa095b2a4fef4a955_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_ddd57bef5c58294971f99af85c0303f66ae61c984ab159390a8c93545562a0eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddd57bef5c58294971f99af85c0303f66ae61c984ab159390a8c93545562a0eb->enter($__internal_ddd57bef5c58294971f99af85c0303f66ae61c984ab159390a8c93545562a0eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_0fc768e8dbf45dc270c9ee20d905a44d253a137448e405acf40bcd20e5fb7aad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fc768e8dbf45dc270c9ee20d905a44d253a137448e405acf40bcd20e5fb7aad->enter($__internal_0fc768e8dbf45dc270c9ee20d905a44d253a137448e405acf40bcd20e5fb7aad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_0fc768e8dbf45dc270c9ee20d905a44d253a137448e405acf40bcd20e5fb7aad->leave($__internal_0fc768e8dbf45dc270c9ee20d905a44d253a137448e405acf40bcd20e5fb7aad_prof);

        
        $__internal_ddd57bef5c58294971f99af85c0303f66ae61c984ab159390a8c93545562a0eb->leave($__internal_ddd57bef5c58294971f99af85c0303f66ae61c984ab159390a8c93545562a0eb_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}

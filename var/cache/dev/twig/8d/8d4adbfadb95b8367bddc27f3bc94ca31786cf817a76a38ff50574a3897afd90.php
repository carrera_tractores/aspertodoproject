<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_66cddb2f595265ef55469e3edeb84aa115da38f77aab6167be380840462604c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46268194c05291c4c9bf031ff977a700eabc997552369ba42af3bfc236697d18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46268194c05291c4c9bf031ff977a700eabc997552369ba42af3bfc236697d18->enter($__internal_46268194c05291c4c9bf031ff977a700eabc997552369ba42af3bfc236697d18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_5c5c9aee8b2581b6384491d96e7d80987238afe8aaf6c6641d8bd4bcf7a93c8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c5c9aee8b2581b6384491d96e7d80987238afe8aaf6c6641d8bd4bcf7a93c8b->enter($__internal_5c5c9aee8b2581b6384491d96e7d80987238afe8aaf6c6641d8bd4bcf7a93c8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })()))));
        
        $__internal_46268194c05291c4c9bf031ff977a700eabc997552369ba42af3bfc236697d18->leave($__internal_46268194c05291c4c9bf031ff977a700eabc997552369ba42af3bfc236697d18_prof);

        
        $__internal_5c5c9aee8b2581b6384491d96e7d80987238afe8aaf6c6641d8bd4bcf7a93c8b->leave($__internal_5c5c9aee8b2581b6384491d96e7d80987238afe8aaf6c6641d8bd4bcf7a93c8b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.atom.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}

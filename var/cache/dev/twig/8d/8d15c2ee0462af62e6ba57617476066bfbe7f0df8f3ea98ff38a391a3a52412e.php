<?php

/* EasyAdminBundle:default:exception.html.twig */
class __TwigTemplate_6dc988ac51199df28f13f836eaafa0c60364ea008dd681c3ab395430b6d79ff4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_class' => array($this, 'block_body_class'),
            'page_title' => array($this, 'block_page_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return $this->loadTemplate(array(0 => ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), "templates", array(), "any", false, true), "layout", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), "templates", array(), "any", false, true), "layout", array()), "")) : ("")), 1 => $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.templates.layout"), 2 => "@EasyAdmin/default/layout.html.twig"), "EasyAdminBundle:default:exception.html.twig", 3);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c4253226d1aefb89ecfbd52fb65ef6067a900f5692269cbd465cbdc49d8bfd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c4253226d1aefb89ecfbd52fb65ef6067a900f5692269cbd465cbdc49d8bfd5->enter($__internal_9c4253226d1aefb89ecfbd52fb65ef6067a900f5692269cbd465cbdc49d8bfd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:exception.html.twig"));

        $__internal_3a6f2a4f2e3363c4ded94bcaf08f8e66e70050541764f23328b20fded923891f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a6f2a4f2e3363c4ded94bcaf08f8e66e70050541764f23328b20fded923891f->enter($__internal_3a6f2a4f2e3363c4ded94bcaf08f8e66e70050541764f23328b20fded923891f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:exception.html.twig"));

        // line 1
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 1, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 3
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9c4253226d1aefb89ecfbd52fb65ef6067a900f5692269cbd465cbdc49d8bfd5->leave($__internal_9c4253226d1aefb89ecfbd52fb65ef6067a900f5692269cbd465cbdc49d8bfd5_prof);

        
        $__internal_3a6f2a4f2e3363c4ded94bcaf08f8e66e70050541764f23328b20fded923891f->leave($__internal_3a6f2a4f2e3363c4ded94bcaf08f8e66e70050541764f23328b20fded923891f_prof);

    }

    // line 8
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_ad01f6c460e85a0606299d43f1477a9c0fa70c5e7e4c021044f6a1d9d772928f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad01f6c460e85a0606299d43f1477a9c0fa70c5e7e4c021044f6a1d9d772928f->enter($__internal_ad01f6c460e85a0606299d43f1477a9c0fa70c5e7e4c021044f6a1d9d772928f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_21c5bd5bfc87db3dfd8f5901cf5d9cbabe010e1e926d8cb120704eb7044916fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21c5bd5bfc87db3dfd8f5901cf5d9cbabe010e1e926d8cb120704eb7044916fe->enter($__internal_21c5bd5bfc87db3dfd8f5901cf5d9cbabe010e1e926d8cb120704eb7044916fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo "error";
        
        $__internal_21c5bd5bfc87db3dfd8f5901cf5d9cbabe010e1e926d8cb120704eb7044916fe->leave($__internal_21c5bd5bfc87db3dfd8f5901cf5d9cbabe010e1e926d8cb120704eb7044916fe_prof);

        
        $__internal_ad01f6c460e85a0606299d43f1477a9c0fa70c5e7e4c021044f6a1d9d772928f->leave($__internal_ad01f6c460e85a0606299d43f1477a9c0fa70c5e7e4c021044f6a1d9d772928f_prof);

    }

    // line 9
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_8bab594da71c929c25226b83eb234bfe2d3ec9c8d742278f7f4ddfd9ccbb651a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bab594da71c929c25226b83eb234bfe2d3ec9c8d742278f7f4ddfd9ccbb651a->enter($__internal_8bab594da71c929c25226b83eb234bfe2d3ec9c8d742278f7f4ddfd9ccbb651a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_4b16a2aadefb73694d8e2482595d8fd2785575c660582127ec2896cea844f9b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b16a2aadefb73694d8e2482595d8fd2785575c660582127ec2896cea844f9b9->enter($__internal_4b16a2aadefb73694d8e2482595d8fd2785575c660582127ec2896cea844f9b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->transchoice("errors", 1, array(), "EasyAdminBundle"), "html", null, true);
        
        $__internal_4b16a2aadefb73694d8e2482595d8fd2785575c660582127ec2896cea844f9b9->leave($__internal_4b16a2aadefb73694d8e2482595d8fd2785575c660582127ec2896cea844f9b9_prof);

        
        $__internal_8bab594da71c929c25226b83eb234bfe2d3ec9c8d742278f7f4ddfd9ccbb651a->leave($__internal_8bab594da71c929c25226b83eb234bfe2d3ec9c8d742278f7f4ddfd9ccbb651a_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_4c53eef12be6a5e11bbecf3ea11a3ad90f28dca8ef197591ec7da56bef38b49b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c53eef12be6a5e11bbecf3ea11a3ad90f28dca8ef197591ec7da56bef38b49b->enter($__internal_4c53eef12be6a5e11bbecf3ea11a3ad90f28dca8ef197591ec7da56bef38b49b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_d6740b6145b78db0fcc6671a064fdc954bc9a9c0f29d052ed43b08923ab62559 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6740b6145b78db0fcc6671a064fdc954bc9a9c0f29d052ed43b08923ab62559->enter($__internal_d6740b6145b78db0fcc6671a064fdc954bc9a9c0f29d052ed43b08923ab62559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    <section id=\"main\" class=\"content\">
        <div class=\"error-description\">
            <h1><i class=\"fa fa-exclamation-circle\"></i> ";
        // line 14
        $this->displayBlock("page_title", $context, $blocks);
        echo "</h1>

            <div class=\"error-message\">
                ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 17, $this->getSourceContext()); })()), "publicMessage", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 17, $this->getSourceContext()); })()), "translationParameters", array()), "EasyAdminBundle"), "html", null, true);
        echo "
            </div>
        </div>
    </section>
";
        
        $__internal_d6740b6145b78db0fcc6671a064fdc954bc9a9c0f29d052ed43b08923ab62559->leave($__internal_d6740b6145b78db0fcc6671a064fdc954bc9a9c0f29d052ed43b08923ab62559_prof);

        
        $__internal_4c53eef12be6a5e11bbecf3ea11a3ad90f28dca8ef197591ec7da56bef38b49b->leave($__internal_4c53eef12be6a5e11bbecf3ea11a3ad90f28dca8ef197591ec7da56bef38b49b_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 17,  93 => 14,  89 => 12,  80 => 11,  62 => 9,  44 => 8,  34 => 3,  32 => 1,  20 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% extends [
    _entity_config.templates.layout|default(''),
    easyadmin_config('design.templates.layout'),
    '@EasyAdmin/default/layout.html.twig'
] %}

{% block body_class 'error' %}
{% block page_title %}{{ 'errors'|transchoice(1, {}, 'EasyAdminBundle') }}{% endblock %}

{% block content %}
    <section id=\"main\" class=\"content\">
        <div class=\"error-description\">
            <h1><i class=\"fa fa-exclamation-circle\"></i> {{ block('page_title') }}</h1>

            <div class=\"error-message\">
                {{ exception.publicMessage|trans(exception.translationParameters, 'EasyAdminBundle') }}
            </div>
        </div>
    </section>
{% endblock %}
", "EasyAdminBundle:default:exception.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/exception.html.twig");
    }
}

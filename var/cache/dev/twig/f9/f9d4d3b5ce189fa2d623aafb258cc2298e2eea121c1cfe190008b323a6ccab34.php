<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_9d100ab2d6669b83347747545c4232484e0334c20e22c3c00b6f21375dffee5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0f5125278fb74e5a21fc5a970eb69a82c9bb14ae0f11f7f92db000bc8bb1b18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0f5125278fb74e5a21fc5a970eb69a82c9bb14ae0f11f7f92db000bc8bb1b18->enter($__internal_f0f5125278fb74e5a21fc5a970eb69a82c9bb14ae0f11f7f92db000bc8bb1b18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_0c02eaf4df6289ab4b23bd19a0c103eb25b9832528267454fe3039d91f63377d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0c02eaf4df6289ab4b23bd19a0c103eb25b9832528267454fe3039d91f63377d->enter($__internal_0c02eaf4df6289ab4b23bd19a0c103eb25b9832528267454fe3039d91f63377d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo "

*/
";
        
        $__internal_f0f5125278fb74e5a21fc5a970eb69a82c9bb14ae0f11f7f92db000bc8bb1b18->leave($__internal_f0f5125278fb74e5a21fc5a970eb69a82c9bb14ae0f11f7f92db000bc8bb1b18_prof);

        
        $__internal_0c02eaf4df6289ab4b23bd19a0c103eb25b9832528267454fe3039d91f63377d->leave($__internal_0c02eaf4df6289ab4b23bd19a0c103eb25b9832528267454fe3039d91f63377d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}

<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_9576916ad9db3d3f8c4f8c92726b9ef2dd4f0adb3332cc31988e9417d7ba5431 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ce055a0b0a5ae1cd1da4776250c5a2d4b6888b352ca365e5e8d7814041b3e51 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ce055a0b0a5ae1cd1da4776250c5a2d4b6888b352ca365e5e8d7814041b3e51->enter($__internal_1ce055a0b0a5ae1cd1da4776250c5a2d4b6888b352ca365e5e8d7814041b3e51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_7203367d89567da4944b430f2b84685d03aa39e8869234937bebef077a9663a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7203367d89567da4944b430f2b84685d03aa39e8869234937bebef077a9663a3->enter($__internal_7203367d89567da4944b430f2b84685d03aa39e8869234937bebef077a9663a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_1ce055a0b0a5ae1cd1da4776250c5a2d4b6888b352ca365e5e8d7814041b3e51->leave($__internal_1ce055a0b0a5ae1cd1da4776250c5a2d4b6888b352ca365e5e8d7814041b3e51_prof);

        
        $__internal_7203367d89567da4944b430f2b84685d03aa39e8869234937bebef077a9663a3->leave($__internal_7203367d89567da4944b430f2b84685d03aa39e8869234937bebef077a9663a3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.atom.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}

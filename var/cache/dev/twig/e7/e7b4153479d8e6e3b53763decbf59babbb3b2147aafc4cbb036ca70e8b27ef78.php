<?php

/* @WebProfiler/Icon/no.svg */
class __TwigTemplate_a4a38528371fbe8e60ff43c07d98dd7f44efb90d6b9e6c054e85c7540ac7a559 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f525fa49fa41eba950649bfe99bae0089cce6d39fef8e12987cd7ae76d497c88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f525fa49fa41eba950649bfe99bae0089cce6d39fef8e12987cd7ae76d497c88->enter($__internal_f525fa49fa41eba950649bfe99bae0089cce6d39fef8e12987cd7ae76d497c88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        $__internal_987509c3a97b289d1a4628fe6722c0e8863d24f419f8182bab62fac05d339283 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_987509c3a97b289d1a4628fe6722c0e8863d24f419f8182bab62fac05d339283->enter($__internal_987509c3a97b289d1a4628fe6722c0e8863d24f419f8182bab62fac05d339283_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/no.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
";
        
        $__internal_f525fa49fa41eba950649bfe99bae0089cce6d39fef8e12987cd7ae76d497c88->leave($__internal_f525fa49fa41eba950649bfe99bae0089cce6d39fef8e12987cd7ae76d497c88_prof);

        
        $__internal_987509c3a97b289d1a4628fe6722c0e8863d24f419f8182bab62fac05d339283->leave($__internal_987509c3a97b289d1a4628fe6722c0e8863d24f419f8182bab62fac05d339283_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/no.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#B0413E\" d=\"M10.4,8.4L8,6l2.4-2.4c0.8-0.8,0.7-1.6,0.2-2.2C10,0.9,9.2,0.8,8.4,1.6L6,4L3.6,1.6C2.8,0.8,2,0.9,1.4,1.4
    C0.9,2,0.8,2.8,1.6,3.6L4,6L1.6,8.4C0.8,9.2,0.9,10,1.4,10.6c0.6,0.6,1.4,0.6,2.2-0.2L6,8l2.4,2.4c0.8,0.8,1.6,0.7,2.2,0.2
    C11.1,10,11.2,9.2,10.4,8.4z\"/>
</svg>
", "@WebProfiler/Icon/no.svg", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/no.svg");
    }
}

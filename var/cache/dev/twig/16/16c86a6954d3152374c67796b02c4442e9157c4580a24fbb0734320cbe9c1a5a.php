<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_46e53f5a6cbcd61f548fa1da40271d1543ae2e578213151135a5db7949415ba5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bcf87c5a8156da57c6c297f02a40d1ea1c9a7dbcf8a486bf38010a3e2157db66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcf87c5a8156da57c6c297f02a40d1ea1c9a7dbcf8a486bf38010a3e2157db66->enter($__internal_bcf87c5a8156da57c6c297f02a40d1ea1c9a7dbcf8a486bf38010a3e2157db66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_0cf8854efff139546c5671824fe5ba6394dd05a188da9c82e8f9da657f6d45fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cf8854efff139546c5671824fe5ba6394dd05a188da9c82e8f9da657f6d45fd->enter($__internal_0cf8854efff139546c5671824fe5ba6394dd05a188da9c82e8f9da657f6d45fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_bcf87c5a8156da57c6c297f02a40d1ea1c9a7dbcf8a486bf38010a3e2157db66->leave($__internal_bcf87c5a8156da57c6c297f02a40d1ea1c9a7dbcf8a486bf38010a3e2157db66_prof);

        
        $__internal_0cf8854efff139546c5671824fe5ba6394dd05a188da9c82e8f9da657f6d45fd->leave($__internal_0cf8854efff139546c5671824fe5ba6394dd05a188da9c82e8f9da657f6d45fd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}

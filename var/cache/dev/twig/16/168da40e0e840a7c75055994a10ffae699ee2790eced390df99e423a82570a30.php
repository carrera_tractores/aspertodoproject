<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_8b98a9791e80042744c59df5197be71ee8f206cc4d335ecd9119eb90a1e90aa7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12aeab10af35ddacde4ac3e6261ca56a65922eca9a60095bd276dd9765288408 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_12aeab10af35ddacde4ac3e6261ca56a65922eca9a60095bd276dd9765288408->enter($__internal_12aeab10af35ddacde4ac3e6261ca56a65922eca9a60095bd276dd9765288408_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_2b8dc0560cab3234af5f844783c002bf27dad25478d7f60853858d6590fe69f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b8dc0560cab3234af5f844783c002bf27dad25478d7f60853858d6590fe69f2->enter($__internal_2b8dc0560cab3234af5f844783c002bf27dad25478d7f60853858d6590fe69f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_12aeab10af35ddacde4ac3e6261ca56a65922eca9a60095bd276dd9765288408->leave($__internal_12aeab10af35ddacde4ac3e6261ca56a65922eca9a60095bd276dd9765288408_prof);

        
        $__internal_2b8dc0560cab3234af5f844783c002bf27dad25478d7f60853858d6590fe69f2->leave($__internal_2b8dc0560cab3234af5f844783c002bf27dad25478d7f60853858d6590fe69f2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}

<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_1874d187fa32ef015d7cb3441a7d09a3e2aac56ec257d8afb779a99080af39a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a8bb32c9927107c7df3ef5c28a25daf05b4d72c5534615742cd86f5b3db4ffa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a8bb32c9927107c7df3ef5c28a25daf05b4d72c5534615742cd86f5b3db4ffa->enter($__internal_1a8bb32c9927107c7df3ef5c28a25daf05b4d72c5534615742cd86f5b3db4ffa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_d7777db362e4395e9b836ea373f928b2ee8db381aecaefd1140961e673c4023a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7777db362e4395e9b836ea373f928b2ee8db381aecaefd1140961e673c4023a->enter($__internal_d7777db362e4395e9b836ea373f928b2ee8db381aecaefd1140961e673c4023a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a8bb32c9927107c7df3ef5c28a25daf05b4d72c5534615742cd86f5b3db4ffa->leave($__internal_1a8bb32c9927107c7df3ef5c28a25daf05b4d72c5534615742cd86f5b3db4ffa_prof);

        
        $__internal_d7777db362e4395e9b836ea373f928b2ee8db381aecaefd1140961e673c4023a->leave($__internal_d7777db362e4395e9b836ea373f928b2ee8db381aecaefd1140961e673c4023a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c8d1ef38d392ba533c940026f54d02a275b83d8a54fcee71cdfa42db24906018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8d1ef38d392ba533c940026f54d02a275b83d8a54fcee71cdfa42db24906018->enter($__internal_c8d1ef38d392ba533c940026f54d02a275b83d8a54fcee71cdfa42db24906018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_b798b3f9ce8fbd2c1760f210363cbdfb0ba993aca84189dc6a3e444fb9fc1431 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b798b3f9ce8fbd2c1760f210363cbdfb0ba993aca84189dc6a3e444fb9fc1431->enter($__internal_b798b3f9ce8fbd2c1760f210363cbdfb0ba993aca84189dc6a3e444fb9fc1431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) || array_key_exists("tokenLifetime", $context) ? $context["tokenLifetime"] : (function () { throw new Twig_Error_Runtime('Variable "tokenLifetime" does not exist.', 7, $this->getSourceContext()); })())), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_b798b3f9ce8fbd2c1760f210363cbdfb0ba993aca84189dc6a3e444fb9fc1431->leave($__internal_b798b3f9ce8fbd2c1760f210363cbdfb0ba993aca84189dc6a3e444fb9fc1431_prof);

        
        $__internal_c8d1ef38d392ba533c940026f54d02a275b83d8a54fcee71cdfa42db24906018->leave($__internal_c8d1ef38d392ba533c940026f54d02a275b83d8a54fcee71cdfa42db24906018_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}

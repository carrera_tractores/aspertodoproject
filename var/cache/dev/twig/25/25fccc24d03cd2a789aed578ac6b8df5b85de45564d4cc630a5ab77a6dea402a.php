<?php

/* EasyAdminBundle:default:field_bigint.html.twig */
class __TwigTemplate_137c8d60d2aca767fb6e86eeded69f11ab4f3f35bf0e988303dd3d3be2645ca1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b22f3e0ee4c4114bb5a7813f6a15fe326b4379f218abe67a4611643cd782913 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8b22f3e0ee4c4114bb5a7813f6a15fe326b4379f218abe67a4611643cd782913->enter($__internal_8b22f3e0ee4c4114bb5a7813f6a15fe326b4379f218abe67a4611643cd782913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_bigint.html.twig"));

        $__internal_6361675a3db0ffb257ba856e992594761a93a76955d410e5c973ba351701c682 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6361675a3db0ffb257ba856e992594761a93a76955d410e5c973ba351701c682->enter($__internal_6361675a3db0ffb257ba856e992594761a93a76955d410e5c973ba351701c682_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_bigint.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 2, $this->getSourceContext()); })()), "format", array()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        }
        
        $__internal_8b22f3e0ee4c4114bb5a7813f6a15fe326b4379f218abe67a4611643cd782913->leave($__internal_8b22f3e0ee4c4114bb5a7813f6a15fe326b4379f218abe67a4611643cd782913_prof);

        
        $__internal_6361675a3db0ffb257ba856e992594761a93a76955d410e5c973ba351701c682->leave($__internal_6361675a3db0ffb257ba856e992594761a93a76955d410e5c973ba351701c682_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_bigint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_bigint.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_bigint.html.twig");
    }
}

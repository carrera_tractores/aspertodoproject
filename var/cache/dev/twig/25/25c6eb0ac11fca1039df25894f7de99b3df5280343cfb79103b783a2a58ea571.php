<?php

/* ADesignsCalendarBundle::calendar.html.twig */
class __TwigTemplate_1065401723676f5e09d428aa2b7adb84bc2ffa3e89ba6dc7d95ca9583652a547 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5b7f343a0c75156932a22ec86f10f24f26594fef44f7e8eed8658bc57999266 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5b7f343a0c75156932a22ec86f10f24f26594fef44f7e8eed8658bc57999266->enter($__internal_e5b7f343a0c75156932a22ec86f10f24f26594fef44f7e8eed8658bc57999266_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ADesignsCalendarBundle::calendar.html.twig"));

        $__internal_c3d1e25672d8c303ac33ded290812bda0c2792b4fb7265c9d33fa8f82f75c50d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3d1e25672d8c303ac33ded290812bda0c2792b4fb7265c9d33fa8f82f75c50d->enter($__internal_c3d1e25672d8c303ac33ded290812bda0c2792b4fb7265c9d33fa8f82f75c50d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ADesignsCalendarBundle::calendar.html.twig"));

        // line 1
        echo "<div id=\"calendar-holder\"></div>
";
        
        $__internal_e5b7f343a0c75156932a22ec86f10f24f26594fef44f7e8eed8658bc57999266->leave($__internal_e5b7f343a0c75156932a22ec86f10f24f26594fef44f7e8eed8658bc57999266_prof);

        
        $__internal_c3d1e25672d8c303ac33ded290812bda0c2792b4fb7265c9d33fa8f82f75c50d->leave($__internal_c3d1e25672d8c303ac33ded290812bda0c2792b4fb7265c9d33fa8f82f75c50d_prof);

    }

    public function getTemplateName()
    {
        return "ADesignsCalendarBundle::calendar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"calendar-holder\"></div>
", "ADesignsCalendarBundle::calendar.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/adesigns/calendar-bundle/ADesigns/CalendarBundle/Resources/views/calendar.html.twig");
    }
}

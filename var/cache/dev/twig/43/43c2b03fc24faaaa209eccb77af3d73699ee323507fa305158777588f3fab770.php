<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_431c608b764e345077641e100ea7b12f2c71d102185042b9d2d565bc4ea6b1aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aec642cfeb2bd502e02c2d5c7699ac07c2dd71b8b7b1644ee529f12dab78b9d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aec642cfeb2bd502e02c2d5c7699ac07c2dd71b8b7b1644ee529f12dab78b9d0->enter($__internal_aec642cfeb2bd502e02c2d5c7699ac07c2dd71b8b7b1644ee529f12dab78b9d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_9ffad7f600ea9e782a55f0325392bb0015bcb2884acf8ac4df126e8dd52c35fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ffad7f600ea9e782a55f0325392bb0015bcb2884acf8ac4df126e8dd52c35fa->enter($__internal_9ffad7f600ea9e782a55f0325392bb0015bcb2884acf8ac4df126e8dd52c35fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_aec642cfeb2bd502e02c2d5c7699ac07c2dd71b8b7b1644ee529f12dab78b9d0->leave($__internal_aec642cfeb2bd502e02c2d5c7699ac07c2dd71b8b7b1644ee529f12dab78b9d0_prof);

        
        $__internal_9ffad7f600ea9e782a55f0325392bb0015bcb2884acf8ac4df126e8dd52c35fa->leave($__internal_9ffad7f600ea9e782a55f0325392bb0015bcb2884acf8ac4df126e8dd52c35fa_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_aa208a8f9d5a86bc059d619b26b2fc2e39fa358ca56519752d6800e32f6533b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa208a8f9d5a86bc059d619b26b2fc2e39fa358ca56519752d6800e32f6533b7->enter($__internal_aa208a8f9d5a86bc059d619b26b2fc2e39fa358ca56519752d6800e32f6533b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_79860ae752655b55ab341010bc6ae36e74f02d51eabd9dba1dd4ab69cee57bad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79860ae752655b55ab341010bc6ae36e74f02d51eabd9dba1dd4ab69cee57bad->enter($__internal_79860ae752655b55ab341010bc6ae36e74f02d51eabd9dba1dd4ab69cee57bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 4, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle");
        
        $__internal_79860ae752655b55ab341010bc6ae36e74f02d51eabd9dba1dd4ab69cee57bad->leave($__internal_79860ae752655b55ab341010bc6ae36e74f02d51eabd9dba1dd4ab69cee57bad_prof);

        
        $__internal_aa208a8f9d5a86bc059d619b26b2fc2e39fa358ca56519752d6800e32f6533b7->leave($__internal_aa208a8f9d5a86bc059d619b26b2fc2e39fa358ca56519752d6800e32f6533b7_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_ef9c048ab0095b85e47cea103e29eddbe75a4f3a762351b8483583ebe4343637 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef9c048ab0095b85e47cea103e29eddbe75a4f3a762351b8483583ebe4343637->enter($__internal_ef9c048ab0095b85e47cea103e29eddbe75a4f3a762351b8483583ebe4343637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_43c40b493191fa753c99fb4f3bf894d0f0076d2c5ca281b5721517f68b3c86d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43c40b493191fa753c99fb4f3bf894d0f0076d2c5ca281b5721517f68b3c86d5->enter($__internal_43c40b493191fa753c99fb4f3bf894d0f0076d2c5ca281b5721517f68b3c86d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 10, $this->getSourceContext()); })()), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) || array_key_exists("confirmationUrl", $context) ? $context["confirmationUrl"] : (function () { throw new Twig_Error_Runtime('Variable "confirmationUrl" does not exist.', 10, $this->getSourceContext()); })())), "FOSUserBundle");
        echo "
";
        
        $__internal_43c40b493191fa753c99fb4f3bf894d0f0076d2c5ca281b5721517f68b3c86d5->leave($__internal_43c40b493191fa753c99fb4f3bf894d0f0076d2c5ca281b5721517f68b3c86d5_prof);

        
        $__internal_ef9c048ab0095b85e47cea103e29eddbe75a4f3a762351b8483583ebe4343637->leave($__internal_ef9c048ab0095b85e47cea103e29eddbe75a4f3a762351b8483583ebe4343637_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_b4f1dec64041c7d42a6677b5c67dd1df3131267d6cb1657cf8399e0912cee792 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4f1dec64041c7d42a6677b5c67dd1df3131267d6cb1657cf8399e0912cee792->enter($__internal_b4f1dec64041c7d42a6677b5c67dd1df3131267d6cb1657cf8399e0912cee792_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_9ce7c04ea970808f45dd62028e77a052cf43bac6bedbeb3ee28c81d29e370caf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ce7c04ea970808f45dd62028e77a052cf43bac6bedbeb3ee28c81d29e370caf->enter($__internal_9ce7c04ea970808f45dd62028e77a052cf43bac6bedbeb3ee28c81d29e370caf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_9ce7c04ea970808f45dd62028e77a052cf43bac6bedbeb3ee28c81d29e370caf->leave($__internal_9ce7c04ea970808f45dd62028e77a052cf43bac6bedbeb3ee28c81d29e370caf_prof);

        
        $__internal_b4f1dec64041c7d42a6677b5c67dd1df3131267d6cb1657cf8399e0912cee792->leave($__internal_b4f1dec64041c7d42a6677b5c67dd1df3131267d6cb1657cf8399e0912cee792_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}

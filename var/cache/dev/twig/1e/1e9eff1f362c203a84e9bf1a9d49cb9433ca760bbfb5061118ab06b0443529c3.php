<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_756255e069b186db411fa8ef7eeca7c1d3b454a33a6e6aede773135bf7d5c464 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8ac42739b7843b89748c3df99ef9e5d050c1398844dfeb87c37cbfde423066d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8ac42739b7843b89748c3df99ef9e5d050c1398844dfeb87c37cbfde423066d->enter($__internal_c8ac42739b7843b89748c3df99ef9e5d050c1398844dfeb87c37cbfde423066d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $__internal_e52e4a07046ab280710fb585162861807e9a447e470c5c6fad7846f1ef6ee41f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e52e4a07046ab280710fb585162861807e9a447e470c5c6fad7846f1ef6ee41f->enter($__internal_e52e4a07046ab280710fb585162861807e9a447e470c5c6fad7846f1ef6ee41f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c8ac42739b7843b89748c3df99ef9e5d050c1398844dfeb87c37cbfde423066d->leave($__internal_c8ac42739b7843b89748c3df99ef9e5d050c1398844dfeb87c37cbfde423066d_prof);

        
        $__internal_e52e4a07046ab280710fb585162861807e9a447e470c5c6fad7846f1ef6ee41f->leave($__internal_e52e4a07046ab280710fb585162861807e9a447e470c5c6fad7846f1ef6ee41f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2a3048b92dacc1cdb13fa877796b0560422e573f4d9c40100af991f39be13e54 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a3048b92dacc1cdb13fa877796b0560422e573f4d9c40100af991f39be13e54->enter($__internal_2a3048b92dacc1cdb13fa877796b0560422e573f4d9c40100af991f39be13e54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a7930f5d1b3dc2ab9057c890973eeba12bc88b2b84c387ade0ed7c2a171d8d90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7930f5d1b3dc2ab9057c890973eeba12bc88b2b84c387ade0ed7c2a171d8d90->enter($__internal_a7930f5d1b3dc2ab9057c890973eeba12bc88b2b84c387ade0ed7c2a171d8d90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_a7930f5d1b3dc2ab9057c890973eeba12bc88b2b84c387ade0ed7c2a171d8d90->leave($__internal_a7930f5d1b3dc2ab9057c890973eeba12bc88b2b84c387ade0ed7c2a171d8d90_prof);

        
        $__internal_2a3048b92dacc1cdb13fa877796b0560422e573f4d9c40100af991f39be13e54->leave($__internal_2a3048b92dacc1cdb13fa877796b0560422e573f4d9c40100af991f39be13e54_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:edit.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/edit.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_tel.html.twig */
class __TwigTemplate_b9330ec077848d3b179c89b94f7c24b7d9ccd4058ab7f2bc70219959b54819d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8f965ac159162dcaab64d03095591488c4614ab65fedac12b8ce94e85b9f116 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8f965ac159162dcaab64d03095591488c4614ab65fedac12b8ce94e85b9f116->enter($__internal_f8f965ac159162dcaab64d03095591488c4614ab65fedac12b8ce94e85b9f116_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_tel.html.twig"));

        $__internal_4866b0333f94d763b04336cf7d908afbcc184b3904fc551f725c988246fbbbdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4866b0333f94d763b04336cf7d908afbcc184b3904fc551f725c988246fbbbdd->enter($__internal_4866b0333f94d763b04336cf7d908afbcc184b3904fc551f725c988246fbbbdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_tel.html.twig"));

        // line 1
        echo "<a href=\"tel:";
        echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>
";
        
        $__internal_f8f965ac159162dcaab64d03095591488c4614ab65fedac12b8ce94e85b9f116->leave($__internal_f8f965ac159162dcaab64d03095591488c4614ab65fedac12b8ce94e85b9f116_prof);

        
        $__internal_4866b0333f94d763b04336cf7d908afbcc184b3904fc551f725c988246fbbbdd->leave($__internal_4866b0333f94d763b04336cf7d908afbcc184b3904fc551f725c988246fbbbdd_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_tel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a href=\"tel:{{ value }}\">{{ value }}</a>
", "EasyAdminBundle:default:field_tel.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_tel.html.twig");
    }
}

<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_01e4e0e8ba2de1be27cf443ab92ea0438288366fd4bc1cd57ff4aae9f7726847 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb5b74e9a5ab2d16b51e8f83db5d73ad4f73078b755670f70626b4599cff3ac7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb5b74e9a5ab2d16b51e8f83db5d73ad4f73078b755670f70626b4599cff3ac7->enter($__internal_bb5b74e9a5ab2d16b51e8f83db5d73ad4f73078b755670f70626b4599cff3ac7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_df1079a46e710eec7965070f9a7f48a1baa3e9dde00ec20bc84a0467f5724e40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df1079a46e710eec7965070f9a7f48a1baa3e9dde00ec20bc84a0467f5724e40->enter($__internal_df1079a46e710eec7965070f9a7f48a1baa3e9dde00ec20bc84a0467f5724e40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()))));
        // line 3
        echo "*/
";
        
        $__internal_bb5b74e9a5ab2d16b51e8f83db5d73ad4f73078b755670f70626b4599cff3ac7->leave($__internal_bb5b74e9a5ab2d16b51e8f83db5d73ad4f73078b755670f70626b4599cff3ac7_prof);

        
        $__internal_df1079a46e710eec7965070f9a7f48a1baa3e9dde00ec20bc84a0467f5724e40->leave($__internal_df1079a46e710eec7965070f9a7f48a1baa3e9dde00ec20bc84a0467f5724e40_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.css.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}

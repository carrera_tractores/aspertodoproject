<?php

/* calendari.html.twig */
class __TwigTemplate_659773ad692d954fc75f9f052dae1d6ff80a24a4884536a7d50996c9c7468d5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("@EasyAdmin/default/layout.html.twig", "calendari.html.twig", 3);
        $this->blocks = array(
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@EasyAdmin/default/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35f2be49479fc550d5489d869d799d2f1a6009b63a5d5aacb826c3f756a868df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35f2be49479fc550d5489d869d799d2f1a6009b63a5d5aacb826c3f756a868df->enter($__internal_35f2be49479fc550d5489d869d799d2f1a6009b63a5d5aacb826c3f756a868df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calendari.html.twig"));

        $__internal_0fdea13e5eec22d042c6f8c14b6bc84868b995641f680e8c5afe10cb80f29721 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fdea13e5eec22d042c6f8c14b6bc84868b995641f680e8c5afe10cb80f29721->enter($__internal_0fdea13e5eec22d042c6f8c14b6bc84868b995641f680e8c5afe10cb80f29721_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calendari.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35f2be49479fc550d5489d869d799d2f1a6009b63a5d5aacb826c3f756a868df->leave($__internal_35f2be49479fc550d5489d869d799d2f1a6009b63a5d5aacb826c3f756a868df_prof);

        
        $__internal_0fdea13e5eec22d042c6f8c14b6bc84868b995641f680e8c5afe10cb80f29721->leave($__internal_0fdea13e5eec22d042c6f8c14b6bc84868b995641f680e8c5afe10cb80f29721_prof);

    }

    // line 5
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_19866cf4d1be992d8ed09913c105d4416fc755e06f489dc50a41d15cb7296339 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19866cf4d1be992d8ed09913c105d4416fc755e06f489dc50a41d15cb7296339->enter($__internal_19866cf4d1be992d8ed09913c105d4416fc755e06f489dc50a41d15cb7296339_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_91deaaba2697c4a91f665028260bad92b168f65e6fe86198811f9e35a163d83e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91deaaba2697c4a91f665028260bad92b168f65e6fe86198811f9e35a163d83e->enter($__internal_91deaaba2697c4a91f665028260bad92b168f65e6fe86198811f9e35a163d83e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/css/fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/css/fullcalendar/calendar.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/calendari.css"), "html", null, true);
        echo "\" />

";
        // line 10
        $this->displayParentBlock("head_stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_91deaaba2697c4a91f665028260bad92b168f65e6fe86198811f9e35a163d83e->leave($__internal_91deaaba2697c4a91f665028260bad92b168f65e6fe86198811f9e35a163d83e_prof);

        
        $__internal_19866cf4d1be992d8ed09913c105d4416fc755e06f489dc50a41d15cb7296339->leave($__internal_19866cf4d1be992d8ed09913c105d4416fc755e06f489dc50a41d15cb7296339_prof);

    }

    // line 14
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_a7310174f57518448355b86b3a5a53290cfbac40aa40d23c6dbfebc6b4ea7e62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7310174f57518448355b86b3a5a53290cfbac40aa40d23c6dbfebc6b4ea7e62->enter($__internal_a7310174f57518448355b86b3a5a53290cfbac40aa40d23c6dbfebc6b4ea7e62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_4c9665c6d8ad0932300cf00bbdd05b5f8411f8213bb68655c3eb38020a6dcc2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c9665c6d8ad0932300cf00bbdd05b5f8411f8213bb68655c3eb38020a6dcc2d->enter($__internal_4c9665c6d8ad0932300cf00bbdd05b5f8411f8213bb68655c3eb38020a6dcc2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 15
        echo "
";
        // line 16
        $this->displayParentBlock("content_header", $context, $blocks);
        echo "

\t<h1>Calendari</h1>
\t";
        // line 19
        $this->loadTemplate("ADesignsCalendarBundle::calendar.html.twig", "calendari.html.twig", 19)->display($context);
        echo " 
\t
\t      ";
        // line 21
        $this->displayBlock('javascripts', $context, $blocks);
        // line 42
        echo "
";
        
        $__internal_4c9665c6d8ad0932300cf00bbdd05b5f8411f8213bb68655c3eb38020a6dcc2d->leave($__internal_4c9665c6d8ad0932300cf00bbdd05b5f8411f8213bb68655c3eb38020a6dcc2d_prof);

        
        $__internal_a7310174f57518448355b86b3a5a53290cfbac40aa40d23c6dbfebc6b4ea7e62->leave($__internal_a7310174f57518448355b86b3a5a53290cfbac40aa40d23c6dbfebc6b4ea7e62_prof);

    }

    // line 21
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9c5f78dd0380e538da46785a5d889dd290951d5fbac4b315ab17fed86b1dc2f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c5f78dd0380e538da46785a5d889dd290951d5fbac4b315ab17fed86b1dc2f5->enter($__internal_9c5f78dd0380e538da46785a5d889dd290951d5fbac4b315ab17fed86b1dc2f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_3e61c8ab46661fcc4ffc7a0323d591dfba09d0572fda1aad9c3d89a19b198374 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e61c8ab46661fcc4ffc7a0323d591dfba09d0572fda1aad9c3d89a19b198374->enter($__internal_3e61c8ab46661fcc4ffc7a0323d591dfba09d0572fda1aad9c3d89a19b198374_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 22
        echo "\t\t\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/calendar-settings.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    \t\t<script src=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>

\t\t\t
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/lib/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    \t\t<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/lib/moment.min.js"), "html", null, true);
        echo "\"></script>  
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/locale/ca.js"), "html", null, true);
        echo "\"></script>
\t\t\t
\t\t\t
    \t\t<script>
    \t\t\t\$(document).ready(function() {
    \t\t\t\t\$('#calendar').fullCalendar({
    \t\t\t\t\tlang: 'ca'
    \t\t\t\t});
    \t\t\t});
    \t\t</script> \t\t
    \t\t
    \t\t";
        
        $__internal_3e61c8ab46661fcc4ffc7a0323d591dfba09d0572fda1aad9c3d89a19b198374->leave($__internal_3e61c8ab46661fcc4ffc7a0323d591dfba09d0572fda1aad9c3d89a19b198374_prof);

        
        $__internal_9c5f78dd0380e538da46785a5d889dd290951d5fbac4b315ab17fed86b1dc2f5->leave($__internal_9c5f78dd0380e538da46785a5d889dd290951d5fbac4b315ab17fed86b1dc2f5_prof);

    }

    public function getTemplateName()
    {
        return "calendari.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 30,  146 => 29,  142 => 28,  136 => 25,  132 => 24,  128 => 23,  123 => 22,  114 => 21,  103 => 42,  101 => 21,  96 => 19,  90 => 16,  87 => 15,  78 => 14,  65 => 10,  60 => 8,  56 => 7,  51 => 6,  42 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/calendari.html.twig #}

{% extends '@EasyAdmin/default/layout.html.twig' %}

{% block head_stylesheets %}
<link rel=\"stylesheet\" href=\"{{ asset('bundles/adesignscalendar/css/fullcalendar/fullcalendar.css') }}\" />
<link rel=\"stylesheet\" href=\"{{ asset('bundles/adesignscalendar/css/fullcalendar/calendar.css') }}\" />
<link rel=\"stylesheet\" href=\"{{ asset('css/calendari.css') }}\" />

{{ parent() }}

{% endblock %}

{% block content_header %}

{{ parent() }}

\t<h1>Calendari</h1>
\t{% include 'ADesignsCalendarBundle::calendar.html.twig' %} 
\t
\t      {% block javascripts %}
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js') }}\"></script>
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/calendar-settings.js') }}\"></script>
\t\t\t<script src=\"{{ asset('bundles/fosjsrouting/js/router.js') }}\"></script>
    \t\t<script src=\"{{ path('fos_js_routing_js', {\"callback\": \"fos.Router.setData\"}) }}\"></script>

\t\t\t
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/lib/jquery-ui.min.js') }}\"></script>
    \t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/lib/moment.min.js') }}\"></script>  
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/locale/ca.js') }}\"></script>
\t\t\t
\t\t\t
    \t\t<script>
    \t\t\t\$(document).ready(function() {
    \t\t\t\t\$('#calendar').fullCalendar({
    \t\t\t\t\tlang: 'ca'
    \t\t\t\t});
    \t\t\t});
    \t\t</script> \t\t
    \t\t
    \t\t{% endblock %}

{% endblock %}
", "calendari.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/calendari.html.twig");
    }
}

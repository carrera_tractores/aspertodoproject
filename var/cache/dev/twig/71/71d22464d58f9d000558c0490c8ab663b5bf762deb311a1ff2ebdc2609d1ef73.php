<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_0685fc2d4c40f46f6320fdaa8645f4ac436a87a74dbd1362c524651233bafb58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9dfb2ed24a643fcd8eb7f2402d3752235beb009ee054f4c8b3f0b171b7d93b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9dfb2ed24a643fcd8eb7f2402d3752235beb009ee054f4c8b3f0b171b7d93b6->enter($__internal_a9dfb2ed24a643fcd8eb7f2402d3752235beb009ee054f4c8b3f0b171b7d93b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_2031b49ec44d3e8ce69efd7e87316a0517e260de196e1c8cb933c3f8403f02d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2031b49ec44d3e8ce69efd7e87316a0517e260de196e1c8cb933c3f8403f02d7->enter($__internal_2031b49ec44d3e8ce69efd7e87316a0517e260de196e1c8cb933c3f8403f02d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_a9dfb2ed24a643fcd8eb7f2402d3752235beb009ee054f4c8b3f0b171b7d93b6->leave($__internal_a9dfb2ed24a643fcd8eb7f2402d3752235beb009ee054f4c8b3f0b171b7d93b6_prof);

        
        $__internal_2031b49ec44d3e8ce69efd7e87316a0517e260de196e1c8cb933c3f8403f02d7->leave($__internal_2031b49ec44d3e8ce69efd7e87316a0517e260de196e1c8cb933c3f8403f02d7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}

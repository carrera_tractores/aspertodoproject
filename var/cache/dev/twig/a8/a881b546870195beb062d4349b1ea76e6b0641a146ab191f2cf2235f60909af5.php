<?php

/* VichUploaderBundle:Form:fields.html.twig */
class __TwigTemplate_83b7dfb48d4040f6e5b5d19e8b1dbaf023ab40be4325cefeca77fee56d05bbcf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'vich_file_row' => array($this, 'block_vich_file_row'),
            'vich_file_widget' => array($this, 'block_vich_file_widget'),
            'vich_image_row' => array($this, 'block_vich_image_row'),
            'vich_image_widget' => array($this, 'block_vich_image_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_239c7a9c56296c00a69fde55c657a264d2d8c307144366abd0f989367d9cf431 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_239c7a9c56296c00a69fde55c657a264d2d8c307144366abd0f989367d9cf431->enter($__internal_239c7a9c56296c00a69fde55c657a264d2d8c307144366abd0f989367d9cf431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "VichUploaderBundle:Form:fields.html.twig"));

        $__internal_93cfb8a2089e9cd3316aef80833857ba7f03918ef6904ee47b22fa9723c9c4ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93cfb8a2089e9cd3316aef80833857ba7f03918ef6904ee47b22fa9723c9c4ec->enter($__internal_93cfb8a2089e9cd3316aef80833857ba7f03918ef6904ee47b22fa9723c9c4ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "VichUploaderBundle:Form:fields.html.twig"));

        // line 1
        $this->displayBlock('vich_file_row', $context, $blocks);
        // line 5
        echo "
";
        // line 6
        $this->displayBlock('vich_file_widget', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('vich_image_row', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('vich_image_widget', $context, $blocks);
        
        $__internal_239c7a9c56296c00a69fde55c657a264d2d8c307144366abd0f989367d9cf431->leave($__internal_239c7a9c56296c00a69fde55c657a264d2d8c307144366abd0f989367d9cf431_prof);

        
        $__internal_93cfb8a2089e9cd3316aef80833857ba7f03918ef6904ee47b22fa9723c9c4ec->leave($__internal_93cfb8a2089e9cd3316aef80833857ba7f03918ef6904ee47b22fa9723c9c4ec_prof);

    }

    // line 1
    public function block_vich_file_row($context, array $blocks = array())
    {
        $__internal_1ce1cb0b8f684349f275345fd13b802bb6df39d3ad78111d07224dd8d1fe15db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ce1cb0b8f684349f275345fd13b802bb6df39d3ad78111d07224dd8d1fe15db->enter($__internal_1ce1cb0b8f684349f275345fd13b802bb6df39d3ad78111d07224dd8d1fe15db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_row"));

        $__internal_eb539c195ac8b24c1c39fbbbedd1c293b3c23aa8e1aeb2a6e12f9931bb6f71a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb539c195ac8b24c1c39fbbbedd1c293b3c23aa8e1aeb2a6e12f9931bb6f71a7->enter($__internal_eb539c195ac8b24c1c39fbbbedd1c293b3c23aa8e1aeb2a6e12f9931bb6f71a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_row"));

        // line 2
        $context["force_error"] = true;
        // line 3
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_eb539c195ac8b24c1c39fbbbedd1c293b3c23aa8e1aeb2a6e12f9931bb6f71a7->leave($__internal_eb539c195ac8b24c1c39fbbbedd1c293b3c23aa8e1aeb2a6e12f9931bb6f71a7_prof);

        
        $__internal_1ce1cb0b8f684349f275345fd13b802bb6df39d3ad78111d07224dd8d1fe15db->leave($__internal_1ce1cb0b8f684349f275345fd13b802bb6df39d3ad78111d07224dd8d1fe15db_prof);

    }

    // line 6
    public function block_vich_file_widget($context, array $blocks = array())
    {
        $__internal_14e5d9e2b3c2efe6351289c870a907f9f4903118406910b92593af5b4fdab03c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_14e5d9e2b3c2efe6351289c870a907f9f4903118406910b92593af5b4fdab03c->enter($__internal_14e5d9e2b3c2efe6351289c870a907f9f4903118406910b92593af5b4fdab03c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_widget"));

        $__internal_3866612ba832aff2982025c04cf5b2802c2bcf13778c0dab665e36c6e9e19788 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3866612ba832aff2982025c04cf5b2802c2bcf13778c0dab665e36c6e9e19788->enter($__internal_3866612ba832aff2982025c04cf5b2802c2bcf13778c0dab665e36c6e9e19788_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_file_widget"));

        // line 7
        ob_start();
        // line 8
        echo "    <div class=\"vich-file\">
        ";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), "file", array()), 'widget');
        echo "
        ";
        // line 10
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "delete", array(), "any", true, true)) {
            // line 11
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 11, $this->getSourceContext()); })()), "delete", array()), 'row');
            echo "
        ";
        }
        // line 13
        echo "
        ";
        // line 14
        if ((array_key_exists("download_uri", $context) && (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 14, $this->getSourceContext()); })()))) {
            // line 15
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 15, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("download", array(), "VichUploaderBundle"), "html", null, true);
            echo "</a>
        ";
        }
        // line 17
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_3866612ba832aff2982025c04cf5b2802c2bcf13778c0dab665e36c6e9e19788->leave($__internal_3866612ba832aff2982025c04cf5b2802c2bcf13778c0dab665e36c6e9e19788_prof);

        
        $__internal_14e5d9e2b3c2efe6351289c870a907f9f4903118406910b92593af5b4fdab03c->leave($__internal_14e5d9e2b3c2efe6351289c870a907f9f4903118406910b92593af5b4fdab03c_prof);

    }

    // line 21
    public function block_vich_image_row($context, array $blocks = array())
    {
        $__internal_4db0877dcc9917211e1bcb054fa9e7ab8fb26ce60f70108695a50d022ad85112 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4db0877dcc9917211e1bcb054fa9e7ab8fb26ce60f70108695a50d022ad85112->enter($__internal_4db0877dcc9917211e1bcb054fa9e7ab8fb26ce60f70108695a50d022ad85112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_row"));

        $__internal_eda0abce992c48a884f2cf120f6292ec4337857392e5431536212afaccbba24c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eda0abce992c48a884f2cf120f6292ec4337857392e5431536212afaccbba24c->enter($__internal_eda0abce992c48a884f2cf120f6292ec4337857392e5431536212afaccbba24c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_row"));

        // line 22
        $context["force_error"] = true;
        // line 23
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_eda0abce992c48a884f2cf120f6292ec4337857392e5431536212afaccbba24c->leave($__internal_eda0abce992c48a884f2cf120f6292ec4337857392e5431536212afaccbba24c_prof);

        
        $__internal_4db0877dcc9917211e1bcb054fa9e7ab8fb26ce60f70108695a50d022ad85112->leave($__internal_4db0877dcc9917211e1bcb054fa9e7ab8fb26ce60f70108695a50d022ad85112_prof);

    }

    // line 26
    public function block_vich_image_widget($context, array $blocks = array())
    {
        $__internal_4b474ef1a749bbaf2248fbe80cdd497762834b932d664b30581173e833743711 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b474ef1a749bbaf2248fbe80cdd497762834b932d664b30581173e833743711->enter($__internal_4b474ef1a749bbaf2248fbe80cdd497762834b932d664b30581173e833743711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_widget"));

        $__internal_3f30d6580f1140fdb25b9902e628c2d5c552d2f7e3dda54a46fed3038c4acdf9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f30d6580f1140fdb25b9902e628c2d5c552d2f7e3dda54a46fed3038c4acdf9->enter($__internal_3f30d6580f1140fdb25b9902e628c2d5c552d2f7e3dda54a46fed3038c4acdf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "vich_image_widget"));

        // line 27
        ob_start();
        // line 28
        echo "    <div class=\"vich-image\">
        ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 29, $this->getSourceContext()); })()), "file", array()), 'widget');
        echo "
        ";
        // line 30
        if (twig_get_attribute($this->env, $this->getSourceContext(), ($context["form"] ?? null), "delete", array(), "any", true, true)) {
            // line 31
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 31, $this->getSourceContext()); })()), "delete", array()), 'row');
            echo "
        ";
        }
        // line 33
        echo "
        ";
        // line 34
        if ((array_key_exists("download_uri", $context) && (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 34, $this->getSourceContext()); })()))) {
            // line 35
            echo "         <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 35, $this->getSourceContext()); })()), "html", null, true);
            echo "\" alt=\"\" /></a>
        ";
        }
        // line 37
        echo "        ";
        if ((((isset($context["show_download_link"]) || array_key_exists("show_download_link", $context) ? $context["show_download_link"] : (function () { throw new Twig_Error_Runtime('Variable "show_download_link" does not exist.', 37, $this->getSourceContext()); })()) && array_key_exists("download_uri", $context)) && (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 37, $this->getSourceContext()); })()))) {
            // line 38
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, (isset($context["download_uri"]) || array_key_exists("download_uri", $context) ? $context["download_uri"] : (function () { throw new Twig_Error_Runtime('Variable "download_uri" does not exist.', 38, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("download", array(), "VichUploaderBundle"), "html", null, true);
            echo "</a>
        ";
        }
        // line 40
        echo "    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_3f30d6580f1140fdb25b9902e628c2d5c552d2f7e3dda54a46fed3038c4acdf9->leave($__internal_3f30d6580f1140fdb25b9902e628c2d5c552d2f7e3dda54a46fed3038c4acdf9_prof);

        
        $__internal_4b474ef1a749bbaf2248fbe80cdd497762834b932d664b30581173e833743711->leave($__internal_4b474ef1a749bbaf2248fbe80cdd497762834b932d664b30581173e833743711_prof);

    }

    public function getTemplateName()
    {
        return "VichUploaderBundle:Form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  197 => 40,  189 => 38,  186 => 37,  178 => 35,  176 => 34,  173 => 33,  167 => 31,  165 => 30,  161 => 29,  158 => 28,  156 => 27,  147 => 26,  137 => 23,  135 => 22,  126 => 21,  114 => 17,  106 => 15,  104 => 14,  101 => 13,  95 => 11,  93 => 10,  89 => 9,  86 => 8,  84 => 7,  75 => 6,  65 => 3,  63 => 2,  54 => 1,  44 => 26,  41 => 25,  39 => 21,  36 => 20,  34 => 6,  31 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block vich_file_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock %}

{% block vich_file_widget %}
{% spaceless %}
    <div class=\"vich-file\">
        {{ form_widget(form.file) }}
        {% if form.delete is defined %}
        {{ form_row(form.delete) }}
        {% endif %}

        {% if download_uri is defined and download_uri %}
        <a href=\"{{ download_uri }}\">{{ 'download'|trans({}, 'VichUploaderBundle') }}</a>
        {% endif %}
    </div>
{% endspaceless %}
{% endblock %}

{% block vich_image_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock %}

{% block vich_image_widget %}
{% spaceless %}
    <div class=\"vich-image\">
        {{ form_widget(form.file) }}
        {% if form.delete is defined %}
        {{ form_row(form.delete) }}
        {% endif %}

        {% if download_uri is defined and download_uri %}
         <a href=\"{{ download_uri }}\"><img src=\"{{ download_uri }}\" alt=\"\" /></a>
        {% endif %}
        {% if show_download_link and download_uri is defined and download_uri%}
        <a href=\"{{ download_uri }}\">{{ 'download'|trans({}, 'VichUploaderBundle') }}</a>
        {% endif %}
    </div>
{% endspaceless %}
{% endblock %}
", "VichUploaderBundle:Form:fields.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/vich/uploader-bundle/Resources/views/Form/fields.html.twig");
    }
}

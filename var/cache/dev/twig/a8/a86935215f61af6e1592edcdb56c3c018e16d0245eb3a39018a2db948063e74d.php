<?php

/* TwigBundle:Exception:traces.xml.twig */
class __TwigTemplate_aa8e149db66947ce2752edd8d156adead8034ae7cff2bd3ba0e8a251ad212ff0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_17b5460dc1ca51c27ca3e15ff5faf17c333bffe4668fbf37dd76b064f9af2b05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17b5460dc1ca51c27ca3e15ff5faf17c333bffe4668fbf37dd76b064f9af2b05->enter($__internal_17b5460dc1ca51c27ca3e15ff5faf17c333bffe4668fbf37dd76b064f9af2b05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        $__internal_54e20f086ca4ac45208fb5c7835b9a261cd33bf9c8b5901348f4fa82889a0764 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54e20f086ca4ac45208fb5c7835b9a261cd33bf9c8b5901348f4fa82889a0764->enter($__internal_54e20f086ca4ac45208fb5c7835b9a261cd33bf9c8b5901348f4fa82889a0764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:traces.xml.twig"));

        // line 1
        echo "        <traces>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()), "trace", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
            // line 3
            echo "            <trace>
";
            // line 4
            $this->loadTemplate("@Twig/Exception/trace.txt.twig", "TwigBundle:Exception:traces.xml.twig", 4)->display(array("trace" => $context["trace"]));
            // line 5
            echo "
            </trace>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "        </traces>
";
        
        $__internal_17b5460dc1ca51c27ca3e15ff5faf17c333bffe4668fbf37dd76b064f9af2b05->leave($__internal_17b5460dc1ca51c27ca3e15ff5faf17c333bffe4668fbf37dd76b064f9af2b05_prof);

        
        $__internal_54e20f086ca4ac45208fb5c7835b9a261cd33bf9c8b5901348f4fa82889a0764->leave($__internal_54e20f086ca4ac45208fb5c7835b9a261cd33bf9c8b5901348f4fa82889a0764_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 8,  37 => 5,  35 => 4,  32 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("        <traces>
{% for trace in exception.trace %}
            <trace>
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

            </trace>
{% endfor %}
        </traces>
", "TwigBundle:Exception:traces.xml.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.xml.twig");
    }
}

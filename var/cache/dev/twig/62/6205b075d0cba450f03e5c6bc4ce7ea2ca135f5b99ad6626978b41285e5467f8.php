<?php

/* @Framework/Form/form_label.html.php */
class __TwigTemplate_ea2d8ba0729407ecd1c2236c343b8a2aa5f9bcd8feb1c6182276a68619b86bcc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_189726d4a67260a4cfa4b9c12d2cd40f8b62b651b5dd5e0b9f31f6de658ac2ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_189726d4a67260a4cfa4b9c12d2cd40f8b62b651b5dd5e0b9f31f6de658ac2ad->enter($__internal_189726d4a67260a4cfa4b9c12d2cd40f8b62b651b5dd5e0b9f31f6de658ac2ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        $__internal_07787571d68bfe7890a068a44d416f7fcba3ca4f124bec5b58eec1162646a5b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07787571d68bfe7890a068a44d416f7fcba3ca4f124bec5b58eec1162646a5b8->enter($__internal_07787571d68bfe7890a068a44d416f7fcba3ca4f124bec5b58eec1162646a5b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_label.html.php"));

        // line 1
        echo "<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label <?php foreach (\$label_attr as \$k => \$v) { printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
";
        
        $__internal_189726d4a67260a4cfa4b9c12d2cd40f8b62b651b5dd5e0b9f31f6de658ac2ad->leave($__internal_189726d4a67260a4cfa4b9c12d2cd40f8b62b651b5dd5e0b9f31f6de658ac2ad_prof);

        
        $__internal_07787571d68bfe7890a068a44d416f7fcba3ca4f124bec5b58eec1162646a5b8->leave($__internal_07787571d68bfe7890a068a44d416f7fcba3ca4f124bec5b58eec1162646a5b8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_label.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (false !== \$label): ?>
<?php if (\$required) { \$label_attr['class'] = trim((isset(\$label_attr['class']) ? \$label_attr['class'] : '').' required'); } ?>
<?php if (!\$compound) { \$label_attr['for'] = \$id; } ?>
<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<label <?php foreach (\$label_attr as \$k => \$v) { printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)); } ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></label>
<?php endif ?>
", "@Framework/Form/form_label.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_label.html.php");
    }
}

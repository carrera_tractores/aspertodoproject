<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_ddbe951828621a72a0276d52179fe570e221bce8c0d70d9c3dffd0a45d62b1dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68c4a0e48f1341a6cfeb4196922faaa10db2d21e08c1b6372d1c219121538503 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68c4a0e48f1341a6cfeb4196922faaa10db2d21e08c1b6372d1c219121538503->enter($__internal_68c4a0e48f1341a6cfeb4196922faaa10db2d21e08c1b6372d1c219121538503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_71548ef9e867e3c027026ddf60b47648eeca32f21288bac5b2e02c345154b208 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71548ef9e867e3c027026ddf60b47648eeca32f21288bac5b2e02c345154b208->enter($__internal_71548ef9e867e3c027026ddf60b47648eeca32f21288bac5b2e02c345154b208_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_68c4a0e48f1341a6cfeb4196922faaa10db2d21e08c1b6372d1c219121538503->leave($__internal_68c4a0e48f1341a6cfeb4196922faaa10db2d21e08c1b6372d1c219121538503_prof);

        
        $__internal_71548ef9e867e3c027026ddf60b47648eeca32f21288bac5b2e02c345154b208->leave($__internal_71548ef9e867e3c027026ddf60b47648eeca32f21288bac5b2e02c345154b208_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}

<?php

/* @EasyAdmin/default/field_text.html.twig */
class __TwigTemplate_fdab84122253e7784bccfffdcc97c6ed96503e41c4b8dd6c48a23ce89115a8e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c9e31255bde403c06f5c9e16ce5af9bafadc43d499e4217df023e80dea60038 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c9e31255bde403c06f5c9e16ce5af9bafadc43d499e4217df023e80dea60038->enter($__internal_2c9e31255bde403c06f5c9e16ce5af9bafadc43d499e4217df023e80dea60038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_text.html.twig"));

        $__internal_fa8f84647887ebc8aea902751d9c174336428c5c5cd67fd6dfc2b6d3688afa7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa8f84647887ebc8aea902751d9c174336428c5c5cd67fd6dfc2b6d3688afa7c->enter($__internal_fa8f84647887ebc8aea902751d9c174336428c5c5cd67fd6dfc2b6d3688afa7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_text.html.twig"));

        // line 1
        if (((isset($context["view"]) || array_key_exists("view", $context) ? $context["view"] : (function () { throw new Twig_Error_Runtime('Variable "view" does not exist.', 1, $this->getSourceContext()); })()) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })()), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        }
        
        $__internal_2c9e31255bde403c06f5c9e16ce5af9bafadc43d499e4217df023e80dea60038->leave($__internal_2c9e31255bde403c06f5c9e16ce5af9bafadc43d499e4217df023e80dea60038_prof);

        
        $__internal_fa8f84647887ebc8aea902751d9c174336428c5c5cd67fd6dfc2b6d3688afa7c->leave($__internal_fa8f84647887ebc8aea902751d9c174336428c5c5cd67fd6dfc2b6d3688afa7c_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value|nl2br }}
{% else %}
    {{ value|easyadmin_truncate }}
{% endif %}
", "@EasyAdmin/default/field_text.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_text.html.twig");
    }
}

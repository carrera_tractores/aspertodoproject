<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_22d336d196c6f720a77570a0604006bea5a31776d7e5fb0a362a1ab92e37d2b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_927bc75338194757aefbf89c7257713a6580e565efc6466736239fbda01a32c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_927bc75338194757aefbf89c7257713a6580e565efc6466736239fbda01a32c4->enter($__internal_927bc75338194757aefbf89c7257713a6580e565efc6466736239fbda01a32c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $__internal_72c0bb0d482277fad025cc585cfc181739c736830ebe017a51d90c62199f2e2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72c0bb0d482277fad025cc585cfc181739c736830ebe017a51d90c62199f2e2a->enter($__internal_72c0bb0d482277fad025cc585cfc181739c736830ebe017a51d90c62199f2e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_927bc75338194757aefbf89c7257713a6580e565efc6466736239fbda01a32c4->leave($__internal_927bc75338194757aefbf89c7257713a6580e565efc6466736239fbda01a32c4_prof);

        
        $__internal_72c0bb0d482277fad025cc585cfc181739c736830ebe017a51d90c62199f2e2a->leave($__internal_72c0bb0d482277fad025cc585cfc181739c736830ebe017a51d90c62199f2e2a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_18f2097067ac9f2c910a8c07a14e9b36b793d600130c012800ed9fdecf9fd01d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18f2097067ac9f2c910a8c07a14e9b36b793d600130c012800ed9fdecf9fd01d->enter($__internal_18f2097067ac9f2c910a8c07a14e9b36b793d600130c012800ed9fdecf9fd01d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_440b74b7a01a4b07638e2ad83c37ac8bfd58bb7ca40652308bc60e8082721721 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_440b74b7a01a4b07638e2ad83c37ac8bfd58bb7ca40652308bc60e8082721721->enter($__internal_440b74b7a01a4b07638e2ad83c37ac8bfd58bb7ca40652308bc60e8082721721_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_440b74b7a01a4b07638e2ad83c37ac8bfd58bb7ca40652308bc60e8082721721->leave($__internal_440b74b7a01a4b07638e2ad83c37ac8bfd58bb7ca40652308bc60e8082721721_prof);

        
        $__internal_18f2097067ac9f2c910a8c07a14e9b36b793d600130c012800ed9fdecf9fd01d->leave($__internal_18f2097067ac9f2c910a8c07a14e9b36b793d600130c012800ed9fdecf9fd01d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:edit.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/edit.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_id.html.twig */
class __TwigTemplate_730bd8c7fb85b457ae91c79d60fa01517edcc045404af04f3c6a7631f9fb08d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83c8a9dcb3e87149305e29baf110764a078b623a20204fafa663c003593b6d52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_83c8a9dcb3e87149305e29baf110764a078b623a20204fafa663c003593b6d52->enter($__internal_83c8a9dcb3e87149305e29baf110764a078b623a20204fafa663c003593b6d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_id.html.twig"));

        $__internal_927e32bf4638e159a9689f7b4549757cc84148b60d3a87e1e8fd7e94559526d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_927e32bf4638e159a9689f7b4549757cc84148b60d3a87e1e8fd7e94559526d4->enter($__internal_927e32bf4638e159a9689f7b4549757cc84148b60d3a87e1e8fd7e94559526d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_id.html.twig"));

        // line 2
        echo twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })()), "html", null, true);
        echo "
";
        
        $__internal_83c8a9dcb3e87149305e29baf110764a078b623a20204fafa663c003593b6d52->leave($__internal_83c8a9dcb3e87149305e29baf110764a078b623a20204fafa663c003593b6d52_prof);

        
        $__internal_927e32bf4638e159a9689f7b4549757cc84148b60d3a87e1e8fd7e94559526d4->leave($__internal_927e32bf4638e159a9689f7b4549757cc84148b60d3a87e1e8fd7e94559526d4_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_id.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# this field template is used to avoid formatting the special ID attribute as a number #}
{{ value }}
", "EasyAdminBundle:default:field_id.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_id.html.twig");
    }
}

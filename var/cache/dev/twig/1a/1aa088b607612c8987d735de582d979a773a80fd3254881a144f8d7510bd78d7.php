<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_f234bff881849d31431d50b0f4caaad116099ea2e2a7f83b5af691962ed538d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e299556ce094ec286e56661404b0b95c1cb443712e47c8db4995b2db4515a169 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e299556ce094ec286e56661404b0b95c1cb443712e47c8db4995b2db4515a169->enter($__internal_e299556ce094ec286e56661404b0b95c1cb443712e47c8db4995b2db4515a169_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_1febd40f266e3af4c6225ebeda0e284f35a97eebbdeb0135c5ae59f2c1420955 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1febd40f266e3af4c6225ebeda0e284f35a97eebbdeb0135c5ae59f2c1420955->enter($__internal_1febd40f266e3af4c6225ebeda0e284f35a97eebbdeb0135c5ae59f2c1420955_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e299556ce094ec286e56661404b0b95c1cb443712e47c8db4995b2db4515a169->leave($__internal_e299556ce094ec286e56661404b0b95c1cb443712e47c8db4995b2db4515a169_prof);

        
        $__internal_1febd40f266e3af4c6225ebeda0e284f35a97eebbdeb0135c5ae59f2c1420955->leave($__internal_1febd40f266e3af4c6225ebeda0e284f35a97eebbdeb0135c5ae59f2c1420955_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_590203a07e6b09b231c2191502aea7035cc443382a2793a1a77d6a9935500767 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_590203a07e6b09b231c2191502aea7035cc443382a2793a1a77d6a9935500767->enter($__internal_590203a07e6b09b231c2191502aea7035cc443382a2793a1a77d6a9935500767_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_fe51aced67f3975d6aeba83080ccb49a39da51ec42d72db6a00b76b3c57bdd3a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe51aced67f3975d6aeba83080ccb49a39da51ec42d72db6a00b76b3c57bdd3a->enter($__internal_fe51aced67f3975d6aeba83080ccb49a39da51ec42d72db6a00b76b3c57bdd3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_fe51aced67f3975d6aeba83080ccb49a39da51ec42d72db6a00b76b3c57bdd3a->leave($__internal_fe51aced67f3975d6aeba83080ccb49a39da51ec42d72db6a00b76b3c57bdd3a_prof);

        
        $__internal_590203a07e6b09b231c2191502aea7035cc443382a2793a1a77d6a9935500767->leave($__internal_590203a07e6b09b231c2191502aea7035cc443382a2793a1a77d6a9935500767_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_datetimetz.html.twig */
class __TwigTemplate_620a3385298446ce7873198e73c7758f83de6d9c8a4af88f2e5551aef40afea7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9801fd36a24dbd5ef24d0ddf46c34655b28b8191e055a4478d2f39a3b35db949 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9801fd36a24dbd5ef24d0ddf46c34655b28b8191e055a4478d2f39a3b35db949->enter($__internal_9801fd36a24dbd5ef24d0ddf46c34655b28b8191e055a4478d2f39a3b35db949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetimetz.html.twig"));

        $__internal_6d63e195e41cf75509543c92506b30de4631f6456fae539d362d4e54c92487ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d63e195e41cf75509543c92506b30de4631f6456fae539d362d4e54c92487ac->enter($__internal_6d63e195e41cf75509543c92506b30de4631f6456fae539d362d4e54c92487ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_datetimetz.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_9801fd36a24dbd5ef24d0ddf46c34655b28b8191e055a4478d2f39a3b35db949->leave($__internal_9801fd36a24dbd5ef24d0ddf46c34655b28b8191e055a4478d2f39a3b35db949_prof);

        
        $__internal_6d63e195e41cf75509543c92506b30de4631f6456fae539d362d4e54c92487ac->leave($__internal_6d63e195e41cf75509543c92506b30de4631f6456fae539d362d4e54c92487ac_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_datetimetz.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "EasyAdminBundle:default:field_datetimetz.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_datetimetz.html.twig");
    }
}

<?php

/* gestiousuaris.html.twig~ */
class __TwigTemplate_5f11209233ad5538e90578adda99e78f669e66c1d01f93b9e947438eae5fd61c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_305a41c8c0323b5ce4f6b4dd005067af54ce108741308c5a063caf81c9bdd3ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_305a41c8c0323b5ce4f6b4dd005067af54ce108741308c5a063caf81c9bdd3ab->enter($__internal_305a41c8c0323b5ce4f6b4dd005067af54ce108741308c5a063caf81c9bdd3ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestiousuaris.html.twig~"));

        $__internal_fd709a5efc6994d486664c5287acccfbf0f1eb0a0d82258d57350c844eac6d43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd709a5efc6994d486664c5287acccfbf0f1eb0a0d82258d57350c844eac6d43->enter($__internal_fd709a5efc6994d486664c5287acccfbf0f1eb0a0d82258d57350c844eac6d43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "gestiousuaris.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/gestiousuaris.css"), "html", null, true);
        echo "\" /> 
        
        <link rel='stylesheet' type='text/css' href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/fullcalendar.min.css"), "html", null, true);
        echo "\" />
        <link rel='stylesheet' media='print' href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/fullcalendar.print.min.css"), "html", null, true);
        echo "\" />  
            
        
    </head>
    <body>
    \t";
        // line 19
        $this->displayBlock('body', $context, $blocks);
        // line 20
        echo "\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola ";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 24, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Gestió Usuaris</h1>
      <hr>

<table border=\"1\">
    <tr>
        <th>id</th>
        <th>nom</th>
        <th>nom usuari</th>
        <th>cognom</th>
        <th>contrasenya</th>
        <th>imatge</th>
        <th>id rol</th>
    </tr>
    ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["viewusuaris"]) || array_key_exists("viewusuaris", $context) ? $context["viewusuaris"] : (function () { throw new Twig_Error_Runtime('Variable "viewusuaris" does not exist.', 60, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 61
            echo "        <tr>
            <td>";
            // line 62
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["usuaris"]) || array_key_exists("usuaris", $context) ? $context["usuaris"] : (function () { throw new Twig_Error_Runtime('Variable "usuaris" does not exist.', 62, $this->getSourceContext()); })()), "id", array()), "html", null, true);
            echo "</td>

        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "</table>
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div id=\"afegir_usuaris\">
\t\t\t<div id=\"imatge_afegir\"></div>
\t\t\t<a href=\"afegir_usuari\">Afegir usuari</a>
\t\t</div>
           
    \t<br>
      </div>

\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        ";
        // line 99
        $this->displayBlock('javascripts', $context, $blocks);
        // line 103
        echo "    </body>
</html>
";
        
        $__internal_305a41c8c0323b5ce4f6b4dd005067af54ce108741308c5a063caf81c9bdd3ab->leave($__internal_305a41c8c0323b5ce4f6b4dd005067af54ce108741308c5a063caf81c9bdd3ab_prof);

        
        $__internal_fd709a5efc6994d486664c5287acccfbf0f1eb0a0d82258d57350c844eac6d43->leave($__internal_fd709a5efc6994d486664c5287acccfbf0f1eb0a0d82258d57350c844eac6d43_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b06044e3df1aa9d51cd620118c9dc47e89f063a33917b88984021ec317faeced = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b06044e3df1aa9d51cd620118c9dc47e89f063a33917b88984021ec317faeced->enter($__internal_b06044e3df1aa9d51cd620118c9dc47e89f063a33917b88984021ec317faeced_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c63ab9ce6a153220628da4de2f768ccd5ead73f5ebaa1f114847dbb7026ee8b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c63ab9ce6a153220628da4de2f768ccd5ead73f5ebaa1f114847dbb7026ee8b9->enter($__internal_c63ab9ce6a153220628da4de2f768ccd5ead73f5ebaa1f114847dbb7026ee8b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Gestió d'Usuaris";
        
        $__internal_c63ab9ce6a153220628da4de2f768ccd5ead73f5ebaa1f114847dbb7026ee8b9->leave($__internal_c63ab9ce6a153220628da4de2f768ccd5ead73f5ebaa1f114847dbb7026ee8b9_prof);

        
        $__internal_b06044e3df1aa9d51cd620118c9dc47e89f063a33917b88984021ec317faeced->leave($__internal_b06044e3df1aa9d51cd620118c9dc47e89f063a33917b88984021ec317faeced_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0883a385f6d09336bf363b09d410f1aa065e952291a4acba9b72c8004f732cad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0883a385f6d09336bf363b09d410f1aa065e952291a4acba9b72c8004f732cad->enter($__internal_0883a385f6d09336bf363b09d410f1aa065e952291a4acba9b72c8004f732cad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e21c8eb43f1e766ee4fbf60a04db1348c60abdfb31266f50c2fc0b515e28ebaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e21c8eb43f1e766ee4fbf60a04db1348c60abdfb31266f50c2fc0b515e28ebaa->enter($__internal_e21c8eb43f1e766ee4fbf60a04db1348c60abdfb31266f50c2fc0b515e28ebaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_e21c8eb43f1e766ee4fbf60a04db1348c60abdfb31266f50c2fc0b515e28ebaa->leave($__internal_e21c8eb43f1e766ee4fbf60a04db1348c60abdfb31266f50c2fc0b515e28ebaa_prof);

        
        $__internal_0883a385f6d09336bf363b09d410f1aa065e952291a4acba9b72c8004f732cad->leave($__internal_0883a385f6d09336bf363b09d410f1aa065e952291a4acba9b72c8004f732cad_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_878be0ccdd6ba8baade614ba3c78a44117b9b8cab84264ff1f2def757aae9438 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_878be0ccdd6ba8baade614ba3c78a44117b9b8cab84264ff1f2def757aae9438->enter($__internal_878be0ccdd6ba8baade614ba3c78a44117b9b8cab84264ff1f2def757aae9438_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3f8692f4fdca45395ede1b0ea151a28310a12507ed3f37af419445ec48f2ecdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f8692f4fdca45395ede1b0ea151a28310a12507ed3f37af419445ec48f2ecdd->enter($__internal_3f8692f4fdca45395ede1b0ea151a28310a12507ed3f37af419445ec48f2ecdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_3f8692f4fdca45395ede1b0ea151a28310a12507ed3f37af419445ec48f2ecdd->leave($__internal_3f8692f4fdca45395ede1b0ea151a28310a12507ed3f37af419445ec48f2ecdd_prof);

        
        $__internal_878be0ccdd6ba8baade614ba3c78a44117b9b8cab84264ff1f2def757aae9438->leave($__internal_878be0ccdd6ba8baade614ba3c78a44117b9b8cab84264ff1f2def757aae9438_prof);

    }

    // line 99
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fff75794d63399a9e158dbf8632acb3ca1cb60cad66cf6ff73b8ed4e789aa72b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fff75794d63399a9e158dbf8632acb3ca1cb60cad66cf6ff73b8ed4e789aa72b->enter($__internal_fff75794d63399a9e158dbf8632acb3ca1cb60cad66cf6ff73b8ed4e789aa72b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_43371192960129bfd0ad16f0d7775aee09a0bfc7261f67f1adc0af2a4c7d3e61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43371192960129bfd0ad16f0d7775aee09a0bfc7261f67f1adc0af2a4c7d3e61->enter($__internal_43371192960129bfd0ad16f0d7775aee09a0bfc7261f67f1adc0af2a4c7d3e61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 100
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>  
        ";
        
        $__internal_43371192960129bfd0ad16f0d7775aee09a0bfc7261f67f1adc0af2a4c7d3e61->leave($__internal_43371192960129bfd0ad16f0d7775aee09a0bfc7261f67f1adc0af2a4c7d3e61_prof);

        
        $__internal_fff75794d63399a9e158dbf8632acb3ca1cb60cad66cf6ff73b8ed4e789aa72b->leave($__internal_fff75794d63399a9e158dbf8632acb3ca1cb60cad66cf6ff73b8ed4e789aa72b_prof);

    }

    public function getTemplateName()
    {
        return "gestiousuaris.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 101,  249 => 100,  240 => 99,  223 => 19,  206 => 6,  188 => 5,  176 => 103,  174 => 99,  139 => 66,  129 => 62,  126 => 61,  122 => 60,  83 => 24,  79 => 23,  74 => 20,  72 => 19,  64 => 14,  60 => 13,  55 => 11,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Gestió d'Usuaris{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/gestiousuaris.css') }}\" /> 
        
        <link rel='stylesheet' type='text/css' href=\"{{ asset('fullcalendar/fullcalendar.min.css') }}\" />
        <link rel='stylesheet' media='print' href=\"{{ asset('fullcalendar/fullcalendar.print.min.css') }}\" />  
            
        
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"{{ path('fos_user_security_logout') }}\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola {{ app.user.username }}!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Gestió Usuaris</h1>
      <hr>

<table border=\"1\">
    <tr>
        <th>id</th>
        <th>nom</th>
        <th>nom usuari</th>
        <th>cognom</th>
        <th>contrasenya</th>
        <th>imatge</th>
        <th>id rol</th>
    </tr>
    {% for user in viewusuaris %}
        <tr>
            <td>{{ usuaris.id }}</td>

        </tr>
    {% endfor %}
</table>
      
      
      
      
      
      
      
      
      
      
      
      
      
      <div id=\"afegir_usuaris\">
\t\t\t<div id=\"imatge_afegir\"></div>
\t\t\t<a href=\"afegir_usuari\">Afegir usuari</a>
\t\t</div>
           
    \t<br>
      </div>

\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>  
        {% endblock %}
    </body>
</html>
", "gestiousuaris.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/gestiousuaris.html.twig~");
    }
}

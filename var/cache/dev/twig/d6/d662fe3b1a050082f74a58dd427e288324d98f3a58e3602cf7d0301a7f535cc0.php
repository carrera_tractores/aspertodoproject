<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_fd635963d1abdc9ce6223d5efc2e55b36ef97335cd765281309b6b382c5cdda1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f893f2215375cb054f5b5c3d7db0efcbdf0d0126e7ce87e8ec76a5601c55eecd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f893f2215375cb054f5b5c3d7db0efcbdf0d0126e7ce87e8ec76a5601c55eecd->enter($__internal_f893f2215375cb054f5b5c3d7db0efcbdf0d0126e7ce87e8ec76a5601c55eecd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_59b73191ba2b7c0f70c4352954b50bf145d6ca0641099432e0e3699e0e935b1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59b73191ba2b7c0f70c4352954b50bf145d6ca0641099432e0e3699e0e935b1c->enter($__internal_59b73191ba2b7c0f70c4352954b50bf145d6ca0641099432e0e3699e0e935b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_f893f2215375cb054f5b5c3d7db0efcbdf0d0126e7ce87e8ec76a5601c55eecd->leave($__internal_f893f2215375cb054f5b5c3d7db0efcbdf0d0126e7ce87e8ec76a5601c55eecd_prof);

        
        $__internal_59b73191ba2b7c0f70c4352954b50bf145d6ca0641099432e0e3699e0e935b1c->leave($__internal_59b73191ba2b7c0f70c4352954b50bf145d6ca0641099432e0e3699e0e935b1c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}

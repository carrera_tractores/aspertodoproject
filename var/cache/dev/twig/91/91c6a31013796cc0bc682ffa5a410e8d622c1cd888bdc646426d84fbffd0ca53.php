<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_c6b64821d9b07d391513dd1b7dfccab032d0897e05ed127575ff84ce92e8fc08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9882ec019514834c6cb3717fcea05da2448a92e9e99b9eb2310dc0374a8a8a7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9882ec019514834c6cb3717fcea05da2448a92e9e99b9eb2310dc0374a8a8a7c->enter($__internal_9882ec019514834c6cb3717fcea05da2448a92e9e99b9eb2310dc0374a8a8a7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_5e068d657599d4faca05d32b645c334b7d410189f8820eb60203c912061b2484 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e068d657599d4faca05d32b645c334b7d410189f8820eb60203c912061b2484->enter($__internal_5e068d657599d4faca05d32b645c334b7d410189f8820eb60203c912061b2484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9882ec019514834c6cb3717fcea05da2448a92e9e99b9eb2310dc0374a8a8a7c->leave($__internal_9882ec019514834c6cb3717fcea05da2448a92e9e99b9eb2310dc0374a8a8a7c_prof);

        
        $__internal_5e068d657599d4faca05d32b645c334b7d410189f8820eb60203c912061b2484->leave($__internal_5e068d657599d4faca05d32b645c334b7d410189f8820eb60203c912061b2484_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d8f00cd38ab4b082172eb9f11dad41ad5f2cb03feac7f80c2eda2ca8b53d4803 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8f00cd38ab4b082172eb9f11dad41ad5f2cb03feac7f80c2eda2ca8b53d4803->enter($__internal_d8f00cd38ab4b082172eb9f11dad41ad5f2cb03feac7f80c2eda2ca8b53d4803_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6f02e511fcee6660ab33b500e7516a0d9f4c98fc16e466589b2a0a0502e5e629 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f02e511fcee6660ab33b500e7516a0d9f4c98fc16e466589b2a0a0502e5e629->enter($__internal_6f02e511fcee6660ab33b500e7516a0d9f4c98fc16e466589b2a0a0502e5e629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_6f02e511fcee6660ab33b500e7516a0d9f4c98fc16e466589b2a0a0502e5e629->leave($__internal_6f02e511fcee6660ab33b500e7516a0d9f4c98fc16e466589b2a0a0502e5e629_prof);

        
        $__internal_d8f00cd38ab4b082172eb9f11dad41ad5f2cb03feac7f80c2eda2ca8b53d4803->leave($__internal_d8f00cd38ab4b082172eb9f11dad41ad5f2cb03feac7f80c2eda2ca8b53d4803_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Profile/show.html.twig");
    }
}

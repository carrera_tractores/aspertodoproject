<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_8902eff569665652943363963110963487c1fb93ea7592ae30a67c0bd20d96a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4889b16b88dfa5559c8a23487f76f55f07c5f7577eaf01c77db24f7a4e84fa24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4889b16b88dfa5559c8a23487f76f55f07c5f7577eaf01c77db24f7a4e84fa24->enter($__internal_4889b16b88dfa5559c8a23487f76f55f07c5f7577eaf01c77db24f7a4e84fa24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_ea553b77580c5deb4484064df09c30d7056429b6e05f9a338a46d25ea12c88b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea553b77580c5deb4484064df09c30d7056429b6e05f9a338a46d25ea12c88b6->enter($__internal_ea553b77580c5deb4484064df09c30d7056429b6e05f9a338a46d25ea12c88b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_4889b16b88dfa5559c8a23487f76f55f07c5f7577eaf01c77db24f7a4e84fa24->leave($__internal_4889b16b88dfa5559c8a23487f76f55f07c5f7577eaf01c77db24f7a4e84fa24_prof);

        
        $__internal_ea553b77580c5deb4484064df09c30d7056429b6e05f9a338a46d25ea12c88b6->leave($__internal_ea553b77580c5deb4484064df09c30d7056429b6e05f9a338a46d25ea12c88b6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}

<?php

/* @EasyAdmin/default/field_string.html.twig */
class __TwigTemplate_d6ca496acb9c5534875709bfd768db4a0eb645646925a29165afda521115a4e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd8cb098b57a1da2f043c9274da5c030d1df2a3e648b605bd78db860315f72ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd8cb098b57a1da2f043c9274da5c030d1df2a3e648b605bd78db860315f72ae->enter($__internal_fd8cb098b57a1da2f043c9274da5c030d1df2a3e648b605bd78db860315f72ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_string.html.twig"));

        $__internal_10d047f875698d8daba7cc1f23a1f3a8d011e674a4a8dedb84d8f863edf892f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10d047f875698d8daba7cc1f23a1f3a8d011e674a4a8dedb84d8f863edf892f8->enter($__internal_10d047f875698d8daba7cc1f23a1f3a8d011e674a4a8dedb84d8f863edf892f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_string.html.twig"));

        // line 1
        if (((isset($context["view"]) || array_key_exists("view", $context) ? $context["view"] : (function () { throw new Twig_Error_Runtime('Variable "view" does not exist.', 1, $this->getSourceContext()); })()) == "show")) {
            // line 2
            echo "    ";
            echo nl2br(twig_escape_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })()), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->truncateText($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        }
        
        $__internal_fd8cb098b57a1da2f043c9274da5c030d1df2a3e648b605bd78db860315f72ae->leave($__internal_fd8cb098b57a1da2f043c9274da5c030d1df2a3e648b605bd78db860315f72ae_prof);

        
        $__internal_10d047f875698d8daba7cc1f23a1f3a8d011e674a4a8dedb84d8f863edf892f8->leave($__internal_10d047f875698d8daba7cc1f23a1f3a8d011e674a4a8dedb84d8f863edf892f8_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if view == 'show' %}
    {{ value|nl2br }}
{% else %}
    {{ value|easyadmin_truncate }}
{% endif %}
", "@EasyAdmin/default/field_string.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_string.html.twig");
    }
}

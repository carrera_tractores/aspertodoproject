<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_7aab7beee3759b4011b5682b3a74ccece4e774cbe66034ad0ed68831695383e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9d146cabd9a7b285becdfe526fb3bed72b25b2ac7274415025b63f2e6535441 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9d146cabd9a7b285becdfe526fb3bed72b25b2ac7274415025b63f2e6535441->enter($__internal_e9d146cabd9a7b285becdfe526fb3bed72b25b2ac7274415025b63f2e6535441_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_9306351563c922aecb1ac5e47fd3b1fc7235ac4686b92c498002cd33a4b5652b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9306351563c922aecb1ac5e47fd3b1fc7235ac4686b92c498002cd33a4b5652b->enter($__internal_9306351563c922aecb1ac5e47fd3b1fc7235ac4686b92c498002cd33a4b5652b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) || array_key_exists("widget", $context) ? $context["widget"] : (function () { throw new Twig_Error_Runtime('Variable "widget" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_e9d146cabd9a7b285becdfe526fb3bed72b25b2ac7274415025b63f2e6535441->leave($__internal_e9d146cabd9a7b285becdfe526fb3bed72b25b2ac7274415025b63f2e6535441_prof);

        
        $__internal_9306351563c922aecb1ac5e47fd3b1fc7235ac4686b92c498002cd33a4b5652b->leave($__internal_9306351563c922aecb1ac5e47fd3b1fc7235ac4686b92c498002cd33a4b5652b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}

<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_6b72fb4e2fdf9cb058d105b9c748e772d8e3b4ccee13114042ebb523e981b704 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d58ec48d0bfb20ab3eddc7d286d52d43315baba17a8bd369164af5e2d8af2d94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d58ec48d0bfb20ab3eddc7d286d52d43315baba17a8bd369164af5e2d8af2d94->enter($__internal_d58ec48d0bfb20ab3eddc7d286d52d43315baba17a8bd369164af5e2d8af2d94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_30361c984d823f2b45cb68fe597360221adfb3befddac23bcaeed7ff21fd650a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30361c984d823f2b45cb68fe597360221adfb3befddac23bcaeed7ff21fd650a->enter($__internal_30361c984d823f2b45cb68fe597360221adfb3befddac23bcaeed7ff21fd650a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_d58ec48d0bfb20ab3eddc7d286d52d43315baba17a8bd369164af5e2d8af2d94->leave($__internal_d58ec48d0bfb20ab3eddc7d286d52d43315baba17a8bd369164af5e2d8af2d94_prof);

        
        $__internal_30361c984d823f2b45cb68fe597360221adfb3befddac23bcaeed7ff21fd650a->leave($__internal_30361c984d823f2b45cb68fe597360221adfb3befddac23bcaeed7ff21fd650a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}

<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_dc3650f6ad492213e23a1fae0a6b4592e0542f934f1662ac5cfcf4120a48585e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce05ca1c0679c6eeb44e20cdda726466181d3d3535e623051ce30b3c7845386b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce05ca1c0679c6eeb44e20cdda726466181d3d3535e623051ce30b3c7845386b->enter($__internal_ce05ca1c0679c6eeb44e20cdda726466181d3d3535e623051ce30b3c7845386b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_72f88bd6d3b717152bc078f6ec47a64f4cc51c108fa8ecc042ee89eff0706f5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72f88bd6d3b717152bc078f6ec47a64f4cc51c108fa8ecc042ee89eff0706f5e->enter($__internal_72f88bd6d3b717152bc078f6ec47a64f4cc51c108fa8ecc042ee89eff0706f5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce05ca1c0679c6eeb44e20cdda726466181d3d3535e623051ce30b3c7845386b->leave($__internal_ce05ca1c0679c6eeb44e20cdda726466181d3d3535e623051ce30b3c7845386b_prof);

        
        $__internal_72f88bd6d3b717152bc078f6ec47a64f4cc51c108fa8ecc042ee89eff0706f5e->leave($__internal_72f88bd6d3b717152bc078f6ec47a64f4cc51c108fa8ecc042ee89eff0706f5e_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f25eb11ab19136e8b8f9c4415edeb6179e79efbf98358212f6da1cfb3e65fabb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f25eb11ab19136e8b8f9c4415edeb6179e79efbf98358212f6da1cfb3e65fabb->enter($__internal_f25eb11ab19136e8b8f9c4415edeb6179e79efbf98358212f6da1cfb3e65fabb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e25fdbfd2ed199f7fe28853c87ea790d5d8fb74a1fe686e8206ccd78de25c94d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e25fdbfd2ed199f7fe28853c87ea790d5d8fb74a1fe686e8206ccd78de25c94d->enter($__internal_e25fdbfd2ed199f7fe28853c87ea790d5d8fb74a1fe686e8206ccd78de25c94d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_e25fdbfd2ed199f7fe28853c87ea790d5d8fb74a1fe686e8206ccd78de25c94d->leave($__internal_e25fdbfd2ed199f7fe28853c87ea790d5d8fb74a1fe686e8206ccd78de25c94d_prof);

        
        $__internal_f25eb11ab19136e8b8f9c4415edeb6179e79efbf98358212f6da1cfb3e65fabb->leave($__internal_f25eb11ab19136e8b8f9c4415edeb6179e79efbf98358212f6da1cfb3e65fabb_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_94daded986dcec2142ed3a5e87ce0d9956a7e01e7ddab4ef74caf04d6571bfb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94daded986dcec2142ed3a5e87ce0d9956a7e01e7ddab4ef74caf04d6571bfb2->enter($__internal_94daded986dcec2142ed3a5e87ce0d9956a7e01e7ddab4ef74caf04d6571bfb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_83efdadfb4c634cb9ee525813ffc15987a490a56fb9d56fcba689a02851cc584 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83efdadfb4c634cb9ee525813ffc15987a490a56fb9d56fcba689a02851cc584->enter($__internal_83efdadfb4c634cb9ee525813ffc15987a490a56fb9d56fcba689a02851cc584_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) || array_key_exists("file", $context) ? $context["file"] : (function () { throw new Twig_Error_Runtime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || array_key_exists("filename", $context) ? $context["filename"] : (function () { throw new Twig_Error_Runtime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $__internal_83efdadfb4c634cb9ee525813ffc15987a490a56fb9d56fcba689a02851cc584->leave($__internal_83efdadfb4c634cb9ee525813ffc15987a490a56fb9d56fcba689a02851cc584_prof);

        
        $__internal_94daded986dcec2142ed3a5e87ce0d9956a7e01e7ddab4ef74caf04d6571bfb2->leave($__internal_94daded986dcec2142ed3a5e87ce0d9956a7e01e7ddab4ef74caf04d6571bfb2_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}

<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_090806969478f73ccb10c5e43f01834fcbbdd37e83c3450daa76dad7fdc2f03d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4df1d515de10664fc004c4cce1eb288b1b53f47b7838890707bc2dc1ea31284f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4df1d515de10664fc004c4cce1eb288b1b53f47b7838890707bc2dc1ea31284f->enter($__internal_4df1d515de10664fc004c4cce1eb288b1b53f47b7838890707bc2dc1ea31284f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_d7973fd197bf5d5e975720970920c2e8735b168718f237408f82583816873cad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7973fd197bf5d5e975720970920c2e8735b168718f237408f82583816873cad->enter($__internal_d7973fd197bf5d5e975720970920c2e8735b168718f237408f82583816873cad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_4df1d515de10664fc004c4cce1eb288b1b53f47b7838890707bc2dc1ea31284f->leave($__internal_4df1d515de10664fc004c4cce1eb288b1b53f47b7838890707bc2dc1ea31284f_prof);

        
        $__internal_d7973fd197bf5d5e975720970920c2e8735b168718f237408f82583816873cad->leave($__internal_d7973fd197bf5d5e975720970920c2e8735b168718f237408f82583816873cad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_attributes.html.php");
    }
}

<?php

/* afegirusuari.html.twig~ */
class __TwigTemplate_35096654a9a80c49570a183c3a5764862a87e9b1b0f271f39a0953d0c75b8e44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65dff4f489101c22feccc6292d05402c9e0f94bb574ce3933ee5a70d2d3f0e77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_65dff4f489101c22feccc6292d05402c9e0f94bb574ce3933ee5a70d2d3f0e77->enter($__internal_65dff4f489101c22feccc6292d05402c9e0f94bb574ce3933ee5a70d2d3f0e77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirusuari.html.twig~"));

        $__internal_4a561a80c4899187154f17b9be4369aaf32a1453712633a8ab66eb2aa9bcdc6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a561a80c4899187154f17b9be4369aaf32a1453712633a8ab66eb2aa9bcdc6c->enter($__internal_4a561a80c4899187154f17b9be4369aaf32a1453712633a8ab66eb2aa9bcdc6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "afegirusuari.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />

        
    </head>
    <body>
    \t";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola ";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir Usuari</h1>
      <hr>
      
\t\t<div id=\"formulari\">
 \t 
    \t  ";
        // line 48
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 48, $this->getSourceContext()); })()), 'form_start');
        echo "
    \t  ";
        // line 49
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 49, $this->getSourceContext()); })()), 'widget');
        echo "
    \t  ";
        // line 50
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 50, $this->getSourceContext()); })()), 'form_end');
        echo "

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        ";
        // line 67
        $this->displayBlock('javascripts', $context, $blocks);
        // line 72
        echo "    </body>
</html>
";
        
        $__internal_65dff4f489101c22feccc6292d05402c9e0f94bb574ce3933ee5a70d2d3f0e77->leave($__internal_65dff4f489101c22feccc6292d05402c9e0f94bb574ce3933ee5a70d2d3f0e77_prof);

        
        $__internal_4a561a80c4899187154f17b9be4369aaf32a1453712633a8ab66eb2aa9bcdc6c->leave($__internal_4a561a80c4899187154f17b9be4369aaf32a1453712633a8ab66eb2aa9bcdc6c_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_8959933a2301207ef9def6f38f74ac74fdcbe148bd49122a90c776ad184a7581 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8959933a2301207ef9def6f38f74ac74fdcbe148bd49122a90c776ad184a7581->enter($__internal_8959933a2301207ef9def6f38f74ac74fdcbe148bd49122a90c776ad184a7581_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_344bfc9f5c8a8ab91e8f8ac3920daf67afc42536133c81c200c609d25a5d7282 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_344bfc9f5c8a8ab91e8f8ac3920daf67afc42536133c81c200c609d25a5d7282->enter($__internal_344bfc9f5c8a8ab91e8f8ac3920daf67afc42536133c81c200c609d25a5d7282_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Afegir usuari";
        
        $__internal_344bfc9f5c8a8ab91e8f8ac3920daf67afc42536133c81c200c609d25a5d7282->leave($__internal_344bfc9f5c8a8ab91e8f8ac3920daf67afc42536133c81c200c609d25a5d7282_prof);

        
        $__internal_8959933a2301207ef9def6f38f74ac74fdcbe148bd49122a90c776ad184a7581->leave($__internal_8959933a2301207ef9def6f38f74ac74fdcbe148bd49122a90c776ad184a7581_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a8a80b7dd2faf00d2742f0e871d465590064f750e77bd82d0efdb991b3fe01ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8a80b7dd2faf00d2742f0e871d465590064f750e77bd82d0efdb991b3fe01ff->enter($__internal_a8a80b7dd2faf00d2742f0e871d465590064f750e77bd82d0efdb991b3fe01ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_9ca53726625084c36cb87ee0787026853e0662804c3d83079437996dde91959a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ca53726625084c36cb87ee0787026853e0662804c3d83079437996dde91959a->enter($__internal_9ca53726625084c36cb87ee0787026853e0662804c3d83079437996dde91959a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_9ca53726625084c36cb87ee0787026853e0662804c3d83079437996dde91959a->leave($__internal_9ca53726625084c36cb87ee0787026853e0662804c3d83079437996dde91959a_prof);

        
        $__internal_a8a80b7dd2faf00d2742f0e871d465590064f750e77bd82d0efdb991b3fe01ff->leave($__internal_a8a80b7dd2faf00d2742f0e871d465590064f750e77bd82d0efdb991b3fe01ff_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_7f64a533a73cc6dfc2b76541eec418a2f6923fa2d8149e2bd93193c5f05b1b5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f64a533a73cc6dfc2b76541eec418a2f6923fa2d8149e2bd93193c5f05b1b5d->enter($__internal_7f64a533a73cc6dfc2b76541eec418a2f6923fa2d8149e2bd93193c5f05b1b5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_92836555e1669e3de333d3306dba7f81d701c0fa2a102a4d78b7d5ea91c1c3d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92836555e1669e3de333d3306dba7f81d701c0fa2a102a4d78b7d5ea91c1c3d6->enter($__internal_92836555e1669e3de333d3306dba7f81d701c0fa2a102a4d78b7d5ea91c1c3d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_92836555e1669e3de333d3306dba7f81d701c0fa2a102a4d78b7d5ea91c1c3d6->leave($__internal_92836555e1669e3de333d3306dba7f81d701c0fa2a102a4d78b7d5ea91c1c3d6_prof);

        
        $__internal_7f64a533a73cc6dfc2b76541eec418a2f6923fa2d8149e2bd93193c5f05b1b5d->leave($__internal_7f64a533a73cc6dfc2b76541eec418a2f6923fa2d8149e2bd93193c5f05b1b5d_prof);

    }

    // line 67
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b6946af239204f5bf14ce27a0bf43c139f22e4bef02db645f915b62e500d35b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6946af239204f5bf14ce27a0bf43c139f22e4bef02db645f915b62e500d35b5->enter($__internal_b6946af239204f5bf14ce27a0bf43c139f22e4bef02db645f915b62e500d35b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_e3f8f901e41906fb12924fb5e1c6199b4db3505e146d52a31dac7a157585261c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3f8f901e41906fb12924fb5e1c6199b4db3505e146d52a31dac7a157585261c->enter($__internal_e3f8f901e41906fb12924fb5e1c6199b4db3505e146d52a31dac7a157585261c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 68
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_e3f8f901e41906fb12924fb5e1c6199b4db3505e146d52a31dac7a157585261c->leave($__internal_e3f8f901e41906fb12924fb5e1c6199b4db3505e146d52a31dac7a157585261c_prof);

        
        $__internal_b6946af239204f5bf14ce27a0bf43c139f22e4bef02db645f915b62e500d35b5->leave($__internal_b6946af239204f5bf14ce27a0bf43c139f22e4bef02db645f915b62e500d35b5_prof);

    }

    public function getTemplateName()
    {
        return "afegirusuari.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 69,  204 => 68,  195 => 67,  178 => 15,  161 => 6,  143 => 5,  131 => 72,  129 => 67,  109 => 50,  105 => 49,  101 => 48,  70 => 20,  66 => 19,  61 => 16,  59 => 15,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Afegir usuari{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />

        
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"{{ path('fos_user_security_logout') }}\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola {{ app.user.username }}!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Afegir Usuari</h1>
      <hr>
      
\t\t<div id=\"formulari\">
 \t 
    \t  {{ form_start(form) }}
    \t  {{ form_widget(form) }}
    \t  {{ form_end(form) }}

    \t</div>



\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>

        {% endblock %}
    </body>
</html>
", "afegirusuari.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/afegirusuari.html.twig~");
    }
}

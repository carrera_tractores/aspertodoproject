<?php

/* EasyAdminBundle:default:field_boolean.html.twig */
class __TwigTemplate_92a04f192218c435bb70f6d1048126e2ef8c0c2648f766f16f359b9b65501154 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a527bb557e35d272c460cf6ef0fd8a193e752f9e3ec2af3d414bf7e3d71d6cb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a527bb557e35d272c460cf6ef0fd8a193e752f9e3ec2af3d414bf7e3d71d6cb4->enter($__internal_a527bb557e35d272c460cf6ef0fd8a193e752f9e3ec2af3d414bf7e3d71d6cb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_boolean.html.twig"));

        $__internal_6cf8ca40f507d724d4cf43836f0d651382f1a283c4e80d7f68371494d813df73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cf8ca40f507d724d4cf43836f0d651382f1a283c4e80d7f68371494d813df73->enter($__internal_6cf8ca40f507d724d4cf43836f0d651382f1a283c4e80d7f68371494d813df73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_boolean.html.twig"));

        // line 2
        echo "
";
        // line 3
        if (((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 3, $this->getSourceContext()); })()) == true)) {
            // line 4
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.true", array(), "EasyAdminBundle"), "html", null, true);
            echo "</span>
";
        } else {
            // line 6
            echo "    <span class=\"label label-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.false", array(), "EasyAdminBundle"), "html", null, true);
            echo "</span>
";
        }
        
        $__internal_a527bb557e35d272c460cf6ef0fd8a193e752f9e3ec2af3d414bf7e3d71d6cb4->leave($__internal_a527bb557e35d272c460cf6ef0fd8a193e752f9e3ec2af3d414bf7e3d71d6cb4_prof);

        
        $__internal_6cf8ca40f507d724d4cf43836f0d651382f1a283c4e80d7f68371494d813df73->leave($__internal_6cf8ca40f507d724d4cf43836f0d651382f1a283c4e80d7f68371494d813df73_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 6,  30 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'EasyAdminBundle' %}

{% if value == true %}
    <span class=\"label label-success\">{{ 'label.true'|trans }}</span>
{% else %}
    <span class=\"label label-danger\">{{ 'label.false'|trans }}</span>
{% endif %}
", "EasyAdminBundle:default:field_boolean.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_boolean.html.twig");
    }
}

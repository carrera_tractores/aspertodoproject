<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_2a498fbb96a8dd39f3b2aa3f91f56370585bc63f72f7a53eb0f616d1d90d87bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55192fd93c0ae74a16f8792ac956eeeac749b5500a12db3b5c8042f027dc771f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55192fd93c0ae74a16f8792ac956eeeac749b5500a12db3b5c8042f027dc771f->enter($__internal_55192fd93c0ae74a16f8792ac956eeeac749b5500a12db3b5c8042f027dc771f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_f45127307a5e74cd0fd37aa7e385e08b64ec81585d268ffac864ae4cae1eda7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f45127307a5e74cd0fd37aa7e385e08b64ec81585d268ffac864ae4cae1eda7b->enter($__internal_f45127307a5e74cd0fd37aa7e385e08b64ec81585d268ffac864ae4cae1eda7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_55192fd93c0ae74a16f8792ac956eeeac749b5500a12db3b5c8042f027dc771f->leave($__internal_55192fd93c0ae74a16f8792ac956eeeac749b5500a12db3b5c8042f027dc771f_prof);

        
        $__internal_f45127307a5e74cd0fd37aa7e385e08b64ec81585d268ffac864ae4cae1eda7b->leave($__internal_f45127307a5e74cd0fd37aa7e385e08b64ec81585d268ffac864ae4cae1eda7b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}

<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_8cefc0c78f1f611ed29e0be3f6af93e29c0190a84489c26e53784bb0c06fac21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c25a67472d901fc2f7e8946b996d65ed4b1e53f958781d940ba7d92a554054de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c25a67472d901fc2f7e8946b996d65ed4b1e53f958781d940ba7d92a554054de->enter($__internal_c25a67472d901fc2f7e8946b996d65ed4b1e53f958781d940ba7d92a554054de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_e1bebba521346163730e2f623b74242327b69d882b948c0aa59d25701957031b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1bebba521346163730e2f623b74242327b69d882b948c0aa59d25701957031b->enter($__internal_e1bebba521346163730e2f623b74242327b69d882b948c0aa59d25701957031b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_c25a67472d901fc2f7e8946b996d65ed4b1e53f958781d940ba7d92a554054de->leave($__internal_c25a67472d901fc2f7e8946b996d65ed4b1e53f958781d940ba7d92a554054de_prof);

        
        $__internal_e1bebba521346163730e2f623b74242327b69d882b948c0aa59d25701957031b->leave($__internal_e1bebba521346163730e2f623b74242327b69d882b948c0aa59d25701957031b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}

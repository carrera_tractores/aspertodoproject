<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_b25a16ec86cedf0082f733c7365615aba2b0a4870a78396e5594b2c7c356a536 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fdbe1ebe00fdb5295f4d3baba883238f3065d1fc096b768a9cf9631e25d2b52b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fdbe1ebe00fdb5295f4d3baba883238f3065d1fc096b768a9cf9631e25d2b52b->enter($__internal_fdbe1ebe00fdb5295f4d3baba883238f3065d1fc096b768a9cf9631e25d2b52b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_8d72920a2f6d0f4230be751fd166cc679ad3a8d330887672e51fd278c244f6b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d72920a2f6d0f4230be751fd166cc679ad3a8d330887672e51fd278c244f6b9->enter($__internal_8d72920a2f6d0f4230be751fd166cc679ad3a8d330887672e51fd278c244f6b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_fdbe1ebe00fdb5295f4d3baba883238f3065d1fc096b768a9cf9631e25d2b52b->leave($__internal_fdbe1ebe00fdb5295f4d3baba883238f3065d1fc096b768a9cf9631e25d2b52b_prof);

        
        $__internal_8d72920a2f6d0f4230be751fd166cc679ad3a8d330887672e51fd278c244f6b9->leave($__internal_8d72920a2f6d0f4230be751fd166cc679ad3a8d330887672e51fd278c244f6b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}

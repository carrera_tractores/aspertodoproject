<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_ccef3bafb450d5d8e50cde630d7fd47b9ad6d77ed3c4c1b9a5c246f1425531ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78c569a1ae03fcc1ca45be298711ec97c8b72d34c4b8524926522c45b929cf68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78c569a1ae03fcc1ca45be298711ec97c8b72d34c4b8524926522c45b929cf68->enter($__internal_78c569a1ae03fcc1ca45be298711ec97c8b72d34c4b8524926522c45b929cf68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_828b5375a4ba889b3ea30ef6c008f94ee1e629a2286cae1efcb965c676675ef3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_828b5375a4ba889b3ea30ef6c008f94ee1e629a2286cae1efcb965c676675ef3->enter($__internal_828b5375a4ba889b3ea30ef6c008f94ee1e629a2286cae1efcb965c676675ef3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78c569a1ae03fcc1ca45be298711ec97c8b72d34c4b8524926522c45b929cf68->leave($__internal_78c569a1ae03fcc1ca45be298711ec97c8b72d34c4b8524926522c45b929cf68_prof);

        
        $__internal_828b5375a4ba889b3ea30ef6c008f94ee1e629a2286cae1efcb965c676675ef3->leave($__internal_828b5375a4ba889b3ea30ef6c008f94ee1e629a2286cae1efcb965c676675ef3_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_290878ea09e4aef130c63765cebff16c4d7281196c98c2881c8e3a425d180df4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_290878ea09e4aef130c63765cebff16c4d7281196c98c2881c8e3a425d180df4->enter($__internal_290878ea09e4aef130c63765cebff16c4d7281196c98c2881c8e3a425d180df4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_01e783111869b957d898962ae113a38ee892e33bea13c48dacd2a2ea1260f47e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01e783111869b957d898962ae113a38ee892e33bea13c48dacd2a2ea1260f47e->enter($__internal_01e783111869b957d898962ae113a38ee892e33bea13c48dacd2a2ea1260f47e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_01e783111869b957d898962ae113a38ee892e33bea13c48dacd2a2ea1260f47e->leave($__internal_01e783111869b957d898962ae113a38ee892e33bea13c48dacd2a2ea1260f47e_prof);

        
        $__internal_290878ea09e4aef130c63765cebff16c4d7281196c98c2881c8e3a425d180df4->leave($__internal_290878ea09e4aef130c63765cebff16c4d7281196c98c2881c8e3a425d180df4_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_1ce28a30a5627266031918985fe91bc1a55bb5d9fdb0e399021306122e493d72 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ce28a30a5627266031918985fe91bc1a55bb5d9fdb0e399021306122e493d72->enter($__internal_1ce28a30a5627266031918985fe91bc1a55bb5d9fdb0e399021306122e493d72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f819d3fb583acc505be13ec8abe4b117b4a116546faa8099d2d32b606ef3daca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f819d3fb583acc505be13ec8abe4b117b4a116546faa8099d2d32b606ef3daca->enter($__internal_f819d3fb583acc505be13ec8abe4b117b4a116546faa8099d2d32b606ef3daca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f819d3fb583acc505be13ec8abe4b117b4a116546faa8099d2d32b606ef3daca->leave($__internal_f819d3fb583acc505be13ec8abe4b117b4a116546faa8099d2d32b606ef3daca_prof);

        
        $__internal_1ce28a30a5627266031918985fe91bc1a55bb5d9fdb0e399021306122e493d72->leave($__internal_1ce28a30a5627266031918985fe91bc1a55bb5d9fdb0e399021306122e493d72_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_887aa5eafd8e69e019484bb93a8679bb0460948e6d8fd700b935fad24235b829 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_887aa5eafd8e69e019484bb93a8679bb0460948e6d8fd700b935fad24235b829->enter($__internal_887aa5eafd8e69e019484bb93a8679bb0460948e6d8fd700b935fad24235b829_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_93b112826bbb5862ac2bb05fcc1bcd3fa58df5b9d2acd2b516e87a62e54b0f77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93b112826bbb5862ac2bb05fcc1bcd3fa58df5b9d2acd2b516e87a62e54b0f77->enter($__internal_93b112826bbb5862ac2bb05fcc1bcd3fa58df5b9d2acd2b516e87a62e54b0f77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_93b112826bbb5862ac2bb05fcc1bcd3fa58df5b9d2acd2b516e87a62e54b0f77->leave($__internal_93b112826bbb5862ac2bb05fcc1bcd3fa58df5b9d2acd2b516e87a62e54b0f77_prof);

        
        $__internal_887aa5eafd8e69e019484bb93a8679bb0460948e6d8fd700b935fad24235b829->leave($__internal_887aa5eafd8e69e019484bb93a8679bb0460948e6d8fd700b935fad24235b829_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}

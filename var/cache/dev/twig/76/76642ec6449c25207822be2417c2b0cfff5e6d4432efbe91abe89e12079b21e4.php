<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_291d81d9ff2740ac5d22dde183d152406b6c162fb8422b6382417fbbb2ebaf90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bad2317ddbaeb1c215450aec79fb05067b6ad9efcf4fd354466c6e2c4c03a143 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bad2317ddbaeb1c215450aec79fb05067b6ad9efcf4fd354466c6e2c4c03a143->enter($__internal_bad2317ddbaeb1c215450aec79fb05067b6ad9efcf4fd354466c6e2c4c03a143_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_84006e515eaadcd9b89508ff23dc6a4eae6d548ca014573da32131929775fff0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84006e515eaadcd9b89508ff23dc6a4eae6d548ca014573da32131929775fff0->enter($__internal_84006e515eaadcd9b89508ff23dc6a4eae6d548ca014573da32131929775fff0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bad2317ddbaeb1c215450aec79fb05067b6ad9efcf4fd354466c6e2c4c03a143->leave($__internal_bad2317ddbaeb1c215450aec79fb05067b6ad9efcf4fd354466c6e2c4c03a143_prof);

        
        $__internal_84006e515eaadcd9b89508ff23dc6a4eae6d548ca014573da32131929775fff0->leave($__internal_84006e515eaadcd9b89508ff23dc6a4eae6d548ca014573da32131929775fff0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5a11cbda7d81ae94b667391b912ab495cc9f4e55f6c7e083b0cbc21578f555ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a11cbda7d81ae94b667391b912ab495cc9f4e55f6c7e083b0cbc21578f555ef->enter($__internal_5a11cbda7d81ae94b667391b912ab495cc9f4e55f6c7e083b0cbc21578f555ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_afb3654935d388f19befa25c89b2da9d1a97a4de5724698eaedf3e22c6cfe0c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afb3654935d388f19befa25c89b2da9d1a97a4de5724698eaedf3e22c6cfe0c5->enter($__internal_afb3654935d388f19befa25c89b2da9d1a97a4de5724698eaedf3e22c6cfe0c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_afb3654935d388f19befa25c89b2da9d1a97a4de5724698eaedf3e22c6cfe0c5->leave($__internal_afb3654935d388f19befa25c89b2da9d1a97a4de5724698eaedf3e22c6cfe0c5_prof);

        
        $__internal_5a11cbda7d81ae94b667391b912ab495cc9f4e55f6c7e083b0cbc21578f555ef->leave($__internal_5a11cbda7d81ae94b667391b912ab495cc9f4e55f6c7e083b0cbc21578f555ef_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_37812f137b2d345762dd3b40929a1ff291be5869fa0d7ec0eeeda43812baa035 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37812f137b2d345762dd3b40929a1ff291be5869fa0d7ec0eeeda43812baa035->enter($__internal_37812f137b2d345762dd3b40929a1ff291be5869fa0d7ec0eeeda43812baa035_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_004f052c8a389326c0026513fe7e7975f4aa7abc0d2784a7c943859920769b89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_004f052c8a389326c0026513fe7e7975f4aa7abc0d2784a7c943859920769b89->enter($__internal_004f052c8a389326c0026513fe7e7975f4aa7abc0d2784a7c943859920769b89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_004f052c8a389326c0026513fe7e7975f4aa7abc0d2784a7c943859920769b89->leave($__internal_004f052c8a389326c0026513fe7e7975f4aa7abc0d2784a7c943859920769b89_prof);

        
        $__internal_37812f137b2d345762dd3b40929a1ff291be5869fa0d7ec0eeeda43812baa035->leave($__internal_37812f137b2d345762dd3b40929a1ff291be5869fa0d7ec0eeeda43812baa035_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_13328412a18e756894bdaf9e987d8ae182758e40287491c15ff80d385c0e23e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13328412a18e756894bdaf9e987d8ae182758e40287491c15ff80d385c0e23e4->enter($__internal_13328412a18e756894bdaf9e987d8ae182758e40287491c15ff80d385c0e23e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_4f1b6cb81bc0138f7fc5d5c0db27e1fa93175c1a024445c8e76cdbd7004e108b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f1b6cb81bc0138f7fc5d5c0db27e1fa93175c1a024445c8e76cdbd7004e108b->enter($__internal_4f1b6cb81bc0138f7fc5d5c0db27e1fa93175c1a024445c8e76cdbd7004e108b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_4f1b6cb81bc0138f7fc5d5c0db27e1fa93175c1a024445c8e76cdbd7004e108b->leave($__internal_4f1b6cb81bc0138f7fc5d5c0db27e1fa93175c1a024445c8e76cdbd7004e108b_prof);

        
        $__internal_13328412a18e756894bdaf9e987d8ae182758e40287491c15ff80d385c0e23e4->leave($__internal_13328412a18e756894bdaf9e987d8ae182758e40287491c15ff80d385c0e23e4_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}

<?php

/* EasyAdminBundle:default:field_decimal.html.twig */
class __TwigTemplate_fdec8217f2667189366b59769a3c54f51f80f1316851cf1dee981d59f36c3d3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1bc70c0625d8274b22c94ff6ca61b62f3d82a3a83682ad6242551e92e30f9bb8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bc70c0625d8274b22c94ff6ca61b62f3d82a3a83682ad6242551e92e30f9bb8->enter($__internal_1bc70c0625d8274b22c94ff6ca61b62f3d82a3a83682ad6242551e92e30f9bb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_decimal.html.twig"));

        $__internal_e8b43c0abc24afbbe74305fd62dd0928540b7d0335f6c8c888282d82d1ed3b11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8b43c0abc24afbbe74305fd62dd0928540b7d0335f6c8c888282d82d1ed3b11->enter($__internal_e8b43c0abc24afbbe74305fd62dd0928540b7d0335f6c8c888282d82d1ed3b11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_decimal.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 2, $this->getSourceContext()); })()), "format", array()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })()), 2), "html", null, true);
            echo "
";
        }
        
        $__internal_1bc70c0625d8274b22c94ff6ca61b62f3d82a3a83682ad6242551e92e30f9bb8->leave($__internal_1bc70c0625d8274b22c94ff6ca61b62f3d82a3a83682ad6242551e92e30f9bb8_prof);

        
        $__internal_e8b43c0abc24afbbe74305fd62dd0928540b7d0335f6c8c888282d82d1ed3b11->leave($__internal_e8b43c0abc24afbbe74305fd62dd0928540b7d0335f6c8c888282d82d1ed3b11_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_decimal.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format(2) }}
{% endif %}
", "EasyAdminBundle:default:field_decimal.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_decimal.html.twig");
    }
}

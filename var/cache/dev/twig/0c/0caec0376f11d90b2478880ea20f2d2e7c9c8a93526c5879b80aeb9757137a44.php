<?php

/* EasyAdminBundle:default:field_object.html.twig */
class __TwigTemplate_2393a5a6abb6b3557dde7c4247fac76bb39e2af7e66459eff8e9edb4c3576dc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4993954048a337d18e348ce95fe08e69e3e1a3f99f25ae9d965062c03de1d0f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4993954048a337d18e348ce95fe08e69e3e1a3f99f25ae9d965062c03de1d0f0->enter($__internal_4993954048a337d18e348ce95fe08e69e3e1a3f99f25ae9d965062c03de1d0f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_object.html.twig"));

        $__internal_7aaba6c46968fcf1580dc86a6c2135cd1cb811f8dec8ac1b933a3482186a1256 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aaba6c46968fcf1580dc86a6c2135cd1cb811f8dec8ac1b933a3482186a1256->enter($__internal_7aaba6c46968fcf1580dc86a6c2135cd1cb811f8dec8ac1b933a3482186a1256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_object.html.twig"));

        // line 1
        echo "<span class=\"label\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.object", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
";
        
        $__internal_4993954048a337d18e348ce95fe08e69e3e1a3f99f25ae9d965062c03de1d0f0->leave($__internal_4993954048a337d18e348ce95fe08e69e3e1a3f99f25ae9d965062c03de1d0f0_prof);

        
        $__internal_7aaba6c46968fcf1580dc86a6c2135cd1cb811f8dec8ac1b933a3482186a1256->leave($__internal_7aaba6c46968fcf1580dc86a6c2135cd1cb811f8dec8ac1b933a3482186a1256_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_object.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<span class=\"label\">{{ 'label.object'|trans(domain = 'EasyAdminBundle') }}</span>
", "EasyAdminBundle:default:field_object.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_object.html.twig");
    }
}

<?php

/* WebProfilerBundle:Profiler:header.html.twig */
class __TwigTemplate_63e4c55e1fb492363478a8b5c6a86f46b22b86e0e89b6d2775c1887bbe312c72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d73fa203c06607dcf8737c45bb7290acee28ce57e358086dbdc24366844656d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d73fa203c06607dcf8737c45bb7290acee28ce57e358086dbdc24366844656d->enter($__internal_5d73fa203c06607dcf8737c45bb7290acee28ce57e358086dbdc24366844656d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        $__internal_15f949263a3ac4fe592c58c285c7bd7b41f984c6602125895ebb1327edda4fa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15f949263a3ac4fe592c58c285c7bd7b41f984c6602125895ebb1327edda4fa7->enter($__internal_15f949263a3ac4fe592c58c285c7bd7b41f984c6602125895ebb1327edda4fa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_5d73fa203c06607dcf8737c45bb7290acee28ce57e358086dbdc24366844656d->leave($__internal_5d73fa203c06607dcf8737c45bb7290acee28ce57e358086dbdc24366844656d_prof);

        
        $__internal_15f949263a3ac4fe592c58c285c7bd7b41f984c6602125895ebb1327edda4fa7->leave($__internal_15f949263a3ac4fe592c58c285c7bd7b41f984c6602125895ebb1327edda4fa7_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "WebProfilerBundle:Profiler:header.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}

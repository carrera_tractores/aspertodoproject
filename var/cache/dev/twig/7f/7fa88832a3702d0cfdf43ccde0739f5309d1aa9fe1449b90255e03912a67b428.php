<?php

/* @EasyAdmin/default/field_datetime.html.twig */
class __TwigTemplate_8be6bf64fcb2e71b86384c060d52b56b36d75e8648a651301ab91b29b3c1971e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a514dc25793d82a3cb393e335c0614a4c6c2f055b23838f42c7ce6b3e1f78854 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a514dc25793d82a3cb393e335c0614a4c6c2f055b23838f42c7ce6b3e1f78854->enter($__internal_a514dc25793d82a3cb393e335c0614a4c6c2f055b23838f42c7ce6b3e1f78854_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_datetime.html.twig"));

        $__internal_358e6e794b45197b0c824ee11d524c44b4d4c72074b65ed1762c844ebc57c60b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_358e6e794b45197b0c824ee11d524c44b4d4c72074b65ed1762c844ebc57c60b->enter($__internal_358e6e794b45197b0c824ee11d524c44b4d4c72074b65ed1762c844ebc57c60b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/field_datetime.html.twig"));

        // line 1
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 1, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())), "html", null, true);
        echo "
";
        
        $__internal_a514dc25793d82a3cb393e335c0614a4c6c2f055b23838f42c7ce6b3e1f78854->leave($__internal_a514dc25793d82a3cb393e335c0614a4c6c2f055b23838f42c7ce6b3e1f78854_prof);

        
        $__internal_358e6e794b45197b0c824ee11d524c44b4d4c72074b65ed1762c844ebc57c60b->leave($__internal_358e6e794b45197b0c824ee11d524c44b4d4c72074b65ed1762c844ebc57c60b_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/field_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ value|date(field_options.format) }}
", "@EasyAdmin/default/field_datetime.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_datetime.html.twig");
    }
}

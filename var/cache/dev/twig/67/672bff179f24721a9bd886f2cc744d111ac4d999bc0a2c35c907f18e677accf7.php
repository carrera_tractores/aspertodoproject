<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_1c46a7bfafcf2ea4038c494d949416cfdc4a2bc15ed213216fce952c295a18ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23409427b3a3d8ee81d4a6a934d331f0af18c50e06b953ca75b5ae773ef48b69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23409427b3a3d8ee81d4a6a934d331f0af18c50e06b953ca75b5ae773ef48b69->enter($__internal_23409427b3a3d8ee81d4a6a934d331f0af18c50e06b953ca75b5ae773ef48b69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_4d83b16003520028572ac6e6e05aff4409453215175124162e11739951ad0eab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d83b16003520028572ac6e6e05aff4409453215175124162e11739951ad0eab->enter($__internal_4d83b16003520028572ac6e6e05aff4409453215175124162e11739951ad0eab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_23409427b3a3d8ee81d4a6a934d331f0af18c50e06b953ca75b5ae773ef48b69->leave($__internal_23409427b3a3d8ee81d4a6a934d331f0af18c50e06b953ca75b5ae773ef48b69_prof);

        
        $__internal_4d83b16003520028572ac6e6e05aff4409453215175124162e11739951ad0eab->leave($__internal_4d83b16003520028572ac6e6e05aff4409453215175124162e11739951ad0eab_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}

<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_64eeb23351c42d0903dfe763eeca584aac77054275c063be145bcc45849f49a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4dcd08bc0e1569a1785cca01b688f0b9c9bd56cc65099c83b1c078b7aaf4fec2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4dcd08bc0e1569a1785cca01b688f0b9c9bd56cc65099c83b1c078b7aaf4fec2->enter($__internal_4dcd08bc0e1569a1785cca01b688f0b9c9bd56cc65099c83b1c078b7aaf4fec2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_537c98fb8df3202bdea16946d989ba6a49bc3e9c882c6da50da96bc106f74578 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_537c98fb8df3202bdea16946d989ba6a49bc3e9c882c6da50da96bc106f74578->enter($__internal_537c98fb8df3202bdea16946d989ba6a49bc3e9c882c6da50da96bc106f74578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["group"]) || array_key_exists("group", $context) ? $context["group"] : (function () { throw new Twig_Error_Runtime('Variable "group" does not exist.', 4, $this->getSourceContext()); })()), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_4dcd08bc0e1569a1785cca01b688f0b9c9bd56cc65099c83b1c078b7aaf4fec2->leave($__internal_4dcd08bc0e1569a1785cca01b688f0b9c9bd56cc65099c83b1c078b7aaf4fec2_prof);

        
        $__internal_537c98fb8df3202bdea16946d989ba6a49bc3e9c882c6da50da96bc106f74578->leave($__internal_537c98fb8df3202bdea16946d989ba6a49bc3e9c882c6da50da96bc106f74578_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show_content.html.twig");
    }
}

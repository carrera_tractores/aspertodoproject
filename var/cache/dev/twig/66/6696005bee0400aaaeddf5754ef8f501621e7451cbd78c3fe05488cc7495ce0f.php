<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_f5dc39c5ebd843add554f3add76a222c48e6445d93118fca55f8013df59983b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ff4db1d6ba3e2c2d365bc4310ca55b8f2c10e7e68d31555c378dfd9432ef498 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ff4db1d6ba3e2c2d365bc4310ca55b8f2c10e7e68d31555c378dfd9432ef498->enter($__internal_8ff4db1d6ba3e2c2d365bc4310ca55b8f2c10e7e68d31555c378dfd9432ef498_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_67850ea9d85319981dc166ef8565a894d05c6633098266de08d3586bf49891a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67850ea9d85319981dc166ef8565a894d05c6633098266de08d3586bf49891a5->enter($__internal_67850ea9d85319981dc166ef8565a894d05c6633098266de08d3586bf49891a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 4, $this->getSourceContext()); })());
        echo " ";
        echo (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 4, $this->getSourceContext()); })());
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_8ff4db1d6ba3e2c2d365bc4310ca55b8f2c10e7e68d31555c378dfd9432ef498->leave($__internal_8ff4db1d6ba3e2c2d365bc4310ca55b8f2c10e7e68d31555c378dfd9432ef498_prof);

        
        $__internal_67850ea9d85319981dc166ef8565a894d05c6633098266de08d3586bf49891a5->leave($__internal_67850ea9d85319981dc166ef8565a894d05c6633098266de08d3586bf49891a5_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}

<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_a93c75d0e5f23a35c0861a8bde5964c0f7c641f0c21b3aec59d0e1edecabeffd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d77c5761f03ebbe2b219b78c40e46b0ad6b45fa4f99fe4f427be145367f0161 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d77c5761f03ebbe2b219b78c40e46b0ad6b45fa4f99fe4f427be145367f0161->enter($__internal_5d77c5761f03ebbe2b219b78c40e46b0ad6b45fa4f99fe4f427be145367f0161_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_b47cbf479ec9950f1f4693aaa07b28cd294dbe1da6c4b0c7296c326becefef7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b47cbf479ec9950f1f4693aaa07b28cd294dbe1da6c4b0c7296c326becefef7d->enter($__internal_b47cbf479ec9950f1f4693aaa07b28cd294dbe1da6c4b0c7296c326becefef7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d77c5761f03ebbe2b219b78c40e46b0ad6b45fa4f99fe4f427be145367f0161->leave($__internal_5d77c5761f03ebbe2b219b78c40e46b0ad6b45fa4f99fe4f427be145367f0161_prof);

        
        $__internal_b47cbf479ec9950f1f4693aaa07b28cd294dbe1da6c4b0c7296c326becefef7d->leave($__internal_b47cbf479ec9950f1f4693aaa07b28cd294dbe1da6c4b0c7296c326becefef7d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6ab913a9f81c3f8f1bb34e2b4ec16059de8482986614d5ac4447949f5ac50874 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ab913a9f81c3f8f1bb34e2b4ec16059de8482986614d5ac4447949f5ac50874->enter($__internal_6ab913a9f81c3f8f1bb34e2b4ec16059de8482986614d5ac4447949f5ac50874_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_18fa97321c65d8f056937ba7321912eb4bdf929ca4acf4a79446f35e31e5b23f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18fa97321c65d8f056937ba7321912eb4bdf929ca4acf4a79446f35e31e5b23f->enter($__internal_18fa97321c65d8f056937ba7321912eb4bdf929ca4acf4a79446f35e31e5b23f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 6, $this->getSourceContext()); })()), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) || array_key_exists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new Twig_Error_Runtime('Variable "targetUrl" does not exist.', 7, $this->getSourceContext()); })())) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) || array_key_exists("targetUrl", $context) ? $context["targetUrl"] : (function () { throw new Twig_Error_Runtime('Variable "targetUrl" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_18fa97321c65d8f056937ba7321912eb4bdf929ca4acf4a79446f35e31e5b23f->leave($__internal_18fa97321c65d8f056937ba7321912eb4bdf929ca4acf4a79446f35e31e5b23f_prof);

        
        $__internal_6ab913a9f81c3f8f1bb34e2b4ec16059de8482986614d5ac4447949f5ac50874->leave($__internal_6ab913a9f81c3f8f1bb34e2b4ec16059de8482986614d5ac4447949f5ac50874_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}

<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_74db5da526aeb6701e41e367ccea6dc7d65fa8d22e8da7f03e05f614496c32ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_858678d912141f58a7cca69ee8138b1981ea09e8926d3f39c3c4c19613e9af2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_858678d912141f58a7cca69ee8138b1981ea09e8926d3f39c3c4c19613e9af2c->enter($__internal_858678d912141f58a7cca69ee8138b1981ea09e8926d3f39c3c4c19613e9af2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_e1e04b9bc63266d4f63791aa37a5dfe0e205ce6639d6c4c19003eec712882c7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1e04b9bc63266d4f63791aa37a5dfe0e205ce6639d6c4c19003eec712882c7d->enter($__internal_e1e04b9bc63266d4f63791aa37a5dfe0e205ce6639d6c4c19003eec712882c7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_858678d912141f58a7cca69ee8138b1981ea09e8926d3f39c3c4c19613e9af2c->leave($__internal_858678d912141f58a7cca69ee8138b1981ea09e8926d3f39c3c4c19613e9af2c_prof);

        
        $__internal_e1e04b9bc63266d4f63791aa37a5dfe0e205ce6639d6c4c19003eec712882c7d->leave($__internal_e1e04b9bc63266d4f63791aa37a5dfe0e205ce6639d6c4c19003eec712882c7d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}

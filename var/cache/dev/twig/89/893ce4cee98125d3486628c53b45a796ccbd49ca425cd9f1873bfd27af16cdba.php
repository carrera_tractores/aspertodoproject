<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_a242d7495b2aee4def63088b6331b26cbcae8043609b105119a12e62f1aa8294 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c93376bde52350751b5aa71d03a73025b4a07ef6c78e1beab7527bfa82b2d96c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c93376bde52350751b5aa71d03a73025b4a07ef6c78e1beab7527bfa82b2d96c->enter($__internal_c93376bde52350751b5aa71d03a73025b4a07ef6c78e1beab7527bfa82b2d96c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_5333dd9311406aa305dd67f0d7507d6658a65dc79a5c02943d80f38b702a1f12 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5333dd9311406aa305dd67f0d7507d6658a65dc79a5c02943d80f38b702a1f12->enter($__internal_5333dd9311406aa305dd67f0d7507d6658a65dc79a5c02943d80f38b702a1f12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_c93376bde52350751b5aa71d03a73025b4a07ef6c78e1beab7527bfa82b2d96c->leave($__internal_c93376bde52350751b5aa71d03a73025b4a07ef6c78e1beab7527bfa82b2d96c_prof);

        
        $__internal_5333dd9311406aa305dd67f0d7507d6658a65dc79a5c02943d80f38b702a1f12->leave($__internal_5333dd9311406aa305dd67f0d7507d6658a65dc79a5c02943d80f38b702a1f12_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}

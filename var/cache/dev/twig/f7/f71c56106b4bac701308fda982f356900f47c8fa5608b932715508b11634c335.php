<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_a11689f7d65020756130823aff8aa58307f40a2ef535e1df4e2374ac0c491d97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_daca88bfcbdcae4aacce4d1445125c12079d606e97143523ab71a3ac39f902c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_daca88bfcbdcae4aacce4d1445125c12079d606e97143523ab71a3ac39f902c4->enter($__internal_daca88bfcbdcae4aacce4d1445125c12079d606e97143523ab71a3ac39f902c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_7d678ba3d23fd207dff6b82bc9b6616814cd169d6fc5aee47f9d9d6ee3d15bb2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d678ba3d23fd207dff6b82bc9b6616814cd169d6fc5aee47f9d9d6ee3d15bb2->enter($__internal_7d678ba3d23fd207dff6b82bc9b6616814cd169d6fc5aee47f9d9d6ee3d15bb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_daca88bfcbdcae4aacce4d1445125c12079d606e97143523ab71a3ac39f902c4->leave($__internal_daca88bfcbdcae4aacce4d1445125c12079d606e97143523ab71a3ac39f902c4_prof);

        
        $__internal_7d678ba3d23fd207dff6b82bc9b6616814cd169d6fc5aee47f9d9d6ee3d15bb2->leave($__internal_7d678ba3d23fd207dff6b82bc9b6616814cd169d6fc5aee47f9d9d6ee3d15bb2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}

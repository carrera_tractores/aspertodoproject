<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_8ea82a7d28817d8e82413cf6a9077e17cfaf1781775c193dfdd3b03ad31ff840 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9f5ab2e15ef2eebd77b6e873af3276fac84dca07baae1d16254dfaef6ca8119e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f5ab2e15ef2eebd77b6e873af3276fac84dca07baae1d16254dfaef6ca8119e->enter($__internal_9f5ab2e15ef2eebd77b6e873af3276fac84dca07baae1d16254dfaef6ca8119e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        $__internal_5f7463f47e9eb4c29120fcf47c0f36569e80e5c2f01875787d707f41a6ed0aac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f7463f47e9eb4c29120fcf47c0f36569e80e5c2f01875787d707f41a6ed0aac->enter($__internal_5f7463f47e9eb4c29120fcf47c0f36569e80e5c2f01875787d707f41a6ed0aac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_9f5ab2e15ef2eebd77b6e873af3276fac84dca07baae1d16254dfaef6ca8119e->leave($__internal_9f5ab2e15ef2eebd77b6e873af3276fac84dca07baae1d16254dfaef6ca8119e_prof);

        
        $__internal_5f7463f47e9eb4c29120fcf47c0f36569e80e5c2f01875787d707f41a6ed0aac->leave($__internal_5f7463f47e9eb4c29120fcf47c0f36569e80e5c2f01875787d707f41a6ed0aac_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
", "@Security/Collector/icon.svg", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/SecurityBundle/Resources/views/Collector/icon.svg");
    }
}

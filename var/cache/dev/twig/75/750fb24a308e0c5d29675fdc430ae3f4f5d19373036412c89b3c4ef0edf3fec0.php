<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_68453531cbf12f04174b0746d766a2514f8ecaa9b452242a005e06f38621a419 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0c6d062f5076045123c17271dea54b516d07805c20913abd46a74f8c0e651977 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c6d062f5076045123c17271dea54b516d07805c20913abd46a74f8c0e651977->enter($__internal_0c6d062f5076045123c17271dea54b516d07805c20913abd46a74f8c0e651977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_3ed0912f84fe6a0de7a1e251d9dd5c968b5ec20e44dfbaff63bbd03d5c54a027 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ed0912f84fe6a0de7a1e251d9dd5c968b5ec20e44dfbaff63bbd03d5c54a027->enter($__internal_3ed0912f84fe6a0de7a1e251d9dd5c968b5ec20e44dfbaff63bbd03d5c54a027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_0c6d062f5076045123c17271dea54b516d07805c20913abd46a74f8c0e651977->leave($__internal_0c6d062f5076045123c17271dea54b516d07805c20913abd46a74f8c0e651977_prof);

        
        $__internal_3ed0912f84fe6a0de7a1e251d9dd5c968b5ec20e44dfbaff63bbd03d5c54a027->leave($__internal_3ed0912f84fe6a0de7a1e251d9dd5c968b5ec20e44dfbaff63bbd03d5c54a027_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}

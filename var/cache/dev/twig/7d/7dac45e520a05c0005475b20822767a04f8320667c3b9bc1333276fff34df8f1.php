<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_1308e42c276b0ccf7900cf9ae61a90099fc43f1ad8cb268cba7ab0e0a4e190cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f45367c0d51ab514e34f151bf89cfeef9e4e6613830a43dba5765cc4f2800c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f45367c0d51ab514e34f151bf89cfeef9e4e6613830a43dba5765cc4f2800c5->enter($__internal_0f45367c0d51ab514e34f151bf89cfeef9e4e6613830a43dba5765cc4f2800c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_4df65e929924bd4eddf5864e30ef3f08de17ed5e74baa117cc28806f01187d8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4df65e929924bd4eddf5864e30ef3f08de17ed5e74baa117cc28806f01187d8f->enter($__internal_4df65e929924bd4eddf5864e30ef3f08de17ed5e74baa117cc28806f01187d8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_0f45367c0d51ab514e34f151bf89cfeef9e4e6613830a43dba5765cc4f2800c5->leave($__internal_0f45367c0d51ab514e34f151bf89cfeef9e4e6613830a43dba5765cc4f2800c5_prof);

        
        $__internal_4df65e929924bd4eddf5864e30ef3f08de17ed5e74baa117cc28806f01187d8f->leave($__internal_4df65e929924bd4eddf5864e30ef3f08de17ed5e74baa117cc28806f01187d8f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}

<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_88ac4dcba91bbdda879a1ab8322ced554b3741ffabb3eef5d38b08715666fee1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24874828827d45a4fbeb2f1318c6277b7b8968f3b9fc8dffaf4ba3aae17d6c66 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24874828827d45a4fbeb2f1318c6277b7b8968f3b9fc8dffaf4ba3aae17d6c66->enter($__internal_24874828827d45a4fbeb2f1318c6277b7b8968f3b9fc8dffaf4ba3aae17d6c66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $__internal_464abed27447c153111e0ffa9bdb39402022dd7d545e06bc2d215610d677d106 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_464abed27447c153111e0ffa9bdb39402022dd7d545e06bc2d215610d677d106->enter($__internal_464abed27447c153111e0ffa9bdb39402022dd7d545e06bc2d215610d677d106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_24874828827d45a4fbeb2f1318c6277b7b8968f3b9fc8dffaf4ba3aae17d6c66->leave($__internal_24874828827d45a4fbeb2f1318c6277b7b8968f3b9fc8dffaf4ba3aae17d6c66_prof);

        
        $__internal_464abed27447c153111e0ffa9bdb39402022dd7d545e06bc2d215610d677d106->leave($__internal_464abed27447c153111e0ffa9bdb39402022dd7d545e06bc2d215610d677d106_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_752abc33a06dbaa3424d66835520cd9b2236bac8d0df7f418874779ad6606129 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_752abc33a06dbaa3424d66835520cd9b2236bac8d0df7f418874779ad6606129->enter($__internal_752abc33a06dbaa3424d66835520cd9b2236bac8d0df7f418874779ad6606129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_1fb15e773214d3e84bf73e38763fff73d9f2f700af407661359784377e22abc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fb15e773214d3e84bf73e38763fff73d9f2f700af407661359784377e22abc7->enter($__internal_1fb15e773214d3e84bf73e38763fff73d9f2f700af407661359784377e22abc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_1fb15e773214d3e84bf73e38763fff73d9f2f700af407661359784377e22abc7->leave($__internal_1fb15e773214d3e84bf73e38763fff73d9f2f700af407661359784377e22abc7_prof);

        
        $__internal_752abc33a06dbaa3424d66835520cd9b2236bac8d0df7f418874779ad6606129->leave($__internal_752abc33a06dbaa3424d66835520cd9b2236bac8d0df7f418874779ad6606129_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:new.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/new.html.twig");
    }
}

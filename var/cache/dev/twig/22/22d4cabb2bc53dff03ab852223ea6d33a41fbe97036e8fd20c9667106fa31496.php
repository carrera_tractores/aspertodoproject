<?php

/* EasyAdminBundle:default:label_inaccessible.html.twig */
class __TwigTemplate_e4bf264da68763f33c660732dd8e51776b3b6d2f786c27bf5624667724dd16d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_babe5b3b391279c53a6ee720b026c38a48d1226cc89165f1feb021a9389f19b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_babe5b3b391279c53a6ee720b026c38a48d1226cc89165f1feb021a9389f19b8->enter($__internal_babe5b3b391279c53a6ee720b026c38a48d1226cc89165f1feb021a9389f19b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_inaccessible.html.twig"));

        $__internal_325dad377062e6ad96ba95c0f7bd87107a66f149af1500f4862bc4f0c595bfa1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_325dad377062e6ad96ba95c0f7bd87107a66f149af1500f4862bc4f0c595bfa1->enter($__internal_325dad377062e6ad96ba95c0f7bd87107a66f149af1500f4862bc4f0c595bfa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:label_inaccessible.html.twig"));

        // line 2
        echo "
<span class=\"label label-danger\" title=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.inaccessible.explanation", array(), "EasyAdminBundle"), "html", null, true);
        echo "\">
    ";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.inaccessible", array(), "EasyAdminBundle"), "html", null, true);
        echo "
</span>
";
        
        $__internal_babe5b3b391279c53a6ee720b026c38a48d1226cc89165f1feb021a9389f19b8->leave($__internal_babe5b3b391279c53a6ee720b026c38a48d1226cc89165f1feb021a9389f19b8_prof);

        
        $__internal_325dad377062e6ad96ba95c0f7bd87107a66f149af1500f4862bc4f0c595bfa1->leave($__internal_325dad377062e6ad96ba95c0f7bd87107a66f149af1500f4862bc4f0c595bfa1_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:label_inaccessible.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 4,  28 => 3,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'EasyAdminBundle' %}

<span class=\"label label-danger\" title=\"{{ 'label.inaccessible.explanation'|trans }}\">
    {{ 'label.inaccessible'|trans }}
</span>
", "EasyAdminBundle:default:label_inaccessible.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/label_inaccessible.html.twig");
    }
}

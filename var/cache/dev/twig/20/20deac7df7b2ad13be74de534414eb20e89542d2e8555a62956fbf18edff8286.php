<?php

/* EasyAdminBundle:default:new.html.twig */
class __TwigTemplate_a045a3a91d595f265f66311b30572e1181f559a8c0124881138d3f1b75d45068 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 7
        return $this->loadTemplate(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 7, $this->getSourceContext()); })()), "templates", array()), "layout", array()), "EasyAdminBundle:default:new.html.twig", 7);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_15becffb26ba1bf075de6aa7fdaef37e464a83882561f82569eeba3ebe2c5b5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15becffb26ba1bf075de6aa7fdaef37e464a83882561f82569eeba3ebe2c5b5a->enter($__internal_15becffb26ba1bf075de6aa7fdaef37e464a83882561f82569eeba3ebe2c5b5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:new.html.twig"));

        $__internal_be1ed603980c5f3620acaed08a9a839a5a014bae26f3f9a8db5290c576d9c5fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be1ed603980c5f3620acaed08a9a839a5a014bae26f3f9a8db5290c576d9c5fb->enter($__internal_be1ed603980c5f3620acaed08a9a839a5a014bae26f3f9a8db5290c576d9c5fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:new.html.twig"));

        // line 1
        $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 1, $this->getSourceContext()); })()), $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.form_theme"));
        // line 3
        $context["_entity_config"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getEntityConfiguration(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 3, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "entity"), "method"));
        // line 4
        $context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"] = twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 4, $this->getSourceContext()); })()), "translation_domain", array());
        // line 5
        $context["_trans_parameters"] = array("%entity_name%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->getSourceContext()); })()), "name", array()), array(),         // line 4
(isset($context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"]) || array_key_exists("__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338", $context) ? $context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338" does not exist.', 4, $this->getSourceContext()); })())), "%entity_label%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(),         // line 5
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->getSourceContext()); })()), "label", array()), array(),         // line 4
(isset($context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"]) || array_key_exists("__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338", $context) ? $context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338" does not exist.', 4, $this->getSourceContext()); })())));
        // line 7
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_15becffb26ba1bf075de6aa7fdaef37e464a83882561f82569eeba3ebe2c5b5a->leave($__internal_15becffb26ba1bf075de6aa7fdaef37e464a83882561f82569eeba3ebe2c5b5a_prof);

        
        $__internal_be1ed603980c5f3620acaed08a9a839a5a014bae26f3f9a8db5290c576d9c5fb->leave($__internal_be1ed603980c5f3620acaed08a9a839a5a014bae26f3f9a8db5290c576d9c5fb_prof);

    }

    // line 9
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_c5dcfe3361ccd6f2194614d36d24511cbb98eef123184bd5bc9f0ad54eddadb9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5dcfe3361ccd6f2194614d36d24511cbb98eef123184bd5bc9f0ad54eddadb9->enter($__internal_c5dcfe3361ccd6f2194614d36d24511cbb98eef123184bd5bc9f0ad54eddadb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_13ca80dac00984d20a7603f9033caf89a3bbf13edb73293b1202b2c3b3a80efd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13ca80dac00984d20a7603f9033caf89a3bbf13edb73293b1202b2c3b3a80efd->enter($__internal_13ca80dac00984d20a7603f9033caf89a3bbf13edb73293b1202b2c3b3a80efd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ("easyadmin-new-" . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 9, $this->getSourceContext()); })()), "name", array())), "html", null, true);
        
        $__internal_13ca80dac00984d20a7603f9033caf89a3bbf13edb73293b1202b2c3b3a80efd->leave($__internal_13ca80dac00984d20a7603f9033caf89a3bbf13edb73293b1202b2c3b3a80efd_prof);

        
        $__internal_c5dcfe3361ccd6f2194614d36d24511cbb98eef123184bd5bc9f0ad54eddadb9->leave($__internal_c5dcfe3361ccd6f2194614d36d24511cbb98eef123184bd5bc9f0ad54eddadb9_prof);

    }

    // line 10
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_ef083878c8994f7e5e2379daf55e3820c5c77395a01eba91626242b96df6d148 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef083878c8994f7e5e2379daf55e3820c5c77395a01eba91626242b96df6d148->enter($__internal_ef083878c8994f7e5e2379daf55e3820c5c77395a01eba91626242b96df6d148_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_5e1227b7baed43b8f7f0284d09e577ef10518f4b0f735f7833b6e4d9c499724e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e1227b7baed43b8f7f0284d09e577ef10518f4b0f735f7833b6e4d9c499724e->enter($__internal_5e1227b7baed43b8f7f0284d09e577ef10518f4b0f735f7833b6e4d9c499724e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("new new-" . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 10, $this->getSourceContext()); })()), "name", array()))), "html", null, true);
        
        $__internal_5e1227b7baed43b8f7f0284d09e577ef10518f4b0f735f7833b6e4d9c499724e->leave($__internal_5e1227b7baed43b8f7f0284d09e577ef10518f4b0f735f7833b6e4d9c499724e_prof);

        
        $__internal_ef083878c8994f7e5e2379daf55e3820c5c77395a01eba91626242b96df6d148->leave($__internal_ef083878c8994f7e5e2379daf55e3820c5c77395a01eba91626242b96df6d148_prof);

    }

    // line 12
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_f119d5adf718c265ed8a80b1a03a99426b3a1626881309b03775247ebeee94b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f119d5adf718c265ed8a80b1a03a99426b3a1626881309b03775247ebeee94b8->enter($__internal_f119d5adf718c265ed8a80b1a03a99426b3a1626881309b03775247ebeee94b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_71899a13ba3809ce72cd3e06ffab9f3dc168d2a4c7777c1d16872d4028dcafca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71899a13ba3809ce72cd3e06ffab9f3dc168d2a4c7777c1d16872d4028dcafca->enter($__internal_71899a13ba3809ce72cd3e06ffab9f3dc168d2a4c7777c1d16872d4028dcafca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 13
        ob_start();
        // line 14
        echo "    ";
        $context["_default_title"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("new.page_title", (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 14, $this->getSourceContext()); })()), "EasyAdminBundle");
        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), "new", array(), "any", false, true), "title", array(), "any", true, true)) ? ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 15, $this->getSourceContext()); })()), "new", array()), "title", array()), (isset($context["_trans_parameters"]) || array_key_exists("_trans_parameters", $context) ? $context["_trans_parameters"] : (function () { throw new Twig_Error_Runtime('Variable "_trans_parameters" does not exist.', 15, $this->getSourceContext()); })()),         // line 4
(isset($context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"]) || array_key_exists("__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338", $context) ? $context["__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_852ead106ddd5824ada2cf96f769ba9b7e5f61c4633e37319f328ec2c9e18338" does not exist.', 4, $this->getSourceContext()); })()))) : (        // line 15
(isset($context["_default_title"]) || array_key_exists("_default_title", $context) ? $context["_default_title"] : (function () { throw new Twig_Error_Runtime('Variable "_default_title" does not exist.', 15, $this->getSourceContext()); })()))), "html", null, true);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_71899a13ba3809ce72cd3e06ffab9f3dc168d2a4c7777c1d16872d4028dcafca->leave($__internal_71899a13ba3809ce72cd3e06ffab9f3dc168d2a4c7777c1d16872d4028dcafca_prof);

        
        $__internal_f119d5adf718c265ed8a80b1a03a99426b3a1626881309b03775247ebeee94b8->leave($__internal_f119d5adf718c265ed8a80b1a03a99426b3a1626881309b03775247ebeee94b8_prof);

    }

    // line 19
    public function block_main($context, array $blocks = array())
    {
        $__internal_54a5994b62ec6413a321c0b4eb02fa65ff814cd24f4e4470551aa769a0489fed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54a5994b62ec6413a321c0b4eb02fa65ff814cd24f4e4470551aa769a0489fed->enter($__internal_54a5994b62ec6413a321c0b4eb02fa65ff814cd24f4e4470551aa769a0489fed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_c9c504fc706acdf1dbdf923c9a3d753d90a73da51dcd1662cdcaba0613e47353 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9c504fc706acdf1dbdf923c9a3d753d90a73da51dcd1662cdcaba0613e47353->enter($__internal_c9c504fc706acdf1dbdf923c9a3d753d90a73da51dcd1662cdcaba0613e47353_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 20
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_c9c504fc706acdf1dbdf923c9a3d753d90a73da51dcd1662cdcaba0613e47353->leave($__internal_c9c504fc706acdf1dbdf923c9a3d753d90a73da51dcd1662cdcaba0613e47353_prof);

        
        $__internal_54a5994b62ec6413a321c0b4eb02fa65ff814cd24f4e4470551aa769a0489fed->leave($__internal_54a5994b62ec6413a321c0b4eb02fa65ff814cd24f4e4470551aa769a0489fed_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_d0334fcb54c42856fb23ced5765d83b776acd1d1759e301d3ef5357835d160c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0334fcb54c42856fb23ced5765d83b776acd1d1759e301d3ef5357835d160c5->enter($__internal_d0334fcb54c42856fb23ced5765d83b776acd1d1759e301d3ef5357835d160c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_7e916a5b6c3e33d58a612af29460780097ed94cce9edd377be827148e0815820 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e916a5b6c3e33d58a612af29460780097ed94cce9edd377be827148e0815820->enter($__internal_7e916a5b6c3e33d58a612af29460780097ed94cce9edd377be827148e0815820_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 21
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 21, $this->getSourceContext()); })()), 'form');
        echo "
    ";
        
        $__internal_7e916a5b6c3e33d58a612af29460780097ed94cce9edd377be827148e0815820->leave($__internal_7e916a5b6c3e33d58a612af29460780097ed94cce9edd377be827148e0815820_prof);

        
        $__internal_d0334fcb54c42856fb23ced5765d83b776acd1d1759e301d3ef5357835d160c5->leave($__internal_d0334fcb54c42856fb23ced5765d83b776acd1d1759e301d3ef5357835d160c5_prof);

    }

    // line 25
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_38022a6571181f8ec4cb4d128998ab3c60c1203a2178f702e0c57af70a5ee814 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38022a6571181f8ec4cb4d128998ab3c60c1203a2178f702e0c57af70a5ee814->enter($__internal_38022a6571181f8ec4cb4d128998ab3c60c1203a2178f702e0c57af70a5ee814_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_e8822523c78b554f5b518699291488d63f0db551facc740531fff6ea5a78d2e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8822523c78b554f5b518699291488d63f0db551facc740531fff6ea5a78d2e7->enter($__internal_e8822523c78b554f5b518699291488d63f0db551facc740531fff6ea5a78d2e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 26
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "

    <script type=\"text/javascript\">
        \$(function() {
            \$('.new-form').areYouSure({ 'message': '";
        // line 30
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("form.are_you_sure", array(), "EasyAdminBundle"), "js"), "html", null, true);
        echo "' });

            \$('.form-actions').easyAdminSticky();
        });
    </script>

    ";
        // line 36
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_e8822523c78b554f5b518699291488d63f0db551facc740531fff6ea5a78d2e7->leave($__internal_e8822523c78b554f5b518699291488d63f0db551facc740531fff6ea5a78d2e7_prof);

        
        $__internal_38022a6571181f8ec4cb4d128998ab3c60c1203a2178f702e0c57af70a5ee814->leave($__internal_38022a6571181f8ec4cb4d128998ab3c60c1203a2178f702e0c57af70a5ee814_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 36,  180 => 30,  172 => 26,  163 => 25,  150 => 21,  131 => 20,  122 => 19,  109 => 15,  108 => 4,  106 => 15,  103 => 14,  101 => 13,  92 => 12,  74 => 10,  56 => 9,  46 => 7,  44 => 4,  43 => 5,  42 => 4,  41 => 5,  39 => 4,  37 => 3,  35 => 1,  23 => 7,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% form_theme form with easyadmin_config('design.form_theme') %}

{% set _entity_config = easyadmin_entity(app.request.query.get('entity')) %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-new-' ~ _entity_config.name %}
{% block body_class 'new new-' ~ _entity_config.name|lower %}

{% block content_title %}
{% spaceless %}
    {% set _default_title = 'new.page_title'|trans(_trans_parameters, 'EasyAdminBundle') %}
    {{ _entity_config.new.title is defined ? _entity_config.new.title|trans(_trans_parameters) : _default_title }}
{% endspaceless %}
{% endblock %}

{% block main %}
    {% block entity_form %}
        {{ form(form) }}
    {% endblock entity_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}

    <script type=\"text/javascript\">
        \$(function() {
            \$('.new-form').areYouSure({ 'message': '{{ 'form.are_you_sure'|trans({}, 'EasyAdminBundle')|e('js') }}' });

            \$('.form-actions').easyAdminSticky();
        });
    </script>

    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "EasyAdminBundle:default:new.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/new.html.twig");
    }
}

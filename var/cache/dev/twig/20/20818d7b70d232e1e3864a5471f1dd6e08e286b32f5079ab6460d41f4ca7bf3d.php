<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_b6e5f1a676419aa491441dc8451081b477c50c0f48adca359ee9dfaf5838958a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0480c7317750929691f3a56d8f07ac9021e0dca27f93171155d6566f53d8c47c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0480c7317750929691f3a56d8f07ac9021e0dca27f93171155d6566f53d8c47c->enter($__internal_0480c7317750929691f3a56d8f07ac9021e0dca27f93171155d6566f53d8c47c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_d128056d393cb56e95a6b406c3a9367eb631b1be6ec9ff4dfd36293289fb07b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d128056d393cb56e95a6b406c3a9367eb631b1be6ec9ff4dfd36293289fb07b9->enter($__internal_d128056d393cb56e95a6b406c3a9367eb631b1be6ec9ff4dfd36293289fb07b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_0480c7317750929691f3a56d8f07ac9021e0dca27f93171155d6566f53d8c47c->leave($__internal_0480c7317750929691f3a56d8f07ac9021e0dca27f93171155d6566f53d8c47c_prof);

        
        $__internal_d128056d393cb56e95a6b406c3a9367eb631b1be6ec9ff4dfd36293289fb07b9->leave($__internal_d128056d393cb56e95a6b406c3a9367eb631b1be6ec9ff4dfd36293289fb07b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}

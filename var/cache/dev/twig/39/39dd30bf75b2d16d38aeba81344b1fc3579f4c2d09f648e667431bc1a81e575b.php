<?php

/* ::new.html.twig */
class __TwigTemplate_41a1c15b2c57d8a12234b9e625859347c84aecd7d0f55d7178172d455cbd0df1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5eab5cc0da14f8d74d301aaa057810a1bf6db4b295f0ad40a965f6bb29cda5df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5eab5cc0da14f8d74d301aaa057810a1bf6db4b295f0ad40a965f6bb29cda5df->enter($__internal_5eab5cc0da14f8d74d301aaa057810a1bf6db4b295f0ad40a965f6bb29cda5df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::new.html.twig"));

        $__internal_0822a979333a835293df595ec7471dde6ffab4bb0dade92afa171e900b7f9ae5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0822a979333a835293df595ec7471dde6ffab4bb0dade92afa171e900b7f9ae5->enter($__internal_0822a979333a835293df595ec7471dde6ffab4bb0dade92afa171e900b7f9ae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::new.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
 \t <h3> ";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new Twig_Error_Runtime('Variable "title" does not exist.', 10, $this->getSourceContext()); })()), "html", null, true);
        echo "</h3>
 \t 
    \t  ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 12, $this->getSourceContext()); })()), 'form_start');
        echo "
    \t  ";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 13, $this->getSourceContext()); })()), 'widget');
        echo "
    \t  ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 14, $this->getSourceContext()); })()), 'form_end');
        echo "
    \t  
        ";
        // line 16
        $this->displayBlock('body', $context, $blocks);
        // line 17
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 18
        echo "    </body>
</html>
";
        
        $__internal_5eab5cc0da14f8d74d301aaa057810a1bf6db4b295f0ad40a965f6bb29cda5df->leave($__internal_5eab5cc0da14f8d74d301aaa057810a1bf6db4b295f0ad40a965f6bb29cda5df_prof);

        
        $__internal_0822a979333a835293df595ec7471dde6ffab4bb0dade92afa171e900b7f9ae5->leave($__internal_0822a979333a835293df595ec7471dde6ffab4bb0dade92afa171e900b7f9ae5_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fc46cb4492466caa3e898a938e208b049a9aac1f49149073fe4ca99934ede629 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc46cb4492466caa3e898a938e208b049a9aac1f49149073fe4ca99934ede629->enter($__internal_fc46cb4492466caa3e898a938e208b049a9aac1f49149073fe4ca99934ede629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_c4833248f882cbc52345af02e887dec1ffbc54e9406cfe18656c7b806349fd21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4833248f882cbc52345af02e887dec1ffbc54e9406cfe18656c7b806349fd21->enter($__internal_c4833248f882cbc52345af02e887dec1ffbc54e9406cfe18656c7b806349fd21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Formulari!";
        
        $__internal_c4833248f882cbc52345af02e887dec1ffbc54e9406cfe18656c7b806349fd21->leave($__internal_c4833248f882cbc52345af02e887dec1ffbc54e9406cfe18656c7b806349fd21_prof);

        
        $__internal_fc46cb4492466caa3e898a938e208b049a9aac1f49149073fe4ca99934ede629->leave($__internal_fc46cb4492466caa3e898a938e208b049a9aac1f49149073fe4ca99934ede629_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_50ce6ad16eac33129419462b6b1fd7c872578caa626844f3b55d7d64e7653087 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50ce6ad16eac33129419462b6b1fd7c872578caa626844f3b55d7d64e7653087->enter($__internal_50ce6ad16eac33129419462b6b1fd7c872578caa626844f3b55d7d64e7653087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_5aee4572b8449ac9f44dc3a1e577e53d8c792b978416f0fe3c3ba58cbaa74707 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aee4572b8449ac9f44dc3a1e577e53d8c792b978416f0fe3c3ba58cbaa74707->enter($__internal_5aee4572b8449ac9f44dc3a1e577e53d8c792b978416f0fe3c3ba58cbaa74707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_5aee4572b8449ac9f44dc3a1e577e53d8c792b978416f0fe3c3ba58cbaa74707->leave($__internal_5aee4572b8449ac9f44dc3a1e577e53d8c792b978416f0fe3c3ba58cbaa74707_prof);

        
        $__internal_50ce6ad16eac33129419462b6b1fd7c872578caa626844f3b55d7d64e7653087->leave($__internal_50ce6ad16eac33129419462b6b1fd7c872578caa626844f3b55d7d64e7653087_prof);

    }

    // line 16
    public function block_body($context, array $blocks = array())
    {
        $__internal_f586954be72756f11c8501a3c6c2a293ac0a71814b3dc434720a3bb0419a30a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f586954be72756f11c8501a3c6c2a293ac0a71814b3dc434720a3bb0419a30a2->enter($__internal_f586954be72756f11c8501a3c6c2a293ac0a71814b3dc434720a3bb0419a30a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_40ae3603100cf20636384c2d6f06f14930069f2df8669af5e8d22e48f9c88acf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40ae3603100cf20636384c2d6f06f14930069f2df8669af5e8d22e48f9c88acf->enter($__internal_40ae3603100cf20636384c2d6f06f14930069f2df8669af5e8d22e48f9c88acf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_40ae3603100cf20636384c2d6f06f14930069f2df8669af5e8d22e48f9c88acf->leave($__internal_40ae3603100cf20636384c2d6f06f14930069f2df8669af5e8d22e48f9c88acf_prof);

        
        $__internal_f586954be72756f11c8501a3c6c2a293ac0a71814b3dc434720a3bb0419a30a2->leave($__internal_f586954be72756f11c8501a3c6c2a293ac0a71814b3dc434720a3bb0419a30a2_prof);

    }

    // line 17
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_909c8cb72dd2f94b13a233da8f2e1360abec72b34591d04db74d629683b745f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_909c8cb72dd2f94b13a233da8f2e1360abec72b34591d04db74d629683b745f7->enter($__internal_909c8cb72dd2f94b13a233da8f2e1360abec72b34591d04db74d629683b745f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_37e4905acfc7879882a4ab7f1f87fa8a9d89f42d4f34bd95463a73d61e29199f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37e4905acfc7879882a4ab7f1f87fa8a9d89f42d4f34bd95463a73d61e29199f->enter($__internal_37e4905acfc7879882a4ab7f1f87fa8a9d89f42d4f34bd95463a73d61e29199f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_37e4905acfc7879882a4ab7f1f87fa8a9d89f42d4f34bd95463a73d61e29199f->leave($__internal_37e4905acfc7879882a4ab7f1f87fa8a9d89f42d4f34bd95463a73d61e29199f_prof);

        
        $__internal_909c8cb72dd2f94b13a233da8f2e1360abec72b34591d04db74d629683b745f7->leave($__internal_909c8cb72dd2f94b13a233da8f2e1360abec72b34591d04db74d629683b745f7_prof);

    }

    public function getTemplateName()
    {
        return "::new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 17,  118 => 16,  101 => 6,  83 => 5,  71 => 18,  68 => 17,  66 => 16,  61 => 14,  57 => 13,  53 => 12,  48 => 10,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Formulari!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
 \t <h3> {{title}}</h3>
 \t 
    \t  {{ form_start(form) }}
    \t  {{ form_widget(form) }}
    \t  {{ form_end(form) }}
    \t  
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::new.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/new.html.twig");
    }
}

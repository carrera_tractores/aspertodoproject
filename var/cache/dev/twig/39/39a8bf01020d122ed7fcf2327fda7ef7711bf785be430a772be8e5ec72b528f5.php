<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_bd17c19613c71452bbf0f935e7cdd4c01103816a19b1af3ba46db7400e25ed40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa3f8174931d50702ae5f672882882930f45c66f8fc5353445b23a5a91459135 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa3f8174931d50702ae5f672882882930f45c66f8fc5353445b23a5a91459135->enter($__internal_aa3f8174931d50702ae5f672882882930f45c66f8fc5353445b23a5a91459135_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_b38fcae242d893fbf47c3ba88a9d9bc87f68c77d1185cb20af5a3bf80c69f138 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b38fcae242d893fbf47c3ba88a9d9bc87f68c77d1185cb20af5a3bf80c69f138->enter($__internal_b38fcae242d893fbf47c3ba88a9d9bc87f68c77d1185cb20af5a3bf80c69f138_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_aa3f8174931d50702ae5f672882882930f45c66f8fc5353445b23a5a91459135->leave($__internal_aa3f8174931d50702ae5f672882882930f45c66f8fc5353445b23a5a91459135_prof);

        
        $__internal_b38fcae242d893fbf47c3ba88a9d9bc87f68c77d1185cb20af5a3bf80c69f138->leave($__internal_b38fcae242d893fbf47c3ba88a9d9bc87f68c77d1185cb20af5a3bf80c69f138_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}

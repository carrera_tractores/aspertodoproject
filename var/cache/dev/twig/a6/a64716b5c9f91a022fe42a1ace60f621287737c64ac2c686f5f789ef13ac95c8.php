<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_9e7199e18faf450000c93fbfa59616637a3be606dbdb3f5910364c6891a8e8ac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fed8e75086bd3bfd6cb2474b980f2bd5240e603ad8924f896433d93202c0416a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fed8e75086bd3bfd6cb2474b980f2bd5240e603ad8924f896433d93202c0416a->enter($__internal_fed8e75086bd3bfd6cb2474b980f2bd5240e603ad8924f896433d93202c0416a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_292ba7cd2be3aaf48fe669ae76362324b0d92eef1743802340b2487ba9bd8c22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_292ba7cd2be3aaf48fe669ae76362324b0d92eef1743802340b2487ba9bd8c22->enter($__internal_292ba7cd2be3aaf48fe669ae76362324b0d92eef1743802340b2487ba9bd8c22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_fed8e75086bd3bfd6cb2474b980f2bd5240e603ad8924f896433d93202c0416a->leave($__internal_fed8e75086bd3bfd6cb2474b980f2bd5240e603ad8924f896433d93202c0416a_prof);

        
        $__internal_292ba7cd2be3aaf48fe669ae76362324b0d92eef1743802340b2487ba9bd8c22->leave($__internal_292ba7cd2be3aaf48fe669ae76362324b0d92eef1743802340b2487ba9bd8c22_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/forward.svg");
    }
}

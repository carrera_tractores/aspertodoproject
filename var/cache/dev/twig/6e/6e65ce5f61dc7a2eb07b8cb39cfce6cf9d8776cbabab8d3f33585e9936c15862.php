<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_aef96eff7d47c3206b946d516a057d34a3b68b64621a8e5c4d7334bcbdcfae35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1595b83b62e1a1391aa63b3f9e466075975c000a9635ab64edbbdc8dec08c8ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1595b83b62e1a1391aa63b3f9e466075975c000a9635ab64edbbdc8dec08c8ff->enter($__internal_1595b83b62e1a1391aa63b3f9e466075975c000a9635ab64edbbdc8dec08c8ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_d5852b4b3d9dd183b6a10379e05f0f786b47068713fa251b1162c0fcf1c69c7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5852b4b3d9dd183b6a10379e05f0f786b47068713fa251b1162c0fcf1c69c7d->enter($__internal_d5852b4b3d9dd183b6a10379e05f0f786b47068713fa251b1162c0fcf1c69c7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_1595b83b62e1a1391aa63b3f9e466075975c000a9635ab64edbbdc8dec08c8ff->leave($__internal_1595b83b62e1a1391aa63b3f9e466075975c000a9635ab64edbbdc8dec08c8ff_prof);

        
        $__internal_d5852b4b3d9dd183b6a10379e05f0f786b47068713fa251b1162c0fcf1c69c7d->leave($__internal_d5852b4b3d9dd183b6a10379e05f0f786b47068713fa251b1162c0fcf1c69c7d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}

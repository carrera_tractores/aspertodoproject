<?php

/* FOSUserBundle:Registration:register_content.html.twig */
class __TwigTemplate_77d5100473a4a58d106e552951b191a4c528df21d74038b3df90161ec74f78a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3537a28033e85c8898c5bf09c5b4c452af0257b41f6748f444de781dbbe5c72d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3537a28033e85c8898c5bf09c5b4c452af0257b41f6748f444de781dbbe5c72d->enter($__internal_3537a28033e85c8898c5bf09c5b4c452af0257b41f6748f444de781dbbe5c72d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        $__internal_24211fe9362da40a800a41ea708ff1ea2b264708d3df72aa2bed5cdc16f3fca4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24211fe9362da40a800a41ea708ff1ea2b264708d3df72aa2bed5cdc16f3fca4->enter($__internal_24211fe9362da40a800a41ea708ff1ea2b264708d3df72aa2bed5cdc16f3fca4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register_content.html.twig"));

        // line 1
        echo "\t\t";
        // line 2
        echo "
\t\t";
        // line 3
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 3, $this->getSourceContext()); })()), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
    \t\t";
        // line 4
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 4, $this->getSourceContext()); })()), 'widget');
        echo "
    \t\t<div>
        \t\t<input type=\"submit\" id=\"_submit\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
   \t\t</div>
\t\t";
        // line 8
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'form_end');
        
        $__internal_3537a28033e85c8898c5bf09c5b4c452af0257b41f6748f444de781dbbe5c72d->leave($__internal_3537a28033e85c8898c5bf09c5b4c452af0257b41f6748f444de781dbbe5c72d_prof);

        
        $__internal_24211fe9362da40a800a41ea708ff1ea2b264708d3df72aa2bed5cdc16f3fca4->leave($__internal_24211fe9362da40a800a41ea708ff1ea2b264708d3df72aa2bed5cdc16f3fca4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 8,  39 => 6,  34 => 4,  30 => 3,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("\t\t{% trans_default_domain 'FOSUserBundle' %}

\t\t{{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
    \t\t{{ form_widget(form) }}
    \t\t<div>
        \t\t<input type=\"submit\" id=\"_submit\" value=\"{{ 'registration.submit'|trans }}\" />
   \t\t</div>
\t\t{{ form_end(form) }}", "FOSUserBundle:Registration:register_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Registration/register_content.html.twig");
    }
}

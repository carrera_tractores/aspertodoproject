<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_64accb351ce50ed6a960c322642acd589ba88a7ac59bf7e5912de22befb6464b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a051b2b97216eb865a3d7ee7ef67c8da80ed19630bf4036c552e6c18ecbb63dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a051b2b97216eb865a3d7ee7ef67c8da80ed19630bf4036c552e6c18ecbb63dd->enter($__internal_a051b2b97216eb865a3d7ee7ef67c8da80ed19630bf4036c552e6c18ecbb63dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_d452b4dee920c70f47bb607b2a0dc400b758ed94c82cebbece00a6df5743544f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d452b4dee920c70f47bb607b2a0dc400b758ed94c82cebbece00a6df5743544f->enter($__internal_d452b4dee920c70f47bb607b2a0dc400b758ed94c82cebbece00a6df5743544f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_a051b2b97216eb865a3d7ee7ef67c8da80ed19630bf4036c552e6c18ecbb63dd->leave($__internal_a051b2b97216eb865a3d7ee7ef67c8da80ed19630bf4036c552e6c18ecbb63dd_prof);

        
        $__internal_d452b4dee920c70f47bb607b2a0dc400b758ed94c82cebbece00a6df5743544f->leave($__internal_d452b4dee920c70f47bb607b2a0dc400b758ed94c82cebbece00a6df5743544f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}

<?php

/* WebProfilerBundle:Collector:exception.css.twig */
class __TwigTemplate_fdb865119190ec9750707832c67ce7770ec068899c432284e8e42de83a7cabc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_feec1b08b522919f3ec46848d095334563c244cb67e4b2fc89645eceb64f14d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_feec1b08b522919f3ec46848d095334563c244cb67e4b2fc89645eceb64f14d1->enter($__internal_feec1b08b522919f3ec46848d095334563c244cb67e4b2fc89645eceb64f14d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        $__internal_91d0cda6e552525e1020f59e075d601109e390e974baeb3f764a592452a322a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91d0cda6e552525e1020f59e075d601109e390e974baeb3f764a592452a322a4->enter($__internal_91d0cda6e552525e1020f59e075d601109e390e974baeb3f764a592452a322a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.css.twig"));

        // line 1
        echo ".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    font-family: Arial, Helvetica, sans-serif;
    position: absolute;
    color: #C9C9C9;
    font-size: 8em;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: -0.5em;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
";
        
        $__internal_feec1b08b522919f3ec46848d095334563c244cb67e4b2fc89645eceb64f14d1->leave($__internal_feec1b08b522919f3ec46848d095334563c244cb67e4b2fc89645eceb64f14d1_prof);

        
        $__internal_91d0cda6e552525e1020f59e075d601109e390e974baeb3f764a592452a322a4->leave($__internal_91d0cda6e552525e1020f59e075d601109e390e974baeb3f764a592452a322a4_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.css.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source(".sf-reset .traces {
    padding: 0 0 1em 1.5em;
}
.sf-reset .traces a {
    font-size: 14px;
}
.sf-reset .traces abbr {
    border-bottom-color: #AAA;
    padding-bottom: 2px;
}
.sf-reset .traces li {
    color: #222;
    font-size: 14px;
    padding: 5px 0;
    list-style-type: decimal;
    margin: 0 0 0 1em;
}
.sf-reset .traces li.selected {
    background: rgba(255, 255, 153, 0.5);
}

.sf-reset .traces ol li {
    font-size: 12px;
    color: #777;
}
.sf-reset #logs .traces li.error {
    color: #AA3333;
}
.sf-reset #logs .traces li.warning {
    background: #FFCC00;
}
.sf-reset .trace {
    border: 1px solid #DDD;
    background: #FFF;
    padding: 10px;
    overflow: auto;
    margin: 1em 0;
}
.sf-reset .trace code,
#traces-text pre {
    font-size: 13px;
}
.sf-reset .block-exception {
    margin-bottom: 2em;
    background-color: #FFF;
    border: 1px solid #EEE;
    padding: 28px;
    word-wrap: break-word;
    overflow: hidden;
}
.sf-reset .block-exception h1 {
    font-size: 21px;
    font-weight: normal;
    margin: 0 0 12px;
}
.sf-reset .block-exception .linked {
    margin-top: 1em;
}

.sf-reset .block {
    margin-bottom: 2em;
}
.sf-reset .block h2 {
    font-size: 16px;
}
.sf-reset .block-exception div {
    font-size: 14px;
}
.sf-reset .block-exception-detected .illustration-exception,
.sf-reset .block-exception-detected .text-exception {
    float: left;
}
.sf-reset .block-exception-detected .illustration-exception {
    width: 110px;
}
.sf-reset .block-exception-detected .text-exception {
    width: 650px;
    margin-left: 20px;
    padding: 30px 44px 24px 46px;
    position: relative;
}
.sf-reset .text-exception .open-quote,
.sf-reset .text-exception .close-quote {
    font-family: Arial, Helvetica, sans-serif;
    position: absolute;
    color: #C9C9C9;
    font-size: 8em;
}
.sf-reset .open-quote {
    top: 0;
    left: 0;
}
.sf-reset .close-quote {
    bottom: -0.5em;
    right: 50px;
}
.sf-reset .toggle {
    vertical-align: middle;
}
", "WebProfilerBundle:Collector:exception.css.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.css.twig");
    }
}

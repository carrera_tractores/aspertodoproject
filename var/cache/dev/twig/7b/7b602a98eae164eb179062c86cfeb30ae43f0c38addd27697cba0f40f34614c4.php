<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_2c0f2c8a6980ae4f2faacb8c78a1ed2abf726e7d5bfb0b7a1aa3a497122cc323 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9f9a0fb75701f6cbce294aba7c23fb1d868c1d72a464dcf2083d2ba65ec4e01 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9f9a0fb75701f6cbce294aba7c23fb1d868c1d72a464dcf2083d2ba65ec4e01->enter($__internal_d9f9a0fb75701f6cbce294aba7c23fb1d868c1d72a464dcf2083d2ba65ec4e01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_09f66273006d2eab3df1b7feda78689173907cf3c3a87598535e12b38c50bf39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09f66273006d2eab3df1b7feda78689173907cf3c3a87598535e12b38c50bf39->enter($__internal_09f66273006d2eab3df1b7feda78689173907cf3c3a87598535e12b38c50bf39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d9f9a0fb75701f6cbce294aba7c23fb1d868c1d72a464dcf2083d2ba65ec4e01->leave($__internal_d9f9a0fb75701f6cbce294aba7c23fb1d868c1d72a464dcf2083d2ba65ec4e01_prof);

        
        $__internal_09f66273006d2eab3df1b7feda78689173907cf3c3a87598535e12b38c50bf39->leave($__internal_09f66273006d2eab3df1b7feda78689173907cf3c3a87598535e12b38c50bf39_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_05f28feb006b0b1deb2a2db27f609a482e98e7a51ec9ea3c005a10df8ef19b6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05f28feb006b0b1deb2a2db27f609a482e98e7a51ec9ea3c005a10df8ef19b6a->enter($__internal_05f28feb006b0b1deb2a2db27f609a482e98e7a51ec9ea3c005a10df8ef19b6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_9e45958bf9c04ed119ee7d2250ab890d39e973eb1b0fca21e8a791968a1575fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e45958bf9c04ed119ee7d2250ab890d39e973eb1b0fca21e8a791968a1575fb->enter($__internal_9e45958bf9c04ed119ee7d2250ab890d39e973eb1b0fca21e8a791968a1575fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_9e45958bf9c04ed119ee7d2250ab890d39e973eb1b0fca21e8a791968a1575fb->leave($__internal_9e45958bf9c04ed119ee7d2250ab890d39e973eb1b0fca21e8a791968a1575fb_prof);

        
        $__internal_05f28feb006b0b1deb2a2db27f609a482e98e7a51ec9ea3c005a10df8ef19b6a->leave($__internal_05f28feb006b0b1deb2a2db27f609a482e98e7a51ec9ea3c005a10df8ef19b6a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}

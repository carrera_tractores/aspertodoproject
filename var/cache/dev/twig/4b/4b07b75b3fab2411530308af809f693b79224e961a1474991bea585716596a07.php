<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_92d795d9be96ea5504b429cd4405fdbf7220618638230ca47e9b14431fc7dc51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79f828205bff9e9614e461c773b0194548449679fe727aa441d8ed79387b0fc8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79f828205bff9e9614e461c773b0194548449679fe727aa441d8ed79387b0fc8->enter($__internal_79f828205bff9e9614e461c773b0194548449679fe727aa441d8ed79387b0fc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_1398d6d79c056229f776b11fd4200c65f6811dcbbdcd240fbf30f8c79e644259 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1398d6d79c056229f776b11fd4200c65f6811dcbbdcd240fbf30f8c79e644259->enter($__internal_1398d6d79c056229f776b11fd4200c65f6811dcbbdcd240fbf30f8c79e644259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_79f828205bff9e9614e461c773b0194548449679fe727aa441d8ed79387b0fc8->leave($__internal_79f828205bff9e9614e461c773b0194548449679fe727aa441d8ed79387b0fc8_prof);

        
        $__internal_1398d6d79c056229f776b11fd4200c65f6811dcbbdcd240fbf30f8c79e644259->leave($__internal_1398d6d79c056229f776b11fd4200c65f6811dcbbdcd240fbf30f8c79e644259_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}

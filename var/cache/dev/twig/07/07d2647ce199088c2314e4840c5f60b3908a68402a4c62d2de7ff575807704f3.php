<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_d14e83ea69fd36a636d6e1f0eac8381b9cfbc5efc3dd7f7f43025f5a7d9f80af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5203b2a9297d1119c3c7c0f01033a5de05e4fcbaac4c2269e094af387b53924a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5203b2a9297d1119c3c7c0f01033a5de05e4fcbaac4c2269e094af387b53924a->enter($__internal_5203b2a9297d1119c3c7c0f01033a5de05e4fcbaac4c2269e094af387b53924a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_05cc14cc594eff0d5e545e2c7fe13028cb12ab5fac17aa0c3c5df064479e65fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05cc14cc594eff0d5e545e2c7fe13028cb12ab5fac17aa0c3c5df064479e65fb->enter($__internal_05cc14cc594eff0d5e545e2c7fe13028cb12ab5fac17aa0c3c5df064479e65fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 3, $this->getSourceContext()); })()), "html", null, true);
        echo "\" />
";
        
        $__internal_5203b2a9297d1119c3c7c0f01033a5de05e4fcbaac4c2269e094af387b53924a->leave($__internal_5203b2a9297d1119c3c7c0f01033a5de05e4fcbaac4c2269e094af387b53924a_prof);

        
        $__internal_05cc14cc594eff0d5e545e2c7fe13028cb12ab5fac17aa0c3c5df064479e65fb->leave($__internal_05cc14cc594eff0d5e545e2c7fe13028cb12ab5fac17aa0c3c5df064479e65fb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}

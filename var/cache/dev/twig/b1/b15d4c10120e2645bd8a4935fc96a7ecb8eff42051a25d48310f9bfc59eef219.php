<?php

/* tasquesextres.html.twig~ */
class __TwigTemplate_b1829f84315ef4ecdbd266baa8a1ea069f9c9dfabfe8368b5d14db5751772fe4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_daace13c254ca9296864c9adab01d6dc6872b13f7db8e2705d7510f0079eb159 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_daace13c254ca9296864c9adab01d6dc6872b13f7db8e2705d7510f0079eb159->enter($__internal_daace13c254ca9296864c9adab01d6dc6872b13f7db8e2705d7510f0079eb159_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "tasquesextres.html.twig~"));

        $__internal_201f7f31b278eeec9dfa8c2dbeb6d1ad125183c1f96e30e38e662834f43ef8a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_201f7f31b278eeec9dfa8c2dbeb6d1ad125183c1f96e30e38e662834f43ef8a5->enter($__internal_201f7f31b278eeec9dfa8c2dbeb6d1ad125183c1f96e30e38e662834f43ef8a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "tasquesextres.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/tascaextra.css"), "html", null, true);
        echo "\" />  
        
    </head>
    <body>
    \t";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 16
        echo "\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
        echo "\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola ";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "user", array()), "username", array()), "html", null, true);
        echo "!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Tasques extres</h1>
      <hr>
      <div id=\"afegir_tasca_extra\">
\t\t\t<div id=\"imatge_afegir\"></div>
\t\t\t<a href=\"afegir_tasca_extra\">Afegir tasca extra</a>
\t\t</div>

\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        ";
        // line 61
        $this->displayBlock('javascripts', $context, $blocks);
        // line 66
        echo "    </body>
</html>
";
        
        $__internal_daace13c254ca9296864c9adab01d6dc6872b13f7db8e2705d7510f0079eb159->leave($__internal_daace13c254ca9296864c9adab01d6dc6872b13f7db8e2705d7510f0079eb159_prof);

        
        $__internal_201f7f31b278eeec9dfa8c2dbeb6d1ad125183c1f96e30e38e662834f43ef8a5->leave($__internal_201f7f31b278eeec9dfa8c2dbeb6d1ad125183c1f96e30e38e662834f43ef8a5_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fed5e2c627136ff305e0e900d36df5d8f935c180c0e286b9c569c407692ebec3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fed5e2c627136ff305e0e900d36df5d8f935c180c0e286b9c569c407692ebec3->enter($__internal_fed5e2c627136ff305e0e900d36df5d8f935c180c0e286b9c569c407692ebec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6213427b3d7a03a0885c7ee7773eee8df55e5c92db427b5f910d6a6e631e2d54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6213427b3d7a03a0885c7ee7773eee8df55e5c92db427b5f910d6a6e631e2d54->enter($__internal_6213427b3d7a03a0885c7ee7773eee8df55e5c92db427b5f910d6a6e631e2d54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Tasques extres";
        
        $__internal_6213427b3d7a03a0885c7ee7773eee8df55e5c92db427b5f910d6a6e631e2d54->leave($__internal_6213427b3d7a03a0885c7ee7773eee8df55e5c92db427b5f910d6a6e631e2d54_prof);

        
        $__internal_fed5e2c627136ff305e0e900d36df5d8f935c180c0e286b9c569c407692ebec3->leave($__internal_fed5e2c627136ff305e0e900d36df5d8f935c180c0e286b9c569c407692ebec3_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_88e68670b4b31e27bf5bb0b87abfa0133eafb773ac67b037c02e04bfa1b6661e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88e68670b4b31e27bf5bb0b87abfa0133eafb773ac67b037c02e04bfa1b6661e->enter($__internal_88e68670b4b31e27bf5bb0b87abfa0133eafb773ac67b037c02e04bfa1b6661e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3e92d3ad12c254a854667e809ad362710ee404bcb7e78d16086320d0b0f41571 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e92d3ad12c254a854667e809ad362710ee404bcb7e78d16086320d0b0f41571->enter($__internal_3e92d3ad12c254a854667e809ad362710ee404bcb7e78d16086320d0b0f41571_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3e92d3ad12c254a854667e809ad362710ee404bcb7e78d16086320d0b0f41571->leave($__internal_3e92d3ad12c254a854667e809ad362710ee404bcb7e78d16086320d0b0f41571_prof);

        
        $__internal_88e68670b4b31e27bf5bb0b87abfa0133eafb773ac67b037c02e04bfa1b6661e->leave($__internal_88e68670b4b31e27bf5bb0b87abfa0133eafb773ac67b037c02e04bfa1b6661e_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_a0eadc7cab00628993bff9728945b5573f5f26e56648b44aff37f5a84f49c93f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0eadc7cab00628993bff9728945b5573f5f26e56648b44aff37f5a84f49c93f->enter($__internal_a0eadc7cab00628993bff9728945b5573f5f26e56648b44aff37f5a84f49c93f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f23260fb090b83984ba07f2bb15c350c4061df7019b4bc5997b8747b9039809f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f23260fb090b83984ba07f2bb15c350c4061df7019b4bc5997b8747b9039809f->enter($__internal_f23260fb090b83984ba07f2bb15c350c4061df7019b4bc5997b8747b9039809f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_f23260fb090b83984ba07f2bb15c350c4061df7019b4bc5997b8747b9039809f->leave($__internal_f23260fb090b83984ba07f2bb15c350c4061df7019b4bc5997b8747b9039809f_prof);

        
        $__internal_a0eadc7cab00628993bff9728945b5573f5f26e56648b44aff37f5a84f49c93f->leave($__internal_a0eadc7cab00628993bff9728945b5573f5f26e56648b44aff37f5a84f49c93f_prof);

    }

    // line 61
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_59a3c2d08fbe807a3abf5d870f12624434446a50524dc8a16a01f7dfa9ca1ce0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59a3c2d08fbe807a3abf5d870f12624434446a50524dc8a16a01f7dfa9ca1ce0->enter($__internal_59a3c2d08fbe807a3abf5d870f12624434446a50524dc8a16a01f7dfa9ca1ce0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5669027eda6294f2ae0c0141589f9356b11c13aac009913881f0c06dcfc252ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5669027eda6294f2ae0c0141589f9356b11c13aac009913881f0c06dcfc252ee->enter($__internal_5669027eda6294f2ae0c0141589f9356b11c13aac009913881f0c06dcfc252ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 62
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-3.2.0.min.js"), "html", null, true);
        echo "\"></script>

        ";
        
        $__internal_5669027eda6294f2ae0c0141589f9356b11c13aac009913881f0c06dcfc252ee->leave($__internal_5669027eda6294f2ae0c0141589f9356b11c13aac009913881f0c06dcfc252ee_prof);

        
        $__internal_59a3c2d08fbe807a3abf5d870f12624434446a50524dc8a16a01f7dfa9ca1ce0->leave($__internal_59a3c2d08fbe807a3abf5d870f12624434446a50524dc8a16a01f7dfa9ca1ce0_prof);

    }

    public function getTemplateName()
    {
        return "tasquesextres.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 63,  192 => 62,  183 => 61,  166 => 15,  149 => 6,  131 => 5,  119 => 66,  117 => 61,  73 => 20,  69 => 19,  64 => 16,  62 => 15,  55 => 11,  51 => 10,  47 => 9,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Tasques extres{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        
        <link rel=\"stylesheet\" href=\"{{ asset('css/style.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap.min.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('css/tascaextra.css') }}\" />  
        
    </head>
    <body>
    \t{% block body %}{% endblock %}
\t\t<header>
\t\t\t<img src=\"/css/img/logo.png\" alt=\"logo\" id=\"logo\" width=\"350\" height=\"170\">\t
        \t<div id=\"power\"> 
        \t\t<a id=\"imatge_power\" href=\"{{ path('fos_user_security_logout') }}\"><img src=\"css/img/power.png\"></a>
\t\t\t\t<div> Hola {{ app.user.username }}!</div>
\t\t</header>  
        
                     
        <div class=\"container-fluid\">
  <div class=\"row content\">
    <div class=\"col-sm-3 sidenav\">
      <ul class=\"nav nav-pills nav-stacked\">
        <li class=\"active\"><a href=\"calendari\">Calendari</a></li>
        <li class=\"active\"><a href=\"gestio_usuaris\">Gestió d'usuaris</a></li>
        <li class=\"active\"><a href=\"tasques_extres\">Tasques Extres</a></li>
      </ul><br>
      <div class=\"input-group\">
        <input type=\"text\" class=\"form-control\" placeholder=\"Search Blog..\">
        <span class=\"input-group-btn\">
          <button class=\"btn btn-default\" type=\"button\">
            <span class=\"glyphicon glyphicon-search\"></span>
          </button>
        </span>
      </div>
    </div>

    <div class=\"col-sm-9\">
      <h1>Tasques extres</h1>
      <hr>
      <div id=\"afegir_tasca_extra\">
\t\t\t<div id=\"imatge_afegir\"></div>
\t\t\t<a href=\"afegir_tasca_extra\">Afegir tasca extra</a>
\t\t</div>

\t</div>       
     
      
    </div>
  </div>
</div>

<footer class=\"container-fluid\">
  <p>AsperToDo</p>
</footer>
        
        {% block javascripts %}
            <script src=\"{{ asset('js/bootstrap.min.js') }}\"></script>
            <script src=\"{{ asset('js/jquery-3.2.0.min.js') }}\"></script>

        {% endblock %}
    </body>
</html>
", "tasquesextres.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/tasquesextres.html.twig~");
    }
}

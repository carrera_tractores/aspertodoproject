<?php

/* FOSUserBundle:Group:list_content.html.twig */
class __TwigTemplate_f4ca48f79fd6dfd6b99bc487c633ae4366dc7b032a8cc3a7395c9453aa9f7663 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04ab83344e130ac7fbc32ca7a671ae619966b2cfd54bc7a24e1dc74cce961038 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04ab83344e130ac7fbc32ca7a671ae619966b2cfd54bc7a24e1dc74cce961038->enter($__internal_04ab83344e130ac7fbc32ca7a671ae619966b2cfd54bc7a24e1dc74cce961038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list_content.html.twig"));

        $__internal_11885b74ec26c125c8e8ffe0fe485f5bd883634270c3a9449957f5d0695e816c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11885b74ec26c125c8e8ffe0fe485f5bd883634270c3a9449957f5d0695e816c->enter($__internal_11885b74ec26c125c8e8ffe0fe485f5bd883634270c3a9449957f5d0695e816c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list_content.html.twig"));

        // line 1
        echo "<div class=\"fos_user_group_list\">
    <ul>
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) || array_key_exists("groups", $context) ? $context["groups"] : (function () { throw new Twig_Error_Runtime('Variable "groups" does not exist.', 3, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 4
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_show", array("groupName" => twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "getName", array(), "method"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "getName", array(), "method"), "html", null, true);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "    </ul>
</div>
";
        
        $__internal_04ab83344e130ac7fbc32ca7a671ae619966b2cfd54bc7a24e1dc74cce961038->leave($__internal_04ab83344e130ac7fbc32ca7a671ae619966b2cfd54bc7a24e1dc74cce961038_prof);

        
        $__internal_11885b74ec26c125c8e8ffe0fe485f5bd883634270c3a9449957f5d0695e816c->leave($__internal_11885b74ec26c125c8e8ffe0fe485f5bd883634270c3a9449957f5d0695e816c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 6,  33 => 4,  29 => 3,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"fos_user_group_list\">
    <ul>
    {% for group in groups %}
        <li><a href=\"{{ path('fos_user_group_show', {'groupName': group.getName()} ) }}\">{{ group.getName() }}</a></li>
    {% endfor %}
    </ul>
</div>
", "FOSUserBundle:Group:list_content.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list_content.html.twig");
    }
}

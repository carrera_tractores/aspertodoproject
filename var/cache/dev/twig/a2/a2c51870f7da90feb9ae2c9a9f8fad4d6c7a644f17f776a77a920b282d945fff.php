<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_b7657cbdcd8c3c5574883e4d9df0e0520056b22327df69a61fd41af594d16ba3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24607c93bd9ddea00e86bd6924bcbb2518d82e783de1beed5a2c648e0e9e676f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24607c93bd9ddea00e86bd6924bcbb2518d82e783de1beed5a2c648e0e9e676f->enter($__internal_24607c93bd9ddea00e86bd6924bcbb2518d82e783de1beed5a2c648e0e9e676f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_41a32f3e6e6914fc997469f5bcaee5212147399925f564b0a18635537d521404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41a32f3e6e6914fc997469f5bcaee5212147399925f564b0a18635537d521404->enter($__internal_41a32f3e6e6914fc997469f5bcaee5212147399925f564b0a18635537d521404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_24607c93bd9ddea00e86bd6924bcbb2518d82e783de1beed5a2c648e0e9e676f->leave($__internal_24607c93bd9ddea00e86bd6924bcbb2518d82e783de1beed5a2c648e0e9e676f_prof);

        
        $__internal_41a32f3e6e6914fc997469f5bcaee5212147399925f564b0a18635537d521404->leave($__internal_41a32f3e6e6914fc997469f5bcaee5212147399925f564b0a18635537d521404_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6697eecbb774eb4ffc29484344f0dc4427dd8f28194b6ba3aff67d0d0c841029 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6697eecbb774eb4ffc29484344f0dc4427dd8f28194b6ba3aff67d0d0c841029->enter($__internal_6697eecbb774eb4ffc29484344f0dc4427dd8f28194b6ba3aff67d0d0c841029_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5c5197e4dd8714eb69986f053bcd43cbad46e82d8ce92e6b93ebb184e398fe2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c5197e4dd8714eb69986f053bcd43cbad46e82d8ce92e6b93ebb184e398fe2d->enter($__internal_5c5197e4dd8714eb69986f053bcd43cbad46e82d8ce92e6b93ebb184e398fe2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_5c5197e4dd8714eb69986f053bcd43cbad46e82d8ce92e6b93ebb184e398fe2d->leave($__internal_5c5197e4dd8714eb69986f053bcd43cbad46e82d8ce92e6b93ebb184e398fe2d_prof);

        
        $__internal_6697eecbb774eb4ffc29484344f0dc4427dd8f28194b6ba3aff67d0d0c841029->leave($__internal_6697eecbb774eb4ffc29484344f0dc4427dd8f28194b6ba3aff67d0d0c841029_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}

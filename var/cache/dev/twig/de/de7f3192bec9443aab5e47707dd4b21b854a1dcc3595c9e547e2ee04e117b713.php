<?php

/* EasyAdminBundle:default:field_smallint.html.twig */
class __TwigTemplate_f5d7e5179ed9b87542ce20ccf12400e7e7bd121761ed631da7407591944947b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bdd20d506f05f8ccf885303513bc9299bbc6d36700c9ab4d1448f1fd21b42a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bdd20d506f05f8ccf885303513bc9299bbc6d36700c9ab4d1448f1fd21b42a9->enter($__internal_9bdd20d506f05f8ccf885303513bc9299bbc6d36700c9ab4d1448f1fd21b42a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_smallint.html.twig"));

        $__internal_0295aa45aecc71ea8c66c0d3cc2aec95a6f92fdba6718e9adceb1b355ee9aa80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0295aa45aecc71ea8c66c0d3cc2aec95a6f92fdba6718e9adceb1b355ee9aa80->enter($__internal_0295aa45aecc71ea8c66c0d3cc2aec95a6f92fdba6718e9adceb1b355ee9aa80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "EasyAdminBundle:default:field_smallint.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 1, $this->getSourceContext()); })()), "format", array())) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, sprintf(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["field_options"]) || array_key_exists("field_options", $context) ? $context["field_options"] : (function () { throw new Twig_Error_Runtime('Variable "field_options" does not exist.', 2, $this->getSourceContext()); })()), "format", array()), (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 2, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        } else {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new Twig_Error_Runtime('Variable "value" does not exist.', 4, $this->getSourceContext()); })())), "html", null, true);
            echo "
";
        }
        
        $__internal_9bdd20d506f05f8ccf885303513bc9299bbc6d36700c9ab4d1448f1fd21b42a9->leave($__internal_9bdd20d506f05f8ccf885303513bc9299bbc6d36700c9ab4d1448f1fd21b42a9_prof);

        
        $__internal_0295aa45aecc71ea8c66c0d3cc2aec95a6f92fdba6718e9adceb1b355ee9aa80->leave($__internal_0295aa45aecc71ea8c66c0d3cc2aec95a6f92fdba6718e9adceb1b355ee9aa80_prof);

    }

    public function getTemplateName()
    {
        return "EasyAdminBundle:default:field_smallint.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  27 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if field_options.format %}
    {{ field_options.format|format(value) }}
{% else %}
    {{ value|number_format }}
{% endif %}
", "EasyAdminBundle:default:field_smallint.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/field_smallint.html.twig");
    }
}

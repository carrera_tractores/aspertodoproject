<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_f7131d7214095240510aa2e55fedcbf820811419cdaee3efdec29c56ba318aa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2934640717f5168889fdc6b420c9a445efe93dcf85c8884511e5c48b7c387059 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2934640717f5168889fdc6b420c9a445efe93dcf85c8884511e5c48b7c387059->enter($__internal_2934640717f5168889fdc6b420c9a445efe93dcf85c8884511e5c48b7c387059_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_c9010783dea0ec77f6ad8c7060054c0eaed5a9fdbcde7d4af0c3845196533bf2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9010783dea0ec77f6ad8c7060054c0eaed5a9fdbcde7d4af0c3845196533bf2->enter($__internal_c9010783dea0ec77f6ad8c7060054c0eaed5a9fdbcde7d4af0c3845196533bf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2934640717f5168889fdc6b420c9a445efe93dcf85c8884511e5c48b7c387059->leave($__internal_2934640717f5168889fdc6b420c9a445efe93dcf85c8884511e5c48b7c387059_prof);

        
        $__internal_c9010783dea0ec77f6ad8c7060054c0eaed5a9fdbcde7d4af0c3845196533bf2->leave($__internal_c9010783dea0ec77f6ad8c7060054c0eaed5a9fdbcde7d4af0c3845196533bf2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5333a766a62f5342ff546af249cfe627b84d66dc74568e9d663caa57aed795d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5333a766a62f5342ff546af249cfe627b84d66dc74568e9d663caa57aed795d4->enter($__internal_5333a766a62f5342ff546af249cfe627b84d66dc74568e9d663caa57aed795d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_531ccbe51d80d1ef93dc1415359e7357eb98cd9118fbc23e83f21437ab744da1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_531ccbe51d80d1ef93dc1415359e7357eb98cd9118fbc23e83f21437ab744da1->enter($__internal_531ccbe51d80d1ef93dc1415359e7357eb98cd9118fbc23e83f21437ab744da1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_531ccbe51d80d1ef93dc1415359e7357eb98cd9118fbc23e83f21437ab744da1->leave($__internal_531ccbe51d80d1ef93dc1415359e7357eb98cd9118fbc23e83f21437ab744da1_prof);

        
        $__internal_5333a766a62f5342ff546af249cfe627b84d66dc74568e9d663caa57aed795d4->leave($__internal_5333a766a62f5342ff546af249cfe627b84d66dc74568e9d663caa57aed795d4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/show.html.twig");
    }
}

<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_36334c1da2398c4c44cf5252a1e952fbc90e05155a721f940d5a155d4ebeac28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0906136efcef322b175ac139d5d2c52f20e031cc85f0b04822f82af58259484 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0906136efcef322b175ac139d5d2c52f20e031cc85f0b04822f82af58259484->enter($__internal_a0906136efcef322b175ac139d5d2c52f20e031cc85f0b04822f82af58259484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_6e7121a2ba0680465f6bc8f65c28ebb3025e1ca470dfdfdf42b1c06c99af71b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e7121a2ba0680465f6bc8f65c28ebb3025e1ca470dfdfdf42b1c06c99af71b9->enter($__internal_6e7121a2ba0680465f6bc8f65c28ebb3025e1ca470dfdfdf42b1c06c99af71b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_a0906136efcef322b175ac139d5d2c52f20e031cc85f0b04822f82af58259484->leave($__internal_a0906136efcef322b175ac139d5d2c52f20e031cc85f0b04822f82af58259484_prof);

        
        $__internal_6e7121a2ba0680465f6bc8f65c28ebb3025e1ca470dfdfdf42b1c06c99af71b9->leave($__internal_6e7121a2ba0680465f6bc8f65c28ebb3025e1ca470dfdfdf42b1c06c99af71b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}

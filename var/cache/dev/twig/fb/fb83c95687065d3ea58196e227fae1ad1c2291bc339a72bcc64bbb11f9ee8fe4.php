<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_04576112ffad83eef71499c0ddbee410582bc89b81de670b46ffa0a16a79b23c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6923e15d02635d534f6d585de5a349f0fea92f86106e7a52cda8c9b99bc65bc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6923e15d02635d534f6d585de5a349f0fea92f86106e7a52cda8c9b99bc65bc3->enter($__internal_6923e15d02635d534f6d585de5a349f0fea92f86106e7a52cda8c9b99bc65bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_60dd6f8a627e9664344f4494fc35235d251a7d865f70d86c38709e1bca60d5ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60dd6f8a627e9664344f4494fc35235d251a7d865f70d86c38709e1bca60d5ee->enter($__internal_60dd6f8a627e9664344f4494fc35235d251a7d865f70d86c38709e1bca60d5ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_6923e15d02635d534f6d585de5a349f0fea92f86106e7a52cda8c9b99bc65bc3->leave($__internal_6923e15d02635d534f6d585de5a349f0fea92f86106e7a52cda8c9b99bc65bc3_prof);

        
        $__internal_60dd6f8a627e9664344f4494fc35235d251a7d865f70d86c38709e1bca60d5ee->leave($__internal_60dd6f8a627e9664344f4494fc35235d251a7d865f70d86c38709e1bca60d5ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}

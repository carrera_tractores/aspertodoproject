<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_83b7572abd496b886a3c3e29113e9d4a09aac7d44ffcb31adcddabb0c6259c18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0597c2edbfdbcd7a4e9c4efdd0e16d41fb8e4a837269767ef1dffe3526ea075 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0597c2edbfdbcd7a4e9c4efdd0e16d41fb8e4a837269767ef1dffe3526ea075->enter($__internal_a0597c2edbfdbcd7a4e9c4efdd0e16d41fb8e4a837269767ef1dffe3526ea075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $__internal_98cc38b7391d4843b6369bb15cdbd5707798c61b849dc58e7984308afcb61935 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98cc38b7391d4843b6369bb15cdbd5707798c61b849dc58e7984308afcb61935->enter($__internal_98cc38b7391d4843b6369bb15cdbd5707798c61b849dc58e7984308afcb61935_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0597c2edbfdbcd7a4e9c4efdd0e16d41fb8e4a837269767ef1dffe3526ea075->leave($__internal_a0597c2edbfdbcd7a4e9c4efdd0e16d41fb8e4a837269767ef1dffe3526ea075_prof);

        
        $__internal_98cc38b7391d4843b6369bb15cdbd5707798c61b849dc58e7984308afcb61935->leave($__internal_98cc38b7391d4843b6369bb15cdbd5707798c61b849dc58e7984308afcb61935_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_69fb8acf6fa3445bee619623f6a51add32a43a9cddd59e787fbde517634e9d3a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69fb8acf6fa3445bee619623f6a51add32a43a9cddd59e787fbde517634e9d3a->enter($__internal_69fb8acf6fa3445bee619623f6a51add32a43a9cddd59e787fbde517634e9d3a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_bf35dc8b71169e8329dd4e3523bde3e999531caa4e2f7b4273d870f25593536a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf35dc8b71169e8329dd4e3523bde3e999531caa4e2f7b4273d870f25593536a->enter($__internal_bf35dc8b71169e8329dd4e3523bde3e999531caa4e2f7b4273d870f25593536a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_bf35dc8b71169e8329dd4e3523bde3e999531caa4e2f7b4273d870f25593536a->leave($__internal_bf35dc8b71169e8329dd4e3523bde3e999531caa4e2f7b4273d870f25593536a_prof);

        
        $__internal_69fb8acf6fa3445bee619623f6a51add32a43a9cddd59e787fbde517634e9d3a->leave($__internal_69fb8acf6fa3445bee619623f6a51add32a43a9cddd59e787fbde517634e9d3a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Resetting:request.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Resetting/request.html.twig");
    }
}

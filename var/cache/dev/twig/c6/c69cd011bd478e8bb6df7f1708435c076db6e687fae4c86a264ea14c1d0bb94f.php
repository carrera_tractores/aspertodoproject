<?php

/* prova.html.twig~ */
class __TwigTemplate_3b7acd7397f6a0a925c30f86c2b0e43c9938524158dff06d6349866598f3d26b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("@EasyAdmin/default/layout.html.twig", "prova.html.twig~", 3);
        $this->blocks = array(
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@EasyAdmin/default/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d3db59380d5faafceaf8a0c70e7c3608bb15dc52ddd8a499ca34082369b6fdc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d3db59380d5faafceaf8a0c70e7c3608bb15dc52ddd8a499ca34082369b6fdc->enter($__internal_3d3db59380d5faafceaf8a0c70e7c3608bb15dc52ddd8a499ca34082369b6fdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "prova.html.twig~"));

        $__internal_da09545146159d3adbf602b94a436a6f861c37b593f9e09b1e065ffe3d517bcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da09545146159d3adbf602b94a436a6f861c37b593f9e09b1e065ffe3d517bcc->enter($__internal_da09545146159d3adbf602b94a436a6f861c37b593f9e09b1e065ffe3d517bcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "prova.html.twig~"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3d3db59380d5faafceaf8a0c70e7c3608bb15dc52ddd8a499ca34082369b6fdc->leave($__internal_3d3db59380d5faafceaf8a0c70e7c3608bb15dc52ddd8a499ca34082369b6fdc_prof);

        
        $__internal_da09545146159d3adbf602b94a436a6f861c37b593f9e09b1e065ffe3d517bcc->leave($__internal_da09545146159d3adbf602b94a436a6f861c37b593f9e09b1e065ffe3d517bcc_prof);

    }

    // line 5
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_5a9d178c2483283779326e58415cbc5af9fcc44ca121f843c42ef79464406e7d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a9d178c2483283779326e58415cbc5af9fcc44ca121f843c42ef79464406e7d->enter($__internal_5a9d178c2483283779326e58415cbc5af9fcc44ca121f843c42ef79464406e7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_f6b2b82381ffe2529d11933cf90e7da9058c1c0d374277b746a5b409840608fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6b2b82381ffe2529d11933cf90e7da9058c1c0d374277b746a5b409840608fe->enter($__internal_f6b2b82381ffe2529d11933cf90e7da9058c1c0d374277b746a5b409840608fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/css/fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/calendari.css"), "html", null, true);
        echo "\" />

";
        // line 9
        $this->displayParentBlock("head_stylesheets", $context, $blocks);
        echo "

<style>
\t
\t.content-wrapper { background-color: yellow; }

</style>

";
        
        $__internal_f6b2b82381ffe2529d11933cf90e7da9058c1c0d374277b746a5b409840608fe->leave($__internal_f6b2b82381ffe2529d11933cf90e7da9058c1c0d374277b746a5b409840608fe_prof);

        
        $__internal_5a9d178c2483283779326e58415cbc5af9fcc44ca121f843c42ef79464406e7d->leave($__internal_5a9d178c2483283779326e58415cbc5af9fcc44ca121f843c42ef79464406e7d_prof);

    }

    // line 19
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_ea1175c96fd834f363e4ece82dc95ec8dd10b7e87940d60d6ade901966b66180 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea1175c96fd834f363e4ece82dc95ec8dd10b7e87940d60d6ade901966b66180->enter($__internal_ea1175c96fd834f363e4ece82dc95ec8dd10b7e87940d60d6ade901966b66180_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_a8f644af37d78b30a501322743e63745b56a32766f57f9bd9b80e393025d69cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8f644af37d78b30a501322743e63745b56a32766f57f9bd9b80e393025d69cc->enter($__internal_a8f644af37d78b30a501322743e63745b56a32766f57f9bd9b80e393025d69cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 20
        echo "
";
        // line 21
        $this->displayParentBlock("content_header", $context, $blocks);
        echo "

\t<h1>Calendari</h1>
\t";
        // line 24
        $this->loadTemplate("ADesignsCalendarBundle::calendar.html.twig", "prova.html.twig~", 24)->display($context);
        echo " 
\t
\t      ";
        // line 26
        $this->displayBlock('javascripts', $context, $blocks);
        // line 33
        echo "
";
        
        $__internal_a8f644af37d78b30a501322743e63745b56a32766f57f9bd9b80e393025d69cc->leave($__internal_a8f644af37d78b30a501322743e63745b56a32766f57f9bd9b80e393025d69cc_prof);

        
        $__internal_ea1175c96fd834f363e4ece82dc95ec8dd10b7e87940d60d6ade901966b66180->leave($__internal_ea1175c96fd834f363e4ece82dc95ec8dd10b7e87940d60d6ade901966b66180_prof);

    }

    // line 26
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_6ad357dd7d0b396de73ee6f188eee752d3f4d40664f3c0bd60ae1b677c8eaecb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ad357dd7d0b396de73ee6f188eee752d3f4d40664f3c0bd60ae1b677c8eaecb->enter($__internal_6ad357dd7d0b396de73ee6f188eee752d3f4d40664f3c0bd60ae1b677c8eaecb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_4b6c0a856ed5ddca3093827cc4c54a639ece711348eb51fb77755a74e47bcab0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b6c0a856ed5ddca3093827cc4c54a639ece711348eb51fb77755a74e47bcab0->enter($__internal_4b6c0a856ed5ddca3093827cc4c54a639ece711348eb51fb77755a74e47bcab0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 27
        echo "         <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/jquery/jquery-1.8.2.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/calendar-settings.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    \t\t<script src=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
    \t\t";
        
        $__internal_4b6c0a856ed5ddca3093827cc4c54a639ece711348eb51fb77755a74e47bcab0->leave($__internal_4b6c0a856ed5ddca3093827cc4c54a639ece711348eb51fb77755a74e47bcab0_prof);

        
        $__internal_6ad357dd7d0b396de73ee6f188eee752d3f4d40664f3c0bd60ae1b677c8eaecb->leave($__internal_6ad357dd7d0b396de73ee6f188eee752d3f4d40664f3c0bd60ae1b677c8eaecb_prof);

    }

    public function getTemplateName()
    {
        return "prova.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 31,  138 => 30,  134 => 29,  130 => 28,  125 => 27,  116 => 26,  105 => 33,  103 => 26,  98 => 24,  92 => 21,  89 => 20,  80 => 19,  61 => 9,  56 => 7,  51 => 6,  42 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/calendari.html.twig #}

{% extends '@EasyAdmin/default/layout.html.twig' %}

{% block head_stylesheets %}
<link rel=\"stylesheet\" href=\"{{ asset('bundles/adesignscalendar/css/fullcalendar/fullcalendar.css') }}\" />
<link rel=\"stylesheet\" href=\"{{ asset('css/calendari.css') }}\" />

{{ parent() }}

<style>
\t
\t.content-wrapper { background-color: yellow; }

</style>

{% endblock %}

{% block content_header %}

{{ parent() }}

\t<h1>Calendari</h1>
\t{% include 'ADesignsCalendarBundle::calendar.html.twig' %} 
\t
\t      {% block javascripts %}
         <script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/jquery/jquery-1.8.2.min.js') }}\"></script>
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js') }}\"></script>
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/calendar-settings.js') }}\"></script>
\t\t\t<script src=\"{{ asset('bundles/fosjsrouting/js/router.js') }}\"></script>
    \t\t<script src=\"{{ path('fos_js_routing_js', {\"callback\": \"fos.Router.setData\"}) }}\"></script>
    \t\t{% endblock %}

{% endblock %}
", "prova.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/prova.html.twig~");
    }
}

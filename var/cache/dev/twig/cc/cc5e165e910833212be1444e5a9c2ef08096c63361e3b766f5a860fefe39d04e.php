<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_3fd1092c249d2e54b4b6cbb8095dca9037b699e92d33f5319d4d618c43f5d750 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a0b2d3133d6aae66500d54d168a0b48df6f3088eb7b89aff3dbb2c9f7b40f4c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a0b2d3133d6aae66500d54d168a0b48df6f3088eb7b89aff3dbb2c9f7b40f4c->enter($__internal_7a0b2d3133d6aae66500d54d168a0b48df6f3088eb7b89aff3dbb2c9f7b40f4c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_bc362d1980cb2526df919b62c5e202bf35cb2a87439b919179e07dca8ff92722 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc362d1980cb2526df919b62c5e202bf35cb2a87439b919179e07dca8ff92722->enter($__internal_bc362d1980cb2526df919b62c5e202bf35cb2a87439b919179e07dca8ff92722_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_7a0b2d3133d6aae66500d54d168a0b48df6f3088eb7b89aff3dbb2c9f7b40f4c->leave($__internal_7a0b2d3133d6aae66500d54d168a0b48df6f3088eb7b89aff3dbb2c9f7b40f4c_prof);

        
        $__internal_bc362d1980cb2526df919b62c5e202bf35cb2a87439b919179e07dca8ff92722->leave($__internal_bc362d1980cb2526df919b62c5e202bf35cb2a87439b919179e07dca8ff92722_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}

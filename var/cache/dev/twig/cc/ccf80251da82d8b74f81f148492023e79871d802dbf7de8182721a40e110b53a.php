<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_7879f771ff2d9507e4b9243b1b0befc84775ef6a12cb7238307589f1a19dc322 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92af5e287aa88e4f2d5b4a0fcaa79303a7024a655f8ff816c0b40926dea0fac1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92af5e287aa88e4f2d5b4a0fcaa79303a7024a655f8ff816c0b40926dea0fac1->enter($__internal_92af5e287aa88e4f2d5b4a0fcaa79303a7024a655f8ff816c0b40926dea0fac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_f9829637bf5048628150c37ef9cf553bbc9cf782976b148902f27f8372336e29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9829637bf5048628150c37ef9cf553bbc9cf782976b148902f27f8372336e29->enter($__internal_f9829637bf5048628150c37ef9cf553bbc9cf782976b148902f27f8372336e29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_92af5e287aa88e4f2d5b4a0fcaa79303a7024a655f8ff816c0b40926dea0fac1->leave($__internal_92af5e287aa88e4f2d5b4a0fcaa79303a7024a655f8ff816c0b40926dea0fac1_prof);

        
        $__internal_f9829637bf5048628150c37ef9cf553bbc9cf782976b148902f27f8372336e29->leave($__internal_f9829637bf5048628150c37ef9cf553bbc9cf782976b148902f27f8372336e29_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_39bfbc23a8b9244b2c61d565e33ae1adcef740c4b33295b70b0addcf4dbb3f5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39bfbc23a8b9244b2c61d565e33ae1adcef740c4b33295b70b0addcf4dbb3f5a->enter($__internal_39bfbc23a8b9244b2c61d565e33ae1adcef740c4b33295b70b0addcf4dbb3f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_225fc9fe0de2c39c4ae830d5a27dbb8fe6de066a0aa46ba8cf1b2117455f7519 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_225fc9fe0de2c39c4ae830d5a27dbb8fe6de066a0aa46ba8cf1b2117455f7519->enter($__internal_225fc9fe0de2c39c4ae830d5a27dbb8fe6de066a0aa46ba8cf1b2117455f7519_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_225fc9fe0de2c39c4ae830d5a27dbb8fe6de066a0aa46ba8cf1b2117455f7519->leave($__internal_225fc9fe0de2c39c4ae830d5a27dbb8fe6de066a0aa46ba8cf1b2117455f7519_prof);

        
        $__internal_39bfbc23a8b9244b2c61d565e33ae1adcef740c4b33295b70b0addcf4dbb3f5a->leave($__internal_39bfbc23a8b9244b2c61d565e33ae1adcef740c4b33295b70b0addcf4dbb3f5a_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ead70a6484cdba79820f8decb457731f78495596cfa67b93eadfa65a7119db89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ead70a6484cdba79820f8decb457731f78495596cfa67b93eadfa65a7119db89->enter($__internal_ead70a6484cdba79820f8decb457731f78495596cfa67b93eadfa65a7119db89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a6b81f163bc5279a270d64fa203098fa485bbc115049d8a0a5bc36bc794f1f7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6b81f163bc5279a270d64fa203098fa485bbc115049d8a0a5bc36bc794f1f7f->enter($__internal_a6b81f163bc5279a270d64fa203098fa485bbc115049d8a0a5bc36bc794f1f7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_a6b81f163bc5279a270d64fa203098fa485bbc115049d8a0a5bc36bc794f1f7f->leave($__internal_a6b81f163bc5279a270d64fa203098fa485bbc115049d8a0a5bc36bc794f1f7f_prof);

        
        $__internal_ead70a6484cdba79820f8decb457731f78495596cfa67b93eadfa65a7119db89->leave($__internal_ead70a6484cdba79820f8decb457731f78495596cfa67b93eadfa65a7119db89_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}

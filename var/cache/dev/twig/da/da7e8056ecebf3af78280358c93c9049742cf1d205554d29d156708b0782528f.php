<?php

/* calendari.html.twig~ */
class __TwigTemplate_7e0d66b20f60021e50ab415666d043016c0fa3a8bc4522700f8adc9383531867 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("@EasyAdmin/default/layout.html.twig", "calendari.html.twig~", 3);
        $this->blocks = array(
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@EasyAdmin/default/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25de107026b4d79b6e9b6046187b7b2cc7f99c541bf2712a29e1a8a41758e5e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25de107026b4d79b6e9b6046187b7b2cc7f99c541bf2712a29e1a8a41758e5e8->enter($__internal_25de107026b4d79b6e9b6046187b7b2cc7f99c541bf2712a29e1a8a41758e5e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calendari.html.twig~"));

        $__internal_f691f785b81e57cd3fb5f14435318639cf50be7ffb463c1f087faf15c0e51ca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f691f785b81e57cd3fb5f14435318639cf50be7ffb463c1f087faf15c0e51ca7->enter($__internal_f691f785b81e57cd3fb5f14435318639cf50be7ffb463c1f087faf15c0e51ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calendari.html.twig~"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25de107026b4d79b6e9b6046187b7b2cc7f99c541bf2712a29e1a8a41758e5e8->leave($__internal_25de107026b4d79b6e9b6046187b7b2cc7f99c541bf2712a29e1a8a41758e5e8_prof);

        
        $__internal_f691f785b81e57cd3fb5f14435318639cf50be7ffb463c1f087faf15c0e51ca7->leave($__internal_f691f785b81e57cd3fb5f14435318639cf50be7ffb463c1f087faf15c0e51ca7_prof);

    }

    // line 5
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_362b5c038353a886d145a50e78e364db01d592ba60a34509f47a81aaa4dfefee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_362b5c038353a886d145a50e78e364db01d592ba60a34509f47a81aaa4dfefee->enter($__internal_362b5c038353a886d145a50e78e364db01d592ba60a34509f47a81aaa4dfefee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_885f53409539afb15fbbbbda5273e7b2d0a0c571ee50a20745149a2a03605c96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_885f53409539afb15fbbbbda5273e7b2d0a0c571ee50a20745149a2a03605c96->enter($__internal_885f53409539afb15fbbbbda5273e7b2d0a0c571ee50a20745149a2a03605c96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 6
        echo "<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/css/fullcalendar/fullcalendar.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/calendari.css"), "html", null, true);
        echo "\" />

";
        // line 9
        $this->displayParentBlock("head_stylesheets", $context, $blocks);
        echo "

";
        
        $__internal_885f53409539afb15fbbbbda5273e7b2d0a0c571ee50a20745149a2a03605c96->leave($__internal_885f53409539afb15fbbbbda5273e7b2d0a0c571ee50a20745149a2a03605c96_prof);

        
        $__internal_362b5c038353a886d145a50e78e364db01d592ba60a34509f47a81aaa4dfefee->leave($__internal_362b5c038353a886d145a50e78e364db01d592ba60a34509f47a81aaa4dfefee_prof);

    }

    // line 13
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_a44e4e5d23e7854263c73ee0499fa4d097e23acdf696413c951ccbdda18fa161 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a44e4e5d23e7854263c73ee0499fa4d097e23acdf696413c951ccbdda18fa161->enter($__internal_a44e4e5d23e7854263c73ee0499fa4d097e23acdf696413c951ccbdda18fa161_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_4a4c3c4abf22f705900f9e66030109fb08f03c778e147132c30dcaa27aac8c34 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a4c3c4abf22f705900f9e66030109fb08f03c778e147132c30dcaa27aac8c34->enter($__internal_4a4c3c4abf22f705900f9e66030109fb08f03c778e147132c30dcaa27aac8c34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 14
        echo "
";
        // line 15
        $this->displayParentBlock("content_header", $context, $blocks);
        echo "

\t<h1>Calendari</h1>
\t";
        // line 18
        $this->loadTemplate("ADesignsCalendarBundle::calendar.html.twig", "calendari.html.twig~", 18)->display($context);
        echo " 
\t
\t      ";
        // line 20
        $this->displayBlock('javascripts', $context, $blocks);
        // line 39
        echo "
";
        
        $__internal_4a4c3c4abf22f705900f9e66030109fb08f03c778e147132c30dcaa27aac8c34->leave($__internal_4a4c3c4abf22f705900f9e66030109fb08f03c778e147132c30dcaa27aac8c34_prof);

        
        $__internal_a44e4e5d23e7854263c73ee0499fa4d097e23acdf696413c951ccbdda18fa161->leave($__internal_a44e4e5d23e7854263c73ee0499fa4d097e23acdf696413c951ccbdda18fa161_prof);

    }

    // line 20
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_684ea1a99502fa590baece2f38bcbfb796460379ab738054a43133adc6e23d96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_684ea1a99502fa590baece2f38bcbfb796460379ab738054a43133adc6e23d96->enter($__internal_684ea1a99502fa590baece2f38bcbfb796460379ab738054a43133adc6e23d96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_a5984e0fd4d949074f7feb9534c03d2b0c1b91c34e37cddf098bae3d086a8e2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5984e0fd4d949074f7feb9534c03d2b0c1b91c34e37cddf098bae3d086a8e2d->enter($__internal_a5984e0fd4d949074f7feb9534c03d2b0c1b91c34e37cddf098bae3d086a8e2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 21
        echo "\t\t\t<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/adesignscalendar/js/calendar-settings.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script src=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
    \t\t<script src=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData"));
        echo "\"></script>
    \t\t
    \t\t<script type=\"text/javascript\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/lib/moment.min.js"), "html", null, true);
        echo "\"></script>  
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/locale/ca.js"), "html", null, true);
        echo "\"></script>
\t\t\t<script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("fullcalendar/fullcalendar.js"), "html", null, true);
        echo "\"></script>
\t\t\t
    \t\t<script>
    \t\t\t\$(document).ready(function() {
    \t\t\t\t\$('#calendar').fullCalendar({
    \t\t\t\t\tlang: 'ca'
    \t\t\t\t});
    \t\t\t});
    \t\t</script> \t\t
    \t\t
    \t\t";
        
        $__internal_a5984e0fd4d949074f7feb9534c03d2b0c1b91c34e37cddf098bae3d086a8e2d->leave($__internal_a5984e0fd4d949074f7feb9534c03d2b0c1b91c34e37cddf098bae3d086a8e2d_prof);

        
        $__internal_684ea1a99502fa590baece2f38bcbfb796460379ab738054a43133adc6e23d96->leave($__internal_684ea1a99502fa590baece2f38bcbfb796460379ab738054a43133adc6e23d96_prof);

    }

    public function getTemplateName()
    {
        return "calendari.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 28,  141 => 27,  137 => 26,  132 => 24,  128 => 23,  124 => 22,  119 => 21,  110 => 20,  99 => 39,  97 => 20,  92 => 18,  86 => 15,  83 => 14,  74 => 13,  61 => 9,  56 => 7,  51 => 6,  42 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/calendari.html.twig #}

{% extends '@EasyAdmin/default/layout.html.twig' %}

{% block head_stylesheets %}
<link rel=\"stylesheet\" href=\"{{ asset('bundles/adesignscalendar/css/fullcalendar/fullcalendar.css') }}\" />
<link rel=\"stylesheet\" href=\"{{ asset('css/calendari.css') }}\" />

{{ parent() }}

{% endblock %}

{% block content_header %}

{{ parent() }}

\t<h1>Calendari</h1>
\t{% include 'ADesignsCalendarBundle::calendar.html.twig' %} 
\t
\t      {% block javascripts %}
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/fullcalendar/jquery.fullcalendar.min.js') }}\"></script>
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('bundles/adesignscalendar/js/calendar-settings.js') }}\"></script>
\t\t\t<script src=\"{{ asset('bundles/fosjsrouting/js/router.js') }}\"></script>
    \t\t<script src=\"{{ path('fos_js_routing_js', {\"callback\": \"fos.Router.setData\"}) }}\"></script>
    \t\t
    \t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/lib/moment.min.js') }}\"></script>  
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/locale/ca.js') }}\"></script>
\t\t\t<script type=\"text/javascript\" src=\"{{ asset('fullcalendar/fullcalendar.js') }}\"></script>
\t\t\t
    \t\t<script>
    \t\t\t\$(document).ready(function() {
    \t\t\t\t\$('#calendar').fullCalendar({
    \t\t\t\t\tlang: 'ca'
    \t\t\t\t});
    \t\t\t});
    \t\t</script> \t\t
    \t\t
    \t\t{% endblock %}

{% endblock %}
", "calendari.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/app/Resources/views/calendari.html.twig~");
    }
}

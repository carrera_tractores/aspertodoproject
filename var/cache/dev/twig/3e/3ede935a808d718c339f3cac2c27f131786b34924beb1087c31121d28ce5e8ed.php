<?php

/* @FOSUser/layout.html.twig~ */
class __TwigTemplate_a4635a5a2b76fa3008e375c94431b2117798455aced0df667d913dfa5a9ba070 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cde530bebe2a60072a63971e47885ae7c2eb50fad1ea751a02022222658d7e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cde530bebe2a60072a63971e47885ae7c2eb50fad1ea751a02022222658d7e7->enter($__internal_3cde530bebe2a60072a63971e47885ae7c2eb50fad1ea751a02022222658d7e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig~"));

        $__internal_ba3272f8f5ff1bc129684ee82b040950010c79340109569a1e45853ff51d4fd7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba3272f8f5ff1bc129684ee82b040950010c79340109569a1e45853ff51d4fd7->enter($__internal_ba3272f8f5ff1bc129684ee82b040950010c79340109569a1e45853ff51d4fd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig~"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    \t  <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/login.css"), "html", null, true);
        echo "\" />
    </head>
    <body>
        <div>
            ";
        // line 9
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 10
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 10, $this->getSourceContext()); })()), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
                <a href=\"";
            // line 11
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                    ";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
            ";
        } else {
            // line 15
            echo "               
            ";
        }
        // line 17
        echo "        </div>

        ";
        // line 19
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 19, $this->getSourceContext()); })()), "request", array()), "hasPreviousSession", array())) {
            // line 20
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 20, $this->getSourceContext()); })()), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 21
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 22
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 23
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "        ";
        }
        // line 28
        echo "
        <div>
            ";
        // line 30
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 32
        echo "        </div>
    </body>
</html>
";
        
        $__internal_3cde530bebe2a60072a63971e47885ae7c2eb50fad1ea751a02022222658d7e7->leave($__internal_3cde530bebe2a60072a63971e47885ae7c2eb50fad1ea751a02022222658d7e7_prof);

        
        $__internal_ba3272f8f5ff1bc129684ee82b040950010c79340109569a1e45853ff51d4fd7->leave($__internal_ba3272f8f5ff1bc129684ee82b040950010c79340109569a1e45853ff51d4fd7_prof);

    }

    // line 30
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ddd23779f377df9860fc85c4201269ee637cd2651cb7fc05c7abaf2cd49e5112 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddd23779f377df9860fc85c4201269ee637cd2651cb7fc05c7abaf2cd49e5112->enter($__internal_ddd23779f377df9860fc85c4201269ee637cd2651cb7fc05c7abaf2cd49e5112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a326f7103e15c174ba0cdd9788d9c1522a440ffe33f6fd5c15117c688099b04b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a326f7103e15c174ba0cdd9788d9c1522a440ffe33f6fd5c15117c688099b04b->enter($__internal_a326f7103e15c174ba0cdd9788d9c1522a440ffe33f6fd5c15117c688099b04b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 31
        echo "            ";
        
        $__internal_a326f7103e15c174ba0cdd9788d9c1522a440ffe33f6fd5c15117c688099b04b->leave($__internal_a326f7103e15c174ba0cdd9788d9c1522a440ffe33f6fd5c15117c688099b04b_prof);

        
        $__internal_ddd23779f377df9860fc85c4201269ee637cd2651cb7fc05c7abaf2cd49e5112->leave($__internal_ddd23779f377df9860fc85c4201269ee637cd2651cb7fc05c7abaf2cd49e5112_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig~";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 31,  118 => 30,  105 => 32,  103 => 30,  99 => 28,  96 => 27,  90 => 26,  81 => 23,  76 => 22,  71 => 21,  66 => 20,  64 => 19,  60 => 17,  56 => 15,  50 => 12,  46 => 11,  41 => 10,  39 => 9,  32 => 5,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
    \t  <link rel=\"stylesheet\" href=\"{{ asset('css/login.css') }}\" />
    </head>
    <body>
        <div>
            {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
               
            {% endif %}
        </div>

        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>
    </body>
</html>
", "@FOSUser/layout.html.twig~", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/layout.html.twig~");
    }
}

<?php

/* @EasyAdmin/default/layout.html.twig */
class __TwigTemplate_d5080cccd0dcc3284bc0babc248bc7e6cde7749c5ac81987124613b2ce4377e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'page_title' => array($this, 'block_page_title'),
            'head_stylesheets' => array($this, 'block_head_stylesheets'),
            'head_favicon' => array($this, 'block_head_favicon'),
            'head_javascript' => array($this, 'block_head_javascript'),
            'adminlte_options' => array($this, 'block_adminlte_options'),
            'body' => array($this, 'block_body'),
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'wrapper' => array($this, 'block_wrapper'),
            'header' => array($this, 'block_header'),
            'header_logo' => array($this, 'block_header_logo'),
            'header_custom_menu' => array($this, 'block_header_custom_menu'),
            'user_menu' => array($this, 'block_user_menu'),
            'user_menu_dropdown' => array($this, 'block_user_menu_dropdown'),
            'sidebar' => array($this, 'block_sidebar'),
            'main_menu_wrapper' => array($this, 'block_main_menu_wrapper'),
            'content' => array($this, 'block_content'),
            'flash_messages' => array($this, 'block_flash_messages'),
            'content_header' => array($this, 'block_content_header'),
            'content_title' => array($this, 'block_content_title'),
            'content_help' => array($this, 'block_content_help'),
            'main' => array($this, 'block_main'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_374517668f5db5ef610b5ce493007b8ce296dfe7fcaad7c2cdc9e3e47cb7a594 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_374517668f5db5ef610b5ce493007b8ce296dfe7fcaad7c2cdc9e3e47cb7a594->enter($__internal_374517668f5db5ef610b5ce493007b8ce296dfe7fcaad7c2cdc9e3e47cb7a594_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/layout.html.twig"));

        $__internal_09d631ae496b7c305563cb20076eb2f3a81764bed9a54c58266ec8fa98fd8084 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09d631ae496b7c305563cb20076eb2f3a81764bed9a54c58266ec8fa98fd8084->enter($__internal_09d631ae496b7c305563cb20076eb2f3a81764bed9a54c58266ec8fa98fd8084_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/default/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, _twig_default_filter(twig_first($this->env, twig_split_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 2, $this->getSourceContext()); })()), "request", array()), "locale", array()), "_")), "en"), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>";
        // line 10
        $this->displayBlock('page_title', $context, $blocks);
        echo "</title>

        ";
        // line 12
        $this->displayBlock('head_stylesheets', $context, $blocks);
        // line 18
        echo "
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.css"));
        foreach ($context['_seq'] as $context["_key"] => $context["css_asset"]) {
            // line 20
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["css_asset"]), "html", null, true);
            echo "\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['css_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "
        ";
        // line 23
        $this->displayBlock('head_favicon', $context, $blocks);
        // line 27
        echo "
        ";
        // line 28
        $this->displayBlock('head_javascript', $context, $blocks);
        // line 45
        echo "
        ";
        // line 46
        if ($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.rtl")) {
            // line 47
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/bootstrap-rtl.min.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/adminlte-rtl.min.css"), "html", null, true);
            echo "\">
        ";
        }
        // line 50
        echo "
        <!--[if lt IE 9]>
            <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/html5shiv.min.css"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/respond.min.css"), "html", null, true);
        echo "\"></script>
        <![endif]-->
    </head>

    ";
        // line 57
        $this->displayBlock('body', $context, $blocks);
        // line 167
        echo "</html>
";
        
        $__internal_374517668f5db5ef610b5ce493007b8ce296dfe7fcaad7c2cdc9e3e47cb7a594->leave($__internal_374517668f5db5ef610b5ce493007b8ce296dfe7fcaad7c2cdc9e3e47cb7a594_prof);

        
        $__internal_09d631ae496b7c305563cb20076eb2f3a81764bed9a54c58266ec8fa98fd8084->leave($__internal_09d631ae496b7c305563cb20076eb2f3a81764bed9a54c58266ec8fa98fd8084_prof);

    }

    // line 10
    public function block_page_title($context, array $blocks = array())
    {
        $__internal_8c1c52c33372a9d72815d62064c5985d6e6f38e13340e79fe541e11ced4d1cbb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c1c52c33372a9d72815d62064c5985d6e6f38e13340e79fe541e11ced4d1cbb->enter($__internal_8c1c52c33372a9d72815d62064c5985d6e6f38e13340e79fe541e11ced4d1cbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        $__internal_a2974300d3f4206323d7fa279be26183c0c34101a3102cf3bdf899d0692ce49e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2974300d3f4206323d7fa279be26183c0c34101a3102cf3bdf899d0692ce49e->enter($__internal_a2974300d3f4206323d7fa279be26183c0c34101a3102cf3bdf899d0692ce49e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_title"));

        echo strip_tags(        $this->renderBlock("content_title", $context, $blocks));
        
        $__internal_a2974300d3f4206323d7fa279be26183c0c34101a3102cf3bdf899d0692ce49e->leave($__internal_a2974300d3f4206323d7fa279be26183c0c34101a3102cf3bdf899d0692ce49e_prof);

        
        $__internal_8c1c52c33372a9d72815d62064c5985d6e6f38e13340e79fe541e11ced4d1cbb->leave($__internal_8c1c52c33372a9d72815d62064c5985d6e6f38e13340e79fe541e11ced4d1cbb_prof);

    }

    // line 12
    public function block_head_stylesheets($context, array $blocks = array())
    {
        $__internal_61c688b03d9abff452aa23e6c15ae9314e6c92f6765d472b238eb68c50042592 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61c688b03d9abff452aa23e6c15ae9314e6c92f6765d472b238eb68c50042592->enter($__internal_61c688b03d9abff452aa23e6c15ae9314e6c92f6765d472b238eb68c50042592_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        $__internal_6d24c505dc4efe7abaefaca909dce81baf50728c382acf673a7edaf1b3eeb6b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d24c505dc4efe7abaefaca909dce81baf50728c382acf673a7edaf1b3eeb6b0->enter($__internal_6d24c505dc4efe7abaefaca909dce81baf50728c382acf673a7edaf1b3eeb6b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_stylesheets"));

        // line 13
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/stylesheet/easyadmin-all.min.css"), "html", null, true);
        echo "\">
            <style>
                ";
        // line 15
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("_internal.custom_css");
        echo "
            </style>
        ";
        
        $__internal_6d24c505dc4efe7abaefaca909dce81baf50728c382acf673a7edaf1b3eeb6b0->leave($__internal_6d24c505dc4efe7abaefaca909dce81baf50728c382acf673a7edaf1b3eeb6b0_prof);

        
        $__internal_61c688b03d9abff452aa23e6c15ae9314e6c92f6765d472b238eb68c50042592->leave($__internal_61c688b03d9abff452aa23e6c15ae9314e6c92f6765d472b238eb68c50042592_prof);

    }

    // line 23
    public function block_head_favicon($context, array $blocks = array())
    {
        $__internal_c4218f6769168bc0aa9be30a2320246d6d2661e19c95aad3e6ecb00523e96860 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4218f6769168bc0aa9be30a2320246d6d2661e19c95aad3e6ecb00523e96860->enter($__internal_c4218f6769168bc0aa9be30a2320246d6d2661e19c95aad3e6ecb00523e96860_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        $__internal_101f88f7a7775b661089572defde1925b562e7a44c0ef41c2ac686e048c1d6c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_101f88f7a7775b661089572defde1925b562e7a44c0ef41c2ac686e048c1d6c2->enter($__internal_101f88f7a7775b661089572defde1925b562e7a44c0ef41c2ac686e048c1d6c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_favicon"));

        // line 24
        echo "            ";
        $context["favicon"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.favicon");
        // line 25
        echo "            <link rel=\"icon\" type=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["favicon"]) || array_key_exists("favicon", $context) ? $context["favicon"] : (function () { throw new Twig_Error_Runtime('Variable "favicon" does not exist.', 25, $this->getSourceContext()); })()), "mime_type", array()), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["favicon"]) || array_key_exists("favicon", $context) ? $context["favicon"] : (function () { throw new Twig_Error_Runtime('Variable "favicon" does not exist.', 25, $this->getSourceContext()); })()), "path", array())), "html", null, true);
        echo "\" />
        ";
        
        $__internal_101f88f7a7775b661089572defde1925b562e7a44c0ef41c2ac686e048c1d6c2->leave($__internal_101f88f7a7775b661089572defde1925b562e7a44c0ef41c2ac686e048c1d6c2_prof);

        
        $__internal_c4218f6769168bc0aa9be30a2320246d6d2661e19c95aad3e6ecb00523e96860->leave($__internal_c4218f6769168bc0aa9be30a2320246d6d2661e19c95aad3e6ecb00523e96860_prof);

    }

    // line 28
    public function block_head_javascript($context, array $blocks = array())
    {
        $__internal_f4fa6bbbcd364772be0c926d92ba6cc298737c84ce3239b316fad1f7c02e6479 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f4fa6bbbcd364772be0c926d92ba6cc298737c84ce3239b316fad1f7c02e6479->enter($__internal_f4fa6bbbcd364772be0c926d92ba6cc298737c84ce3239b316fad1f7c02e6479_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        $__internal_6636fe30f16770c0abd08e67e351078646a65477345608133e6d67a8600f7f9c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6636fe30f16770c0abd08e67e351078646a65477345608133e6d67a8600f7f9c->enter($__internal_6636fe30f16770c0abd08e67e351078646a65477345608133e6d67a8600f7f9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head_javascript"));

        // line 29
        echo "            ";
        $this->displayBlock('adminlte_options', $context, $blocks);
        // line 42
        echo "
            <script src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/easyadmin/javascript/easyadmin-all.min.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_6636fe30f16770c0abd08e67e351078646a65477345608133e6d67a8600f7f9c->leave($__internal_6636fe30f16770c0abd08e67e351078646a65477345608133e6d67a8600f7f9c_prof);

        
        $__internal_f4fa6bbbcd364772be0c926d92ba6cc298737c84ce3239b316fad1f7c02e6479->leave($__internal_f4fa6bbbcd364772be0c926d92ba6cc298737c84ce3239b316fad1f7c02e6479_prof);

    }

    // line 29
    public function block_adminlte_options($context, array $blocks = array())
    {
        $__internal_85cb7d2435dbd9e47b382154301173fd9cf860dd9f018ad79ad0f7ef456835ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85cb7d2435dbd9e47b382154301173fd9cf860dd9f018ad79ad0f7ef456835ba->enter($__internal_85cb7d2435dbd9e47b382154301173fd9cf860dd9f018ad79ad0f7ef456835ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        $__internal_486abd44e92b4a2a995dbe0b77fccddffd464090c7d691dd49d1bb6ffe57c586 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_486abd44e92b4a2a995dbe0b77fccddffd464090c7d691dd49d1bb6ffe57c586->enter($__internal_486abd44e92b4a2a995dbe0b77fccddffd464090c7d691dd49d1bb6ffe57c586_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "adminlte_options"));

        // line 30
        echo "                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            ";
        
        $__internal_486abd44e92b4a2a995dbe0b77fccddffd464090c7d691dd49d1bb6ffe57c586->leave($__internal_486abd44e92b4a2a995dbe0b77fccddffd464090c7d691dd49d1bb6ffe57c586_prof);

        
        $__internal_85cb7d2435dbd9e47b382154301173fd9cf860dd9f018ad79ad0f7ef456835ba->leave($__internal_85cb7d2435dbd9e47b382154301173fd9cf860dd9f018ad79ad0f7ef456835ba_prof);

    }

    // line 57
    public function block_body($context, array $blocks = array())
    {
        $__internal_ef2f61cff9708cc8dc6236e76aac865c634c8bec30ae6f2b00de07c89339246d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef2f61cff9708cc8dc6236e76aac865c634c8bec30ae6f2b00de07c89339246d->enter($__internal_ef2f61cff9708cc8dc6236e76aac865c634c8bec30ae6f2b00de07c89339246d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1ec34e490496ebd30261c175ecc703f49003a4d91ebea72bcfde3829b5e830f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ec34e490496ebd30261c175ecc703f49003a4d91ebea72bcfde3829b5e830f0->enter($__internal_1ec34e490496ebd30261c175ecc703f49003a4d91ebea72bcfde3829b5e830f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 58
        echo "    <body id=\"";
        $this->displayBlock('body_id', $context, $blocks);
        echo "\" class=\"easyadmin sidebar-mini ";
        $this->displayBlock('body_class', $context, $blocks);
        echo " ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 58, $this->getSourceContext()); })()), "request", array()), "cookies", array()), "has", array(0 => "_easyadmin_navigation_iscollapsed"), "method")) ? ("sidebar-collapse") : (""));
        echo "\">
        <div class=\"wrapper\">
        ";
        // line 60
        $this->displayBlock('wrapper', $context, $blocks);
        // line 158
        echo "        </div>

        ";
        // line 160
        $this->displayBlock('body_javascript', $context, $blocks);
        // line 161
        echo "
        ";
        // line 162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.assets.js"));
        foreach ($context['_seq'] as $context["_key"] => $context["js_asset"]) {
            // line 163
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["js_asset"]), "html", null, true);
            echo "\"></script>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js_asset'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 165
        echo "    </body>
    ";
        
        $__internal_1ec34e490496ebd30261c175ecc703f49003a4d91ebea72bcfde3829b5e830f0->leave($__internal_1ec34e490496ebd30261c175ecc703f49003a4d91ebea72bcfde3829b5e830f0_prof);

        
        $__internal_ef2f61cff9708cc8dc6236e76aac865c634c8bec30ae6f2b00de07c89339246d->leave($__internal_ef2f61cff9708cc8dc6236e76aac865c634c8bec30ae6f2b00de07c89339246d_prof);

    }

    // line 58
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_fb2f6982fc24411a95a3f4bf29f669c0bde71bcff197888714f737c695a52f49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb2f6982fc24411a95a3f4bf29f669c0bde71bcff197888714f737c695a52f49->enter($__internal_fb2f6982fc24411a95a3f4bf29f669c0bde71bcff197888714f737c695a52f49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_4d787cd42de011133997bd85c518f303f2d48f4ae6548afb04e0e97e7f9a822d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d787cd42de011133997bd85c518f303f2d48f4ae6548afb04e0e97e7f9a822d->enter($__internal_4d787cd42de011133997bd85c518f303f2d48f4ae6548afb04e0e97e7f9a822d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_4d787cd42de011133997bd85c518f303f2d48f4ae6548afb04e0e97e7f9a822d->leave($__internal_4d787cd42de011133997bd85c518f303f2d48f4ae6548afb04e0e97e7f9a822d_prof);

        
        $__internal_fb2f6982fc24411a95a3f4bf29f669c0bde71bcff197888714f737c695a52f49->leave($__internal_fb2f6982fc24411a95a3f4bf29f669c0bde71bcff197888714f737c695a52f49_prof);

    }

    public function block_body_class($context, array $blocks = array())
    {
        $__internal_2b1dc05ce443d49d38e07972f834e426ad015951f78d5693dd595a50b2a84c79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b1dc05ce443d49d38e07972f834e426ad015951f78d5693dd595a50b2a84c79->enter($__internal_2b1dc05ce443d49d38e07972f834e426ad015951f78d5693dd595a50b2a84c79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_2897ed5fd7ca40fcf80222d079ff6b7cfc3baf64d0719bcac5089b6b70c20fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2897ed5fd7ca40fcf80222d079ff6b7cfc3baf64d0719bcac5089b6b70c20fd8->enter($__internal_2897ed5fd7ca40fcf80222d079ff6b7cfc3baf64d0719bcac5089b6b70c20fd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        
        $__internal_2897ed5fd7ca40fcf80222d079ff6b7cfc3baf64d0719bcac5089b6b70c20fd8->leave($__internal_2897ed5fd7ca40fcf80222d079ff6b7cfc3baf64d0719bcac5089b6b70c20fd8_prof);

        
        $__internal_2b1dc05ce443d49d38e07972f834e426ad015951f78d5693dd595a50b2a84c79->leave($__internal_2b1dc05ce443d49d38e07972f834e426ad015951f78d5693dd595a50b2a84c79_prof);

    }

    // line 60
    public function block_wrapper($context, array $blocks = array())
    {
        $__internal_ce11323d6dc4eca34ffe66c6b51331a50b46fe1cd1f1b93878e86fa203c2fb89 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce11323d6dc4eca34ffe66c6b51331a50b46fe1cd1f1b93878e86fa203c2fb89->enter($__internal_ce11323d6dc4eca34ffe66c6b51331a50b46fe1cd1f1b93878e86fa203c2fb89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        $__internal_28abecff79c86406630774cca675630f50ec111074e148571f76dba426d0bfeb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28abecff79c86406630774cca675630f50ec111074e148571f76dba426d0bfeb->enter($__internal_28abecff79c86406630774cca675630f50ec111074e148571f76dba426d0bfeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "wrapper"));

        // line 61
        echo "            <header class=\"main-header\">
            ";
        // line 62
        $this->displayBlock('header', $context, $blocks);
        // line 115
        echo "            </header>

            <aside class=\"main-sidebar\">
            ";
        // line 118
        $this->displayBlock('sidebar', $context, $blocks);
        // line 129
        echo "            </aside>

            <div class=\"content-wrapper\">
            ";
        // line 132
        $this->displayBlock('content', $context, $blocks);
        // line 156
        echo "            </div>
        ";
        
        $__internal_28abecff79c86406630774cca675630f50ec111074e148571f76dba426d0bfeb->leave($__internal_28abecff79c86406630774cca675630f50ec111074e148571f76dba426d0bfeb_prof);

        
        $__internal_ce11323d6dc4eca34ffe66c6b51331a50b46fe1cd1f1b93878e86fa203c2fb89->leave($__internal_ce11323d6dc4eca34ffe66c6b51331a50b46fe1cd1f1b93878e86fa203c2fb89_prof);

    }

    // line 62
    public function block_header($context, array $blocks = array())
    {
        $__internal_e61fdba2b41befd2d547e10a6f96c585e502803b9705303b3eb924299cd079e9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e61fdba2b41befd2d547e10a6f96c585e502803b9705303b3eb924299cd079e9->enter($__internal_e61fdba2b41befd2d547e10a6f96c585e502803b9705303b3eb924299cd079e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_a855072baba7d70c35436bdbecd9922583498aa797948a2225c1f94b59113655 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a855072baba7d70c35436bdbecd9922583498aa797948a2225c1f94b59113655->enter($__internal_a855072baba7d70c35436bdbecd9922583498aa797948a2225c1f94b59113655_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 63
        echo "                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("toggle_navigation", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>
                    </a>

                    <div id=\"header-logo\">
                        ";
        // line 69
        $this->displayBlock('header_logo', $context, $blocks);
        // line 74
        echo "                    </div>

                    <div class=\"navbar-custom-menu\">
                    ";
        // line 77
        $this->displayBlock('header_custom_menu', $context, $blocks);
        // line 112
        echo "                    </div>
                </nav>
            ";
        
        $__internal_a855072baba7d70c35436bdbecd9922583498aa797948a2225c1f94b59113655->leave($__internal_a855072baba7d70c35436bdbecd9922583498aa797948a2225c1f94b59113655_prof);

        
        $__internal_e61fdba2b41befd2d547e10a6f96c585e502803b9705303b3eb924299cd079e9->leave($__internal_e61fdba2b41befd2d547e10a6f96c585e502803b9705303b3eb924299cd079e9_prof);

    }

    // line 69
    public function block_header_logo($context, array $blocks = array())
    {
        $__internal_a8594b680c1cdc4f9fc4679a6543711926f674506540700ff76ab7398fde5e9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8594b680c1cdc4f9fc4679a6543711926f674506540700ff76ab7398fde5e9c->enter($__internal_a8594b680c1cdc4f9fc4679a6543711926f674506540700ff76ab7398fde5e9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        $__internal_78c37e2e4c23aed0c5b3a5e6af8ea0e4d56349f8e254af2851f124ebb18168e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78c37e2e4c23aed0c5b3a5e6af8ea0e4d56349f8e254af2851f124ebb18168e9->enter($__internal_78c37e2e4c23aed0c5b3a5e6af8ea0e4d56349f8e254af2851f124ebb18168e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_logo"));

        // line 70
        echo "                            <a class=\"logo ";
        echo (((twig_length_filter($this->env, $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")) > 14)) ? ("logo-long") : (""));
        echo "\" title=\"";
        echo twig_escape_filter($this->env, strip_tags($this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name")), "html", null, true);
        echo "\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("easyadmin");
        echo "\">
                                ";
        // line 71
        echo $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("site_name");
        echo "
                            </a>
                        ";
        
        $__internal_78c37e2e4c23aed0c5b3a5e6af8ea0e4d56349f8e254af2851f124ebb18168e9->leave($__internal_78c37e2e4c23aed0c5b3a5e6af8ea0e4d56349f8e254af2851f124ebb18168e9_prof);

        
        $__internal_a8594b680c1cdc4f9fc4679a6543711926f674506540700ff76ab7398fde5e9c->leave($__internal_a8594b680c1cdc4f9fc4679a6543711926f674506540700ff76ab7398fde5e9c_prof);

    }

    // line 77
    public function block_header_custom_menu($context, array $blocks = array())
    {
        $__internal_964bb3596eea70e3989c556a5754e309a44362cb9f98546d67cf28062f34c60e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_964bb3596eea70e3989c556a5754e309a44362cb9f98546d67cf28062f34c60e->enter($__internal_964bb3596eea70e3989c556a5754e309a44362cb9f98546d67cf28062f34c60e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        $__internal_bda5c035f3520b49aa3ea777f3cfe48cd139f268eb2c1b9d31c542ae3b843a26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bda5c035f3520b49aa3ea777f3cfe48cd139f268eb2c1b9d31c542ae3b843a26->enter($__internal_bda5c035f3520b49aa3ea777f3cfe48cd139f268eb2c1b9d31c542ae3b843a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_custom_menu"));

        // line 78
        echo "                        ";
        $context["_logout_path"] = $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getLogoutPath();
        // line 79
        echo "                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                ";
        // line 81
        $this->displayBlock('user_menu', $context, $blocks);
        // line 109
        echo "                            </li>
                        </ul>
                    ";
        
        $__internal_bda5c035f3520b49aa3ea777f3cfe48cd139f268eb2c1b9d31c542ae3b843a26->leave($__internal_bda5c035f3520b49aa3ea777f3cfe48cd139f268eb2c1b9d31c542ae3b843a26_prof);

        
        $__internal_964bb3596eea70e3989c556a5754e309a44362cb9f98546d67cf28062f34c60e->leave($__internal_964bb3596eea70e3989c556a5754e309a44362cb9f98546d67cf28062f34c60e_prof);

    }

    // line 81
    public function block_user_menu($context, array $blocks = array())
    {
        $__internal_6faf1e72f7f8fcbb6e94cc66a6845787179bbd8f608fc5170f65757bc70c3598 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6faf1e72f7f8fcbb6e94cc66a6845787179bbd8f608fc5170f65757bc70c3598->enter($__internal_6faf1e72f7f8fcbb6e94cc66a6845787179bbd8f608fc5170f65757bc70c3598_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        $__internal_430c1d6e366f92f1ca0f688bcfc1c0939b764bc70dfa2b1cdd4896e5fa7947a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_430c1d6e366f92f1ca0f688bcfc1c0939b764bc70dfa2b1cdd4896e5fa7947a6->enter($__internal_430c1d6e366f92f1ca0f688bcfc1c0939b764bc70dfa2b1cdd4896e5fa7947a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu"));

        // line 82
        echo "                                    <span class=\"sr-only\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.logged_in_as", array(), "EasyAdminBundle"), "html", null, true);
        echo "</span>

                                    ";
        // line 84
        if ((((twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array()), false)) : (false)) == false)) {
            // line 85
            echo "                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        ";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.anonymous", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                                    ";
        } elseif ( !        // line 87
(isset($context["_logout_path"]) || array_key_exists("_logout_path", $context) ? $context["_logout_path"] : (function () { throw new Twig_Error_Runtime('Variable "_logout_path" does not exist.', 87, $this->getSourceContext()); })())) {
            // line 88
            echo "                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        ";
            // line 89
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                    ";
        } else {
            // line 91
            echo "                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                ";
            // line 94
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["app"] ?? null), "user", array(), "any", false, true), "username", array()), $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.unnamed", array(), "EasyAdminBundle"))), "html", null, true);
            echo "
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                ";
            // line 100
            $this->displayBlock('user_menu_dropdown', $context, $blocks);
            // line 105
            echo "                                            </ul>
                                        </div>
                                    ";
        }
        // line 108
        echo "                                ";
        
        $__internal_430c1d6e366f92f1ca0f688bcfc1c0939b764bc70dfa2b1cdd4896e5fa7947a6->leave($__internal_430c1d6e366f92f1ca0f688bcfc1c0939b764bc70dfa2b1cdd4896e5fa7947a6_prof);

        
        $__internal_6faf1e72f7f8fcbb6e94cc66a6845787179bbd8f608fc5170f65757bc70c3598->leave($__internal_6faf1e72f7f8fcbb6e94cc66a6845787179bbd8f608fc5170f65757bc70c3598_prof);

    }

    // line 100
    public function block_user_menu_dropdown($context, array $blocks = array())
    {
        $__internal_7e49669d24ad41c9eebaece3daca8ee2ff8a051ec146902a440690917f978db6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e49669d24ad41c9eebaece3daca8ee2ff8a051ec146902a440690917f978db6->enter($__internal_7e49669d24ad41c9eebaece3daca8ee2ff8a051ec146902a440690917f978db6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        $__internal_66bf971f8d3314ac89fb4bf54194f4a3c5b556bf567de207eb7d81abbe52a9df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66bf971f8d3314ac89fb4bf54194f4a3c5b556bf567de207eb7d81abbe52a9df->enter($__internal_66bf971f8d3314ac89fb4bf54194f4a3c5b556bf567de207eb7d81abbe52a9df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_menu_dropdown"));

        // line 101
        echo "                                                <li>
                                                    <a href=\"";
        // line 102
        echo twig_escape_filter($this->env, (isset($context["_logout_path"]) || array_key_exists("_logout_path", $context) ? $context["_logout_path"] : (function () { throw new Twig_Error_Runtime('Variable "_logout_path" does not exist.', 102, $this->getSourceContext()); })()), "html", null, true);
        echo "\"><i class=\"fa fa-sign-out\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("user.signout", array(), "EasyAdminBundle"), "html", null, true);
        echo "</a>
                                                </li>
                                                ";
        
        $__internal_66bf971f8d3314ac89fb4bf54194f4a3c5b556bf567de207eb7d81abbe52a9df->leave($__internal_66bf971f8d3314ac89fb4bf54194f4a3c5b556bf567de207eb7d81abbe52a9df_prof);

        
        $__internal_7e49669d24ad41c9eebaece3daca8ee2ff8a051ec146902a440690917f978db6->leave($__internal_7e49669d24ad41c9eebaece3daca8ee2ff8a051ec146902a440690917f978db6_prof);

    }

    // line 118
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_9527f52e589464d53c802e6dd761fe1e43b765c38598c99fbd7ae6afa8e1e989 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9527f52e589464d53c802e6dd761fe1e43b765c38598c99fbd7ae6afa8e1e989->enter($__internal_9527f52e589464d53c802e6dd761fe1e43b765c38598c99fbd7ae6afa8e1e989_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_964f174c16160a0221e24ac08e2d07ede39bc0cbaae6ce498819408efd3f5fe4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_964f174c16160a0221e24ac08e2d07ede39bc0cbaae6ce498819408efd3f5fe4->enter($__internal_964f174c16160a0221e24ac08e2d07ede39bc0cbaae6ce498819408efd3f5fe4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 119
        echo "                <section class=\"sidebar\">
                    ";
        // line 120
        $this->displayBlock('main_menu_wrapper', $context, $blocks);
        // line 127
        echo "                </section>
            ";
        
        $__internal_964f174c16160a0221e24ac08e2d07ede39bc0cbaae6ce498819408efd3f5fe4->leave($__internal_964f174c16160a0221e24ac08e2d07ede39bc0cbaae6ce498819408efd3f5fe4_prof);

        
        $__internal_9527f52e589464d53c802e6dd761fe1e43b765c38598c99fbd7ae6afa8e1e989->leave($__internal_9527f52e589464d53c802e6dd761fe1e43b765c38598c99fbd7ae6afa8e1e989_prof);

    }

    // line 120
    public function block_main_menu_wrapper($context, array $blocks = array())
    {
        $__internal_80980113aebd0e4d13a46df1a5dbc3b69b351919bfcf89ec86cb6730e550c9f9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80980113aebd0e4d13a46df1a5dbc3b69b351919bfcf89ec86cb6730e550c9f9->enter($__internal_80980113aebd0e4d13a46df1a5dbc3b69b351919bfcf89ec86cb6730e550c9f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        $__internal_5696b05fd8bcc5d60194b31e2dcb083c06f30a848132cb7d2307201076ac5ce8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5696b05fd8bcc5d60194b31e2dcb083c06f30a848132cb7d2307201076ac5ce8->enter($__internal_5696b05fd8bcc5d60194b31e2dcb083c06f30a848132cb7d2307201076ac5ce8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_menu_wrapper"));

        // line 121
        echo "                        ";
        echo twig_include($this->env, $context, array(0 => ((        // line 122
array_key_exists("_entity_config", $context)) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 122, $this->getSourceContext()); })()), "templates", array()), "menu", array())) : ("")), 1 => $this->env->getExtension('JavierEguiluz\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension')->getBackendConfiguration("design.templates.menu"), 2 => "@EasyAdmin/default/menu.html.twig"));
        // line 125
        echo "
                    ";
        
        $__internal_5696b05fd8bcc5d60194b31e2dcb083c06f30a848132cb7d2307201076ac5ce8->leave($__internal_5696b05fd8bcc5d60194b31e2dcb083c06f30a848132cb7d2307201076ac5ce8_prof);

        
        $__internal_80980113aebd0e4d13a46df1a5dbc3b69b351919bfcf89ec86cb6730e550c9f9->leave($__internal_80980113aebd0e4d13a46df1a5dbc3b69b351919bfcf89ec86cb6730e550c9f9_prof);

    }

    // line 132
    public function block_content($context, array $blocks = array())
    {
        $__internal_4eb7b91aae3e72279aba7f0e8e2f7f28d061d51ee9a22055ffb9f8816972b3b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4eb7b91aae3e72279aba7f0e8e2f7f28d061d51ee9a22055ffb9f8816972b3b0->enter($__internal_4eb7b91aae3e72279aba7f0e8e2f7f28d061d51ee9a22055ffb9f8816972b3b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_1f566e66d1de40865f57e9ebf4edd335a34c3c38f675b39be0cd22c9a572904a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f566e66d1de40865f57e9ebf4edd335a34c3c38f675b39be0cd22c9a572904a->enter($__internal_1f566e66d1de40865f57e9ebf4edd335a34c3c38f675b39be0cd22c9a572904a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 133
        echo "                ";
        $this->displayBlock('flash_messages', $context, $blocks);
        // line 136
        echo "
                <section class=\"content-header\">
                ";
        // line 138
        $this->displayBlock('content_header', $context, $blocks);
        // line 141
        echo "                ";
        $this->displayBlock('content_help', $context, $blocks);
        // line 150
        echo "                </section>

                <section id=\"main\" class=\"content\">
                    ";
        // line 153
        $this->displayBlock('main', $context, $blocks);
        // line 154
        echo "                </section>
            ";
        
        $__internal_1f566e66d1de40865f57e9ebf4edd335a34c3c38f675b39be0cd22c9a572904a->leave($__internal_1f566e66d1de40865f57e9ebf4edd335a34c3c38f675b39be0cd22c9a572904a_prof);

        
        $__internal_4eb7b91aae3e72279aba7f0e8e2f7f28d061d51ee9a22055ffb9f8816972b3b0->leave($__internal_4eb7b91aae3e72279aba7f0e8e2f7f28d061d51ee9a22055ffb9f8816972b3b0_prof);

    }

    // line 133
    public function block_flash_messages($context, array $blocks = array())
    {
        $__internal_9040aa97ac36ac508f2b0e5104ac6c06dc3eeacfa0241571571b9482a0a50a45 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9040aa97ac36ac508f2b0e5104ac6c06dc3eeacfa0241571571b9482a0a50a45->enter($__internal_9040aa97ac36ac508f2b0e5104ac6c06dc3eeacfa0241571571b9482a0a50a45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        $__internal_bfb60370ceea1dd758b2cad8cf2ccbda7e45283e545ec0affa6510b38109369b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfb60370ceea1dd758b2cad8cf2ccbda7e45283e545ec0affa6510b38109369b->enter($__internal_bfb60370ceea1dd758b2cad8cf2ccbda7e45283e545ec0affa6510b38109369b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "flash_messages"));

        // line 134
        echo "                    ";
        echo twig_include($this->env, $context, ((array_key_exists("_entity_config", $context)) ? (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 134, $this->getSourceContext()); })()), "templates", array()), "flash_messages", array())) : ("@EasyAdmin/default/flash_messages.html.twig")));
        echo "
                ";
        
        $__internal_bfb60370ceea1dd758b2cad8cf2ccbda7e45283e545ec0affa6510b38109369b->leave($__internal_bfb60370ceea1dd758b2cad8cf2ccbda7e45283e545ec0affa6510b38109369b_prof);

        
        $__internal_9040aa97ac36ac508f2b0e5104ac6c06dc3eeacfa0241571571b9482a0a50a45->leave($__internal_9040aa97ac36ac508f2b0e5104ac6c06dc3eeacfa0241571571b9482a0a50a45_prof);

    }

    // line 138
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_93b407c2f0ae5caeca1640b616104adb2e34d71609772bbe397035e614fb9dd5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93b407c2f0ae5caeca1640b616104adb2e34d71609772bbe397035e614fb9dd5->enter($__internal_93b407c2f0ae5caeca1640b616104adb2e34d71609772bbe397035e614fb9dd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_b886ad93e656f9dc23dda246dc0e5fdf813c240cc90b57b9eb29bfcdcdc92bc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b886ad93e656f9dc23dda246dc0e5fdf813c240cc90b57b9eb29bfcdcdc92bc8->enter($__internal_b886ad93e656f9dc23dda246dc0e5fdf813c240cc90b57b9eb29bfcdcdc92bc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        // line 139
        echo "                    <h1 class=\"title\">";
        $this->displayBlock('content_title', $context, $blocks);
        echo "</h1>
                ";
        
        $__internal_b886ad93e656f9dc23dda246dc0e5fdf813c240cc90b57b9eb29bfcdcdc92bc8->leave($__internal_b886ad93e656f9dc23dda246dc0e5fdf813c240cc90b57b9eb29bfcdcdc92bc8_prof);

        
        $__internal_93b407c2f0ae5caeca1640b616104adb2e34d71609772bbe397035e614fb9dd5->leave($__internal_93b407c2f0ae5caeca1640b616104adb2e34d71609772bbe397035e614fb9dd5_prof);

    }

    public function block_content_title($context, array $blocks = array())
    {
        $__internal_8527a2ec6ba698a5bcafe11381e78f270198b62a228ee891aa4b0ccde2af5f60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8527a2ec6ba698a5bcafe11381e78f270198b62a228ee891aa4b0ccde2af5f60->enter($__internal_8527a2ec6ba698a5bcafe11381e78f270198b62a228ee891aa4b0ccde2af5f60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_b1b28def26f559c5c41359f0ae4620ba20b80cb756932d111d549d8db51120b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1b28def26f559c5c41359f0ae4620ba20b80cb756932d111d549d8db51120b9->enter($__internal_b1b28def26f559c5c41359f0ae4620ba20b80cb756932d111d549d8db51120b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        
        $__internal_b1b28def26f559c5c41359f0ae4620ba20b80cb756932d111d549d8db51120b9->leave($__internal_b1b28def26f559c5c41359f0ae4620ba20b80cb756932d111d549d8db51120b9_prof);

        
        $__internal_8527a2ec6ba698a5bcafe11381e78f270198b62a228ee891aa4b0ccde2af5f60->leave($__internal_8527a2ec6ba698a5bcafe11381e78f270198b62a228ee891aa4b0ccde2af5f60_prof);

    }

    // line 141
    public function block_content_help($context, array $blocks = array())
    {
        $__internal_8373f641ea64d5153bf5421d3cd4906b012aaf2ef46673c7fa0a07ecf7941045 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8373f641ea64d5153bf5421d3cd4906b012aaf2ef46673c7fa0a07ecf7941045->enter($__internal_8373f641ea64d5153bf5421d3cd4906b012aaf2ef46673c7fa0a07ecf7941045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        $__internal_33ee038fb2b080734d6f3a5a85633d6ba967fe9e745fa3d1b0a0a40c116f103f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33ee038fb2b080734d6f3a5a85633d6ba967fe9e745fa3d1b0a0a40c116f103f->enter($__internal_33ee038fb2b080734d6f3a5a85633d6ba967fe9e745fa3d1b0a0a40c116f103f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_help"));

        // line 142
        echo "                    ";
        if ((array_key_exists("_entity_config", $context) && ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 142, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["_entity_config"] ?? null), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 142, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array", false, true), "help", array(), "array"), false)) : (false)))) {
            // line 143
            echo "                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                ";
            // line 145
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 145, $this->getSourceContext()); })()), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 145, $this->getSourceContext()); })()), "request", array()), "query", array()), "get", array(0 => "action"), "method"), array(), "array"), "help", array(), "array"));
            echo "
                            </div>
                        </div>
                    ";
        }
        // line 149
        echo "                ";
        
        $__internal_33ee038fb2b080734d6f3a5a85633d6ba967fe9e745fa3d1b0a0a40c116f103f->leave($__internal_33ee038fb2b080734d6f3a5a85633d6ba967fe9e745fa3d1b0a0a40c116f103f_prof);

        
        $__internal_8373f641ea64d5153bf5421d3cd4906b012aaf2ef46673c7fa0a07ecf7941045->leave($__internal_8373f641ea64d5153bf5421d3cd4906b012aaf2ef46673c7fa0a07ecf7941045_prof);

    }

    // line 153
    public function block_main($context, array $blocks = array())
    {
        $__internal_add209333876fdd4d02328d800560b7d5eda461ed7a4c572c45653e7da9531ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_add209333876fdd4d02328d800560b7d5eda461ed7a4c572c45653e7da9531ca->enter($__internal_add209333876fdd4d02328d800560b7d5eda461ed7a4c572c45653e7da9531ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_e3d92fc07ae71794150a40aa20a5df2e9acd579246c1175c448e7bdc8429121b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3d92fc07ae71794150a40aa20a5df2e9acd579246c1175c448e7bdc8429121b->enter($__internal_e3d92fc07ae71794150a40aa20a5df2e9acd579246c1175c448e7bdc8429121b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        
        $__internal_e3d92fc07ae71794150a40aa20a5df2e9acd579246c1175c448e7bdc8429121b->leave($__internal_e3d92fc07ae71794150a40aa20a5df2e9acd579246c1175c448e7bdc8429121b_prof);

        
        $__internal_add209333876fdd4d02328d800560b7d5eda461ed7a4c572c45653e7da9531ca->leave($__internal_add209333876fdd4d02328d800560b7d5eda461ed7a4c572c45653e7da9531ca_prof);

    }

    // line 160
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_44b25ec5e10fcaca7507c7f83760da691d1cd84d3b427e54662543975c4b87e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44b25ec5e10fcaca7507c7f83760da691d1cd84d3b427e54662543975c4b87e2->enter($__internal_44b25ec5e10fcaca7507c7f83760da691d1cd84d3b427e54662543975c4b87e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_ce9b16509e1f372b39cb981b8d705873fd52c67716ebc4c8f240a28a91701ae0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce9b16509e1f372b39cb981b8d705873fd52c67716ebc4c8f240a28a91701ae0->enter($__internal_ce9b16509e1f372b39cb981b8d705873fd52c67716ebc4c8f240a28a91701ae0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        
        $__internal_ce9b16509e1f372b39cb981b8d705873fd52c67716ebc4c8f240a28a91701ae0->leave($__internal_ce9b16509e1f372b39cb981b8d705873fd52c67716ebc4c8f240a28a91701ae0_prof);

        
        $__internal_44b25ec5e10fcaca7507c7f83760da691d1cd84d3b427e54662543975c4b87e2->leave($__internal_44b25ec5e10fcaca7507c7f83760da691d1cd84d3b427e54662543975c4b87e2_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/default/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  786 => 160,  769 => 153,  759 => 149,  752 => 145,  748 => 143,  745 => 142,  736 => 141,  707 => 139,  698 => 138,  685 => 134,  676 => 133,  665 => 154,  663 => 153,  658 => 150,  655 => 141,  653 => 138,  649 => 136,  646 => 133,  637 => 132,  626 => 125,  624 => 122,  622 => 121,  613 => 120,  602 => 127,  600 => 120,  597 => 119,  588 => 118,  573 => 102,  570 => 101,  561 => 100,  551 => 108,  546 => 105,  544 => 100,  535 => 94,  530 => 91,  525 => 89,  522 => 88,  520 => 87,  516 => 86,  513 => 85,  511 => 84,  505 => 82,  496 => 81,  484 => 109,  482 => 81,  478 => 79,  475 => 78,  466 => 77,  453 => 71,  444 => 70,  435 => 69,  423 => 112,  421 => 77,  416 => 74,  414 => 69,  407 => 65,  403 => 63,  394 => 62,  383 => 156,  381 => 132,  376 => 129,  374 => 118,  369 => 115,  367 => 62,  364 => 61,  355 => 60,  322 => 58,  311 => 165,  302 => 163,  298 => 162,  295 => 161,  293 => 160,  289 => 158,  287 => 60,  277 => 58,  268 => 57,  247 => 30,  238 => 29,  226 => 43,  223 => 42,  220 => 29,  211 => 28,  196 => 25,  193 => 24,  184 => 23,  171 => 15,  165 => 13,  156 => 12,  138 => 10,  127 => 167,  125 => 57,  118 => 53,  114 => 52,  110 => 50,  105 => 48,  100 => 47,  98 => 46,  95 => 45,  93 => 28,  90 => 27,  88 => 23,  85 => 22,  76 => 20,  72 => 19,  69 => 18,  67 => 12,  62 => 10,  51 => 2,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale|split('_')|first|default('en') }}\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"robots\" content=\"noindex, nofollow, noarchive, nosnippet, noodp, noimageindex, notranslate, nocache\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <meta name=\"generator\" content=\"EasyAdmin\" />

        <title>{% block page_title %}{{ block('content_title')|striptags|raw }}{% endblock %}</title>

        {% block head_stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/easyadmin-all.min.css') }}\">
            <style>
                {{ easyadmin_config('_internal.custom_css')|raw }}
            </style>
        {% endblock %}

        {% for css_asset in easyadmin_config('design.assets.css') %}
            <link rel=\"stylesheet\" href=\"{{ asset(css_asset) }}\">
        {% endfor %}

        {% block head_favicon %}
            {% set favicon = easyadmin_config('design.assets.favicon') %}
            <link rel=\"icon\" type=\"{{ favicon.mime_type }}\" href=\"{{ asset(favicon.path) }}\" />
        {% endblock %}

        {% block head_javascript %}
            {% block adminlte_options %}
                <script type=\"text/javascript\">
                    var AdminLTEOptions = {
                        animationSpeed: 'normal',
                        sidebarExpandOnHover: false,
                        enableBoxRefresh: false,
                        enableBSToppltip: false,
                        enableFastclick: false,
                        enableControlSidebar: false,
                        enableBoxWidget: false
                    };
                </script>
            {% endblock %}

            <script src=\"{{ asset('bundles/easyadmin/javascript/easyadmin-all.min.js') }}\"></script>
        {% endblock head_javascript %}

        {% if easyadmin_config('design.rtl') %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/bootstrap-rtl.min.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/easyadmin/stylesheet/adminlte-rtl.min.css') }}\">
        {% endif %}

        <!--[if lt IE 9]>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/html5shiv.min.css') }}\"></script>
            <script src=\"{{ asset('bundles/easyadmin/stylesheet/respond.min.css') }}\"></script>
        <![endif]-->
    </head>

    {% block body %}
    <body id=\"{% block body_id %}{% endblock %}\" class=\"easyadmin sidebar-mini {% block body_class %}{% endblock %} {{ app.request.cookies.has('_easyadmin_navigation_iscollapsed') ? 'sidebar-collapse' }}\">
        <div class=\"wrapper\">
        {% block wrapper %}
            <header class=\"main-header\">
            {% block header %}
                <nav class=\"navbar\" role=\"navigation\">
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">{{ 'toggle_navigation'|trans(domain = 'EasyAdminBundle') }}</span>
                    </a>

                    <div id=\"header-logo\">
                        {% block header_logo %}
                            <a class=\"logo {{ easyadmin_config('site_name')|length > 14 ? 'logo-long' }}\" title=\"{{ easyadmin_config('site_name')|striptags }}\" href=\"{{ path('easyadmin') }}\">
                                {{ easyadmin_config('site_name')|raw }}
                            </a>
                        {% endblock header_logo %}
                    </div>

                    <div class=\"navbar-custom-menu\">
                    {% block header_custom_menu %}
                        {% set _logout_path = easyadmin_logout_path() %}
                        <ul class=\"nav navbar-nav\">
                            <li class=\"dropdown user user-menu\">
                                {% block user_menu %}
                                    <span class=\"sr-only\">{{ 'user.logged_in_as'|trans(domain = 'EasyAdminBundle') }}</span>

                                    {% if app.user|default(false) == false %}
                                        <i class=\"hidden-xs fa fa-user-times\"></i>
                                        {{ 'user.anonymous'|trans(domain = 'EasyAdminBundle') }}
                                    {% elseif not _logout_path %}
                                        <i class=\"hidden-xs fa fa-user\"></i>
                                        {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                    {% else %}
                                        <div class=\"btn-group\">
                                            <button type=\"button\" class=\"btn\" data-toggle=\"dropdown\">
                                                <i class=\"hidden-xs fa fa-user\"></i>
                                                {{ app.user.username|default('user.unnamed'|trans(domain = 'EasyAdminBundle')) }}
                                            </button>
                                            <button type=\"button\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                {% block user_menu_dropdown %}
                                                <li>
                                                    <a href=\"{{ _logout_path }}\"><i class=\"fa fa-sign-out\"></i> {{ 'user.signout'|trans(domain = 'EasyAdminBundle') }}</a>
                                                </li>
                                                {% endblock user_menu_dropdown %}
                                            </ul>
                                        </div>
                                    {% endif %}
                                {% endblock user_menu %}
                            </li>
                        </ul>
                    {% endblock header_custom_menu %}
                    </div>
                </nav>
            {% endblock header %}
            </header>

            <aside class=\"main-sidebar\">
            {% block sidebar %}
                <section class=\"sidebar\">
                    {% block main_menu_wrapper %}
                        {{ include([
                            _entity_config is defined ? _entity_config.templates.menu,
                            easyadmin_config('design.templates.menu'),
                            '@EasyAdmin/default/menu.html.twig'
                        ]) }}
                    {% endblock main_menu_wrapper %}
                </section>
            {% endblock sidebar %}
            </aside>

            <div class=\"content-wrapper\">
            {% block content %}
                {% block flash_messages %}
                    {{ include(_entity_config is defined ? _entity_config.templates.flash_messages : '@EasyAdmin/default/flash_messages.html.twig') }}
                {% endblock flash_messages %}

                <section class=\"content-header\">
                {% block content_header %}
                    <h1 class=\"title\">{% block content_title %}{% endblock %}</h1>
                {% endblock content_header %}
                {% block content_help %}
                    {% if _entity_config is defined and _entity_config[app.request.query.get('action')]['help']|default(false) %}
                        <div class=\"box box-widget help-entity\">
                            <div class=\"box-body\">
                                {{ _entity_config[app.request.query.get('action')]['help']|trans|raw }}
                            </div>
                        </div>
                    {% endif %}
                {% endblock content_help %}
                </section>

                <section id=\"main\" class=\"content\">
                    {% block main %}{% endblock %}
                </section>
            {% endblock content %}
            </div>
        {% endblock wrapper %}
        </div>

        {% block body_javascript %}{% endblock body_javascript %}

        {% for js_asset in easyadmin_config('design.assets.js') %}
            <script src=\"{{ asset(js_asset) }}\"></script>
        {% endfor %}
    </body>
    {% endblock body %}
</html>
", "@EasyAdmin/default/layout.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/default/layout.html.twig");
    }
}

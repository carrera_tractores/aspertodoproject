<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_a358e5ce4b8f1f4a8b1f6091f10fa9ea74e2f5f5fa9be82b8518f2d1b2d55b90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66a59788a21d50255f9b98bc86a629c78adaa31731cb82d9c0376faf0a9a8939 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66a59788a21d50255f9b98bc86a629c78adaa31731cb82d9c0376faf0a9a8939->enter($__internal_66a59788a21d50255f9b98bc86a629c78adaa31731cb82d9c0376faf0a9a8939_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_b85782780f40be27f27421c88cdce6363971091bb4d4d6ba400a21d4ab13c42e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b85782780f40be27f27421c88cdce6363971091bb4d4d6ba400a21d4ab13c42e->enter($__internal_b85782780f40be27f27421c88cdce6363971091bb4d4d6ba400a21d4ab13c42e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })()))));
        // line 3
        echo "*/
";
        
        $__internal_66a59788a21d50255f9b98bc86a629c78adaa31731cb82d9c0376faf0a9a8939->leave($__internal_66a59788a21d50255f9b98bc86a629c78adaa31731cb82d9c0376faf0a9a8939_prof);

        
        $__internal_b85782780f40be27f27421c88cdce6363971091bb4d4d6ba400a21d4ab13c42e->leave($__internal_b85782780f40be27f27421c88cdce6363971091bb4d4d6ba400a21d4ab13c42e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.js.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}

<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_a2d99eb8b342235f421e7fbce293cdafac5dab65bdd5bed51ee9af2ede5a05a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4112e3a0a7f7e627910d81bdd0c60db2200ab84a5c91967d632958c9a12cc65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4112e3a0a7f7e627910d81bdd0c60db2200ab84a5c91967d632958c9a12cc65->enter($__internal_e4112e3a0a7f7e627910d81bdd0c60db2200ab84a5c91967d632958c9a12cc65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_4d77626ff8b1c42b10b63d00d3a4ef3176136f7afd86554370b543474c499083 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d77626ff8b1c42b10b63d00d3a4ef3176136f7afd86554370b543474c499083->enter($__internal_4d77626ff8b1c42b10b63d00d3a4ef3176136f7afd86554370b543474c499083_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e4112e3a0a7f7e627910d81bdd0c60db2200ab84a5c91967d632958c9a12cc65->leave($__internal_e4112e3a0a7f7e627910d81bdd0c60db2200ab84a5c91967d632958c9a12cc65_prof);

        
        $__internal_4d77626ff8b1c42b10b63d00d3a4ef3176136f7afd86554370b543474c499083->leave($__internal_4d77626ff8b1c42b10b63d00d3a4ef3176136f7afd86554370b543474c499083_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9092e12512dd5088a0d05568b11f73f77ee3cf1410242831b787ef87915071ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9092e12512dd5088a0d05568b11f73f77ee3cf1410242831b787ef87915071ef->enter($__internal_9092e12512dd5088a0d05568b11f73f77ee3cf1410242831b787ef87915071ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_92e56cc8f94c0314d2da0150e4d20a1931c83c14acd971db5431fd17ac5ddbf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92e56cc8f94c0314d2da0150e4d20a1931c83c14acd971db5431fd17ac5ddbf0->enter($__internal_92e56cc8f94c0314d2da0150e4d20a1931c83c14acd971db5431fd17ac5ddbf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_92e56cc8f94c0314d2da0150e4d20a1931c83c14acd971db5431fd17ac5ddbf0->leave($__internal_92e56cc8f94c0314d2da0150e4d20a1931c83c14acd971db5431fd17ac5ddbf0_prof);

        
        $__internal_9092e12512dd5088a0d05568b11f73f77ee3cf1410242831b787ef87915071ef->leave($__internal_9092e12512dd5088a0d05568b11f73f77ee3cf1410242831b787ef87915071ef_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/friendsofsymfony/user-bundle/Resources/views/Group/list.html.twig");
    }
}

<?php

/* @EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_7df0d4f70b9fcdcd740fb030e4d41276bef0ae3a1c3d159f244e579293740532 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("@EasyAdmin/form/bootstrap_3_layout.html.twig", "@EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."@EasyAdmin/form/bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'checkbox_radio_row' => array($this, 'block_checkbox_radio_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b44c9bb86062048c0ed465654150f1a75084a7307af42b85411fab4a62df797 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b44c9bb86062048c0ed465654150f1a75084a7307af42b85411fab4a62df797->enter($__internal_6b44c9bb86062048c0ed465654150f1a75084a7307af42b85411fab4a62df797_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig"));

        $__internal_0f08ad706e94bd0e209a4c27138ce0fe48d5bbab9d054670eefc3a0742ae6b28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f08ad706e94bd0e209a4c27138ce0fe48d5bbab9d054670eefc3a0742ae6b28->enter($__internal_0f08ad706e94bd0e209a4c27138ce0fe48d5bbab9d054670eefc3a0742ae6b28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 22
        echo "
";
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('form_row', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('radio_row', $context, $blocks);
        // line 60
        echo "
";
        // line 61
        $this->displayBlock('checkbox_radio_row', $context, $blocks);
        // line 72
        echo "
";
        // line 73
        $this->displayBlock('submit_row', $context, $blocks);
        // line 83
        echo "
";
        // line 84
        $this->displayBlock('reset_row', $context, $blocks);
        // line 94
        echo "
";
        // line 95
        $this->displayBlock('form_group_class', $context, $blocks);
        
        $__internal_6b44c9bb86062048c0ed465654150f1a75084a7307af42b85411fab4a62df797->leave($__internal_6b44c9bb86062048c0ed465654150f1a75084a7307af42b85411fab4a62df797_prof);

        
        $__internal_0f08ad706e94bd0e209a4c27138ce0fe48d5bbab9d054670eefc3a0742ae6b28->leave($__internal_0f08ad706e94bd0e209a4c27138ce0fe48d5bbab9d054670eefc3a0742ae6b28_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_5b21ed4f584bab4d656e787221c9ad56909f604b7c25c55c9097e7f89cab26a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b21ed4f584bab4d656e787221c9ad56909f604b7c25c55c9097e7f89cab26a9->enter($__internal_5b21ed4f584bab4d656e787221c9ad56909f604b7c25c55c9097e7f89cab26a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_10e91cac032269b09bff6eb9cbadf2b397297f760567908681085dd4c0f0fb56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10e91cac032269b09bff6eb9cbadf2b397297f760567908681085dd4c0f0fb56->enter($__internal_10e91cac032269b09bff6eb9cbadf2b397297f760567908681085dd4c0f0fb56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["_easyadmin_form_type"] = "horizontal";
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_10e91cac032269b09bff6eb9cbadf2b397297f760567908681085dd4c0f0fb56->leave($__internal_10e91cac032269b09bff6eb9cbadf2b397297f760567908681085dd4c0f0fb56_prof);

        
        $__internal_5b21ed4f584bab4d656e787221c9ad56909f604b7c25c55c9097e7f89cab26a9->leave($__internal_5b21ed4f584bab4d656e787221c9ad56909f604b7c25c55c9097e7f89cab26a9_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_8580e8189583ddd95b743e4d509c9ee28068bd53c3c2eb7769419f3978efaa6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8580e8189583ddd95b743e4d509c9ee28068bd53c3c2eb7769419f3978efaa6f->enter($__internal_8580e8189583ddd95b743e4d509c9ee28068bd53c3c2eb7769419f3978efaa6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_8e422c3660ad682bd3ebca960c2e1adf043aa19fd2d872a316f5f5a23ce8f54d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e422c3660ad682bd3ebca960c2e1adf043aa19fd2d872a316f5f5a23ce8f54d->enter($__internal_8e422c3660ad682bd3ebca960c2e1adf043aa19fd2d872a316f5f5a23ce8f54d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        ob_start();
        // line 12
        echo "    ";
        if (((isset($context["label"]) || array_key_exists("label", $context) ? $context["label"] : (function () { throw new Twig_Error_Runtime('Variable "label" does not exist.', 12, $this->getSourceContext()); })()) === false)) {
            // line 13
            echo "        <div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>
    ";
        } else {
            // line 15
            echo "        ";
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) || array_key_exists("label_attr", $context) ? $context["label_attr"] : (function () { throw new Twig_Error_Runtime('Variable "label_attr" does not exist.', 15, $this->getSourceContext()); })()), array("class" => twig_trim_filter(((((twig_get_attribute($this->env, $this->getSourceContext(), ($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), ($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 16
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_8e422c3660ad682bd3ebca960c2e1adf043aa19fd2d872a316f5f5a23ce8f54d->leave($__internal_8e422c3660ad682bd3ebca960c2e1adf043aa19fd2d872a316f5f5a23ce8f54d_prof);

        
        $__internal_8580e8189583ddd95b743e4d509c9ee28068bd53c3c2eb7769419f3978efaa6f->leave($__internal_8580e8189583ddd95b743e4d509c9ee28068bd53c3c2eb7769419f3978efaa6f_prof);

    }

    // line 21
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_080a7d654c4686642e8cb6ed3a6e443904838ac280b3fffd2e6a156fd562cd87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_080a7d654c4686642e8cb6ed3a6e443904838ac280b3fffd2e6a156fd562cd87->enter($__internal_080a7d654c4686642e8cb6ed3a6e443904838ac280b3fffd2e6a156fd562cd87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_28a063ba8e7fa0a4b40099518615900bc74163401d3249d2c3f23f414e250bec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28a063ba8e7fa0a4b40099518615900bc74163401d3249d2c3f23f414e250bec->enter($__internal_28a063ba8e7fa0a4b40099518615900bc74163401d3249d2c3f23f414e250bec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        echo "col-sm-2";
        
        $__internal_28a063ba8e7fa0a4b40099518615900bc74163401d3249d2c3f23f414e250bec->leave($__internal_28a063ba8e7fa0a4b40099518615900bc74163401d3249d2c3f23f414e250bec_prof);

        
        $__internal_080a7d654c4686642e8cb6ed3a6e443904838ac280b3fffd2e6a156fd562cd87->leave($__internal_080a7d654c4686642e8cb6ed3a6e443904838ac280b3fffd2e6a156fd562cd87_prof);

    }

    // line 25
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_429b0aeef1f7fc7ac8488f184d1f2265bd17520e19f69bedd867377f8d46d8ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_429b0aeef1f7fc7ac8488f184d1f2265bd17520e19f69bedd867377f8d46d8ed->enter($__internal_429b0aeef1f7fc7ac8488f184d1f2265bd17520e19f69bedd867377f8d46d8ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_134c306977ed3e58f4ed608812c47615af8cf42c78c7e36d21dc689d8ef0e9cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_134c306977ed3e58f4ed608812c47615af8cf42c78c7e36d21dc689d8ef0e9cc->enter($__internal_134c306977ed3e58f4ed608812c47615af8cf42c78c7e36d21dc689d8ef0e9cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 26
        ob_start();
        // line 27
        echo "    ";
        $context["_field_type"] = ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "fieldType", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "fieldType", array()), "default")) : ("default"));
        // line 28
        echo "    <div class=\"form-group ";
        if ((( !(isset($context["compound"]) || array_key_exists("compound", $context) ? $context["compound"] : (function () { throw new Twig_Error_Runtime('Variable "compound" does not exist.', 28, $this->getSourceContext()); })()) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) || array_key_exists("force_error", $context) ? $context["force_error"] : (function () { throw new Twig_Error_Runtime('Variable "force_error" does not exist.', 28, $this->getSourceContext()); })()), false)) : (false))) &&  !(isset($context["valid"]) || array_key_exists("valid", $context) ? $context["valid"] : (function () { throw new Twig_Error_Runtime('Variable "valid" does not exist.', 28, $this->getSourceContext()); })()))) {
            echo "has-error";
        }
        echo " field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, (isset($context["block_prefixes"]) || array_key_exists("block_prefixes", $context) ? $context["block_prefixes"] : (function () { throw new Twig_Error_Runtime('Variable "block_prefixes" does not exist.', 28, $this->getSourceContext()); })()),  -2)), "html", null, true);
        echo "\">
        ";
        // line 29
        $context["_field_label"] = ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "label", array(), "array", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "label", array(), "array"), null)) : (null));
        // line 30
        echo "        ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 30, $this->getSourceContext()); })()), 'label', array("translation_domain" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["easyadmin"]) || array_key_exists("easyadmin", $context) ? $context["easyadmin"] : (function () { throw new Twig_Error_Runtime('Variable "easyadmin" does not exist.', 30, $this->getSourceContext()); })()), "entity", array()), "translation_domain", array())) + (twig_test_empty($_label_ = (isset($context["_field_label"]) || array_key_exists("_field_label", $context) ? $context["_field_label"] : (function () { throw new Twig_Error_Runtime('Variable "_field_label" does not exist.', 30, $this->getSourceContext()); })())) ? array() : array("label" => $_label_)));
        echo "
        <div class=\"";
        // line 31
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 32, $this->getSourceContext()); })()), 'widget');
        echo "

            ";
        // line 34
        if ((twig_in_filter((isset($context["_field_type"]) || array_key_exists("_field_type", $context) ? $context["_field_type"] : (function () { throw new Twig_Error_Runtime('Variable "_field_type" does not exist.', 34, $this->getSourceContext()); })()), array(0 => "datetime", 1 => "date", 2 => "time", 3 => "birthday")) && ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "nullable", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "nullable", array()), false)) : (false)))) {
            // line 35
            echo "                <div class=\"nullable-control\">
                    <label>
                        <input type=\"checkbox\" ";
            // line 37
            if ((null === (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new Twig_Error_Runtime('Variable "data" does not exist.', 37, $this->getSourceContext()); })()))) {
                echo "checked=\"checked\"";
            }
            echo ">
                        ";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("label.nullable_field", array(), "EasyAdminBundle"), "html", null, true);
            echo "
                    </label>
                </div>
            ";
        }
        // line 42
        echo "
            ";
        // line 43
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 43, $this->getSourceContext()); })()), 'errors');
        echo "

            ";
        // line 45
        if ((((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "help", array(), "any", true, true)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["easyadmin"] ?? null), "field", array(), "any", false, true), "help", array()), "")) : ("")) != "")) {
            // line 46
            echo "                <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> ";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["easyadmin"]) || array_key_exists("easyadmin", $context) ? $context["easyadmin"] : (function () { throw new Twig_Error_Runtime('Variable "easyadmin" does not exist.', 46, $this->getSourceContext()); })()), "field", array()), "help", array()), array(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["easyadmin"]) || array_key_exists("easyadmin", $context) ? $context["easyadmin"] : (function () { throw new Twig_Error_Runtime('Variable "easyadmin" does not exist.', 46, $this->getSourceContext()); })()), "entity", array()), "translation_domain", array()));
            echo "</span>
            ";
        }
        // line 48
        echo "        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_134c306977ed3e58f4ed608812c47615af8cf42c78c7e36d21dc689d8ef0e9cc->leave($__internal_134c306977ed3e58f4ed608812c47615af8cf42c78c7e36d21dc689d8ef0e9cc_prof);

        
        $__internal_429b0aeef1f7fc7ac8488f184d1f2265bd17520e19f69bedd867377f8d46d8ed->leave($__internal_429b0aeef1f7fc7ac8488f184d1f2265bd17520e19f69bedd867377f8d46d8ed_prof);

    }

    // line 53
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_2555849f7e305620f70aa5b35695121991fb052bb22c58a45f77cbb765a9e7a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2555849f7e305620f70aa5b35695121991fb052bb22c58a45f77cbb765a9e7a5->enter($__internal_2555849f7e305620f70aa5b35695121991fb052bb22c58a45f77cbb765a9e7a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_2d13e248f796b60cb4474993d6212f57e759bbc69462d7c93794520044fb865f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d13e248f796b60cb4474993d6212f57e759bbc69462d7c93794520044fb865f->enter($__internal_2d13e248f796b60cb4474993d6212f57e759bbc69462d7c93794520044fb865f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 54
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_2d13e248f796b60cb4474993d6212f57e759bbc69462d7c93794520044fb865f->leave($__internal_2d13e248f796b60cb4474993d6212f57e759bbc69462d7c93794520044fb865f_prof);

        
        $__internal_2555849f7e305620f70aa5b35695121991fb052bb22c58a45f77cbb765a9e7a5->leave($__internal_2555849f7e305620f70aa5b35695121991fb052bb22c58a45f77cbb765a9e7a5_prof);

    }

    // line 57
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_bb83fb717725f8d5424e77d33c17044c4a74de3a30c273b543351e63266e22df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb83fb717725f8d5424e77d33c17044c4a74de3a30c273b543351e63266e22df->enter($__internal_bb83fb717725f8d5424e77d33c17044c4a74de3a30c273b543351e63266e22df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_cd78241ad675133ed6cc32508f98ef82c24874e77dd04564adca60daebb233a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd78241ad675133ed6cc32508f98ef82c24874e77dd04564adca60daebb233a7->enter($__internal_cd78241ad675133ed6cc32508f98ef82c24874e77dd04564adca60daebb233a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 58
        $this->displayBlock("checkbox_radio_row", $context, $blocks);
        
        $__internal_cd78241ad675133ed6cc32508f98ef82c24874e77dd04564adca60daebb233a7->leave($__internal_cd78241ad675133ed6cc32508f98ef82c24874e77dd04564adca60daebb233a7_prof);

        
        $__internal_bb83fb717725f8d5424e77d33c17044c4a74de3a30c273b543351e63266e22df->leave($__internal_bb83fb717725f8d5424e77d33c17044c4a74de3a30c273b543351e63266e22df_prof);

    }

    // line 61
    public function block_checkbox_radio_row($context, array $blocks = array())
    {
        $__internal_41042d3361228fbbeb322dffc241297759bbb1d49e3a5e7f7af22d01e81d2025 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41042d3361228fbbeb322dffc241297759bbb1d49e3a5e7f7af22d01e81d2025->enter($__internal_41042d3361228fbbeb322dffc241297759bbb1d49e3a5e7f7af22d01e81d2025_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        $__internal_6aa1b67ee89295fb263bc1bd42cd6dce4f5c2c213035ee6797693c0d21c277db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6aa1b67ee89295fb263bc1bd42cd6dce4f5c2c213035ee6797693c0d21c277db->enter($__internal_6aa1b67ee89295fb263bc1bd42cd6dce4f5c2c213035ee6797693c0d21c277db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_row"));

        // line 62
        ob_start();
        // line 63
        echo "    <div class=\"form-group ";
        if ( !(isset($context["valid"]) || array_key_exists("valid", $context) ? $context["valid"] : (function () { throw new Twig_Error_Runtime('Variable "valid" does not exist.', 63, $this->getSourceContext()); })())) {
            echo "has-error";
        }
        echo " field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, (isset($context["block_prefixes"]) || array_key_exists("block_prefixes", $context) ? $context["block_prefixes"] : (function () { throw new Twig_Error_Runtime('Variable "block_prefixes" does not exist.', 63, $this->getSourceContext()); })()),  -2)), "html", null, true);
        echo "\">
        <div class=\"";
        // line 64
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 65
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 66
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 66, $this->getSourceContext()); })()), 'widget');
        echo "
            ";
        // line 67
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 67, $this->getSourceContext()); })()), 'errors');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_6aa1b67ee89295fb263bc1bd42cd6dce4f5c2c213035ee6797693c0d21c277db->leave($__internal_6aa1b67ee89295fb263bc1bd42cd6dce4f5c2c213035ee6797693c0d21c277db_prof);

        
        $__internal_41042d3361228fbbeb322dffc241297759bbb1d49e3a5e7f7af22d01e81d2025->leave($__internal_41042d3361228fbbeb322dffc241297759bbb1d49e3a5e7f7af22d01e81d2025_prof);

    }

    // line 73
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_95236a468dd6d674a1c2ba845a1b210709d1412474c3b1fd558a74079f8cdaef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95236a468dd6d674a1c2ba845a1b210709d1412474c3b1fd558a74079f8cdaef->enter($__internal_95236a468dd6d674a1c2ba845a1b210709d1412474c3b1fd558a74079f8cdaef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_e994c0a90e8ca82598e9a6da49c30c095f3a433835c27c852cdaf69235b2d7e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e994c0a90e8ca82598e9a6da49c30c095f3a433835c27c852cdaf69235b2d7e8->enter($__internal_e994c0a90e8ca82598e9a6da49c30c095f3a433835c27c852cdaf69235b2d7e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 74
        ob_start();
        // line 75
        echo "    <div class=\"form-group field-";
        echo twig_escape_filter($this->env, twig_first($this->env, twig_slice($this->env, (isset($context["block_prefixes"]) || array_key_exists("block_prefixes", $context) ? $context["block_prefixes"] : (function () { throw new Twig_Error_Runtime('Variable "block_prefixes" does not exist.', 75, $this->getSourceContext()); })()),  -2)), "html", null, true);
        echo "\">
        <div class=\"";
        // line 76
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 77
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 78
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 78, $this->getSourceContext()); })()), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_e994c0a90e8ca82598e9a6da49c30c095f3a433835c27c852cdaf69235b2d7e8->leave($__internal_e994c0a90e8ca82598e9a6da49c30c095f3a433835c27c852cdaf69235b2d7e8_prof);

        
        $__internal_95236a468dd6d674a1c2ba845a1b210709d1412474c3b1fd558a74079f8cdaef->leave($__internal_95236a468dd6d674a1c2ba845a1b210709d1412474c3b1fd558a74079f8cdaef_prof);

    }

    // line 84
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_2bd800ed916784c17e46cc5d2e4a82534b0770e3da62fb4fa11945da58b11fe0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bd800ed916784c17e46cc5d2e4a82534b0770e3da62fb4fa11945da58b11fe0->enter($__internal_2bd800ed916784c17e46cc5d2e4a82534b0770e3da62fb4fa11945da58b11fe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_d8ee886567f4666e0b654f9690a8ac15cfb55caddc9aefc7129b01a49bc8db54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8ee886567f4666e0b654f9690a8ac15cfb55caddc9aefc7129b01a49bc8db54->enter($__internal_d8ee886567f4666e0b654f9690a8ac15cfb55caddc9aefc7129b01a49bc8db54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 85
        ob_start();
        // line 86
        echo "    <div class=\"form-group\">
        <div class=\"";
        // line 87
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>
        <div class=\"";
        // line 88
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">
            ";
        // line 89
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 89, $this->getSourceContext()); })()), 'widget');
        echo "
        </div>
    </div>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_d8ee886567f4666e0b654f9690a8ac15cfb55caddc9aefc7129b01a49bc8db54->leave($__internal_d8ee886567f4666e0b654f9690a8ac15cfb55caddc9aefc7129b01a49bc8db54_prof);

        
        $__internal_2bd800ed916784c17e46cc5d2e4a82534b0770e3da62fb4fa11945da58b11fe0->leave($__internal_2bd800ed916784c17e46cc5d2e4a82534b0770e3da62fb4fa11945da58b11fe0_prof);

    }

    // line 95
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_0dc16bce5bf35afeffd84d473c03830f34abcd5b67761fa4372e09ca2eada5ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0dc16bce5bf35afeffd84d473c03830f34abcd5b67761fa4372e09ca2eada5ad->enter($__internal_0dc16bce5bf35afeffd84d473c03830f34abcd5b67761fa4372e09ca2eada5ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_cd71b62ad43949ee21a13eb9a05e42cca62900b38284ad832157e5906a285ec1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd71b62ad43949ee21a13eb9a05e42cca62900b38284ad832157e5906a285ec1->enter($__internal_cd71b62ad43949ee21a13eb9a05e42cca62900b38284ad832157e5906a285ec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        echo "col-sm-10";
        
        $__internal_cd71b62ad43949ee21a13eb9a05e42cca62900b38284ad832157e5906a285ec1->leave($__internal_cd71b62ad43949ee21a13eb9a05e42cca62900b38284ad832157e5906a285ec1_prof);

        
        $__internal_0dc16bce5bf35afeffd84d473c03830f34abcd5b67761fa4372e09ca2eada5ad->leave($__internal_0dc16bce5bf35afeffd84d473c03830f34abcd5b67761fa4372e09ca2eada5ad_prof);

    }

    public function getTemplateName()
    {
        return "@EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  433 => 95,  418 => 89,  414 => 88,  410 => 87,  407 => 86,  405 => 85,  396 => 84,  381 => 78,  377 => 77,  373 => 76,  368 => 75,  366 => 74,  357 => 73,  342 => 67,  338 => 66,  334 => 65,  330 => 64,  321 => 63,  319 => 62,  310 => 61,  300 => 58,  291 => 57,  281 => 54,  272 => 53,  259 => 48,  253 => 46,  251 => 45,  246 => 43,  243 => 42,  236 => 38,  230 => 37,  226 => 35,  224 => 34,  219 => 32,  215 => 31,  210 => 30,  208 => 29,  199 => 28,  196 => 27,  194 => 26,  185 => 25,  167 => 21,  155 => 16,  152 => 15,  146 => 13,  143 => 12,  141 => 11,  132 => 10,  122 => 5,  120 => 4,  111 => 3,  101 => 95,  98 => 94,  96 => 84,  93 => 83,  91 => 73,  88 => 72,  86 => 61,  83 => 60,  81 => 57,  78 => 56,  76 => 53,  73 => 52,  71 => 25,  68 => 24,  65 => 22,  63 => 21,  60 => 20,  58 => 10,  55 => 9,  52 => 7,  50 => 3,  47 => 2,  14 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"@EasyAdmin/form/bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set _easyadmin_form_type = 'horizontal' %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
{% spaceless %}
    {% if label is same as(false) %}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {% else %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) %}
        {{- parent() -}}
    {% endif %}
{% endspaceless %}
{%- endblock form_label %}

{% block form_label_class 'col-sm-2' %}

{# Rows #}

{% block form_row -%}
{% spaceless %}
    {% set _field_type = easyadmin.field.fieldType|default('default') %}
    <div class=\"form-group {% if (not compound or force_error|default(false)) and not valid %}has-error{% endif %} field-{{ block_prefixes|slice(-2)|first }}\">
        {% set _field_label = easyadmin.field['label']|default(null) %}
        {{ form_label(form, _field_label, { translation_domain: easyadmin.entity.translation_domain }) }}
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}

            {% if _field_type in ['datetime', 'date', 'time', 'birthday'] and easyadmin.field.nullable|default(false) %}
                <div class=\"nullable-control\">
                    <label>
                        <input type=\"checkbox\" {% if data is null %}checked=\"checked\"{% endif %}>
                        {{ 'label.nullable_field'|trans({}, 'EasyAdminBundle')}}
                    </label>
                </div>
            {% endif %}

            {{ form_errors(form) }}

            {% if easyadmin.field.help|default('') != '' %}
                <span class=\"help-block\"><i class=\"fa fa-info-circle\"></i> {{ easyadmin.field.help|trans(domain = easyadmin.entity.translation_domain)|raw }}</span>
            {% endif %}
        </div>
    </div>
{% endspaceless %}
{%- endblock form_row %}

{% block checkbox_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock checkbox_row %}

{% block radio_row -%}
    {{- block('checkbox_radio_row') -}}
{%- endblock radio_row %}

{% block checkbox_radio_row -%}
{% spaceless %}
    <div class=\"form-group {% if not valid %}has-error{% endif %} field-{{ block_prefixes|slice(-2)|first }}\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{% endspaceless %}
{%- endblock checkbox_radio_row %}

{% block submit_row -%}
{% spaceless %}
    <div class=\"form-group field-{{ block_prefixes|slice(-2)|first }}\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock submit_row %}

{% block reset_row -%}
{% spaceless %}
    <div class=\"form-group\">
        <div class=\"{{ block('form_label_class') }}\"></div>
        <div class=\"{{ block('form_group_class') }}\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endspaceless %}
{% endblock reset_row %}

{% block form_group_class 'col-sm-10' %}
", "@EasyAdmin/form/bootstrap_3_horizontal_layout.html.twig", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/javiereguiluz/easyadmin-bundle/Resources/views/form/bootstrap_3_horizontal_layout.html.twig");
    }
}

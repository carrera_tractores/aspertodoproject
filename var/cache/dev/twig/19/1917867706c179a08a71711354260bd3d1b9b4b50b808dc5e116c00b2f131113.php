<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_c61a357c30329f6a69bf4fe1ece60f2235b16b930eaba704fb20ba0f69e3407b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_060309d6a50095083b5931dcd91f252447234412a319ed74d11d20388922f2be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_060309d6a50095083b5931dcd91f252447234412a319ed74d11d20388922f2be->enter($__internal_060309d6a50095083b5931dcd91f252447234412a319ed74d11d20388922f2be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_63bba2959d3f25bb5c48a8e7766d34bec578964d74fabeae7866320481f7592d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63bba2959d3f25bb5c48a8e7766d34bec578964d74fabeae7866320481f7592d->enter($__internal_63bba2959d3f25bb5c48a8e7766d34bec578964d74fabeae7866320481f7592d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_060309d6a50095083b5931dcd91f252447234412a319ed74d11d20388922f2be->leave($__internal_060309d6a50095083b5931dcd91f252447234412a319ed74d11d20388922f2be_prof);

        
        $__internal_63bba2959d3f25bb5c48a8e7766d34bec578964d74fabeae7866320481f7592d->leave($__internal_63bba2959d3f25bb5c48a8e7766d34bec578964d74fabeae7866320481f7592d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}

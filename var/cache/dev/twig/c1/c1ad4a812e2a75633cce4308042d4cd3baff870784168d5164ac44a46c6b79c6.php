<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_65e4ceae177610743d7057ccef561bf78818a10e2efd7208a95d0a6fb78681a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d42375d41199f087f05f17bc13e4f8060a18849266ade2e2bb45e98cae54443 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d42375d41199f087f05f17bc13e4f8060a18849266ade2e2bb45e98cae54443->enter($__internal_4d42375d41199f087f05f17bc13e4f8060a18849266ade2e2bb45e98cae54443_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_9b37eeb912a580ed9e336f4d735d787875d802d3152593cd04a468d2513989fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b37eeb912a580ed9e336f4d735d787875d802d3152593cd04a468d2513989fe->enter($__internal_9b37eeb912a580ed9e336f4d735d787875d802d3152593cd04a468d2513989fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_4d42375d41199f087f05f17bc13e4f8060a18849266ade2e2bb45e98cae54443->leave($__internal_4d42375d41199f087f05f17bc13e4f8060a18849266ade2e2bb45e98cae54443_prof);

        
        $__internal_9b37eeb912a580ed9e336f4d735d787875d802d3152593cd04a468d2513989fe->leave($__internal_9b37eeb912a580ed9e336f4d735d787875d802d3152593cd04a468d2513989fe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/ubuntu/Escriptori/Projectes/Aspertodo/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}

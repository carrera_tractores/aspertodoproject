<?php
// src/AppBundle/Entity/Tasca.php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tasca")
 */

class Tasca
{
	 /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */     
    private $id_tasca;
    
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom_tasca;
    
     /**
     * @ORM\Column(type="time")
     */
    private $hora_inici;
    
     /**
     * @ORM\Column(type="time")
     */
    private $hora_fi;
    
     /**
     * @ORM\Column(type="integer", scale=2)
     */
    private $id_assignatura;
    
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $imatge_tasca;
    
     /**
     * @ORM\Column(type="string", length=50)
     */
    private $descripcio;
    
     /**
     * @ORM\Column(type="integer", scale=2)
     */
    private $id_tasca_extra;
    
     /**
     * @ORM\Column(type="integer", scale=2)
     */
    private $id_usuari;

    /**
     * Get idTasca
     *
     * @return integer
     */
    public function getIdTasca()
    {
        return $this->id_tasca;
    }

    /**
     * Set nomTasca
     *
     * @param string $nomTasca
     *
     * @return Tasca
     */
    public function setNomTasca($nomTasca)
    {
        $this->nom_tasca = $nomTasca;

        return $this;
    }

    /**
     * Get nomTasca
     *
     * @return string
     */
    public function getNomTasca()
    {
        return $this->nom_tasca;
    }

    /**
     * Set horaInici
     *
     * @param \DateTime $horaInici
     *
     * @return Tasca
     */
    public function setHoraInici($horaInici)
    {
        $this->hora_inici = $horaInici;

        return $this;
    }

    /**
     * Get horaInici
     *
     * @return \DateTime
     */
    public function getHoraInici()
    {
        return $this->hora_inici;
    }

    /**
     * Set horaFi
     *
     * @param \DateTime $horaFi
     *
     * @return Tasca
     */
    public function setHoraFi($horaFi)
    {
        $this->hora_fi = $horaFi;

        return $this;
    }

    /**
     * Get horaFi
     *
     * @return \DateTime
     */
    public function getHoraFi()
    {
        return $this->hora_fi;
    }

    /**
     * Set idAssignatura
     *
     * @param integer $idAssignatura
     *
     * @return Tasca
     */
    public function setIdAssignatura($idAssignatura)
    {
        $this->id_assignatura = $idAssignatura;

        return $this;
    }

    /**
     * Get idAssignatura
     *
     * @return integer
     */
    public function getid_assignatura()
    {
        return $this->id_assignatura;
    }

    /**
     * Set imatgeTasca
     *
     * @param string $imatgeTasca
     *
     * @return Tasca
     */
    public function setImatgeTasca($imatgeTasca)
    {
        $this->imatge_tasca = $imatgeTasca;

        return $this;
    }

    /**
     * Get imatgeTasca
     *
     * @return string
     */
    public function getImatgeTasca()
    {
        return $this->imatge_tasca;
    }

    /**
     * Set descripcio
     *
     * @param string $descripcio
     *
     * @return Tasca
     */
    public function setDescripcio($descripcio)
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    /**
     * Get descripcio
     *
     * @return string
     */
    public function getDescripcio()
    {
        return $this->descripcio;
    }

    /**
     * Set idTascaExtra
     *
     * @param integer $idTascaExtra
     *
     * @return Tasca
     */
    public function setIdTascaExtra($idTascaExtra)
    {
        $this->id_tasca_extra = $idTascaExtra;

        return $this;
    }

    /**
     * Get idTascaExtra
     *
     * @return integer
     */
    public function getIdTascaExtra()
    {
        return $this->id_tasca_extra;
    }

    /**
     * Set idUsuari
     *
     * @param integer $idUsuari
     *
     * @return Tasca
     */
    public function setIdUsuari($idUsuari)
    {
        $this->id_usuari = $idUsuari;

        return $this;
    }

    /**
     * Get idUsuari
     *
     * @return integer
     */
    public function getIdUsuari()
    {
        return $this->id_usuari;
    }
}

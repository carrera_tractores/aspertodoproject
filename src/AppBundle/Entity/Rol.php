<?php
// src/AppBundle/Entity/Rol.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="rol")
 */

class Rol
{
	 /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */     
    private $id_rol;
    
     /**
     * @ORM\Column(type="string", length=10)
     */
    private $r;

    /**
     * Get idRol
     *
     * @return integer
     */
    public function getIdRol()
    {
        return $this->id_rol;
    }

    /**
     * Set r
     *
     * @param string $r
     *
     * @return Rol
     */
    public function setR($r)
    {
        $this->r = $r;

        return $this;
    }

    /**
     * Get r
     *
     * @return string
     */
    public function getR()
    {
        return $this->r;
    }
}

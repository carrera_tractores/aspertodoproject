<?php
// src/AppBundle/Entity/Usuaris.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuaris")
 */
class Usuaris
{
		/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */  	
    private $id_usuari;
    
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom_usuari;
    
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom;
    
     /**
     * @ORM\Column(type="string", length=20)
     */
    private $cognom;
    
     /**
     * @ORM\Column(type="string", length=10)
     */
    private $contrassenya;
    
     /**
     * @ORM\Column(type="string", length=100)
     */
    private $imatge;
    
		/**
     * @ORM\Column(type="integer", scale=2)
     */
    private $id_rol;
    

    /**
     * Get idUsuari
     *
     * @return integer
     */
    public function getIdUsuari()
    {
        return $this->id_usuari;
    }

    /**
     * Set nomUsuari
     *
     * @param string $nomUsuari
     *
     * @return Usuaris
     */
    public function setNomUsuari($nomUsuari)
    {
        $this->nom_usuari = $nomUsuari;

        return $this;
    }

    /**
     * Get nomUsuari
     *
     * @return string
     */
    public function getNomUsuari()
    {
        return $this->nom_usuari;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Usuaris
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cognom
     *
     * @param string $cognom
     *
     * @return Usuaris
     */
    public function setCognom($cognom)
    {
        $this->cognom = $cognom;

        return $this;
    }

    /**
     * Get cognom
     *
     * @return string
     */
    public function getCognom()
    {
        return $this->cognom;
    }

    /**
     * Set contrassenya
     *
     * @param string $contrassenya
     *
     * @return Usuaris
     */
    public function setContrassenya($contrassenya)
    {
        $this->contrassenya = $contrassenya;

        return $this;
    }

    /**
     * Get contrassenya
     *
     * @return string
     */
    public function getContrassenya()
    {
        return $this->contrassenya;
    }

    /**
     * Set imatge
     *
     * @param string $imatge
     *
     * @return Usuaris
     */
    public function setImatge($imatge)
    {
        $this->imatge = $imatge;

        return $this;
    }

    /**
     * Get imatge
     *
     * @return string
     */
    public function getImatge()
    {
        return $this->imatge;
    }

    /**
     * Set idRol
     *
     * @param integer $idRol
     *
     * @return Usuaris
     */
    public function setIdRol($idRol)
    {
        $this->id_rol = $idRol;

        return $this;
    }

    /**
     * Get idRol
     *
     * @return integer
     */
    public function getIdRol()
    {
        return $this->id_rol;
    }
}

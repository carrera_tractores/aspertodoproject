<?php
// src/AppBundle/Entity/Tasca_extra.php
namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="tasca_extra")
 */
class Tasca_extra
{  
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */  
    public $id_tasca_extra;
    
     /**
     * @ORM\Column(type="string", length=15)
     */
    private $nom_tasca_extra;
    
     /**
     * @ORM\Column(type="string", length=200)
     */
    private $descripcio;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;
    
     /**
     * @Assert\Image(
     *      maxSize="2M",
     *      mimeTypes={"image/png", "image/jpeg", "image/jpg"}
     * )
     * @Vich\UploadableField(mapping="tasca_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;
    
     /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;
    
    
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }
    
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get idTascaExtra
     *
     * @return integer
     */
    public function getid_tasca_extra()
    {
        return $this->id_tasca_extra;
    }

    /**
     * Set nomTascaExtra
     *
     * @param string $nomTascaExtra
     *
     * @return Tasca_extra
     */
    public function setNomTascaExtra($nomTascaExtra)
    {
        $this->nom_tasca_extra = $nomTascaExtra;

        return $this;
    }

    /**
     * Get nomTascaExtra
     *
     * @return string
     */
    public function getNomTascaExtra()
    {
        return $this->nom_tasca_extra;
    }

    /**
     * Set descripcio
     *
     * @param string $descripcio
     *
     * @return Tasca_extra
     */
    public function setDescripcio($descripcio)
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    /**
     * Get descripcio
     *
     * @return string
     */
    public function getDescripcio()
    {
        return $this->descripcio;
    }

    /**
     * Get idTascaExtra
     *
     * @return integer
     */
    public function getIdTascaExtra()
    {
        return $this->id_tasca_extra;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Tasca_extra
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

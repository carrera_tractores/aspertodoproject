<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

use Doctrine\ORM\Mapping as ORM;

 /**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="companyEvents")
 */
class companyEvents
{

/**
 * @ORM\Column(name="id", type="integer")
 * @ORM\Id
 * @ORM\GeneratedValue(strategy="AUTO")
 */
private $id;


/**
 * @ORM\Column(name="eventName")
 */
private $eventName;

/**
 * @ORM\Column(name="Title", type="string")
 */
private $Title;


/**
 * @ORM\Column(name="eventDate", type="datetime")
 */
private $eventDate;


/**
 * @ORM\Column(name="startEventDate", type="datetime")
 * @var \DateTime
 */
 private $startEventDate;


 /**
  * @ORM\Column(name="endEventDate", type="datetime")
  */
  private $endEventDate;
  
 /**
  * @ORM\Column(name="AllDayEvent", type="boolean")
  */
  private $AllDayEvent;
  
      /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

     /**
     * @Assert\Image(
     *      maxSize="2M",
     *      mimeTypes={"image/png", "image/jpeg", "image/jpg"}
     * )
     * @Vich\UploadableField(mapping="events_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;


/**
 * Set eventName
 *
 * @param string $eventName
 *
 * @return companyEvents
 */
public function setEventName($eventName)
{
    $this->eventName = $eventName;

    return $this;
}


/**
 * Get eventName
 *
 * @return string
 */
public function getEventName()
{
    return $this->eventName;
}


/**
 * Set eventDate
 *
 * @param string $eventDate
 *
 * @return CompanyEvents
 */
public function setEventDate($eventDate)
{
    $this->eventDate = $eventDate;

    return $this;
}


/**
 * Get eventDate
 *
 * @return string
 */
public function getEventDate()
{
    return $this->eventDate;
}


/**
* Set start event date
* @param string $startEventDate
*
* @return companyEvents
*/
public function setStartEventDate($startEventDate)
{
    $this->startEventDate = $startEventDate;

    return $this;
}


/**
*Get start event date
* @return string
*/
public function getStartEventDate()
{
    return $this->startEventDate;
}


/**
* Set start event date
* @param string $endEventDate
*
* @return companyEvents
*/
public function setEndEventDate($endEventDate)
{
    $this->endEventDate = $endEventDate;

    return $this;
}


/**
*Get start event date
* @return string
*/
public function getEndEventDate()
{
    return $this->endEventDate;
}


/**
 * Get id
 *
 * @return integer
 */
public function getId()
{
    return $this->id;
}

    

    /**
     * Set allDayEvent
     *
     * @param boolean $allDayEvent
     *
     * @return companyEvents
     */
    public function setAllDayEvent($allDayEvent)
    {
        $this->AllDayEvent = $allDayEvent;

        return $this;
    }

    /**
     * Get allDayEvent
     *
     * @return boolean
     */
    public function getAllDayEvent()
    {
        return $this->AllDayEvent;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return companyEvents
     */
    public function setTitle($title)
    {
        $this->Title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->Title;
    }
    
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->eventDate = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
    
    
    
    
    
}

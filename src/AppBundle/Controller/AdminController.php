<?php

// src/AppBundle/Controller/AdminController.php
namespace AppBundle\Controller;

use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{
    public function createNewUsuariEntity()
    {
        return $this->get('fos_user.user_manager')->createUsuari();
    }

    public function prePersistUsuariEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }
    
    public function preUpdateUsuariEntity($user)
    {
        $this->get('fos_user.user_manager')->updateUser($user, false);
    }
   
}
<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ProvaController extends Controller
{
     
     /**
     * @Route("/calendari", name="calendari")
     */
    public function calendariAction()
    {         
        return $this->render('calendari.html.twig');
    } 
}